@extends('layouts.admin.master')

@section('content')
<div class="page-wrapper">
            <div class="container-fluid">
				<!-- Title -->
				<div class="row ">
					<div class="col-lg-5 col-sm-12 col-md-5 col-xs-12">
					  <div class="row">
                        <div class="col-xs-12" align="left">
                            <h1 class="weight-500" style="font-size: 20px;">BAREME DES ACTES MEDICAUX</h1>
                        </div>
                      </div>					     
                    </div>
					<!-- Breadcrumb -->
					<div class="col-lg-7 col-sm-12 col-md-7 col-xs-12" >
                        <div class="row" style="padding-top: 15px;">
                            <div class="col-lg-3 col-sm-6 col-md-3 col-xs-12">
                                <button class="btn btn-success btn-anim lanceModal" href="{{route('AddBLoc')}}"><i class="fa fa-edit"></i><span class="btn-text">AJOUTER UN BLOC</span></button>
                            </div>
                            <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
                                <button class="btn btn-primary btn-anim lanceModal" href="{{route('AddCatPrestation')}}"><i class="fa fa-edit"></i><span class="btn-text">AJOUTER UNE CATEGORIE</span></button>
                            </div>
                             <div class="col-lg-5 col-sm-6 col-md-5 col-xs-12">
                                <button class="btn btn-danger btn-anim lanceModal" href="{{route('ADDPrestation')}}"><i class="fa fa-edit"></i><span class="btn-text">AJOUTER UNE PRESTATION</span></button>
                            </div>
                        </div>
					</div>
					<!-- /Breadcrumb -->
				</div>
                <hr>
				<!-- /Title -->
				
				<!-- Row -->
                <div class="row">
							<div class="col-md-12">
								@if(session()->has('messageErreur'))
									<div class="alert alert-danger alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><strong>Erreur ! </strong> {{ Session::get('messageErreur') }}</p>
										<div class="clearfix"></div>
									</div>
									@endif
									@if(session()->has('message'))
									<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><strong>Félicitations ! </strong> {{ Session::get('message') }}</p> 
										<div class="clearfix"></div>
									</div>
								@endif
							</div>
						</div>
				<div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark weight-500">BAREME DES ACTES MEDICAUX</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                                <!-- id="datable_1" -->
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>NATURE DE PRESTATIONS</th>
                                                <th>TAUX DE COUVERTURE</th>
                                                <th>PLAFOND DE REMBOURSSEMENT</th>
                                                <th>ENTENTE PREALABLE</th>
                                                <th>ACTIONS</th>
                                            </tr>
                                            </thead>
                                            
                                                <tbody>
                                                    @if($b->Count()>0)
                                                        @foreach($b as $bloc)
                                                        <tr  style="padding: 4px !important; background:#00abc7">
                                                            <td align="center" >
                                                                <span class="text-uppercase" style="color: #fff; font-weight:bold; font-size:20px;">{{$bloc->libelle}}</span> 
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style="color: #fff; font-weight:bold;">ENTENTE PREALABLE</td>
                                                            <td style="width: 170px;">
                                                                <!-- <button data-toggle="modal" data-target="#" class="btn  btn-success btn-rounded mr-10" style="padding: 3px 4px 1px 9px !important ;"><i class="fa fa-eye mr-5"></i> </button> -->
                                                                <button href="{{route('UpdateBloc',$bloc->id)}}" title="Modifier" class="btn btn-warning btn-icon-anim btn-square lanceModal"><i class="fa fa-edit"></i></button>
														        <button href="{{route('DeleteBloc',$bloc->id)}}" title="Supprimer" class="btn btn-danger btn-icon-anim btn-square DeleteRedirect"><i class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>

                                                        @if($bloc->typePrestation->count()>0)
                                                            @foreach($bloc->typePrestation as $btp)
                                                            @if($btp->etat)
                                                            <tr style="padding: 2px !important;">
                                                                <td style="text-transform: uppercase; font-size: 16px; font-weight:bold; color:black" >{{ $btp->libelle }}</td>              
                                                                <td style="text-transform: uppercase; font-size: 16px; font-weight:bold; color:black">
                                                                    @if(!is_null($btp->ParamtypePrestation->last()->taux))
                                                                        {{$btp->ParamtypePrestation->last()->taux}} %
                                                                    @endif
                                                                </td> 
                                                                <td style="text-transform: uppercase; font-size: 16px; font-weight:bold; color:black">
                                                                    @if(!is_null($btp->ParamtypePrestation->last()->plafond))
                                                                    {{number_format($btp->ParamtypePrestation->last()->plafond, 0, ',', '.')}} Fcfa
                                                                    @endif
                                                                    @if(!is_null($btp->ParamtypePrestation->last()->lettre))
                                                                        {{$btp->ParamtypePrestation->last()->lettre}} = {{$btp->ParamtypePrestation->last()->valeurLettre}} FCFA
                                                                    @endif
                                                                </td> 
                                                                <td></td>
                                                                <td>
                                                                    <!-- <button data-toggle="modal" data-target="#" class="btn  btn-success btn-rounded mr-10" style="padding: 3px 4px 1px 9px !important ;"><i class="fa fa-eye mr-5"></i> </button> -->
                                                                    <button href="{{route('UpdateTypeP',$btp->id)}}" class="btn  btn-primary btn-rounded mr-10 lanceModal" style="padding: 3px 4px 1px 9px !important ;"><i class="fa fa-pencil-square-o mr-5"></i> </button>
                                                                    <button href="{{route('DeleteTypeP', $btp->id)}}" class="btn btn-danger btn-rounded DeleteRedirect" style="padding: 3px 4px 1px 9px !important ;"> <i class="fa fa-trash-o mr-5"></i> </button>
                                                                </td>
                                                            </tr>
                                                                @if($btp->Prestation->count()>0)
                                                                @foreach($btp->Prestation as $btpp)
                                                                @if($btpp->etat)
                                                                <tr style="height: 25px; padding:0px;" >
                                                                    <td style="padding-left: 5em; text-transform: lowercase; font-size: 14px;  color:black; height: 10px !important;"> 
                                                                    - {{$btpp->libelle}} &ensp; &ensp; &ensp; 
                                                                    @if($btpp->exclusion) 
                                                                    <span class="label label-danger">EXCLUS</span>
                                                                    @endif
                                                                </td>
                                                                @if(!$btpp->exclusion) 
                                                                    <td style="text-transform: lowercase; font-size: 14px;  color:black">
                                                                        @if($btpp->ParamPrestation->last()->taux)
                                                                            {{$btpp->ParamPrestation->last()->taux}}%
                                                                        @endif
                                                                    </td>
                                                                    <td style="font-size: 14px;  color:black">
                                                                        @if($btpp->ParamPrestation->last()->plafond)
                                                                             {{number_format($btpp->ParamPrestation->last()->plafond, 0, ',', '.')}} Fcfa 
                                                                        @endif
                                                                        @if($btpp->ParamPrestation->last()->qte)
                                                                           / {{$btpp->ParamPrestation->last()->qte}} 
                                                                        @endif
                                                                        @if($btpp->ParamPrestation->last()->duree)
                                                                           / {{$btpp->ParamPrestation->last()->duree}} an(s)
                                                                        @endif 
                                                                        @if($btpp->ParamPrestation->last()->entite)
                                                                           / {{$btpp->ParamPrestation->last()->entite}}
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if($btpp->ententePrealable)
                                                                            OUI
                                                                            @else
                                                                            NON
                                                                        @endif
                                                                    </td>
                                                                    @else
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    @endif
                                                                    <td>
                                                                        <!-- <button title="Voir plus" style="color:green"><i class="fa fa-eye"></i></button> -->
                                                                        <button href="{{route('UpdatePrestation', $btpp->id)}}"  title="Modifier" class="lanceModal" style="color:orange"><i class="fa fa-edit"></i></button>
                                                                        <button href="{{route('DeletePrestation', $btpp->id)}}" title="Supprimer " class="DeleteRedirect"  style="color:red"><i class="fa fa-trash"></i></button>
                                                                    </td>
                                                                </tr>
                                                                @endif
                                                                @endforeach
                                                                @endif
                                                            @endif
                                                            @endforeach
                                                        @endif
                                                
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                               
                                        </table>

                                      







                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>	
                    </div>
				</div>
				<!-- /Row -->
			</div>
			
            @include('layouts.admin._footer')
			
        </div>
           
       
<!-- /Main Content -->
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')

@endpush


@push('script.footer1')
@endpush

@section('title')
PARAMETRES SYSTEME
@endsection


@section('parametre')
active
@endsection





@push('script.footer2')
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Data table JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/jquery.slimscroll.js')}}"></script>

<!-- simpleWeather JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/simpleweather-data.js')}}"></script>

<!-- Progressbar Animation JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{URL::asset('erpfiles/dist/js/dropdown-bootstrap-extended.js')}}"></script>

<!-- Sparkline JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- Owl JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- ChartJS JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/chart.js/Chart.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/morris.js/morris.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/morris-data.js')}}"></script>
<!-- Switchery JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>



<!-- Data table JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/dataTables-data.js')}}"></script>
<!-- Init JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/init.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/dashboard-data.js')}}"></script>

<script>
    
    $(document).on("change",".changeCat", function(){
        
        var valueSelected = this.value;
        //alert(valueSelected);
        $.ajax({
            method: 'get',
            url: "/dashbord/create/change/catpres/" + valueSelected,
            success : function(response){
                // console.log(response.code);
                // $.toast({
                //     heading: response.title,
                //     text: response.message,
                //     position: 'top-right',
                //     loaderBg:'#aa8105',
                //     icon: 'succes',
                //     hideAfter: 7000, 
                //     stack: 6
                // });
                $('.retouronchange').html(response.code);
             }
        })
    });
</script>
@endpush