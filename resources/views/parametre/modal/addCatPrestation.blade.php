<div class="modal fade affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #2879ff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge" style="color:#fff !important;">AJOUTER UNE CATEGORIE DE PRESTATION</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('AddCATPres')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
               <label for="" class="txt-dark">Bloc *</label>
               <select name="bloc_ic" class="form-control" required="" id="">
                <option></option>
                  @if($bloc)
                    @foreach($bloc as $b)
                      <option value="{{$b->id}}">{{$b->libelle}}</option>
                    @endforeach
                  @endif
               </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
               <label for="" class="txt-dark">Catégorie de prestation*</label>
               <input type="text" name="libelle" class="form-control"  required="">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
              <label for="" class="txt-dark">Taux de couverture</label>
              <select name="taux" class="form-control" id="">
                <option></option>
                <option value="70">70%</option>
                <option value="80">80%</option>
                <option value="90">90%</option>
                <option value="100">100%</option>
              </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
               <label for="" class="txt-dark">Plafond de Rembourssement</label>
               <input type="number" name="plafond" class="form-control">
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
               <label for="" class="txt-dark">La lettre</label>
               <input type="text" name="lettre" class="form-control">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-9"> <br>
               <label for="" class="txt-dark">Valeur de la lettre</label>
               <input type="number" name="valeur" class="form-control">
            </div>
          </div>
          
          

          <div class="row">
            <div class="col-lg-12"> <br>
            <button type="submit" style="margin-top: 1.5em; width:100%;" class="btn btn-primary"><i class="fa fa-save"></i> AJOUTER</button>  
            </div>
          </div>

        </form>   
      </div>
    </div>
  </div>
</div>