<div class="modal fade affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#fec107;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge" style="color:#fff !important;">Modifier: {{$bloc->libelle}}</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('UpdatingBLOC',$bloc->id)}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"> 
               <label for="" class="txt-dark">LIBELLE *</label>
               <input type="text" class="form-control" value="{{$bloc->libelle}}" name="libelle" required="">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> <br>
            <button style="background:#fec107;" type="submit" style="margin-top: 1.5em;" class="btn btn-warning"><i class="fa fa-save"></i> MODIFIER </button>  
            </div>
           
          </div>
         
        </form>   
      </div>
    </div>
  </div>
</div>