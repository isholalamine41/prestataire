<div class="modal fade affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #ff2a00;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge" style="color:#fff !important;">AJOUTER UNE PRESTATION</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('AddPRES')}}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Catégorie*</label>
                    <select name="type_prestation_id" class="form-control changeCat" required="" id="">
                        <option></option>
                        @if($tp->count()>0)
                            @foreach($tp as $t)
                                <option value="{{$t->id}}">{{$t->libelle}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Prestation *</label>
                    <input type="text" class="form-control" name="prestation"  required="">
                </div>
            </div>
            <div class="row retouronchange" >
                
            </div> 
            <div class="row" >
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Duree</label>
                    <select name="duree" class="form-control" id="">
                        <option></option>
                        <option value="1">1 an</option>
                        <option value="2">2 ans</option>
                        <option value="3">3 ans</option>
                    </select>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Assuré/famille</label>
                    <select name="entite" class="form-control" id="">
                        <option></option>
                        <option value="Assuré">Assuré</option>
                        <option value="Famille">Famille</option>
                    </select>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Quantité</label>
                    <input type="number" name="qte" class="form-control" id="">
                </div>
            </div>
            <div class="row"><br>
                <div class="col-md-12">
                    <label for="" class="txt-dark">Commentaire</label>
                    <textarea name="description" class="form-control" id="" cols="30" rows="2"></textarea>
                </div>
            </div>
            <div class="row"><br>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <select name="exclusion" class="form-control" id="">
                        <option selected value="0">PAS EXCLUS</option>
                        <option value="1">EXCLUS</option>
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" align="right">
                    <button type="submit" style=" width:100%" class="btn btn-danger"> <i class="fa fa-save"></i> MODIFIER</button>  
                </div>
            </div>
         
        </form>   
      </div>
    </div>
  </div>
</div>