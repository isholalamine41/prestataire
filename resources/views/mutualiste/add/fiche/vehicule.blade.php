
<div class="row">
    <div class="col-md-12">
        <label for="" class="txt-dark">DETAIL DU VEHICULE</label>
        <textarea name="commande" id="" class="form-control" cols="30" rows="3"></textarea>
    </div>  
    <div class="col-md-6"><br>
        <label class="txt-dark" for="">APPORT INITIAL</label>
        <input name="avance" type="number" class="form-control avance" required="">
    </div> 
    <div class="col-md-6"><br>
        <label class="txt-dark" for="">ECHEANCE (mois)</label>
        <input name="nbmois" type="number" class="form-control nbmois" required="">
    </div>
    <div class="col-md-6"><br>
        <label class="txt-dark" for="">MONTANT PRET</label> 
        <input name="montantpret" type="number" class="form-control montantpret" required="">
    </div>
    <div class="col-md-6"><br>
        <label class="txt-dark" for="">MONTANT TOTAL A REMBOURSER</label> 
        <input name="montant" type="number" class="form-control montant " required="">
    </div>
    
    
</div>
<div class="row">
    <div class="col-md-4"><br>
        <label class="txt-dark" for="">MENSUALITE</label>
        <input name="mensualite" type="number" class="form-control mensualite" required="">
    </div>
    <div class="col-md-8"> <br><br>
        <button type="submit" style="width:100%;" class="btn btn-primary waves-effect" data-dismiss="modal">ENREGISTRER</button> <br>
    </div>
</div>