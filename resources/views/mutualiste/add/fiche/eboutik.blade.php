<div class="row">
    <div class="col-md-4">
        <label class="txt-dark" for="">MONTANT COMMANDE</label> 
        <input name="montant" type="number" class="form-control montant" required="">
    </div>
    <div class="col-md-4">
        <label class="txt-dark" for="">AVANCE</label> 
        <input name="avance" type="number" class="form-control avance" required="">
    </div> 
    <div class="col-md-4">
        <label class="txt-dark" for="">ECHEANCE</label> 
        <input name="nbmois" type="number" class="form-control nbmois" required="">
    </div>
    <div class="col-md-12"><br>
        <label for="" class="txt-dark">COMMANDE SOUSCRIPTEUR</label>
        <textarea name="commande" required="" id="" class="form-control" cols="30" rows="3"></textarea>
    </div>
</div>
<div class="row">
    <div class="col-md-12"> <br>
        <button type="submit" style="width:100%;" class="btn btn-success waves-effect" data-dismiss="modal">ENREGISTRER</button> <br>
    </div>
</div>