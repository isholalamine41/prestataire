<div class="modal fade example-modal-lg affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge">FICHIER TERRAIN YAKRO</h4>
      <div class="modal-body">
        <form action="{{route('AddEXCELETERRAIN')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-md-12">
                <blockquote>
                    Importer uniquement le fichier des souscripteur pour les terrain de yakro MUSCOP-CI
                </blockquote>
            </div>
            <div class="col-md-6"> <br>
               <input type="file" class="form-control" name="exel" required="">
            </div>
            <div class="col-md-6" align="center">  
              <button type="submit" style="width:250px; margin-top: 1.5em;" class="btn btn-warning"><i class="fa fa-file-excel-o"></i> IMPORTER LE FICHER </button>
            </div>
          </div>
                          
         
        </form>   
      </div>
    </div>
  </div>
</div>