<div class="modal fade example-modal-lg affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge">DETAILS PROJET : {{$pm->Projet->libelle}}</h4>
      <div class="modal-body">
        <form action="{{route('AddEXCELFILE')}}" method="post" enctype="multipart/form-data">
          @csrf
                        <div class="row">
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
								<div class="panel panel-default card-view pa-0">
									<div class="panel-wrapper collapse in">
										<div class="panel-body pa-0">
											<div class="sm-data-box bg-blue">
												<div class="container-fluid">
													<div class="row">
														<a href="">
														<div class="col-xs-9 text-center pl-0 pr-0 data-wrap-left">
															<span class="txt-light block counter" style="font-size:16px;">TOTAL SOUSCRIPTIONS</span> <br>
															<span style="font-size: 22px;" class=" uppercase-font txt-light block"><span>
                                                                {{number_format($pm->Versement->SUM('montant_projet'), 0, ',', ' ')}}
															</span> FCFA</span>
														</div>
														<div class="col-xs-3 text-center  pl-0 pr-0 data-wrap-right">
															<i class="fa fa-money txt-light data-right-rep-icon"></i>
														</div>
														</a>
													</div>  
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
								<div class="panel panel-default card-view pa-0">
									<div class="panel-wrapper collapse in">
										<div class="panel-body pa-0">
											<div class="sm-data-box bg-green">
												<div class="container-fluid">
													<div class="row">
														<a href="">
														<div class="col-xs-9 text-center pl-0 pr-0 data-wrap-left">
														<span class="txt-light block counter" style="font-size:16px;">MONTANT REMBOUSSE</span> <br>
														<span style="font-size: 22px;" class="uppercase-font txt-light block"><span> 
                                                            {{number_format($pm->Versement->SUM('valeur'), 0, ',', ' ')}}
														</span> FCFA</span>
														</div>
														<div class="col-xs-3 text-center  pl-0 pr-0 data-wrap-right">
														<i class="fa fa-money txt-light data-right-rep-icon"></i>
														</div>
														</a>
													</div>  
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
								<div class="panel panel-default card-view pa-0">
									<div class="panel-wrapper collapse in">
										<div class="panel-body pa-0">
											<div class="sm-data-box bg-yellow">
												<div class="container-fluid">
													<div class="row">
														<a href="#">
														<div class="col-xs-9 text-center pl-0 pr-0 data-wrap-left">
															<span class=" txt-light block counter" style="font-size: 16px;">MONTANT RESTANT</span> <br>
															<span style="font-size: 22px;" class="uppercase-font txt-light block"><span> {{number_format($pm->Versement->SUM('montant_projet') - $pm->Versement->SUM('valeur'), 0, ',', ' ')}} </span> FCFA</span>
														</div>
														<div class="col-xs-3 text-center  pl-0 pr-0 data-wrap-right">
														<i class="fa fa-money txt-light data-right-rep-icon"></i>
														</div>
														</a>
													</div>  
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						
						</div>

                        <div class="row">
                            


                                <form action="/managment/create/opreation" method="post" enctype="multipart/form-data">@csrf
                                    <div class="row">
                                        
                                        <div class="panel-body" style="width: 100%">
                                            <h6>LISTE DES VERSEMENTS POUR CE PROJET</h6>
                                            <br>  
                                            <table class="table table-bordered table-hover toggle-circle" id="exampleFootableFiltering"
                                            data-paging="true" data-filtering="true" data-sorting="true" style="width: 100%">
                                            <thead>
                                                <tr>
                                                <th data-name="id" data-type="number">N°</th>
                                                <th>Date</th>
                                                <th>Mode de versement</th>
                                                <th >Montant</th>
                                                <th data-breakpoints="xs">Total versement</th>
                                                <th data-breakpoints="xs">Reste à payer</th>
                                                <th data-breakpoints="xs">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody class="retour">
                                                <?php $n=0;?>
                                                
                                                @foreach($vsmt as $vsmt)
                                                
                                                <?php $n++;?>
                                                <tr>
                                                @if($vsmt->valeur)
                                                <td>{{$n}}</td>
                                                <td>{{(new DateTime($vsmt->date))->format('d/m/Y')}}</td>
                                                <td>{{$vsmt->mode_reglement}}</td>
                                                <td class="txt-dark">{{number_format($vsmt->valeur,0,',',' ')}} FCFA</td>
                                                <td class="txt-dark">
                                                    <b>{{number_format($vsmt->montant_payer,0,',',' ')}} FCFA</b>
                                                </td>
                                                <td class="txt-dark">{{number_format($vsmt->reste_a_payer,0,',',' ')}} FCFA</td>
                                                <td align="center">
                                                    <button href="{{route('recuReglement',$vsmt->id)}}" title="Télécharger le réçu" class="btn btn-warning btn-icon-anim btn-square ViewPage"><i class="fa fa-download"></i></button>
                                                    @if($vsmt->mode_reglement != "NP")
                                                    <button href="{{route('suppReglement',$vsmt->id)}}" title="Supprimer le versment"  class="btn btn-danger btn-icon-anim btn-square DeleteModale"><i class="fa fa-trash"></i></button>
                                                    @endif
                                                </td>
                                                @endif
                                                
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                            </div>
                                    </div>
                                    </form>   
                                </div>
                                                
                        <br><br><br>
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">ECHEANCIER DE VERSEMENT</h6>
								</div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap mt-40">
										<div class="table-responsive">
											<table class="table mb-0">
												<thead>
												  <tr>
													<th>N°</th>
													<th>DATE A VERSER</th>
													<th>MONTANT A VERSER</th>
                                                    <th>DATE VERSEMENT</th>
													<th>MONTANT VERSER</th>
													<th>ETAT</th>
												  </tr>
												</thead>
												<tbody>
                                                <?php $n=0;?>
                                                
                                                @foreach($pm->Amortissement as $pma)
                                                <?php $n++;?>
                                               
												  <tr>
													<td>{{$n}}</td>
													<td class="txt-dark">{{(new DateTime($pma->date_a_verser))->format('d/m/Y')}}</td>
													<td class="txt-dark">{{number_format($pma->montant_a_verser,0,',',' ')}} FCFA</td>
                                                    @if($pma->date_verement)
													<td class="txt-dark">{{(new DateTime($pma->date_verement))->format('d/m/Y')}}</td>
                                                    @else
                                                    <td class="txt-dark"></td>
                                                    @endif
													<td class="txt-dark">{{number_format($pma->montant_verser,0,',',' ')}} FCAF</td>
													<td>
                                                        @if($pma->etat == "Non Réglé")
                                                        <span class="label label-danger">{{$pma->etat}}</span> 
                                                        @else
                                                        <span class="label label-success">{{$pma->etat}}</span> 
                                                        @endif
                                                    </td>
												  </tr>
                                                 
												@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

                          
         
        </form>   
      </div>
    </div>
  </div>
</div>



        