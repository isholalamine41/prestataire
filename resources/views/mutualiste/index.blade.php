@extends('layouts.admin.master')

@section('content')
<!-- Main Content -->
<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
						<div class="panel panel-success card-view">
							<div class="panel-heading small-panel-heading relative">
								<div class="pull-left">
									<h6 class="panel-title txt-light">NOMBRE DE MUTUALISTES</h6>
								</div>
								<div class="clearfix"></div>
								<div class="head-overlay"></div>
							</div>		
							<div class="panel-wrapper collapse in">
								<div class="panel-body row pa-0">
									<div class="bg-green">
										<div class="container-fluid">
											<div class="row">
												<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
													<span  style="font-size:26px;" class="txt-dark block counter"><span class="counter-anim  txt-light">{{$mutualistes->count()}}</span></span>
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
						<div class="panel panel-warning card-view">
							<div class="panel-heading small-panel-heading relative">
								<div class="pull-left">
									<h6 class="panel-title txt-light">NOMBRE DE RETRAITES </h6>
								</div>
								<div class="clearfix"></div>
								<div class="head-overlay"></div>
							</div>		
							<div class="panel-wrapper collapse in">
								<div class="panel-body row pa-0">
									<div class="bg-yellow">
										<div class="container-fluid">
											<div class="row">
												<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
													<span style="font-size:26px;" class="txt-dark block counter"><span class="counter-anim  txt-light">{{$retraite->count()}}</span></span>
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
						<div class="panel panel-primary card-view">
							<div class="panel-heading small-panel-heading relative">
								<div class="pull-left">
									<h6 class="panel-title txt-light">DECES OU CAS PARTICULIER</h6>
								</div>
								<div class="clearfix"></div>
								<div class="head-overlay"></div>
							</div>		
							<div class="panel-wrapper collapse in">
								<div class="panel-body row pa-0">
									<div class="bg-blue">
										<div class="container-fluid">
											<div class="row">
												<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
													<span style="font-size:26px;" class="txt-light block counter"><span class="counter-anim">{{$caspart->count()}}</span></span>
												</div>
												
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
						<div class="panel panel-danger card-view">
							<div class="panel-heading small-panel-heading relative">
								<div class="pull-left">
									<h6 class="panel-title txt-light">NOMBRE DE DEMISSION</h6>
								</div>
								<div class="clearfix"></div>
								<div class="head-overlay"></div>
							</div>		
							<div class="panel-wrapper collapse in">
								<div class="panel-body row pa-0">
									<div class="bg-red">
										<div class="container-fluid">
											<div class="row">
												<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
													<span  style="font-size:26px;" class="txt-dark block counter"><span class="counter-anim txt-light">{{$demission->count()}}</span></span>
												</div>
												
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<!-- end row-->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel">
							<div class="button-list" align="right">
								<button href="{{route('AddMutuaExcelFile')}}" class="btn btn-success lanceModal btn-outline btn-icon right-icon"><i class="fa fa-file-excel-o"></i> <span>IMPORTER UNE LISTE</span> </button>
								<button href="{{route('AddMutualiste')}}" class="btn btn-danger lanceModal btn-outline btn-icon right-icon"><i class="fa fa-save"></i> <span>AJOUTER UN MUTUALISTE</span> </button>
							</div>
						</div>
					</div>
				</div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-12">
						@if(session()->has('messageErreur'))
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><strong>Erreur ! </strong> {{ Session::get('messageErreur') }}</p>
								<div class="clearfix"></div>
							</div>
							@endif
							@if(session()->has('message'))
							<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><strong>Félicitations ! </strong> {{ Session::get('message') }}</p> 
								<div class="clearfix"></div>
							</div>
						@endif
					</div>
				</div>
				<div class="row">
					
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                       <div class="panel panel-default card-view panel-refresh">
								<div class="refresh-container">
									<div class="la-anim-1"></div>
								</div>
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">MUTUALISTES PAR TYPE {{date('Y')}}</h6>
									</div>
									<div class="clearfix"></div>
								</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<canvas id="chart_7" height="330"></canvas>
								</div>	
							</div>
							<div class="refresh-container">
									<div class="la-anim-1"></div>
								</div>
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">MUTUALISTES PAR TYPE {{date('Y')}}</h6>
									</div>
									<div class="clearfix"></div>
								</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="morris_extra_bar_chart" class="morris-chart" style="height:243px;"></div>
									
								</div>	
							</div>
						</div>
					</div>

					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                       <div class="panel panel-default card-view panel-refresh">
						<div class="panel panel-default card-view panel-refresh">
								<div class="refresh-container">
									<div class="la-anim-1"></div>
								</div>
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">DETAILS MUTUALISTE PAR POSITION {{date('Y')}}</h6>
									</div>
									<div class="pull-right">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-danger btn-outline dropdown-toggle  " type="button"> <i style="color:red" class="fa fa-download"></i>  TELECHARGER <span class="caret"></span></button>
										<ul role="menu" class="dropdown-menu">
											<li><a href="#"><i class="fa fa-file-pdf-o"></i> EN PDF</a></li>
											<li><a href="#"><i class="fa fa-file-excel-o"></i> EN CSV / EXCEL</a></li>
										</ul>
									</div>
										
										
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body row pa-0">
										<div class="table-wrap">
											<div class="table-responsive">
												<table class="table table-hover mb-0">
													<thead>
														<tr>
															<th>POSITION</th>
															<th>NOMBRE </th>
															<th>POURCENTAGE</th>
															<th>ACTION</th>
														</tr>
													</thead>
													<tbody>
														@if($typeMutua->count()>0)
														@foreach($typeMutua as $tyM)
														<tr>
															<td><span class="txt-dark weight-500">{{$tyM->position}}</span></td>
															<td>{{$tyM->nb}}</td>
															<td>
															@if($tyM->nb>800)
															<span class="txt-success">
																<span class="label label-success">{{number_format(($tyM->nb*100)/$mutualistes->count(), 2, ',', ' ')}}%</span>
															</span>
															@else
																@if($tyM->nb>=50)
																	<span class="txt-success">
																		<span class="label label-warning">{{number_format(($tyM->nb*100)/$mutualistes->count(), 2, ',', ' ')}}%</span>
																	</span>
																	@else
																	<span class="txt-success">
																		<span class="label label-danger">{{number_format(($tyM->nb*100)/$mutualistes->count(), 2, ',', ' ')}}%</span>
																	</span>
																@endif
															@endif
															</td>
															
															<td>
															<button title="Voir les Détails" class="btn btn-primary btn-icon-anim btn-square"><i class="fa fa-eye"></i></button>

															</td>
														</tr>
														@endforeach
														@endif
														
														
													</tbody>
												</table>
											</div>
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>


				</div>
				<!-- /Row -->
				
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark"><b style="font-size:15px; font-weight:bold;">LISTE DES MUTUALISTES ({{$mutualistes->count()}})</b></h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="datable_1" class="table table-hover display  pb-30" >
												<thead>
													<tr>
														<th>Matricule</th>
														<th>Nom Complet</th>
														<th>Fonction</th>
														
														<th>GRADE</th>
														<th>ECHELON</th>
														<th>POSITION</th>
														<th>Projets</th>
														<th>Actions</th>
													</tr>
												</thead>
												
												<tbody>
													@if($mutualistes->count()>0)
													@foreach($mutualistes as $Mutua)
													<tr>
														<td style="height: 70px;">
															<div class="profile-img-wrap">
																@if(!$Mutua->photo)
																<img href="{{route('ProfilUser',$Mutua->id)}}" class="inline-block mb-10 DetailView" src="{{URL::asset('erpfiles/dist/img/mock1.jpg')}}" alt="user"/>
																	@else
																<img  href="{{route('ProfilUser',$Mutua->id)}}" class="inline-block mb-10" src="{{URL::asset('/storage/app/'.$Mutua->photo)}}"/>
																@endif
															</div>
															{{$Mutua->registration_number}}
														</td>
														<td>{{$Mutua->first_name}} {{$Mutua->name}} </td>
														<td>{{$Mutua->role ?? 'pas d\'infos'}}</td>
														<td>{{$Mutua->grade}}</td>
														<td>{{$Mutua->echelon}}</td>
														<td>{{$Mutua->statutory_position}}</td>
														<td>
															<select name="" id="" class="form-control">
																@if($Mutua->ProjetMutua)
																@foreach($Mutua->ProjetMutua as $pm)
																	<option value="">{{$pm->Projet->libelle}}</option>
																@endforeach
																@endif
															</select>
														</td>
														<td style="width:180px;">
															
														<button href="{{route('ProfilUser',$Mutua->id)}}" title="Voir les Détails" class="btn btn-success btn-icon-anim btn-square DetailView"><i class="fa fa-eye"></i></button>
														<button title="Modifier" class="btn btn-warning btn-icon-anim btn-square"><i class="fa fa-edit"></i></button>
														<button title="Ajouter un versement" href="{{route('VersementProjet',['id' => $Mutua->id])}}" class="btn btn-primary lanceModal btn-icon-anim btn-square"><i class="fa fa-edit"></i></button>
														<button href="{{route('DeleteMutua',$Mutua->id)}}" title="Supprimer" class="btn btn-danger btn-icon-anim btn-square DeleteRedirect"><i class="fa fa-trash"></i></button>
														</td>
													</tr>
													@endforeach
													@endif
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
			<!-- Footer -->
            @include('layouts.admin._footer')
			<!-- /Footer -->
			
		</div>
<!-- /Main Content -->
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')

	
@endpush


@push('script.footer1')

@endpush


@section('title')
MUTUALISTES
@endsection

@section('mutualiste')
active
@endsection


@push('script.footer2')
 <!-- jQuery -->
 <script src="{{URL::asset('erpfiles/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Counter Animation JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>

<!-- Data table JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/productorders-data.js')}}"></script>
	<!-- Data table JavaScript -->
	<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/dist/js/dataTables-data.js')}}"></script>
<!-- Owl JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Switchery JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/jquery.slimscroll.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{URL::asset('erpfiles/dist/js/dropdown-bootstrap-extended.js')}}"></script>

<!-- Sparkline JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/morris.js/morris.min.js')}}"></script>

<!-- Chartist JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/chartist/dist/chartist.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/chartist-data.js')}}"></script>

<!-- ChartJS JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/chart.js/Chart.min.js')}}"></script>

<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<!-- Sweet-Alert  -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/sweetalert-data.js')}}"></script>
<!-- Init JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/init.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/dashboard3-data.js')}}"></script>
<script>

	if($('#morris_extra_bar_chart').length > 0){
		// Morris bar chart
		Morris.Bar({
			element: 'morris_extra_bar_chart',
			data: [{
				y: "{{date('Y')}}",
				m: "{{$mutualistes->count()}}",
				r: "{{$retraite->count()}}",
				c: "{{$caspart->count()}}",
				d: "{{$demission->count()}}"
				
			}],
			xkey: 'y',
			ykeys: ['m', 'r', 'c','d'],
			labels: ['MUTUALISTE', 'RETRAITE', 'CAS PARTICULIER','DEMISSION'],
			barColors:['#34d375','#fecd39', '#5394ff', '#ff5533'],
			hideHover: 'auto',
			gridLineColor: '#878787',
			resize: true,
			barGap:7,
			gridTextColor:'#878787',
			gridTextFamily:"Roboto"
		});
	}

	if( $('#chart_7').length > 0 ){
		var ctx7 = document.getElementById("chart_7").getContext("2d");
		var data7 = {
			 labels: [
			"MUTUALISTE",
			"RETRAITE",
			"CAS PARTICULIER",
			"DEMISSION",
		],
		datasets: [
			{
				data: ["{{$mutualistes->count()}}", "{{$retraite->count()}}", "{{$caspart->count()}}","{{$demission->count()}}"],
				backgroundColor: [
					"#34d375",
					"#fecd39",
					"#5394ff",
					"#ff5533"
				],
				hoverBackgroundColor: [
					"#34d375",
					"#fecd39",
					"#5394ff",
					"#ff5533"
				]
			}]
		};
		
		var doughnutChart = new Chart(ctx7, {
			type: 'doughnut',
			data: data7,
			options: {
				animation: {
					duration:	2000
				},
				responsive: true,
				maintainAspectRatio:false,
				legend: {
					labels: {
					fontFamily: "Roboto",
					fontColor:"#878787"
					}
				},
				elements: {
					arc: {
						borderWidth: 0
					}
				},
				tooltip: {
					backgroundColor:'rgba(33,33,33,1)',
					cornerRadius:0,
					footerFontFamily:"'Roboto'"
				}
			}
		});
	}

</script>
@endpush



