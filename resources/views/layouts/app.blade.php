<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{config("app.name")}} | @yield('title')</title>

    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="{{URL::asset('erpfiles/images/favicon.png')}}" type="image/x-icon">

    <link href="{{URL::asset('erpfiles/css/style.css')}}" rel="shortcut icon" type="text/css">
    <link rel="shortcut icon" type="image/png" href="images/favicon.png">
    <script type="text/javascript" src="//www.turnjs.com/lib/turn.min.js "></script>
</head>
<body class="vh-100" style="background-image:url('images/bg.png'); background-position:center;">

            @yield('content')
       



        <script src="{{URL::asset('erpfiles/vendor/global/global.min.js')}}"></script>
        <script src="{{URL::asset('erpfiles/js/custom.js')}}"></script>
        <script src="{{URL::asset('erpfiles/js/deznav-init.js')}}"></script>
</body>
</html>



