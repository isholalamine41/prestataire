<!-- Left Sidebar Menu -->

<div class="deznav" >
    <div class="deznav-scroll">
        <ul class="metismenu" id="menu">
            @if(Auth::user()->typePrestataire == "Super Prestataire")
            <li><a href="{{route('dashbord')}}" class="DetailView2" aria-expanded="false">
                <div class="menu-icon">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.13478 20.7733V17.7156C9.13478 16.9351 9.77217 16.3023 10.5584 16.3023H13.4326C13.8102 16.3023 14.1723 16.4512 14.4393 16.7163C14.7063 16.9813 14.8563 17.3408 14.8563 17.7156V20.7733C14.8539 21.0978 14.9821 21.4099 15.2124 21.6402C15.4427 21.8705 15.756 22 16.0829 22H18.0438C18.9596 22.0024 19.8388 21.6428 20.4872 21.0008C21.1356 20.3588 21.5 19.487 21.5 18.5778V9.86686C21.5 9.13246 21.1721 8.43584 20.6046 7.96467L13.934 2.67587C12.7737 1.74856 11.1111 1.7785 9.98539 2.74698L3.46701 7.96467C2.87274 8.42195 2.51755 9.12064 2.5 9.86686V18.5689C2.5 20.4639 4.04738 22 5.95617 22H7.87229C8.55123 22 9.103 21.4562 9.10792 20.7822L9.13478 20.7733Z" fill="#90959F"/>
                    </svg>
                </div>	
                <span class="nav-text">TABLEAU DE BORD</span>
                </a>
            </li>
            <li><a href="{{route('GestionDesDroits')}}" class="DetailView2" aria-expanded="false">
                <div class="menu-icon">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g opacity="0.5">
                        <path opacity="0.4" d="M16.0755 2H19.4615C20.8637 2 22 3.14585 22 4.55996V7.97452C22 9.38864 20.8637 10.5345 19.4615 10.5345H16.0755C14.6732 10.5345 13.537 9.38864 13.537 7.97452V4.55996C13.537 3.14585 14.6732 2 16.0755 2Z" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M4.53852 2H7.92449C9.32676 2 10.463 3.14585 10.463 4.55996V7.97452C10.463 9.38864 9.32676 10.5345 7.92449 10.5345H4.53852C3.13626 10.5345 2 9.38864 2 7.97452V4.55996C2 3.14585 3.13626 2 4.53852 2ZM4.53852 13.4655H7.92449C9.32676 13.4655 10.463 14.6114 10.463 16.0255V19.44C10.463 20.8532 9.32676 22 7.92449 22H4.53852C3.13626 22 2 20.8532 2 19.44V16.0255C2 14.6114 3.13626 13.4655 4.53852 13.4655ZM19.4615 13.4655H16.0755C14.6732 13.4655 13.537 14.6114 13.537 16.0255V19.44C13.537 20.8532 14.6732 22 16.0755 22H19.4615C20.8637 22 22 20.8532 22 19.44V16.0255C22 14.6114 20.8637 13.4655 19.4615 13.4655Z" fill="white"/>
                        </g>
                    </svg>
                </div>	
                    <span class="nav-text">GESTION DES DROITS</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->UserMenu->where('libelle',"NOS PRESTATIONS")->count()>0 or Auth::user()->typePrestataire == "Super Prestataire")
            <li><a href="{{route('Prestations')}}" class="DetailView2" aria-expanded="false">
                <div class="menu-icon">
                    <i style="font-size: 25px;" class="las la-notes-medical"></i>
                </div>
                &ensp; <span class="nav-text">NOS PRESTATIONS</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->UserMenu->where('libelle',"ACTES MEDICAUX")->count()>0 or Auth::user()->typePrestataire == "Super Prestataire")
            <li><a href="{{route('ActesMedicaux')}}" class="DetailView2" aria-expanded="false">
                <div class="menu-icon">
                    <i style="font-size: 25px;" class="las la-stethoscope"></i>
                </div>	
                &ensp;<span class="nav-text">ACTES MEDICAUX</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->UserMenu->where('libelle',"ORDONNANCES")->count()>0 or Auth::user()->typePrestataire == "Super Prestataire")
            <li><a href="{{route('OrdonnanceMedicaux')}}" aria-expanded="false">
                <div class="menu-icon">
                    <i style="font-size: 25px;" class="las la-notes-medical"></i>
                </div>
                &ensp; <span class="nav-text">ORDONNANCES</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->UserMenu->where('libelle',"HOSPITALISATIONS")->count()>0 or Auth::user()->typePrestataire == "Super Prestataire")
            <li><a href="{{route('Hospitalisations')}}" aria-expanded="false">
                <div class="menu-icon">
                    <i style="font-size: 25px;" class="las la-notes-medical"></i>
                </div>
                &ensp; <span class="nav-text">HOSPITALISATIONS</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->UserMenu->where('libelle',"DEMANDES")->count()>0 or Auth::user()->typePrestataire == "Super Prestataire")
            <li><a href="{{route('DemandeActesMedicaux')}}" class="DetailView2" aria-expanded="false">
                <div class="menu-icon">
                 <i style="font-size: 25px;"  class="las la-praying-hands"></i>
                </div>	
                &ensp; <span class="nav-text">DEMANDES</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->UserMenu->where('libelle',"FACTURES")->count()>0 or Auth::user()->typePrestataire == "Super Prestataire")
            <li><a href="{{route('FacturesActesMedicaux')}}" class="DetailView2" aria-expanded="false">
                <div class="menu-icon">
                    <i style="font-size: 25px;" class="las la-file-invoice-dollar"></i>
                </div>	
                &ensp; <span class="nav-text">FACTURES</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->UserMenu->where('libelle',"MESSAGERIE")->count()>0 or Auth::user()->typePrestataire == "Super Prestataire")
            <li><a href="{{route('Messagerie')}}" class="DetailView2" aria-expanded="false">
                <div class="menu-icon">
                    <i style="font-size: 25px;" class="la la-send"></i>
                </div>	
                &ensp; <span class="nav-text">MESSAGERIE</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->UserMenu->where('libelle',"PRISE EN CHARGE")->count()>0 or Auth::user()->typePrestataire == "Super Prestataire")
            <li>
                <a href="{{route('PriseEnCharge')}}" class="DetailView" aria-expanded="false">
                    <button type="button" class="btn btn-rounded btn-outline-danger" style="font-size: 15px;"><span class="nav-text weight-bold"> <i  class="la la-user"></i>  PRISE EN CHARGE</span></button>
                </a>
            </li>
            @endif
            
        </ul>
    </div>
</div>
<!-- /Left Sidebar Menu -->