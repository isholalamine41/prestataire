@extends('layouts.admin.master')
@section('content')

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="row">
									<div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
										<a href="{{route('AllActesMedicaux')}}">
                                            <div class="widget-stat card bg-primary headerP">
                                                <div class="card-body  ">
                                                    <div class="media">
                                                        <span class="me-1">
                                                            <i class="las la-stethoscope"></i>
                                                        </span>
                                                        <div class="media-body text-white">
                                                            <p class="mb-1">ACTES MEDICAUX</p>
                                                            <h3 class="text-white">&ensp; {{$actes->count()}}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
									</div>
									<div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
										<div class="widget-stat card  bg-success headerP">
											<div class="card-body">
												<div class="media">
													<span class="me-1">
														<i class="la la-users"></i>
													</span>
													<div class="media-body text-white">
														<p class="mb-1">MONTANT GLOBAL REGLE</p>
														<h4 class="text-white">{{number_format($p->Facture->where('payer',1)->SUM('part_muscopci'), 0, ',', ' ')}} </h4>
														<span>FCFA</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
										<div class="widget-stat card  bg-warning headerP">
											<div class="card-body">
												<div class="media">
													<span class="me-1">
														<i class="las la-file-invoice-dollar"></i>
													</span>
													<div class="media-body text-white">
														<p class="mb-1">FACTURE EN COURS</p>
														<h4 class="text-white">{{number_format($p->Facture->where('payer',0)->SUM('part_muscopci'), 0, ',', ' ')}}</h4>
														<span>FCFA</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="{{route('PayerActesMedicaux')}}">
                                            <div class="card same-card headerP">
                                                <div class="card-body depostit-card ">
                                                    <div class="depostit-card-media d-flex justify-content-between style-1">
                                                        <div>
                                                            <h6>ACTES PAYES</h6>
                                                            <h3>{{number_format($actesp->count(), 0, ',', ' ')}}</h3>
                                                        </div>
                                                        <div class="icon-box bg-success">
                                                            <i style="color: #fff;" class="las la-file-invoice-dollar"></i>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="{{route('ImpayerActesMedicaux')}}">
                                            <div class="card same-card headerP">
                                                <div class="card-body depostit-card p-0">
                                                    <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                        <div>
                                                            <h6>ACTES IMPAYES</h6>
                                                            <h3>{{number_format($actesi->count(), 0, ',', ' ')}}</h3>
                                                        </div>
                                                        <div class="icon-box bg-success">
                                                            <i class="las la-file-invoice-dollar" style="color: #fff;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="{{route('DemandeActesMedicauxAccordees')}}">
                                            <div class="card same-card headerP">
                                                <div class="card-body depostit-card">
                                                    <div class="depostit-card-media d-flex justify-content-between style-1">
                                                        <div>
                                                            <h6>DEMANDES ACCORDEES</h6>
                                                            <h3>{{number_format($demandes->where('entente','Accordée')->count(), 0, ',', ' ')}}</h3>
                                                        </div>
                                                        <div class="icon-box bg-warning">
                                                            <i class="las la-praying-hands" style="color: #fff;"></i>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="{{route('DemandeActesMedicauxRejetees')}}">
                                            <div class="card same-card same-card headerP">
                                                <div class="card-body depostit-card p-0">
                                                    <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                        <div>
                                                            <h6>DEMANDES REJETEES</h6>
                                                            <h3>{{number_format($demandes->where('entente','Rejetée')->count(), 0, ',', ' ')}}</h3>
                                                        </div>
                                                        <div class="icon-box bg-danger">
                                                        <i class="las la-praying-hands" style="color: #fff;"></i>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0">Nombre D'Acte Par mois</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div id="overiewChart"></div>
                                        <!-- <div class="ttl-project">
                                            <div class="pr-data">
                                                <h5>12,721</h5>
                                                <span>Number of Projects</span>
                                            </div>
                                            <div class="pr-data">
                                                <h5 class="text-primary">721</h5>
                                                <span>Active Projects</span>
                                            </div>
                                            <div class="pr-data">
                                                <h5>$2,50,523</h5>
                                                <span>Revenue</span>
                                            </div>
                                            <div class="pr-data">
                                                <h5 class="text-success">12,275h</h5>
                                                <span>Working Hours</span>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0">LISTE DES PATIENTS DU JOUR ({{$pj->count()}})</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table id="example" class="display table" style="min-width: 845px">
                                                <thead>
                                                    <tr>
                                                        <th>ASSURE</th>
                                                        <th>DATE : HEURE</th>
                                                        <th>Nbre ACTES</th>
                                                        <th>MONTANT</th>
                                                        <th>TIER PAYANT</th>
                                                        <th>PAR ASSURE</th>
                                                        <th>ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                @if($actepj->count()>0)
                                                <tbody>
                                                    @foreach($actepj as $ajp)
                                                        @php
                                                            $facture = App\Models\Facture::find($ajp->facture_id);
                                                            $acte = $facture->PrestaMutua()->first();
                                                            $data = $ajp->prestamutable_type::find($ajp->prestamutable_id);
                                                        @endphp
                                                    <tr>
                                                        <td class="txt-dark">
                                                            <div class="d-flex align-items-center">
                                                                @if(!$data->photo)
                                                                    <img src="{{URL::asset('erpfiles/dist/img/mock1.jpg')}}" class="avatar avatar-md rounded-circle" alt="">
                                                                    @else
                                                                        @if($ajp->prestamutable_type =='App\Models\Mutualiste')
                                                                            <img src='{{config("app.imgLink")}}{{$data->photo}}' class="avatar avatar-md rounded-circle" alt="">
                                                                        @else
                                                                            <img src='{{config("app.adLink")}}{{$data->photo}}' class="avatar avatar-md rounded-circle" alt="">
                                                                        @endif
                                                                @endif
                                                                <p class="mb-0 ms-2 txt-dark">
                                                                    <a href="">
                                                                        {{$data->first_name}} {{$data->last_name ?? $data->name}}
                                                                    </a>
                                                                </p>	
                                                            </div>
                                                        </td>
													    <td class="txt-dark">{{date('d/m/Y', strtotime($facture->date))}} à <b>{{$facture->heure}}</b></td>
                                                        <td class="txt-dark text-center" ><span class="badge badge-primary border-0">{{$facture->PrestaMutua->count()}}</span></td>
                                                        <td class="txt-dark">{{number_format($facture->montant, 0, ',', ' ')}} FCFA</td>
                                                        <td class="txt-dark">{{number_format($facture->part_assure, 0, ',', ' ')}} FCFA</td>
                                                        <td class="txt-dark"><b>{{number_format($facture->part_muscopci, 0, ',', ' ')}} FCFA</b></td>
                                                        <td>
                                                            <button type="button" href="{{route('ViewFacture', [$ajp->facture_id ] )}}" class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
														</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')
@endpush


@push('script.footer1')
@endpush

@section('title')
TABLEAU DE BORD
@endsection


@section('dd')
active
@endsection



@push('script.footer2')
<script>
    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });

   

    var data = [
        @foreach($acteparmois as $am)
        {{$am->nbre}},
        @endforeach
    ];
    var moisanne = [
        @foreach($acteparmois as $am)
        "{{$am->mois}}",
        @endforeach
    ];

    var actepayeparmois = [
        @foreach($actepayeparmois as $am)
        {{$am->nbre}},
        @endforeach
    ];

    var acteinpayeparmois = [
        @foreach($acteinpayeparmois as $am)
        {{$am->nbre}},
        @endforeach
    ];
    
    

    console.log(data);
    console.log(moisanne);
    console.log(actepayeparmois);
    console.log(acteinpayeparmois);

    var overiewChart = function(){
		 var options = {
          series: [{
          name: 'Nombre d\'actes',
          type: 'column',
          data: data
        }, {
          name: 'Actes payés',
          type: 'area',
          data: actepayeparmois
        }, {
          name: 'Actes Impayés',
          type: 'line',
          data: acteinpayeparmois
        }],
          chart: {
          height: 300,
          type: 'line',
          stacked: false,
		  toolbar: {
				show: false,
			},
        },
        stroke: {
          width: [0, 1, 1],
          curve: 'straight',
		  dashArray: [0, 0, 5]
        },
		legend: {
			fontSize: '13px',
			fontFamily: 'poppins',
			 labels: {
				  colors:'#888888', 
			 }
		},
        plotOptions: {
          bar: {
            columnWidth: '18%',
			 borderRadius:6	,
          }
        },
        
        fill: {
          //opacity: [0.1, 0.1, 1],
		  type : 'gradient',
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            /* opacityFrom: 0.85,
            opacityTo: 0.55, */
			colorStops : [
				[
					{
					  offset: 0,
					  color: 'var(--primary)',
					  opacity: 1
					},
					{
					  offset: 100,
					  color: 'var(--primary)',
					  opacity: 1
					}
				],
				[
					{
					  offset: 0,
					  color: '#3AC977',
					  opacity: 1
					},
					{
					  offset: 0.4,
					  color: '#3AC977',
					  opacity: .15
					},
					{
					  offset: 100,
					  color: '#3AC977',
					  opacity: 0
					}
				],
				[
					{
					  offset: 0,
					  color: '#FF5E5E',
					  opacity: 1
					},
					{
					  offset: 100,
					  color: '#FF5E5E',
					  opacity: 1
					}
				],
			],
            stops: [0, 100, 100, 100]
          }
        },
		colors:["var(--primary)","#3AC977","#FF5E5E"],
        labels: moisanne,
        markers: {
          size: 0
        },
        xaxis: {
          type: 'month',
		  labels: {
			   style: {
				   fontSize: '13px',
				   colors:'#888888',
			   },
		  },
        },
        yaxis: {
          min: 0,
		  tickAmount: 4,
		  labels: {
			   style: {
				   fontSize: '13px',
				   colors:'#888888',
			   },
		  },
        },
        tooltip: {
          shared: true,
          intersect: false,
          y: {
            formatter: function (y) {
              if (typeof y !== "undefined") {
                return y.toFixed(0);
              }
              return y;
        
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#overiewChart"), options);
        chart.render();
		
		$(".mix-chart-tab .nav-link").on('click',function(){
			var seriesType = $(this).attr('data-series');
			var columnData = [];
			var areaData = [];
			var lineData = [];
			switch(seriesType) {
				default:
					columnData = [75, 80, 72, 100, 50, 100, 80, 30, 95, 35, 75,100];
					areaData = [44, 65, 55, 75, 45, 55, 40, 60, 75, 45, 50,42];
					lineData = [30, 25, 45, 30, 25, 35, 20, 45, 35, 30, 35,20];
			}
			
		})
	 
	}
</script>
@endpush





