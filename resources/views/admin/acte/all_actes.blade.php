@extends('layouts.admin.master')
@section('content')

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="row">
									
                                  
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="{{route('PayerActesMedicaux')}}">
                                            <div class="card same-card headerP">
                                                <div class="card-body depostit-card ">
                                                    <div class="depostit-card-media d-flex justify-content-between style-1">
                                                        <div>
                                                            <h6>ACTES PAYES</h6>
                                                            <h3>{{number_format($actesp->count(), 0, ',', ' ')}}</h3>
                                                        </div>
                                                        <div class="icon-box bg-success">
                                                            <i style="color: #fff;" class="las la-file-invoice-dollar"></i>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="{{route('ImpayerActesMedicaux')}}">
                                            <div class="card same-card headerP">
                                                <div class="card-body depostit-card p-0">
                                                    <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                        <div>
                                                            <h6>ACTES IMPAYES</h6>
                                                            <h3>{{number_format($actesi->count(), 0, ',', ' ')}}</h3>
                                                        </div>
                                                        <div class="icon-box bg-success">
                                                            <i class="las la-file-invoice-dollar" style="color: #fff;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="{{route('DemandeActesMedicauxAccordees')}}">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card">
                                                <div class="depostit-card-media d-flex justify-content-between style-1">
                                                    <div>
                                                        <h6>DEMANDES ACCORDEES</h6>
                                                        <h3>{{number_format($demandes->where('entente','Accordée')->count(), 0, ',', ' ')}}</h3>
                                                    </div>
                                                    <div class="icon-box bg-warning">
														<i class="las la-praying-hands" style="color: #fff;"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="{{route('DemandeActesMedicauxRejetees')}}">
                                        <div class="card same-card same-card headerP">
                                            <div class="card-body depostit-card p-0">
                                                <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                    <div>
                                                        <h6>DEMANDES REJETEES</h6>
                                                        <h3>{{number_format($demandes->where('entente','Rejetée')->count(), 0, ',', ' ')}}</h3>
                                                    </div>
                                                    <div class="icon-box bg-danger">
													<i class="las la-praying-hands" style="color: #fff;"></i>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP" style="padding: 15px;">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Date début</label>
                                            <input type="date" id="datedebut" name="datedebut" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Date fin</label>
                                            <input type="date" id="datefin" name="datefin" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Type d'assuré</label>
                                            <select name="nature_id" id="type" class="form-control border-success">
                                                <option value="all">Tous les assurés</option>
                                                <option value="App\Models\Mutualiste">Assuré principal</option>
                                                <option value="App\Models\Ayantdroit">Ayants-droit</option>
                                            </select>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Nature d'Acte</label>
                                            <select name="nature_id" id="nature_id" class="form-control border-success">
                                                <option value="all">Tout</option>
                                                @foreach($ListePrestation as $data)
                                                    <option value="{{$data->id}}">{{$data->libelle}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Statut</label>
                                            <select name="statut" id="statut" class="form-control border-success">
                                                <option value="all">Tout</option>
                                                <option value="En attente">En attente</option>
                                                <option value="Accordée">Accordé</option>
                                                <option value="Rejété">Rejété</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <button class="btn btn-success btn-icon right-icon" id="search" style="margin-top: 2em; width:100%"><span>RECHERCHEZ </span> <i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                               
                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0" id="HeaderTitle">{{$title}} ({{number_format($dataQuery->count(), 0, ',', ' ')}})</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table id="table" class="display table" style="min-width: 845px">
                                                <thead>
                                                    <tr>
                                                        <th>ASSURE</th>
                                                        <th>DATE : HEURE</th>
                                                        <th>ACTES</th>
                                                        <th>MONTANT</th>
                                                        <th>TIER PAYANT</th>
                                                        <th>PART ASSURE</th>
                                                        <th>ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                @if($dataQuery->count()>0)
                                                <tbody id="tbody">
                                                    @foreach($dataQuery as $ai)
                                                        @php
                                                            $data = $ai->prestamutable_type::find($ai->prestamutable_id);
                                                        @endphp
                                                    <tr>
                                                        <td class="txt-dark">
                                                            <div class="d-flex align-items-center">
                                                                @if(!$data->photo)
                                                                    <img src="{{URL::asset('erpfiles/dist/img/mock1.jpg')}}" class="avatar avatar-md rounded-circle" alt="">
                                                                    @else
                                                                        @if($ai->prestamutable_type =='App\Models\Mutualiste')
                                                                            <img src='{{config("app.imgLink")}}{{$data->photo}}' class="avatar avatar-md rounded-circle" alt="">
                                                                        @else
                                                                            <img src='{{config("app.adLink")}}{{$data->photo}}' class="avatar avatar-md rounded-circle" alt="">
                                                                        @endif
                                                                @endif
                                                                <p class="mb-0 ms-2 txt-dark">
                                                                    <a href="">
                                                                        {{$data->first_name}} {{$data->last_name ?? $data->name}}
                                                                    </a>
                                                                </p>	
                                                            </div>
                                                        </td>
													    <td class="txt-dark">{{date('d/m/Y', strtotime($ai->date))}} à <b>{{$ai->heure}}</b></td>
													    <td class="txt-dark">{{$ai->libelle}}</td>
                                                        
                                                        <td class="txt-dark">{{number_format($ai->part_assure+$ai->part_muscopci, 0, ',', ' ')}} FCFA</td>
                                                        <td class="txt-dark">{{number_format($ai->part_assure, 0, ',', ' ')}} FCFA</td>
                                                        <td class="txt-dark"><b>{{number_format($ai->part_muscopci, 0, ',', ' ')}} FCFA</b></td>
                                                        <td>
                                                        @php
                                                            if($ai->prestamutable_type =='App\Models\Mutualiste'){
                                                                $type = "Mutualiste";
                                                            }else{
                                                                $type = "Ayant-droit";
                                                            }
                                                        @endphp
                                                        
														<button type="button" href="{{route('ViewActeTampon', [$ai->id , $ai->prestamutable_id,$type] )}}" class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
														</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')
@endpush


@push('script.footer1')
<script>

    var table = $('#table').DataTable();

    $('#search').on('click',()=>{

        $('#search').html('<div class="spinner-border" role="status"></div>');

        let datedebut = $('#datedebut').val();
        let datefin = $('#datefin').val();
        let type = $('#type').val();
        let nature_id = $('#nature_id').val();
        let statut = $('#statut').val();
        
        table.destroy();

        $.ajax({
            method: 'GET',
            data : {
                datedebut : datedebut,
                datefin : datefin,
                type : type,
                nature_id : nature_id,
                statut : statut
            },
            url: "{{route('RechecheActesMedicaux')}}",
            success : function(response){

                $("#tbody").html(response);
                table = $('#table').DataTable();
                $('#search').html('<span>RECHERCHEZ </span> <i class="fa fa-search"></i>');
                $('#HeaderTitle').html('<h4 id="HeaderTitle" class="heading mb-0 HeaderTitle">RESULTATS : ('+table.rows().count()+') </h4>');

            }
        });

    });

    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
</script>
@endpush

@section('title')
ACTES MEDICAUX
@endsection


@section('dd')
active
@endsection



@push('script.footer2')

@endpush





