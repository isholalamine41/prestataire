<?php if($resultats->count()>0) : ?>
            <?php foreach($resultats as $facture): ?>
                <?php
                    $ajp = $facture->PrestaMutua()->first();
                    $data = $ajp->prestamutable_type::find($ajp->prestamutable_id);
                ?>
                <tr>
                    <td class="txt-dark">
                        <div class="d-flex align-items-center">
                            <?php if(!$data->photo): ?>
                                <img src="<?=URL::asset('erpfiles/dist/img/mock1.jpg')?>" class="avatar avatar-md rounded-circle" alt="">
                            <?php else: ?>
                                <?php if($ajp->prestamutable_type =='App\Models\Mutualiste'): ?>
                                    <img src='<?=config("app.imgLink")?><?=$data->photo?>' class="avatar avatar-md rounded-circle" alt="">
                                <?php else: ?>
                                    <img src='<?=config("app.adLink")?><?=$data->photo?>' class="avatar avatar-md rounded-circle" alt="">
                                <?php endif ?>
                            <?php endif ?>
                            <p class="mb-0 ms-2 txt-dark">
                                <a href="">
                                    <?=$data->first_name?> <?=$data->last_name ?? $data->name?>
                                </a>
                            </p>	
                        </div>
                    </td>
                    <td class="txt-dark"><?=date('d/m/Y', strtotime($facture->date))?> à <b><?=$facture->heure?></b></td>
                    <td class="txt-dark text-center" ><span class="badge badge-primary border-0"><?=$facture->PrestaMutua->count()?></span></td>
                    <td class="txt-dark"><?=number_format($facture->montant, 0, ',', ' ')?> FCFA</td>
                    <td class="txt-dark"><?=number_format($facture->part_assure, 0, ',', ' ')?> FCFA</td>
                    <td class="txt-dark"><b><?=number_format($facture->part_muscopci, 0, ',', ' ')?> FCFA</b></td>
                    <td>
                        <button type="button" href="<?=route('ViewFacture', [$facture->id] )?>" class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>