@extends('layouts.admin.master')
@section('content')

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">

                            
                            <div class="row">
                                
                                <div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
                                    <div class="widget-stat card  bg-success headerP">
                                        <div class="card-body">
                                            <div class="media">
                                                <span class="me-1">
                                                    <i class="la la-users"></i>
                                                </span>
                                                <div class="media-body text-white">
                                                    <p class="mb-1">MONTANT GLOBAL</p>
                                                    <h4 class="text-white">{{number_format($prestataire->Facture->SUM('part_muscopci'), 0, ',', ' ')}} </h4>
                                                    <span>FCFA</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
                                    <div class="widget-stat card  bg-warning headerP">
                                        <div class="card-body">
                                            <div class="media">
                                                <span class="me-1">
                                                    <i class="la la-users"></i>
                                                </span>
                                                <div class="media-body text-white">
                                                    <p class="mb-1">MONTANT GLOBAL REGLE</p>
                                                    <h4 class="text-white">{{number_format($prestataire->Facture->where('payer',1)->SUM('part_muscopci'), 0, ',', ' ')}} </h4>
                                                    <span>FCFA</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
                                    <div class="widget-stat card  bg-danger headerP">
                                        <div class="card-body">
                                            <div class="media">
                                                <span class="me-1">
                                                    <i class="las la-file-invoice-dollar"></i>
                                                </span>
                                                <div class="media-body text-white">
                                                    <p class="mb-1">FACTURE EN COURS</p>
                                                    <h4 class="text-white">{{number_format($prestataire->Facture->where('payer',0)->SUM('part_muscopci'), 0, ',', ' ')}}</h4>
                                                    <span>FCFA</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              


                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP" style="padding: 15px;">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date début</label>
                                            <input type="date" id="datedebut" name="datedebut" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date fin</label>
                                            <input type="date" id="datefin" name="datefin" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Statut</label>
                                            <select name="statut" id="statut" class="form-control border-success">
                                                <option value="all">Tout</option>
                                                <option value="1">Payer</option>
                                                <option value="0">Impayer</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <button class="btn btn-success btn-icon right-icon" id="search" style="margin-top: 2em; width:100%"><span>RECHERCHEZ </span> <i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                               

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0" id="HeaderTitle">FACTURES IMPAYEES ({{$prestataire->Facture->where('payer',0)->count()}})</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table id="table" class="display table" style="min-width: 845px">
                                                <thead>
                                                    <tr>
                                                        <th>ASSURE</th>
                                                        <th>DATE : HEURE</th>
                                                        <th>Nbre ACTES</th>
                                                        <th>MONTANT</th>
                                                        <th>TIER PAYANT</th>
                                                        <th>PAR ASSURE</th>
                                                        <th>ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                @if($prestataire->Facture->where('payer',0)->count()>0)
                                                <tbody id="tbody" >
                                                    @foreach($prestataire->Facture->where('payer',0) as $facture)
                                                        @php
                                                            $acte = $facture->PrestaMutua()->first();
                                                        @endphp
                                                        @if($acte)
                                                            <tr>
                                                                <td class="txt-dark">
                                                                    <div class="d-flex align-items-center">
                                                                        @if($acte)
                                                                        @if(!$acte->prestamutable->photo)
                                                                                <img src="{{URL::asset('erpfiles/dist/img/mock1.jpg')}}" class="avatar avatar-md rounded-circle" alt="">
                                                                                @else
                                                                                @if($acte->prestamutable_type =='App\Models\Mutualiste')
                                                                                    <img src='{{config("app.imgLink")}}{{$acte->prestamutable->photo}}' class="avatar avatar-md rounded-circle" alt="">
                                                                                @else
                                                                                    <img src='{{config("app.adLink")}}{{$acte->prestamutable->photo}}' class="avatar avatar-md rounded-circle" alt="">
                                                                                @endif
                                                                        @endif
                                                                        <p class="mb-0 ms-2 txt-dark">
                                                                            <a href="">
                                                                                {{$acte->prestamutable->first_name}} {{$acte->prestamutable->last_name ?? $acte->prestamutable->name}}
                                                                            </a>
                                                                        </p>	
                                                                        @endif
                                                                    </div>
                                                                </td>
                                                                <td class="txt-dark">{{date('d/m/Y', strtotime($facture->date))}} à <b>{{$facture->heure}}</b></td>
                                                                <td class="txt-dark text-center" ><span class="badge badge-primary border-0">{{$facture->PrestaMutua->count()}}</span></td>
                                                                <td class="txt-dark">{{number_format($facture->montant, 0, ',', ' ')}} FCFA</td>
                                                                <td class="txt-dark">{{number_format($facture->part_assure, 0, ',', ' ')}} FCFA</td>
                                                                <td class="txt-dark"><b>{{number_format($facture->part_muscopci, 0, ',', ' ')}} FCFA</b></td>
                                                                <td>
                                                                    <button type="button" href="{{route('ViewFacture', [$facture->id] )}}" class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')
@endpush


@push('script.footer1')
@endpush

@section('title')
Factures
@endsection


@section('dd')
active
@endsection



@push('script.footer2')
<script>
    
    var table = $('#table').DataTable();

    $('#search').on('click',()=>{

        $('#search').html('<div class="spinner-border" role="status"></div>');

        let datedebut = $('#datedebut').val();
        let datefin = $('#datefin').val();
        let statut = $('#statut').val();
        
        table.destroy();

        $.ajax({
            method: 'GET',
            data : {
                datedebut : datedebut,
                datefin : datefin,
                statut : statut,
            },
            url: "{{route('RechecheFacture')}}",
            success : function(response){

                $("#tbody").html(response);
                table = $('#table').DataTable();
                $('#search').html('<span>RECHERCHEZ </span> <i class="fa fa-search"></i>');
                $('#HeaderTitle').html('<h4 id="HeaderTitle" class="heading mb-0 HeaderTitle">RESULTATS : ('+table.rows().count()+') </h4>');
            }
        });

    });
</script>
<script>
    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
</script>
@endpush





