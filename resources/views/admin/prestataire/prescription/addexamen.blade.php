<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-dark" style="background: #000 !important">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">PRESCRIRE UN EXAMEN</h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body"> 
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <h4>Acte : {{$a->libelle}}</h4>
                    <form action="{{route('AddExamen',[$a->id, $assure->id, $type])}}" method="post">@csrf
                        <div class="row">
                        <div class="col-xl-6 col-xxl-6 col-md-6 col-lg-6">
                            <label for="">TYPE DE PRESCRIPTION *</label>
                            <select style="border: 1px solid #000;" id="type_prescription_id" class="form-control">
                                @foreach($typep as $tp)
                                <option value="{{$tp->id}}">{{$tp->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xl-6 col-xxl-6 col-md-6 col-lg-6">
                            <label for="">LIBELLE *</label>
                            <input  style="border: 1px solid #000;" type="text" class="form-control" id="libelle">
                        </div>
                        <div class="col-xl-12 col-xxl-12 col-md-12"><br>
                            <label for="">MOTIF</label>
                            <textarea style="border: 1px solid #000;" id="description" class="form-control" id="" cols="30" rows="3"></textarea>
                        </div>
                        <div align="right"><br>
                        <button type="button" style="width: 250px;" class="btn btn-warning" id="add-button"> AJOUTER <i class="fa fa-plus"></i> </button>
                        </div>
                        </div><br><br>
                        <table class="table table-striped">
                            <tbody id="tbody" >
                            </tbody>
                        </table>
                        <button type="submit" style="width: 250px;background: #000;float: right;" class="btn btn-dark title hidden submit"> METTRE EN LIGNE </button>
                    </form>
                </div>
                <br><br><br>
                <h4>LISTE DES PRESCRIPTIONS DEJA EFFECTUEES</h4>
                <table class="table card-table border-no success-tbl p-0 headerP">
                    <thead>
                        <tr>
                            <th>DATE : HEURE </th>
                            <th>NATURE </th>
                            <th>PRESCRIPTION</th>
                            <th>CODE</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($a->Prescription->count()>0)
                        @foreach($a->Prescription as $p)
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="ms-2 cat-name">
                                        <p class="mb-0">{{date("d/m/Y"), strtotime($p->date)}} à {{$p->heure}}</p>
                                    </div>	
                                </div>
                            </td>
                            <td class="txt-dark">{{$p->TypePrescription->libelle}}</td>
                            <td class="txt-dark">{{$p->libelle}}</td>

                            <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                                {{$p->Code->code}}
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                
                </table>
               

    

            </div>
        </div>
    </div>
</div>


<style>
    .line{
        margin-bottom: 15px !important;
    }
    .td{
        color:black !important;
    }
    .hidden{
        display: none;
    }
    .fa-times{
        cursor: pointer;
    }
</style>
<script>

    $('.submit').on('click',function(){
        $('.submit').addClass('hidden');
    });

    $('#add-button').on('click',function(){

        type_prescription_id = $('#type_prescription_id').val();
        libelle = $('#libelle').val();
        description = $('#description').val();
        type =  $('#type_prescription_id option:selected').text();

        in_array = false;

        $('.libelle').each(function(index, element) {

            if($(element).val()==libelle)
                in_array = true;
        });


        if(in_array){
            alert("Cette option déjà présente dans la prescription");
            return;
        }
        
        if(type_prescription_id!='' && libelle!=''){
            $('#tbody').append(`
                <tr>
                    <td class="td">${type} <input name="type_prescription_id[]" type="hidden" value="${type_prescription_id}"</td>
                    <td class="td">${libelle} <input type="hidden" class="libelle"  name="libelle[]" value="${libelle}" ></td>
                    <td class="td">${description} <input type="hidden" class="description"  name="description[]" value="${description}" ></td>
                    <td class="td" class="text-center"><i onclick="delete_line(this)" class="fa fa-times text-danger"></i></td>
                </tr>
            `);

            $('#type_prescription_id').val('');
            $('#libelle').val('');
            $('#description').val('');

            if($('#tbody').text().trim()!=''){
                $('.title').removeClass('hidden');
            }
       }
    });

    function delete_line(self){
        $(self).parent().parent().remove();
        if($('#tbody').text().trim()!=''){
            $('.title').removeClass('hidden');
        }
    }

</script>
