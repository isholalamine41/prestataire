<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-warning"  style="background: #000 !important">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">PRESCRIRE UN EXAMEN</h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body"> 
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <form action="{{route('AddPrescription')}}" method="post">
                        @csrf
                        <div class="row">
                            <input type="hidden" value="{{$assure->id}}" name="assure_id">
                            <input type="hidden" value="{{$type}}" name="type">
                            <div class="col-xl-12 col-xxl-12 col-md-12 col-lg-12">
                                <label for="">ACTES *</label>
                                <select style="border: 1px solid #ff9f00;" name="acte_id" class="form-control">
                                    @foreach($actes as $tp)
                                    <option value="{{$tp->id}}">{{$tp->libelle}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xl-6 col-xxl-6 col-md-6 col-lg-6">
                                <label for="">TYPE DE PRESCRIPTION *</label>
                                <select style="border: 1px solid #ff9f00;" id="type_prescription_id" class="form-control">
                                    @foreach($typep as $tp)
                                    <option value="{{$tp->id}}">{{$tp->libelle}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xl-6 col-xxl-6 col-md-6 col-lg-6">
                                <label for="">LIBELLE *</label>
                                <input  style="border: 1px solid #ff9f00;" type="text" class="form-control" id="libelle">
                            </div>
                            <div class="col-xl-12 col-xxl-12 col-md-12"><br>
                                <label for="">MOTIF</label>
                                <textarea style="border: 1px solid #ff9f00;" id="description" class="form-control" cols="30" rows="3"></textarea>
                            </div>
                            <div align="right"><br>
                            <button type="button" style="width: 250px;" class="btn btn-warning" id="add-button"> AJOUTER <i class="fa fa-plus"></i> </button>
                            </div>
                            </div><br><br>
                            <table class="table table-striped">
                                <tbody id="tbody" >
                                </tbody>
                            </table>
                            <button type="submit" style="width: 250px;background: #000 !important;float:right" class="btn btn-warning title hidden submit"> METTRE EN LIGNE </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    .line{
        margin-bottom: 15px !important;
    }
    .td{
        color:black !important;
    }
    .hidden{
        display: none;
    }
    .fa-times{
        cursor: pointer;
    }
</style>
<script>
    
    $('.submit').on('click',function(){
        $('.submit').addClass('hidden');
    });

    $('#add-button').on('click',function(){

        type_prescription_id = $('#type_prescription_id').val();
        libelle = $('#libelle').val();
        description = $('#description').val();
        type =  $('#type_prescription_id option:selected').text();

        in_array = false;

        $('.libelle').each(function(index, element) {

            if($(element).val()==libelle)
                in_array = true;
        });


        if(in_array){
            alert("Cette option déjà présente dans la prescription");
            return;
        }
        
        if(type_prescription_id!='' && libelle!=''){
            $('#tbody').append(`
                <tr>
                    <td class="td">${type} <input name="type_prescription_id[]" type="hidden" value="${type_prescription_id}"</td>
                    <td class="td">${libelle} <input type="hidden" class="libelle"  name="libelle[]" value="${libelle}" ></td>
                    <td class="td">${description} <input type="hidden" class="description"  name="description[]" value="${description}" ></td>
                    <td class="td" class="text-center"><i onclick="delete_line(this)" class="fa fa-times text-danger"></i></td>
                </tr>
            `);

            $('#type_prescription_id').val('');
            $('#libelle').val('');
            $('#description').val('');

            if($('#tbody').text().trim()!=''){
                $('.title').removeClass('hidden');
            }
       }
    });

    function delete_line(self){
        $(self).parent().parent().remove();
        if($('#tbody').text().trim()!=''){
            $('.title').removeClass('hidden');
        }
    }

</script>

