
<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-success">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">Détails actes facture</h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <table class="table card-table border-no success-tbl p-0 headerP">
                    <thead>
                        <tr>
                            <th>PRESTATION</th>
                            <th>MONTANT</th>
                            <th>PART ASSURANCE</th>
                            <th>PART ASSURE</th>
                            <th>QTE</th>
                            <th>STATUT</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($prestaMutua as $a)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-2 cat-name">
                                            <p class="mb-0">{{$a->libelle}}</p>
                                        </div>	
                                    </div>
                                </td>
                                <td class="txt-dark">{{number_format($a->part_muscopci + $a->part_assure, 0, ',', ' ')}} FCFA</td>
                                <td class="txt-dark">
                                    {{number_format($a->part_muscopci, 0, ',', ' ')}} FCFA  
                                </td>

                                <td  class="txt-dark">
                                    {{number_format($a->part_assure, 0, ',', ' ')}} FCFA
                                </td>
                                <td  class="txt-dark">
                                    {{$a->qte}}
                                </td>
                                <td>
                                    @if($a->type == "demande")
                                        @if($a->entente == 'En attente')
                                        <span class="badge badge-warning border-0">{{$a->entente}}</span>
                                        @endif
                                        @if($a->entente == 'Accordée')
                                        <span class="badge badge-success border-0">{{$a->entente}}</span>
                                        @endif
                                        @if($a->entente == 'Rejetée')
                                        <span class="badge badge-danger border-0">{{$a->entente}}</span>
                                        @endif
                                        @else
                                        Brouillon
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                
                </table>
                

            </div>
        </div>
    </div>
</div>
