<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">
                    Hospitalisation de {{$hospitalisation->assurer->first_name}}
                </h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="text-danger">Code : {{$hospitalisation->Code->code}}</h3>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('printHospitalisation',[$hospitalisation->id])}}" class="btn btn-warning btn-block">Imprimer <i class="fa fa-print"></i></a>
                    </div>
                </div>
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <table class="table table-striped">
                        <thead>
                            <th>Quantité</th>
                            <th>Acte</th>
                            <th>Montant</th>
                            <th>Part assuré</th>
                            <th>Part MUSCOP CI</th>
                        </thead>
                        <tbody>
                            @foreach($hospitalisation->lignes_facture() as $ligne)
                                <tr style="text-align: left;">
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->quantite}}</td>
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->prestation_name}}</td>
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->quantite*$ligne->prix}}</td>
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->part_assure}}</td>
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->part_muscopci}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>