<style>
    *{
        font-family: 'Courier New', Courier, monospace
    }
</style>
<div style="width: 100%;">
    <div style="border: 1px solid #ccc; padding: 15px; margin-top: 10px;">
        <div style="font-weight: bold; font-size: 1.5em;">Code hospitalisation : <strong>{{$hospitalisation->Code->code}}</strong> <span style="float: right; font-weight: bold;"><img class="avatar avatar-md" style="width:70px" src="https://www.muscop-ci.com/websitefiles/images/logo.png" alt="Muscop-ci Logo"></span></div>
        <h3>Hospitalisation {{$hospitalisation->Prestataire->rs}}</h3>
        <br>
        <div class="table-responsive">

            <table class="table table-striped"  style="width: 100%">
                <thead>
                    <th style="text-align: left;padding: 7px" >Quantité</th>
                    <th style="text-align: left;padding: 7px" >Acte</th>
                    <th style="text-align: left;padding: 7px" >Montant</th>
                    <th style="text-align: left;padding: 7px" >Part assuré</th>
                    <th style="text-align: left;padding: 7px" >Part MUSCOP CI</th>
                </thead>
                <tbody>
                    @foreach($hospitalisation->lignes_facture() as $ligne)
                        <tr style="text-align: left;">
                            <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->quantite}}</td>
                            <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->prestation_name}}</td>
                            <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->quantite*$ligne->prix}}</td>
                            <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->quantite*$ligne->part_assure}}</td>
                            <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$ligne->quantite*$ligne->part_muscopci}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tr style="text-align: left;">
                    <td></td>
                    <td></td>
                    <td colspan="2">Total</td>
                    <td style="padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" > <b></b>{{$hospitalisation->amount()}}</td>
                </tr>
                <tr style="text-align: left;">
                    <td></td>
                    <td></td>
                    <td colspan="2">Part Muscop-ci </td>
                    <td style="padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" > <b></b>{{$hospitalisation->part_muscopci()}}</td>
                </tr>
                <tr style="text-align: left;">
                    <td></td>
                    <td></td>
                    <td colspan="2">Part assuré </td>
                    <td style="padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" > <b></b>{{$hospitalisation->part_assure()}}</td>
                </tr>
            </table>

        </div>
        <br>
        <div style="margin-top: 10px;">
            <div style="display: flex; justify-content: flex-end; margin-bottom: 15px;">
                <div>
                    <img src="https://chart.googleapis.com/chart?chs=110x110&cht=qr&chl={{$hospitalisation->Code->code}}&chld=L|1&choe=UTF-8" alt="" style="width: 110px;" class="img-fluid">
                </div>
            </div>
            <p>
                <i style="color:black">
                    {{date('d/m/Y H:i:s',strtotime($hospitalisation->created_at))}}
                </i>
            </p>
        </div>
    </div>
</div>
