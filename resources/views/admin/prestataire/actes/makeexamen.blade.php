@extends('layouts.admin.master')
@section('content')+

<div class="content-body">
    <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="card-body" style="border:1px solid #452b90; border-radius:5px;" >
                            <div  class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3  col-xs-3 ">
                                    <div class="crd-bx-img" align="left">
                                        @if($type == "Mutualiste")
                                        <img src='{{config("app.imgLink")}}{{$assure->photo}}' style="height:80px;" class="rounded-circle" alt="">
                                        @else
                                        <img src='{{config("app.adLink")}}{{$assure->photo}}' style="height:80px;" class="rounded-circle" alt="">
                                        @endif
                                        <div class="active"></div>
                                    </div>
                                </div>

                                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9  col-xs-9">
                                    <div class="card__text" align="left" style="margin-left: 6px;">
                                        <h4 class="mb-0 txt-dark">{{$assure->first_name}} {{$assure->name ?? $assure->last_name}}</h4>
                                        <p class="mb-0">{{$assure->role ?? $assure->assurance_number}}</p><p class="mb-0" style="margin-top: -0.5em; padding:0px;"><b>{{$assure->birth_date ?? $assure->date_naissance}}</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <br>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="card">
                        <div class="card-header bg bg-success">
                            <h4 class="card-title text-white">RECHERCHE </h4>
                        </div>
                        <div class="card-body"  style="border:1px solid #3a9b94; border-radius:5px; margin-top:-3px;">
                            <input type="hidden" class="assure_id" name="assure_id" value="{{$assure->id}}">
                            <input type="hidden" class="type" name="type" value="{{$type}}">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="txt-dark weight-bold">Saisissez le code de la prescription</span>
                                    <input type="search" class="form-control" id="code" style="border-color:#452b90;margin-top:10px;margin-bottom:10px;">
                                    <div class="row">
                                        <div class="col-md-12 select_area hidden">
                                            <span class="txt-dark weight-bold">Sélectionnez l'acte médical</span>
                                            <select class="match-grouped-options select2-hidden-accessible border-success onChangeSelecet lpid" id="presta_muta" data-select2-id="64" tabindex="-1" aria-hidden="true">
                                                <option></option>
                                                @if($prestations->count()>0)
                                                        @foreach($prestations as $p)
                                                            <optgroup label="{{$p->libelle}}">
                                                                @if($prestations->count()>0)
                                                                    @foreach($p->ListePrestation->where('prestataire_id', $prestataire->id) as $lp)
                                                                        @if($lp->ParamlistePrestation->last()!=null)
                                                                            <option 
                                                                            data-prix="{{$lp->ParamlistePrestation->last()->cout}}"
                                                                            data-part_assure="{{$lp->ParamlistePrestation->last()->parAssure}}"
                                                                            data-part_muscopci="{{$lp->ParamlistePrestation->last()->tierPayant}}"
                                                                            data-id="{{$lp->id}}"
                                                                            value="{{ $lp->id }}">{{ $lp->libelle }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </optgroup>
                                                        @endforeach
                                                @endif
                                            </select>
                                            <br><br>
                                        </div>
                                    </div>
                                    <button id="valider-code" class="btn btn-primary btn-block">RECHERCHEZ <i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-8 col-lg-8 col-md-8">
                    <div class="card retourFacture"  style="border:1px solid #58bad7; border-radius:10px;">
                        <div class="card-header bg bg-info">
                                <h4 class="card-title text-white" id="code-title" >PRESCRIPTIONS </h4>
                            </div>
                            <div class="card-header" style="width: 100%;">
                                <div class="row" style="width: 100%;">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 hidden validate" >
                                        <h4 class="heading mb-0">MONTANT GLOBAL : <span id="global">0</span> FCFA  </h4>
                                        <h4 class="heading mb-0">PART ASSURANCE : <span id="assurance">0</span> FCFA  </h4>
                                        <h4 class="heading mb-0" style="color: brown;">MONTANT A PAYER : <span id="payer">0</span> FCFA  </h4>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                        @csrf
                                        <input type="hidden" name="prescription_id">
                                        <button type="button" class="btn  btn-square btn-primary validate hidden" id="valider-ordonnance"><i class="fa fa-save"></i> VALIDER L'EXAMEN</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body" >
                                <div class="card-body p-0">
                                    <div class="table-responsive p-0">
                                        <table class="table card-table border-no success-tbl p-0">
                                            <thead >
                                                <tr >
                                                    <th style="text-align: start">PRESCRIPTION</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody">
                                            </tbody>
                                        </table>
                                        <br>
                                        <table class="table card-table border-no success-tbl p-0">
                                            <thead >
                                                <tr >
                                                    <th style="text-align: start">ACTE</th>
                                                    <th style="text-align: start">MONTANT</th>
                                                    <th style="text-align: start">PART ASSURANCE</th>
                                                    <th style="text-align: start">PART ASSURE</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody-actes">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        @if(session()->has('messageErreur'))
                            
                            <div class="alert alert-danger solid alert-dismissible fade show">
                                <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                <strong>Erreur ! </strong> {{ Session::get('messageErreur') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"><span><i class="fa-solid fa-xmark"></i></span>
                                </button>
                            </div>
                            
                            @endif
                            @if(session()->has('message'))
                            
                            <div class="alert alert-success solid alert-dismissible fade show">
                                <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="me-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                <strong>Félicitations ! </strong> {{ Session::get('message') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                <span><i class="fa-solid fa-xmark"></i></span>
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="card"  style="border:1px solid #3a9b94; border-radius:10px;">
                    <div class="card-header bg bg-success">
                            <h4 class="card-title text-white">ACTE(S) MEDICAUX</h4>
                        </div>
                     
                        <div class="card-body" >
                            
                        <div class="card-body p-0">
                            <div class="table-responsive p-0">
                                <table class="table card-table border-no success-tbl p-0">
                                    <thead >
                                        <tr >
                                            <th>DATE : HEURE</th>
                                            <th>PRESTATION</th>
                                            <th>MONTANT</th>
                                            <th>PART ASSURANCE</th>
                                            <th>PART ASSURE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($actesM as $a)	
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="ms-2 cat-name">
                                                        <p class="mb-0">{{date('d/m/Y', strtotime($a->date))}} à {{$a->heure}}</p>
                                                    </div>	
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="ms-2 cat-name">
                                                        <p class="mb-0">{{$a->libelle}}</p>
                                                    </div>	
                                                </div>
                                            </td>
                                            <td>{{number_format($a->part_muscopci+$a->part_assure, 0, ',', ' ')}} FCFA</td>
                                            <td> {{number_format($a->part_muscopci, 0, ',', ' ')}} FCFA  </td>
                                            <td>{{number_format($a->part_assure, 0, ',', ' ')}} FCFA</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                
                                </table>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            
            </div>
            
    </div>
</div>
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')
    <link rel="stylesheet" href="{{URL::asset('erpfiles/vendor/select2/css/select2.min.css')}}">
	<link href="{{URL::asset('erpfiles/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">   
@endpush
<style>
    .hidden{
        display: none !important;
    }
</style>

@push('script.footer1')
<script>

    
    function amount(){
        var checkedChoices = $('.choice');
        var assurance = 0;
        var assure = 0;
        var global = 0;
        $.each(checkedChoices, function(index, element) {
            let price = parseFloat($(element).find('.prix').text());
            let part_muscopci = parseFloat($(element).find('.part_muscopci').text());
            let part_assure = parseFloat($(element).find('.part_assure').text());
            assurance+=part_muscopci;
            assure+=part_assure;
            global+=price;
        });

        $("#global").text(global);
        $("#assurance").text(assurance);
        $("#payer").text(assure);
    }

    $('#presta_muta').on('change',function(){


        let prestation = $('#presta_muta :checked');
        let prestation_text = prestation.text().trim();
        in_array = false;

        $('.prestation_id').each(function(index, element) {

            if($(element).val()==prestation.data('id'))
                in_array = true;
        });

        if(in_array){
            alert("Prestation déjà ajoutée ");
            return;
        }

        
        $("#tbody-actes").append(`
            <tr class="choice">
                <td>
                    <div class="d-flex align-items-center">
                        <p> ${prestation_text} </p>
                    </div>
                    <input name="prestation_id[]" type="hidden" class="prestation_id" value="${prestation.data('id')}">
                </td>
                <td class="prix">${prestation.data('prix')}</td>
                <td class="part_muscopci">${prestation.data('part_muscopci')}</td>
                <td class="part_assure">${prestation.data('part_assure')}</td>
                <td class="td" class="text-center"><i onclick="delete_line(this)" class="fa fa-trash text-danger"></i></td>
            </tr>
        `);

        if($('#tbody-actes').html().trim()!=''){
            $('.validate').removeClass('hidden');
        }

        amount();

    });

    $('#valider-code').on('click',()=>{

        let code = $('#code').val();

        if(code=='')
            return;

        $("#global").text(0);
        $("#assurance").text(0);
        $("#payer").text(0);


        $('#valider-code').html('<div class="spinner-border" role="status"></div>');
        $("#tbody").html('');

        $.ajax({
            method: 'GET',
            data : {
                code : code,
            },
            dataType: 'json',
            url: "{{route('GetPrescription')}}",
            success : function(response){
                
                if(response.success){
                    $('#code-title').html('CODE : '+code)
                    $("input[name=prescription_id]").val(response.id);
                    $("#tbody").append(`
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <h3> ${response.libelle} </h3>
                                </div>
                            </td>
                        </tr>
                    `);
                    $('.select_area').removeClass('hidden');
                }else{
                    $('.validate').addClass('hidden');
                    $('.select_area').addClass('hidden');
                    toastr.error('Aucune prescription ne correspond à ce code', 'Code invalide', {
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        closeButton: !0,
                        debug: !1,
                        newestOnTop: !0,
                        progressBar: !0,
                        preventDuplicates: !0,
                        onclick: null,
                        showDuration: "1500",
                        hideDuration: "1000",
                        extendedTimeOut: "1000",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                        tapToDismiss: !1
                    });
                }
                $('#code').val("");
                $('#valider-code').html('RECHERCHEZ <i class="fa fa-search"></i>');

            }
        });

    });

    function delete_line(self){

        $(self).parent().parent().remove();

        amount();

        if($('#tbody-actes').html().trim()!=''){
            $('.validate').removeClass('hidden');
        }

    }

    $('#valider-ordonnance').on('click',function(){

        let array = [];

        $('.prestation_id').each(function(index, element) {
            array.push($(element).val());
        });

        if(array.length==0){
            return false;
        }

        $('#valider-ordonnance').html('<div class="spinner-border" role="status"></div>');
        $('#valider-ordonnance').attr('disabled',true);

        $.ajax({
            method: 'get',
            url: "{{route('setPrescription')}}",
            data: {
                prescription_id : $("input[name=prescription_id]").val(),
                prestation_id : array,
                type : '{{$type}}',
                type_id : '{{$assure->id}}',
            },
            dataType: 'json',
            success : function(response){

                if (response.statut == 'success') {
                    window.location.reload();
                }else{

                    $('#valider-ordonnance').html("<i class='fa fa-save'></i> VALIDER L'EXAMEN");
                    $('#valider-ordonnance').attr('disabled',false);
                    
                    toastr.error(response.message, '', {
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        closeButton: !0,
                        debug: !1,
                        newestOnTop: !0,
                        progressBar: !0,
                        preventDuplicates: !0,
                        onclick: null,
                        showDuration: "1500",
                        hideDuration: "1000",
                        extendedTimeOut: "1000",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                        tapToDismiss: !1
                    });
                }
            }
        });

    });

</script>
@endpush

@section('title')
PRISE EN CHARGE
@endsection


@section('dd')
active
@endsection



@push('script.footer2')

@endpush





