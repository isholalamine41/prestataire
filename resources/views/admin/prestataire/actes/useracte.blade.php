@extends('layouts.admin.master')
@section('content')

<div class="content-body">
    <div class="container-fluid">

                @include('layouts.admin.block-assure')

                <div class="row">
                    <div class="col-md-12">
                        @if(session()->has('messageErreur'))
                            
                            <div class="alert alert-danger solid alert-dismissible fade show">
                                <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                <strong>Erreur ! </strong> {{ Session::get('messageErreur') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"><span><i class="fa-solid fa-xmark"></i></span>
                                </button>
                            </div>
                            
                            @endif
                            @if(session()->has('message'))
                            
                            <div class="alert alert-success solid alert-dismissible fade show">
                                <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="me-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                <strong>Félicitations ! </strong> {{ Session::get('message') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                <span><i class="fa-solid fa-xmark"></i></span>
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
                
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="card"  style="border:1px solid #3a9b94; border-radius:10px;">
                    <div class="card-header bg bg-success">
                            <h4 class="card-title text-white">ACTE(S) MEDICAUX</h4>
                        </div>
                    
                        <div class="card-body" >
                            
                        <div class="card-body p-0">
                            <div class="table-responsive-none p-0">
                                <table class="table card-table border-no success-tbl p-0">
                                    <thead >
                                        <tr >
                                            <th>DATE : HEURE</th>
                                            <th>PRESTATION</th>
                                            <th>MONTANT</th>
                                            <th>PART ASSURANCE</th>
                                            <th>PART ASSURE</th>
                                            <th>PRESCRIPTION</th>
                                            <th>ORDONNANCE</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($actesM as $a)	
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="ms-2 cat-name">
                                                        <p class="mb-0">{{date('d/m/Y', strtotime($a->date))}} à {{$a->heure}}</p>
                                                    </div>	
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="ms-2 cat-name">
                                                        <p class="mb-0">{{$a->libelle}}</p>
                                                    </div>	
                                                </div>
                                            </td>
                                            <td>{{number_format($a->part_muscopci+$a->part_assure, 0, ',', ' ')}} FCFA</td>
                                            <td> {{number_format($a->part_muscopci, 0, ',', ' ')}} FCFA  </td>
                                            <td>{{number_format($a->part_assure, 0, ',', ' ')}} FCFA</td>
                                            <td><span class="badge badge-primary">{{$a->Prescription->count()}}</span></td>
                                            <td><span class="badge badge-primary lancdModal">{{$a->Ordonnance->count()}}</span></td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                    <button type="button" class="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">PRESCRIPTION</button>
                                                    <div class="dropdown-menu" style="">
                                                        <a class="dropdown-item text-dark lanceModal"  href="{{route('PrescriptionOrdonnanceByActe',[$assure->id, $type,$a->id])}}">PRESCRIRE UNE ORDONNANCE</a>
                                                        <a class="dropdown-item lanceModal text-dark lanceModal" href="{{route('PrescriptionExam', [$a->id , $assure->id, $type] )}}"> PRESCRIRE UN EXAMEN / AUTRE</button>
                                                        <a class="dropdown-item lanceModal text-dark " href="{{route('ViewActeTampon', [$a->id , $assure->id, $type] )}}"> VOIR LES DETAILS</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                
                                </table>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            
            </div>
            
    </div>
</div>
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')
    <link rel="stylesheet" href="{{URL::asset('erpfiles/vendor/select2/css/select2.min.css')}}">
	<link href="{{URL::asset('erpfiles/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
@endpush


@push('script.footer1')
<script>
    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
   
    $(".loaderFacture").hide();
    $("#loaderACTES").hide();
     $(document).on("change",".onChangeSelecet", function(){
        $("#loaderACTES").show();
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        $.ajax({
            method: 'get',
            url: "/dashbord/prestataire/add-in-bill/liste-prestation/" + valueSelected,
            success : function(response){
                console.log(response.code);
                $('.resteformulaire').html(response.code);
                $("#loader").modal("hide"); 
             }
        });
       
    });

          $(".loaderFacture").hide();
          $(document).on("click","#AddActes", function(e){
            e.preventDefault();
            
            var _token = $('input[name="_token"]').val();
            var commentaire = $('.commentaire').val();
            var lpid = $('.lpid').val();
            var docteur = $('.docteur').val();
            var assure_id = $('.assure_id').val();
            var qte = $('.qte').val();
            var demande = $('.demande').val();
            var type = $('.type').val();

            var formData = {
              commentaire: commentaire,
              lpid: lpid,
              _token: _token,
              docteur: docteur,
              assure_id: assure_id,
              qte: qte,
              demande: demande,
              type: type,
            };

            if(commentaire !=''  && lpid !='' && type !='' && assure_id !=''){
                $(".loaderFacture").show();
              $.ajax({
                method: 'POST',
                url: '/dashbord/create/add-acte',
                data: formData,
                dataType:"json",
                success : function(response){
                  console.log(response.statut);
                  if (response.statut == 'success') {
                    $('.retourFacture').html(response.code);
                    $(".loaderFacture").hide(); 
                  }else{
                        $(".loaderFacture").hide();
                        toastr.warning(response.message, response.title, {
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        closeButton: !0,
                        debug: !1,
                        newestOnTop: !0,
                        progressBar: !0,
                        preventDuplicates: !0,
                        onclick: null,
                        showDuration: "500",
                        hideDuration: "1000",
                        extendedTimeOut: "1000",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                        tapToDismiss: !1
                        });
                         
                  }
                }
              });
            }else{
                alert("Veuillez renseigner tous les champs Svp.");
            }
            
        });



        $(document).on("click",".DeleteActeTamponAjax", function(e){
            
            e.preventDefault();
            var a=$(this);
            var url = a.attr("href");

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

          swalWithBootstrapButtons.fire({
            title: 'RETIRER UN ACTE ?',
            text: "ETES-VOUS SURE DE VOULOIR RETIRER CET ACTE",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "OUI, RETIRER",
            cancelButtonText: "NON, ANNULER",
            reverseButtons: true
          }).then((result) => {
            if (result.isConfirmed) {
                $(".loaderFacture").show();
                $.ajax({
                method: 'get',
                url: a.attr("href"),
                    success : function(response){
                    console.log(response.code);
                    $('.retourFacture').html(response.code);
                    $(".loaderFacture").hide(); 
                    if (response.statut == 'success') {
                        $(".loaderFacture").hide(); 
                        toastr.success(response.message, response.title, {
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        closeButton: !0,
                        debug: !1,
                        newestOnTop: !0,
                        progressBar: !0,
                        preventDuplicates: !0,
                        onclick: null,
                        showDuration: "500",
                        hideDuration: "1000",
                        extendedTimeOut: "1000",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                        tapToDismiss: !1
                        });
                        $(".loaderFacture").hide(); 
                    }{
                        $(".loaderFacture").hide(); 
                        toastr.error(response.message, response.title, {
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        closeButton: !0,
                        debug: !1,
                        newestOnTop: !0,
                        progressBar: !0,
                        preventDuplicates: !0,
                        onclick: null,
                        showDuration: "1500",
                        hideDuration: "1000",
                        extendedTimeOut: "1000",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                        tapToDismiss: !1
                        });
                    }
                }
              }) ;
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'ANNULATION',
                'RETRAIT ANNULE :)',
                'error'
              )
            }
          })
        });

        $(document).on("click",".deleteACTETamPon", function(e){
            
            e.preventDefault();
            var a=$(this);
            var url = a.attr("href");

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

          swalWithBootstrapButtons.fire({
            title: 'RETIRER TOUS LES ACTES ?',
            text: "ETES-VOUS SURE DE VOULOIR RETIRER TOUS LES ACTES",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "OUI, RETIRER",
            cancelButtonText: "NON, ANNULER",
            reverseButtons: true
          }).then((result) => {
            if (result.isConfirmed) {
                $(".loaderFacture").show();
                $.ajax({
                method: 'get',
                url: a.attr("href"),
                    success : function(response){
                    console.log(response.code);
                    $('.retourFacture').html(response.code);
                    $(".loaderFacture").hide(); 
                    if (response.statut == 'success') {
                        $(".loaderFacture").hide(); 
                        toastr.success(response.message, response.title, {
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        closeButton: !0,
                        debug: !1,
                        newestOnTop: !0,
                        progressBar: !0,
                        preventDuplicates: !0,
                        onclick: null,
                        showDuration: "500",
                        hideDuration: "1000",
                        extendedTimeOut: "1000",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                        tapToDismiss: !1
                        });
                        $(".loaderFacture").hide(); 
                    }{
                        $(".loaderFacture").hide(); 
                        toastr.error(response.message, response.title, {
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        closeButton: !0,
                        debug: !1,
                        newestOnTop: !0,
                        progressBar: !0,
                        preventDuplicates: !0,
                        onclick: null,
                        showDuration: "1500",
                        hideDuration: "1000",
                        extendedTimeOut: "1000",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                        tapToDismiss: !1
                        });
                    }
                }
              }) ;
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'ANNULATION',
                'RETRAIT ANNULE :)',
                'error'
              )
            }
          })
        });

        

</script>

@endpush

@section('title')
ACTES MEDICAUX {{$assure->first_name}} {{$assure->last_name ?? $assure->name}}
@endsection


@section('dd')
active
@endsection



@push('script.footer2')

@endpush





