<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-success">
                <!-- {{$a->ListePrestation->listeprestable->typePrestation->libelle}}    -->
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">{{$a->ListePrestation->listeprestable->libelle}} - {{$a->ListePrestation->libelle}} </h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <h4>{{$a->ListePrestation->Prestataire->rs}}</h4>
                <table class="table card-table border-no success-tbl p-0 headerP">
                    <thead>
                        <tr>
                            <th>PRESTATION</th>
                            <th>MONTANT</th>
                            <th>TIER PAYANT</th>
                            <th>PART ASSURE</th>
                            <th>QTE</th>
                            <th>STATUT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="ms-2 cat-name">
                                        <p class="mb-0">{{$a->libelle}}</p>
                                    </div>	
                                </div>
                            </td>
                            <td class="txt-dark">{{number_format($a->part_muscopci+$a->part_assure, 0, ',', ' ')}} FCFA</td>
                            <td class="txt-dark">
                                {{number_format($a->part_muscopci, 0, ',', ' ')}} FCFA  
                            </td>

                            <td  class="txt-dark">
                                {{number_format($a->part_assure, 0, ',', ' ')}} FCFA
                            </td>
                            <td  class="txt-dark">
                                {{$a->qte}}
                            </td>
                            <td>
                                @if($a->type == "demande")
                                    @if($a->entente == 'En attente')
                                    <span class="badge badge-warning border-0">{{$a->entente}}</span>
                                    @endif
                                    @if($a->entente == 'Accordée')
                                    <span class="badge badge-success border-0">{{$a->entente}}</span>
                                    @endif
                                    @if($a->entente == 'Rejetée')
                                    <span class="badge badge-danger border-0">{{$a->entente}}</span>
                                    @endif
                                    @else 
                                    {{$a->entente}}
                                @endif
                            </td>
                        </tr>
                    </tbody>
                
                </table>
                <p class="txt-dark weight-bold">Commentaire / Description</p>
                <div class="row " style="padding: 15px; margin-top:-2em;">
                
                    <div class="col-md-12 border-success">
                        {{$a->description}}
                    </div>
               </div>
                <br><br>

                @if($a->Ordonnance->count()>0)

                    <h4>LISTE DES ORDONNANCES ({{$a->Ordonnance->count()}})</h4>
                    <table class="table card-table border-no success-tbl p-0 headerP">
                        <thead>
                            <tr>
                                <th>DATE : HEURE </th>
                                <th>ACTE </th>
                                <th>MEDICAMENT</th>
                                <th>CODE</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($a->Ordonnance as $ordonnance)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-2 cat-name">
                                            <p class="mb-0">{{date("d/m/Y"), strtotime($ordonnance->date)}} à {{$ordonnance->heure}}</p>
                                        </div>	
                                    </div>
                                </td>
                                <td>{{$ordonnance->presta_muta->libelle}}</td>
                                <td class="text-center">
                                    <ul>
                                        @foreach($ordonnance->medicaments() as $data)
                                            <li>{{$data->quantite}} x {{$data->medicament}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                                {{ $ordonnance->Code ? $ordonnance->Code->code : ''}} 
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    
                    </table>
                    <br>
                
                @endif

                @if($a->Prescription->count()>0)

                    <h4>LISTE DES PRESCRIPTIONS DEJA EFFECTUEES ({{$a->Prescription->count()}})</h4>
                    <table class="table card-table border-no success-tbl p-0 headerP">
                        <thead>
                            <tr>
                                <th>DATE : HEURE </th>
                                <th>NATURE </th>
                                <th>PRESCRIPTION</th>
                                <th>CODE</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($a->Prescription as $p)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-2 cat-name">
                                            <p class="mb-0">{{date("d/m/Y"), strtotime($p->date)}} à {{$p->heure}}</p>
                                        </div>	
                                    </div>
                                </td>
                                <td class="txt-dark">{{$p->TypePrescription->libelle}}</td>
                                <td class="txt-dark">{{$p->libelle}}</td>

                                <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                                    {{$p->Code->code}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    
                    </table>
                @endif

                

            </div>
        </div>
    </div>
</div>
