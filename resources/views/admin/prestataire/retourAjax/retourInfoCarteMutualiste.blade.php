                        <style>
                            .ImgCarteMutualiste {
                                height: 175px;
                                width: 139px;
                                top: 220px;
                                Left: 71px;
                                position: absolute;
                                z-index: 1;
                                border-radius: 20px;
                                margin-top: -6px;
                            }
                            .assureNameMutualiste {
                                top: 237.5px;
                                Left: 371px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                            .dateNaisseMutualiste {
                                top: 287px;
                                Left: 371px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }

                            .assureSexeMutualiste {
                                top: 335px;
                                Left: 371px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                            .assurance_numberMutualiste {
                                top: 189px;
                                Left: 433px;
                                position: absolute;
                                z-index: 1;
                                color: red;
                                font-size: 16px;
                                font-weight: bold;
                            }
                            .assurePoliceMutualiste {
                                top: 420px;
                                Left: 173px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                        </style>
                        @if(!is_null($assure))
                            <img class="inline-block mb-10 ImgCarteMutualiste" src='{{config("app.imgLink")}}{{$assure->photo}}'/>
                            <div style="margin-top: -1.2em;" >
                                @if($assure->active)
                                    <p style="color: green;">
                                        ASSURE(E) ACTIVE(E)
                                    </p>
                                    @else
                                    
                                    <p align="center"  class="headerP" style="color: red; font-size:46px; font-weight:bold;  z-index:1; position:absolute; width:460px; bottom:330px;left:95px; ">
                                        ASSURE(E) BLOQUE(E)
                                    </p>
                                @endif
                            </div>
                            <div  align="center" style="text-align: center; vertical-align:middle; height:400px;">
                                <p class="assurance_numberMutualiste">
                                    {{$assure->assurance_number}}
                                </p>
                                <p class="assureNameMutualiste">{{$assure->first_name}} {{$assure->name ?? $assure->last_name }}</p>
                                <p class="dateNaisseMutualiste">{{$assure->birth_date}}</p>
                                <p class="assureSexeMutualiste">
                                    @if($assure->sex == "M")
                                        Masculin
                                    @else
                                        Feminin
                                    @endif
                                </p>
                                
                                <p class="assurePoliceMutualiste" >{{$assure->registration_number}}</p>
                                <img class="headerP CartePosition" src="{{URL::asset('erpfiles/images/carte-assure-2.jpeg')}}" style="height:350px;border-radius:20px" alt="">
                            </div>
                            @if($assure->active)
                                @if($assure->bareme->last()->plafondGeneraleBeneficiaire > 0 and $assure->bareme->last()->plafondGeneraleFamillle > 0)
                                <div align="center">
                                    <button type="submit" href="{{route('GotoMakeActe',[$assure->id, $typeASS])}}" class="btn btn-outline-warning DetailView" style="width: 320px; font-size: 25px;">  SUIVANT &ensp; <i style="font-weight:bold;" class="las la-hand-point-right"></i></button>
                                </div>
                                @endif
                            @endif
                        @endif
                        
                        