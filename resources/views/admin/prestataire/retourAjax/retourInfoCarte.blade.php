                        <style>
                            .assureName {
                                top: 230px;
                                Left: 338px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                            .dateNaisse {
                                top: 271px;
                                Left: 338px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                            .assureSexe {
                                top: 313px;
                                Left: 338px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                            .ImgCarte {
                                height: 177px;
                                width: 137px;
                                top: 211px;
                                Left: 71px;
                                position: absolute;
                                z-index: 9000;
                                border-radius: 10px;
                            }
                            .assurance_number {
                                top: 187px;
                                Left: 420px;
                                position: absolute;
                                z-index: 1;
                                color: red;
                                font-size: 16px;
                                font-weight: bold;
                            }
                            .assurePolice {
                                top: 417px;
                                Left: 208px;
                                position: absolute;
                                z-index: 9000;
                                color: black;
                                font-weight: bold;
                            }
                        </style>                        
                        @if(!is_null($assure))
                        
                            <div class="basic-form"><br>
                                @if($typeASS == "Mutualiste")
                                <img class="inline-block mb-10 ImgCarte" src='{{config("app.imgLink")}}{{$assure->photo}}'/>
                                @else
                                <img class="inline-block mb-10 ImgCarte" src='{{config("app.adLink")}}{{$assure->photo}}'/>
                                @endif
                                    <div style="margin-top: -2.5em;" >
                                        @if($assure->active)
                                            <p style="color: green;">
                                               ASSURE(E) ACTIVE(E)
                                            </p>
                                            @else
                                            
                                            <p align="center"  class="headerP" style="color: red; font-size:46px; font-weight:bold;  z-index:9999; position:absolute; width:460px; bottom:330px;left:95px; ">
                                                ASSURE(E) BLOQUE(E)
                                            </p>
                                        @endif
                                    </div>
                                <div  align="center" style="text-align: center; vertical-align:middle; height:400px;">
                                    <p class="assurance_number">
                                        {{$assure->assurance_number}}
                                    </p>
                                    <p class="assureName">{{$assure->first_name}} {{$assure->name ?? $assure->last_name }}</p>
                                    <p class="dateNaisse">{{$assure->date_naissance}}</p>
                                    <p class="assureSexe">
                                        @if($assure->sexe == "M")
                                            Masculin
                                        @else
                                            Feminin
                                        @endif
                                    </p>
                                    
                                    <p class="assurePolice" >{{$assure->Mutualiste->registration_number}}</p>
                                    <img class="headerP CartePosition" src="{{URL::asset('erpfiles/images/carte-ayantdroit-2.jpeg')}}" style="height:350px;border-radius: 25px;" alt="">
                                </div>
                            </div>
                            
                            @else
                            <div class="row" >
                                <div  align="center" style="text-align: center;">
                               
                                    <img src="{{URL::asset('erpfiles/images/g4.gif')}}" style="max-height: 400px; max-width: 250px;" alt="">
                                </div>
                            </div>
                            @endif
                            @if($assure->active)
                                @if($assure->bareme->last()->plafondGeneraleBeneficiaire > 0 and $assure->bareme->last()->plafondGeneraleFamillle > 0)
                                <div align="center">
                                    <button type="submit" href="{{route('GotoMakeActe',[$assure->id, $typeASS])}}" class="btn btn-outline-warning DetailView" style="width: 320px; font-size: 25px;">  SUIVANT &ensp; <i style="font-weight:bold;" class="las la-hand-point-right"></i></button>
                                </div>
                                @endif
                            @endif
                            
                            