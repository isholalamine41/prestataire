@extends('layouts.admin.master')
@section('content')

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            {{-- <div class="col-xl-12">
                                <div class="row">
									
                                    <div class="col-xl-4 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card ">
                                                <div class="depostit-card-media d-flex justify-content-between style-1">
                                                    <div>
                                                        <h6>TOTAL ORDONNANCE</h6>
                                                        <h3>{{number_format($data->count(), 0, ',', ' ')}}</h3>
                                                    </div>
                                                    <div class="icon-box bg-primary">
                                                        <i style="color: #fff;" class="las la-notes-medical"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card ">
                                                <div class="depostit-card-media d-flex justify-content-between style-1">
                                                    <div>
                                                        <h6>ORDONNANCE EN LIGNE</h6>
                                                        <h3>{{number_format($data_en_ligne, 0, ',', ' ')}}</h3>
                                                    </div>
                                                    <div class="icon-box bg-info">
                                                        <i style="color: #fff;" class="las la-file-invoice-dollar"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card p-0">
                                                <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                    <div>
                                                        <h6>ORDONNANCE TRAITÉ</h6>
                                                        <h3>{{number_format($data_traiter, 0, ',', ' ')}}</h3>
                                                    </div>
                                                    <div class="icon-box bg-success">
                                                        <i class="las la-file-invoice-dollar" style="color: #fff;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP" style="padding: 15px;">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date début</label>
                                            <input type="date" id="datedebut" name="datedebut" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date fin</label>
                                            <input type="date" id="datefin" name="datefin" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Type d'assuré</label>
                                            <select name="nature_id" id="type" class="form-control border-success">
                                                <option value="all">Tous les assurés</option>
                                                <option value="App\Models\Mutualiste">Assuré principal</option>
                                                <option value="App\Models\Ayantdroit">Ayants-droit</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <button class="btn btn-success btn-icon right-icon" id="search" style="margin-top: 2em; width:100%"><span>RECHERCHEZ </span> <i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                               
                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0" id="HeaderTitle">Total ordonnance ({{number_format($data->count(), 0, ',', ' ')}})</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table class="table card-table border-no success-tbl p-0" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>DATE : HEURE </th>
                                                        <th>ASSURE</th>
                                                        <th>ACTE</th>
                                                        <th class="text-center">NOMBRE DE MEDICAMENT </th>
                                                        <th>CODE</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody">
                                                    @if($data->count()>0)
                                                        @foreach($data->get() as $ordonnance)
                                                        <tr>
                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="ms-2 cat-name">
                                                                        <p class="mb-0">{{date("d/m/Y", strtotime($ordonnance->date))}} à {{$ordonnance->heure}}</p>
                                                                    </div>	
                                                                </div>
                                                            </td>
                                                            <td>{{$ordonnance->assurer->last_name ?? $ordonnance->assurer->name}} {{$ordonnance->assurer->first_name}}</td>
                                                            <td>{{$ordonnance->presta_muta->libelle}}</td>
                                                            <td class="text-center">
                                                                <div class="badge badge-primary">
                                                                    {{count($ordonnance->medicaments())}}
                                                                </div>
                                                            </td>
                                                            <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                                                                {{$ordonnance->Code->code}}
                                                            </td>
                                                            <td>
                                                                <button href="{{route('DetailsOrdonnance',[$ordonnance->id])}}" class="btn btn-success lanceModal">
                                                                    <i class="las la-eye"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')
@endpush


@push('script.footer1')
<script>

    var table = $('#table').DataTable();

    $('#search').on('click',()=>{

        $('#search').html('<div class="spinner-border" role="status"></div>');

        let datedebut = $('#datedebut').val();
        let datefin = $('#datefin').val();
        let type = $('#type').val();
        
        table.destroy();

        $.ajax({
            method: 'GET',
            data : {
                datedebut : datedebut,
                datefin : datefin,
                type : type,
            },
            url: "{{route('RechecheOrdonnanceMedicaux')}}",
            success : function(response){

                $("#tbody").html(response);
                table = $('#table').DataTable();
                $('#search').html('<span>RECHERCHEZ </span> <i class="fa fa-search"></i>');
                $('#HeaderTitle').html('<h4 id="HeaderTitle" class="heading mb-0 HeaderTitle">RESULTATS : ('+table.rows().count()+') </h4>');

            }
        });

    });

    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
</script>
@endpush

@section('title')
ORDONNANCES
@endsection


@section('dd')
active
@endsection



@push('script.footer2')

@endpush





