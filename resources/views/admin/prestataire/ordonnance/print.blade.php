<style>
    *{
        font-family: 'Courier New', Courier, monospace
    }
</style>
<div style="width: 100%;">
    <div style="border: 1px solid #ccc; padding: 15px; margin-top: 10px;">
        <div style="font-weight: bold; font-size: 1.5em;">Code ordonnace : <strong>{{$ordonnance->Code->code}}</strong> <span style="float: right; font-weight: bold;"><img class="avatar avatar-md" style="width:70px" src="https://www.muscop-ci.com/websitefiles/images/logo.png" alt="Muscop-ci Logo"></span></div>
        <h3>Ordonnance {{$ordonnance->Prestataire->rs}}</h3>
        <br>
        <div class="table-responsive">
            <table class="table table-border" style="width: 100%">
                <thead>
                    <tr style="text-align: left;">
                        <th style="text-align: left;padding: 7px" >Quantité</th>
                        <th style="text-align: left;padding: 7px"  class="right">Prescription</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ordonnance->medicaments() as $medicament)
                        <tr style="text-align: left;">
                            <td style="padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$medicament->quantite}}</td>
                            <td style="padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{explode(' / ',$medicament->medicament)[0]}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <br>
        <h3>Acte {{$prestataire->rs}}</h3>
        <br>
        <div class="table-responsive">
            <table class="table table-border" style="width: 100%">
                <thead>
                    <tr style="text-align: left;">
                        <th style="text-align: left;padding: 7px" >Quantité</th>
                        <th style="text-align: left;padding: 7px"  class="">Médicament</th>
                        <th style="text-align: left;padding: 7px" class="">Prix UT</th>
                        <th style="text-align: left;padding: 7px" class="">Prix TT</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ordonnance->actes()->where('prestataire_id',$prestataire->id)->get() as $presta_muta)
                        @foreach(explode(',',$presta_muta->description) as $line)
                            @php 
                                $data = explode('* ',$line);
                                $data2 = explode(' = ',$data[1]);
                            @endphp
                            <tr style="text-align: left;">
                                <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$data[0]}}</td>
                                <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$data2[0]}}</td>
                                <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$data2[1]/$data[0]}}</td>
                                <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center">{{$data2[1]}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    <tr style="text-align: left;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" > <b>Total : </b>{{$facture->montant}}</td>
                    </tr>
                    <tr style="text-align: left;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" > <b>Part Muscop-ci : </b>{{$facture->part_muscopci}}</td>
                    </tr>
                    <tr style="text-align: left;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" > <b>Part assuré : </b>{{$facture->part_assure}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="margin-top: 10px;">
            <div style="display: flex; justify-content: flex-end; margin-bottom: 15px;">
                <div>
                    <img src="https://chart.googleapis.com/chart?chs=110x110&cht=qr&chl={{$ordonnance->Code->code}}&chld=L|1&choe=UTF-8" alt="" style="width: 110px;" class="img-fluid">
                </div>
            </div>
            <p>
                <i style="color:black">
                    {{date('d/m/Y H:i:s',strtotime($presta_muta->created_at))}}
                </i>
            </p>
        </div>
    </div>
</div>
