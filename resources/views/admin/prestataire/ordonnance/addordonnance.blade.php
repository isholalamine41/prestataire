<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">PRESCRIRE UNE ORDONNANCES</h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body"> 
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <form action="{{route('AddOrdonnance')}}" method="post"><br>
                        @csrf
                        <div class="row">
                            <input type="hidden" value="{{$assure->id}}" name="assure_id">
                            <input type="hidden" value="{{$type}}" name="type">
                        </div>
                        @if(isset($acte_id))
                            <h4>Acte : {{$acte->libelle}}</h4>
                            <input type="hidden" value="{{$acte_id}}" name="acte_id">
                            <div class="col-xl-12 col-xxl-12 col-md-12 col-lg-12">
                                <label for="">TYPE DE MEDICAMENT *</label>
                                <select style="border: 1px solid #452a90;" id="prestation_id" required="" class="form-control">
                                    <option value=""></option>
                                    @foreach($liste_prestations as $tp)
                                        <option value="{{$tp->id}}">{{$tp->Prestation->exclusion==1 ? '(EXCLUS)' : '' }} {{$tp->libelle}}</option>
                                    @endforeach
                                </select>
                            </div><br>
                        @else
                            <div class="row line">
                                <div class="col-xl-6 col-xxl-6 col-md-6 col-lg-6">
                                    <label for="">ACTES *</label>
                                    <select style="border: 1px solid #452a90;" name="acte_id" id="" required="" class="form-control">
                                        @foreach($actes as $tp)
                                        <option value="{{$tp->id}}">{{$tp->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xl-6 col-xxl-6 col-md-6 col-lg-6">
                                    <label for="">TYPE DE MEDICAMENT *</label>
                                    <select style="border: 1px solid #452a90;" id="prestation_id" required="" class="form-control">
                                        <option value=""></option>
                                        @foreach($liste_prestations as $tp)
                                            <option value="{{$tp->id}}">{{$tp->Prestation->exclusion==1 ? '(EXCLUS)' : '' }} {{$tp->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row line hidden">
                            <div class="col-md-3">
                                <input type="number" min="1" placeholder="Quantité" id="quantite" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <input type="text" placeholder="Médicament" id="medicament" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-primary btn-block" id="add-button" type="button">Ajouter <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <br>
                        <span class="title hidden">Liste des médicaments</span><br><br>
                        <table class="table table-striped">
                            <tbody id="tbody" >
                            </tbody>
                        </table>
                        <button type="submit" style="width: 250px;float:right;background:black" class="btn btn-warning title hidden submit"> METTRE EN LIGNE </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .line{
        margin-bottom: 15px !important;
    }
    .td{
        color:black !important;
    }
    .hidden{
        display: none;
    }
    .fa-times{
        cursor: pointer;
    }
</style>
<script>

    $('#prestation_id').on('change',function(){
        if($('.prestation_id').val()==''){
            $('.line').addClass('hidden');
        }else{
            $('.line').removeClass('hidden');
        }
    });

    $('.submit').on('click',function(){
        $('.submit').addClass('hidden');
    });

    $('#add-button').on('click',function(){

        medicament = $('#medicament').val();
        quantite = $('#quantite').val();
        prestation_id = $('#prestation_id').val();

        in_array = false;

        $('.medicament').each(function(index, element) {

            if($(element).val()==medicament || $('.medicament').length==3)
                in_array = true;
        });


        if($('.medicament').length==3){
            alert("Vous ne pouvez pas ajouter plus de 3 médicaments");
            return;
        }

        if(in_array){
            alert("Médicament déjà présent sur l'ordonnance");
            return;
        }
        
        if(quantite!='' && medicament!='' && prestation_id!=''){
            $('#tbody').append(`
                <tr>
                    <td class="td">${quantite} x <input name="quantite[]" type="hidden" value="${quantite}"</td>
                    <td class="td">${medicament} <input type="hidden" class="medicament"  name="medicament[]" value="${medicament}" > <input type="hidden" name="prestation_id[]" value="${prestation_id}" ></td>
                    <td class="td" class="text-center"><i onclick="delete_line(this)" class="fa fa-times text-danger"></i></td>
                </tr>
            `);

            $('#medicament').val('');
            $('#quantite').val('');
       }

       if($('#tbody').html().trim()!=''){
            $('.title').removeClass('hidden');
       }

    });

    function delete_line(self){

        $(self).parent().parent().remove();

        console.log($('#tbody').html());

       if($('# ').html().trim()==''){
            $('.title').addClass('hidden');
       }

    }

</script>	