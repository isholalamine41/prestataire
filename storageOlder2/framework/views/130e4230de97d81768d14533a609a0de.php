
<?php $__env->startSection('content'); ?>

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-4 col-lg-4">
                <div class="card"style="max-height: 700px;">
                    <div class="card-header bg bg-danger">
                        <h4 class="card-title text-white" >RECHERCHE ASSURE(E)</h4>
                    </div>
                    <div class="card-body" style="border:1px solid #ff5e5e; border-radius:5px; margin-top:-3px;" >
                            <div class="row">
                                <img src="<?php echo e(URL::asset('erpfiles/images/log.png')); ?>" style="height:auto;" alt="">
                            </div>
                        <div class="basic-form"><br>
                            <form method="POST" class="SearchAssureForm" action="<?php echo e(route('SearchAssure',$p->id)); ?>"><?php echo csrf_field(); ?>
                                <div class="mb-3">
                                    <input type="hidden" value="<?php echo e($p->id); ?>" class="prandor" >
                                </div>
                                <div class="mb-3">
                                     <label for="" class="txt-dark weight-bold">Numéro de carte</label>
                                    <input type="text" required="" name="numeAssure" class="form-control numeAssure" placeholder="Numero de la carte"  style="border:1px solid #ff5e5e;">
                                </div>
                                <br>
                                <button type="submit" id="SearchAssureForm" class="btn btn-outline-danger" style="width: 100%; font-size: 25px;"><i style="font-weight:bold;" class="las la-search"></i> &ensp; RECHERCHE </button>
                            </form> 

                        </div>
                    </div>
                </div>
            </div>


          <div class="col-md-8">
         
          <div class="card">
                    <div class="card-header bg bg-success">
                        <h4 class="card-title text-white" >INFOS ASSURE(E)</h4>
                    </div>
                    <div class="card-body retour_CarteAssure"  style="border:1px solid #3a9b94; border-radius:5px; margin-top:-3px;">
                    <div id="loader" class="" align="center" ><img style="max-height: 100px; vertical-align:middle;" src="<?php echo e(URL::asset('erpfiles/images/loader.gif')); ?>" alt=""></div>
                            <div id="loader" class="" align="center" ><img style="height: 200px; vertical-align:middle; margin-top: 100px;" src="<?php echo e(URL::asset('erpfiles/images/g1.gif')); ?>" alt=""></div>
                            
                            
                    </div>
                </div>
          </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>
wrapper theme-4-active pimary-color-red
<?php $__env->stopSection(); ?>


<?php $__env->startPush('style.hearder'); ?>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('script.footer1'); ?>
<script>
    $("#loader").hide() 
</script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title'); ?>
PRISE EN CHARGE
<?php $__env->stopSection(); ?>


<?php $__env->startSection('dd'); ?>
active
<?php $__env->stopSection(); ?>



<?php $__env->startPush('script.footer2'); ?>

<?php $__env->stopPush(); ?>






<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/priseEnCharge.blade.php ENDPATH**/ ?>