<?php $__env->startSection('content'); ?>

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="row">
									
                                    <div class="col-xl-4 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card ">
                                                <div class="depostit-card-media d-flex justify-content-between style-1">
                                                    <div>
                                                        <h6>TOTAL HOSPITALISATIONS</h6>
                                                        <h3><?php echo e(number_format($data->count(), 0, ',', ' ')); ?></h3>
                                                    </div>
                                                    <div class="icon-box bg-primary">
                                                        <i style="color: #fff;" class="las la-notes-medical"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card ">
                                                <div class="depostit-card-media d-flex justify-content-between style-1">
                                                    <div>
                                                        <h6>HOSPITALISATIONS EN COURS</h6>
                                                        <h3><?php echo e(number_format($en_cours, 0, ',', ' ')); ?></h3>
                                                    </div>
                                                    <div class="icon-box bg-info">
                                                        <i style="color: #fff;" class="las la-file-invoice-dollar"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card p-0">
                                                <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                    <div>
                                                        <h6>HOSPITALISATIONS TERMINÉES</h6>
                                                        <h3><?php echo e(number_format($traiter, 0, ',', ' ')); ?></h3>
                                                    </div>
                                                    <div class="icon-box bg-success">
                                                        <i class="las la-file-invoice-dollar" style="color: #fff;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP" style="padding: 15px;">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date début</label>
                                            <input type="date" id="datedebut" name="datedebut" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date fin</label>
                                            <input type="date" id="datefin" name="datefin" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Type d'assuré</label>
                                            <select name="nature_id" id="type" class="form-control border-success">
                                                <option value="all">Tous les assurés</option>
                                                <option value="App\Models\Mutualiste">Assuré principal</option>
                                                <option value="App\Models\Ayantdroit">Ayants-droit</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <button class="btn btn-success btn-icon right-icon" id="search" style="margin-top: 2em; width:100%"><span>RECHERCHEZ </span> <i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if(session()->has('messageErreur')): ?>
                                        
                                        <div class="alert alert-danger solid alert-dismissible fade show">
                                            <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                                            <strong>Erreur ! </strong> <?php echo e(Session::get('messageErreur')); ?>

                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"><span><i class="fa-solid fa-xmark"></i></span>
                                            </button>
                                        </div>
                                        
                                        <?php endif; ?>
                                        <?php if(session()->has('message')): ?>
                                        
                                        <div class="alert alert-success solid alert-dismissible fade show">
                                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="me-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                            <strong>Félicitations ! </strong> <?php echo e(Session::get('message')); ?>

                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                                            <span><i class="fa-solid fa-xmark"></i></span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                               
                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0" id="HeaderTitle">Total hospitalisations (<?php echo e(number_format($data->count(), 0, ',', ' ')); ?>)</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table class="table card-table border-no success-tbl p-0" id="table">
                                                <thead>
                                                    <tr>
                                                        <tr>
                                                            <th>DATE : HEURE </th>
                                                            <th>ASSURE</th>
                                                            <th>CODE</th>
                                                            <th>HOSPITALISATION</th>
                                                            <th class="text-center">ACTES</th>
                                                            <th>MONTANT TOTAL</th>
                                                            <th>PART ASSURE</th>
                                                            <th>PART MUSCOPCI</th>
                                                            <th>STATUT</th>
                                                            <th></th>
                                                        </tr>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody">
                                                    <?php if($data->count()>0): ?>
                                                        <?php $__currentLoopData = $data->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hospitalisation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                                <td>
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="ms-2 cat-name">
                                                                            <p class="mb-0"><?php echo e(date("d/m/Y", strtotime($hospitalisation->date))); ?> à <?php echo e($hospitalisation->heure); ?></p>
                                                                        </div>	
                                                                    </div>
                                                                </td>
                                                                <td><?php echo e($hospitalisation->assurer->last_name ?? $hospitalisation->assurer->name); ?> <?php echo e($hospitalisation->assurer->first_name); ?></td>
                                                                <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                                                                    <?php echo e($hospitalisation->Code->code); ?>

                                                                </td>
                                                                <td>Hospitalisation x <?php echo e($hospitalisation->nombre_de_jour); ?> jour<?php echo e($hospitalisation->nombre_de_jour>1 ? 's' : ''); ?> <?php echo $hospitalisation->etat==1 ? '<span class="badge badge-sm badge-success">Terminée</span>' : ''; ?></td>
                                                                <td class="text-center">
                                                                    <a class="badge badge-primary lanceModal" href="<?php echo e(route('DetailsHospitalisation',[$hospitalisation->id])); ?>" class="lanceModal">
                                                                        <?php echo e(count($hospitalisation->lignes_facture())); ?>

                                                                    </a>
                                                                </td>
                                                                <td><?php echo e($hospitalisation->amount()); ?></td>
                                                                <td><?php echo e($hospitalisation->part_assure()); ?></td>
                                                                <td><?php echo e($hospitalisation->part_muscopci()); ?></td>
                                                                <td><?php echo $hospitalisation->statut=='Accordée' || $hospitalisation->statut=='Corrigée' ? '<span class="badge badge-success">'.$hospitalisation->statut.'</span>' : ( $hospitalisation->statut=='Rejetée' ? '<span class="badge badge-danger">'.$hospitalisation->statut.'</span>' : '<span class="badge badge-primary">'.$hospitalisation->statut.'</span>'); ?></td>
                                                                <td>
                                                                    <div class="btn-group" role="group">
                                                                        <button type="button" class="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">ACTIONS</button>
                                                                        <div class="dropdown-menu" style="">
                                                                            <?php if($hospitalisation->statut!='Rejetée'): ?>
                                                                                <a class="dropdown-item text-dark lanceModal" href="<?php echo e(route('AddFactureHospitalisation',[$hospitalisation->hospitalisationtable_id, $hospitalisation->hospitalisationtable_type,$hospitalisation->id])); ?>" >AJOUTER DES ACTES</a>
                                                                                <a class="dropdown-item text-dark lanceModal" href="<?php echo e(route('DetailsHospitalisation',[$hospitalisation->id])); ?>" >LISTE DES ACTES</a>
                                                                                <a class="dropdown-item text-dark" href="<?php echo e(route('printHospitalisation',[$hospitalisation->id])); ?>" >FACTURE D'HOSPITALISATION</a>
                                                                            <?php endif; ?>
                                                                            <?php if($hospitalisation->fichier_default!=''): ?>
                                                                            <a class="dropdown-item text-dark" href="<?php echo e(asset('storage/'.$hospitalisation->fichier_default)); ?>" >AVIS D'HOSPITALISATION</a>
                                                                            <?php endif; ?>
                                                                            <?php if(($hospitalisation->statut=='Accordée' || $hospitalisation->statut=='Corrigée') && $hospitalisation->etat==0): ?>
                                                                                <a class="dropdown-item text-dark" href="<?php echo e(route('endHospitalisation',[$hospitalisation->id])); ?>" >TERMINER L'HOSPITALISATION</a>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>
wrapper theme-4-active pimary-color-red
<?php $__env->stopSection(); ?>


<?php $__env->startPush('style.hearder'); ?>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('script.footer1'); ?>
<script>

    var table = $('#table').DataTable();

    $('#search').on('click',()=>{

        $('#search').html('<div class="spinner-border" role="status"></div>');

        let datedebut = $('#datedebut').val();
        let datefin = $('#datefin').val();
        let type = $('#type').val();
        
        table.destroy();

        $.ajax({
            method: 'GET',
            data : {
                datedebut : datedebut,
                datefin : datefin,
                type : type,
            },
            url: "<?php echo e(route('RechecheHospitalisation')); ?>",
            success : function(response){

                $("#tbody").html(response);
                table = $('#table').DataTable();
                $('#search').html('<span>RECHERCHEZ </span> <i class="fa fa-search"></i>');
                $('#HeaderTitle').html('<h4 id="HeaderTitle" class="heading mb-0 HeaderTitle">RESULTATS : ('+table.rows().count()+') </h4>');

            }
        });

    });

    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
</script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title'); ?>
HOSPITALISATIONS
<?php $__env->stopSection(); ?>


<?php $__env->startSection('dd'); ?>
active
<?php $__env->stopSection(); ?>



<?php $__env->startPush('script.footer2'); ?>

<?php $__env->stopPush(); ?>






<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/hospitalisation/liste-hospitalisation-prestataire.blade.php ENDPATH**/ ?>