<style>
    .form-control{
        border: 1px solid #58bad7
    }
</style>
<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-info">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">DEMANDE D'HOSPITALISTATION</h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body"> 
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <form action="<?php echo e(route('AddHospitalisation')); ?>" method="post" enctype="multipart/form-data"><br>
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <input type="hidden" value="<?php echo e($assure->id); ?>" name="assure_id">
                            <input type="hidden" value="<?php echo e($type); ?>" name="type">
                        </div>
                        <?php if(isset($acte_id)): ?>
                            <h4>Acte : <?php echo e($acte->libelle); ?></h4>
                            <input type="hidden" value="<?php echo e($acte_id); ?>" name="acte_id">
                        <?php else: ?>
                            <div class="row line">
                                <div class="col-xl-12 col-xxl-12 col-md-12 col-lg-12">
                                    <label for="">ACTES *</label>
                                    <select  name="acte_id" id="" required="" class="form-control">
                                        <?php $__currentLoopData = $actes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($tp->id); ?>"><?php echo e($tp->libelle); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                        <?php endif; ?>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <input placeholder="Docteur" name="docteur" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min="1" placeholder="Nombre de jour" name="nombre_de_jour" required class="form-control">
                            </div>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <textarea name="motif_hospitalisation" placeholder="Motif" class="form-control" id="" required rows="5"></textarea>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <label for="">Importer l'avis d'hospitalisation PDF/DOCX</label>
                            <input type="file" name="fichier_default" class="form-control" >
                        </div>
                        <br>
                        <button type="submit" style="width: 250px;float:right;background:black" class="btn btn-warning title submit hidden"> DEMANDE D'HOSPIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

    $('.submit').on('click',function(){
        $('.submit').addClass('hidden');
    });

</script><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/hospitalisation/addhospitalisation.blade.php ENDPATH**/ ?>