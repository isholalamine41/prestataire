<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypePrescription extends Model
{
    use HasFactory;

    protected $fillable = [
        'id','libelle','description','etat'
    ];

    public function Prescription()
    {
        return $this->hasMany(Prescription::class);
    }
}
