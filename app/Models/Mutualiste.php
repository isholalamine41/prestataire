<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use App\Models\PrestaMuta;

class Mutualiste extends Model
{
    use HasFactory;
    Protected $fillable = [
        'registration_number','first_name','chronic_disease','name','username','phone','role',
        'grade','echelon','city','sex','active','email','birth_date',
        'birth_place','spouse_name','spouse_phone','mother_name','father_name','assurance_number',
        'subscription_date','statutory_position','residence','marital_status','photo','user_id'
    ];

    

    public function Ayantdroits()
    {
        return $this->hasMany('App\Models\Ayantdroit');
    }

    public function Ascendant()
    {
        return $this->hasMany('App\Models\Ascendant');
    }
    
    public function presta_mutua(): MorphMany
    {
        return $this->morphMany(PrestaMuta::class, 'prestamutable');
    }

    public function bareme(): MorphMany
    {
        return $this->morphMany(Bareme::class, 'baremetable');
    }

    public function Prescription(): MorphMany
    {
        return $this->morphMany(Prescription::class, 'prescriptiontable');
    }

    public function ordonnance(): MorphMany
    {
        return $this->morphMany(Ordonnance::class, 'prescriptiontable');
    }
}
