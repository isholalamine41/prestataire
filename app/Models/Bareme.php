<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;


class Bareme extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'type_prestation_id',
        'baremetable_id',
        'baremetable_type',
        'prestation_id',
        'libelle',
        'consoBeneficiairePrestation',
        'consoFamillePrestation',
        'plafondBeneficiairePrestation',
        'plafondFamillePrestation',
        'qteUtilise',
        'firstdate',
        'lastedate',
        'plafondGeneraleFamillle',
        'plafondGeneraleBeneficiaire',
        'description',
        'datedebut',
        'datefin',

    ];

    public function baremetable(): MorphTo
    {
        return $this->morphTo();
    }

    public function Prestation()
    {
       return $this->belongsTo(Prestation::class);
    }

    public function TypePrestation()
    {
       return $this->belongsTo(typePrestation::class);
    }
}
