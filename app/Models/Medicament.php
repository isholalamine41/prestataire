<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Medicament extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'codePro',
        'libelle',
        'groupe',
        'dci',
        'baseRemboussement',
        'categorie_id',
        'type',
        'regime',
        'added_by',
        'date',
        'heure',
    ];
   
    // public function presta_mutua(): MorphMany
    // {
    //     return $this->morphMany(ListePrestation::class, 'listeprestable');
    // }
    
    public function Prescription()
    {
    	return $this->hasMany(Prescription::class);
    }
}
