<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','libelle','description','departement_id'
    ];

    public function Departement() {
        return $this->belongsTo(Departement::class);
    }

    public function Commune() {
        return $this->hasMany(Commune::class);
    }
}
