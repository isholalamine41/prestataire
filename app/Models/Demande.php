<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Demande extends Model
{
    protected $fillable = [
        'facture_numerique',
        'date_traitement',
        'type_assure',
        'type_demande',
        'statut',
        'type_acte',
        'id_assure',
        'centre',
        'montant',
        'description',
        'medecin_id',
        'motif'
    ];


    public $facture_normalisee;
    public $facture_detaillee;
    public $rapport_medical;


    public function assurer()
    {
        return $this->belongsTo("$this->type_assure",'id_assure');
    }


    public function factures_rapports()
    {
        $data = json_decode($this->facture_numerique);

        $this->facture_normalisee = $data->facture_detaille;
        $this->facture_detaillee = $data->facture_detaillee;
        $this->rapport_medical = $data->rapport_medical;
    }


    
}
