<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PrestaMuta;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Ayantdroit extends Model
{
    use HasFactory;
    protected $fillable = [
        'mutualiste_id',
        'matriculeParent',
        'last_name',
        'first_name',
        'date_naissance',
        'lieu_naissance',
        'num_pocile',
        'typedroit_id',
        'sexe',
        'situation_matrimoniale',
        'profession',
        'contact',
        'email',
        'chronic_disease',
        'photo',
        'assurance_number',
        'active'
    ];

    public function Mutualiste()
    {
    	return $this->belongsTo(Mutualiste::class,);
    }

    public function Typedroit()
    {
    	return $this->belongsTo(Typedroit::class,);
    }

    public function presta_mutua(): MorphMany
    {
        return $this->morphMany(PrestaMuta::class, 'prestamutable');
    }

    public function ordonnance(): MorphMany
    {
        return $this->morphMany(Ordonnance::class, 'prescriptiontable');
    }

    public function bareme(): MorphMany
    {
        return $this->morphMany(Bareme::class, 'baremetable');
    }
    
    public function Prescription(): MorphMany
    {
        return $this->morphMany(Prescription::class, 'prescriptiontable');
    }
}
