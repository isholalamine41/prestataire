<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParamPrestataire extends Model
{
    use HasFactory;
    protected $fillable = [
        'rs',
        'prestataire_id',
        'taux',
        'lastedate',
    ];

    public function Prestataire()
    {
       return $this->belongsTo(Prestataire::class);
    }
}
