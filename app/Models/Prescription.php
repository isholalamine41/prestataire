<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Prescription extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'prestataire_id',
        'facture_id',
        'medicament_id',
        'presciptiontable_id',
        'presciptiontable_type',
        'description',
        'docteur',
        'tampon',
        'type_prescription_id',
        'presta_muta_id',
        'code_id',
        'added_by',
        'date',
        'heure',
        'qte',
        'etat',
    ];

    public function presciptiontable(): MorphTo
    {
        return $this->morphTo();
    }

    public function Facture()
    {
        return $this->belongsTo(Facture::class);
    }

    public function TypePrescription()
    {
        return $this->belongsTo(TypePrescription::class);
    }

    public function PrestaMuta()
    {
        return $this->belongsTo(PrestaMuta::class);
    }

    public function Prestataire()
    {
        return $this->belongsTo(Prestataire::class);
    }

    public function Code()
    {
        return $this->belongsTo(Code::class);
    }

    public function Medicament()
    {
        return $this->belongsTo(Medicament::class);
    }

}
