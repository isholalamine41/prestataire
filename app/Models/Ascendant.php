<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ascendant extends Model
{
    use HasFactory;
    protected $fillable = [
        'mutualiste_id',
        'last_name',
        'first_name',
        'date_naissance',
        'lieu_naissance',
        'profession',
        'contact',
        'type',
        'statut'
    ];

    public function Mutualiste()
    {
        return $this->belongsTo('App\Models\Mutualiste');
    }
}
