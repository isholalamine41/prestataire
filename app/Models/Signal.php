<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Signal extends Model
{
    use HasFactory;
    protected $fillable = [
        'mutualiste_id',
        'type',
        'type_id',
        'alert'
    ];

}
