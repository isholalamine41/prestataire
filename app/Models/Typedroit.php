<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Typedroit extends Model
{
    use HasFactory;
    protected $fillable = [
        'libelle',
        'descrition',
    ];

    public function Mutualiste()
    {
    	return $this->hasMany(Mutualiste::class,);
    }

    public function Ayantdroit()
    {
    	return $this->hasMany(Ayantdroit::class,);
    }
}
