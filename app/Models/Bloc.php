<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bloc extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','libelle','description','etat'
    ];

    public function typePrestation()
    {
        return $this->hasMany(typePrestation::class);
    }
}
