<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParamPrestation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'CoefiLettre',
        'prestation_id',
        'taux',
        'plafond',
        'qte',
        'duree',
        'entite',
        'description',
        'datedebut',
        'datefin',
        'annee',
        'added_by',
    ];

    public function ListePrestation()
    {
       return $this->hasMany(ListePrestation::class);
    }
    
    public function Prestation()
    {
       return $this->belongsTo(Prestation::class);
    }

    public function typePrestation()
    {
       return $this->belongsTo(typePrestation::class);
    }
}
