<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class PrestaMuta extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'listePrestation_id',
        'medecin_id',
        'facture_id',
        'prestamutable_id',
        'prestamutable_type',
        'description',
        'cout',
        'part_assure',
        'part_muscopci',
        'entente',
        'docteur',
        'tampon',
        'type',
        'added_by',
        'date',
        'heure',
        'qte',
        'etat',
        'prestataire_id',
        'nombre_de_jour',
        'motif_hospitalisation',
        'motif_status',
        'fichier_default',
        'fichier_update'
    ];

    public function prestamutable(): MorphTo
    {
        return $this->morphTo();
    }
    
    public function ListePrestation()
    {
        return $this->belongsTo(ListePrestation::class);
    }

    public function Medecin()
    {
        return $this->belongsTo(Medecin::class);
    }

    public function Facture()
    {
        return $this->belongsTo(Facture::class);
    }

    public function Prestataire()
    {
        return $this->belongsTo(Prestataire::class);
    }

    public function Prescription()
    {
        return $this->hasMany(Prescription::class);
    }

    public function Ordonnance()
    {
        return $this->hasMany(Ordonnance::class);
    }


    public function assurer()
    {
        return $this->belongsTo("$this->prestamutable_type",'prestamutable_id');
    }


    // public function Ayantdroits()
    // {
    //     return $this->belongsTo(Ayantdroit::class);
    // }
}
