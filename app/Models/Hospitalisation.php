<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Hospitalisation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'prestataire_id',
        'facture_id',
        'medicament_id',
        'hospitalisationtable_id',
        'hospitalisationtable_type',
        'description',
        'docteur',
        'tampon',
        'presta_muta_id',
        'code_id',
        'added_by',
        'date',
        'heure',
        'nombre_de_jour',
        'motif_hospitalisation',
        'motif_status',
        'fichier_default',
        'fichier_update',
        'facture',
        'facture_modifier',
        'facture_finale',
        'etat'
    ];

    public function assurer()
    {
        return $this->belongsTo("$this->hospitalisationtable_type",'hospitalisationtable_id');
    }

    public function Code()
    {
        return $this->belongsTo(Code::class);
    }

    public function lignes_facture()
    {
        return json_decode($this->facture) ?? [];
    }

    public function part_assure()
    {
        $datas = json_decode($this->facture) ?? [];
        $amount = 0;

        foreach($datas as $data){

            $amount += $data->part_assure;
        }

        return $amount;
    }

    public function Prestataire()
    {
        return $this->belongsTo(Prestataire::class);
    }

    public function part_muscopci()
    {
        $datas = json_decode($this->facture) ?? [];
        $amount = 0;

        foreach($datas as $data){

            $amount += $data->part_muscopci*$data->quantite;
        }

        return $amount;
    }

    public function amount()
    {
        $datas = json_decode($this->facture) ?? [];
        $amount = 0;

        foreach($datas as $data){

            $amount += $data->prix*$data->quantite;
        }

        return $amount;
    }
}
