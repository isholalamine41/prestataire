<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParamtypePrestation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'lettre',
        'valeurLettre',
        'taux',
        'plafond',
        'description',
        'added_by',
        'annee',
        'datedebut',
        'datefin',
    ];

    public function typePrestation(){
        
        return $this->hasMany(typePrestation::class);
    }

    public function ParamPrestation()
    {
       return $this->hasMany(ParamPrestation::class);
    }

}
