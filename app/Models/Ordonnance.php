<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Ordonnance extends Model
{
    use HasFactory;
    Protected $guarded = [
        'created_at','updated_at'
    ];

    public function Code()
    {
        return $this->belongsTo(Code::class);
    }

    public function assurer()
    {
        return $this->belongsTo("$this->prescriptiontable_type",'prescriptiontable_id');
    }

    public function presta_muta()
    {
        return $this->belongsTo(PrestaMuta::class);
    }

    public function medicaments()
    {
        return json_decode($this->medicaments);
    }

    
    public function actes()
    {
        return $this->hasMany(PrestaMuta::class,'medecin_id');
    }

    public function Prestataire()
    {
        return $this->belongsTo(Prestataire::class);
    }

    public function setState()
    {
        $medicaments = $this->medicaments();
        $conter = 0;
        $conter_medicaments = count($medicaments);
        $cout = 0;

        foreach($medicaments as $medicament){

            if(!is_null($medicament->prix)){
                $conter++; 
                $cout += $medicament->quantite * $medicament->prix;
            }

        }

        $this->prix = $cout;

        if($conter==$conter_medicaments){
            $this->etat=1;
        }
        
        $this->save();
    }
    
}
