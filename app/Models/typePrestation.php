<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class typePrestation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'bloc_id',
        'description',
        'plafond',
        'entente',
        'etat',
        'ententePrealable',
        'added_by',
    ];

    public function PrestataireTypePrestation()
    {
        return $this->hasMany(PrestataireTypePrestation::class);
    }
    public function Prestation()
    {
        return $this->hasMany(Prestation::class);
    }
    public function ListePrestation(){
        
        return $this->hasMany(Prestation::class);
    }
    public function ParamtypePrestation(){
        
        return $this->hasMany(ParamtypePrestation::class);
    }

    public function Bloc(){
        
        return $this->belongsTo(Bloc::class);
    }
}
