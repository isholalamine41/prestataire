<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class typePrestataire extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','libelle','description',
    ];

    public function Prestataire()
    {
        return $this->hasMany(Prestataire::class);
    }
}
