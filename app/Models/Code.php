<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','code','description','etat','prestataire_id','dade','heure'
    ];

    public function Prescription()
    {
    	return $this->hasMany(Prescription::class);
    }

    public function Ordonnance()
    {
    	return $this->hasMany(Ordonnance::class);
    }
}
