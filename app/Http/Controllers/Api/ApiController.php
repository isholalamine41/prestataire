<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Prestataire;
use App\Models\typePrestataire;
use App\Models\Mutualiste;
use App\Models\Ayantdroit;
use App\Models\PrestaMuta;
use App\Models\Ordonnance;
use App\Models\ListePrestation;
use App\Models\Hospitalisation;
use App\Models\typePrestation;
use App\Models\User;
use App\Models\Demande;
use App\Models\OneSignal;
use App\Models\Note;
use App\Models\Facture;
use App\Models\Suggestion;
use App\Models\Signal;

class ApiController extends Controller
{
   
    public function __construct()
    {
        
    } 

    public function ReseauDeSoins(Request $request)
    {
        $type_pharamacie = typePrestataire::where("libelle","PHARMACIE")->first();
        $datas = Prestataire::where('etat',1)->where('type_prestataire_id','!=',$type_pharamacie->id)->orderBy('rs')->get();
        foreach($datas as $data):

            $data->mutualiste_note =  0;

            if(!is_null($request->mutualiste_id)){
                $note = Note::where('mutualiste_id',$request->mutualiste_id)->where('prestataire_id',$data->id)->first();
                if($note){
                    $data->mutualiste_note =  $note->note;
                }
            }

            $data->note = round(Note::where('prestataire_id',$data->id)->avg('note'),2);
            $data->type = $data->typePrestataire->libelle;
        endforeach;
        echo json_encode(["data"=>$datas,"success"=>true,"type_prestatire"=>typePrestataire::all()->pluck('libelle')->toArray()]);
    }

    public function ReseauDePharmacie()
    {
        $type_pharamacie = typePrestataire::where("libelle","PHARMACIE")->first();
        echo json_encode(["data"=>Prestataire::where('etat',1)->where('type_prestataire_id', $type_pharamacie->id)->orderBy('rs')->get(),"success"=>true]);
    }

    public function TypePrestation()
    {
        echo json_encode(["data"=>typePrestataire::all(),"success"=>true]);
    }

    public function GetDemandesActesByAssure(Request $request)
    {
        $mutualiste = Mutualiste::where("registration_number",$request->matricule)->first();
        $assures = [];
        $assures_key = [];
        $assures[] = $mutualiste->first_name." ".$mutualiste->name;
        $assures_key[$mutualiste->id."_App\Models\Mutualiste"] = $mutualiste->first_name." ".$mutualiste->name;

        if($mutualiste){

            $actes = PrestaMuta::where('prestamutable_type','App\Models\Mutualiste')->where('prestamutable_id',$mutualiste->id)->where('etat',1)->where('type','demande')->get();

            foreach($mutualiste->Ayantdroits as $data):
                $actes = $actes->merge(PrestaMuta::where('prestamutable_type','App\Models\Ayantdroit')->where('prestamutable_id',$data->id)->where('etat',1)->where('type','demande')->get());
                $assures[] = $data->first_name." ".$data->last_name;
                $assures_key[$data->id."_App\Models\Ayantdroit"] = $data->first_name." ".$data->last_name;
            endforeach;

            foreach($actes as $data):
                $data->Prestataire = $data->Prestataire;
                $data->typePrestation = $data->ListePrestation->Prestation->typePrestation;
                $data->date = $this->dateFr($data->date.' '.$data->heure);
                $data->is_ordonnance = $data->medecin_id==null ? false : true;

                $data->fullname = $assures_key[$data->prestamutable_id."_".$data->prestamutable_type];

                if($data->is_ordonnance){
                    $data->lines = Ordonnance::find($data->medecin_id)->medicaments();
                    $medicament_data = [];
                    $medicaments = [];

                    foreach(explode(',',$data->description) as $line):
                        $medicaments[] = explode(' = ',explode('* ',$line)[1])[0];
                    endforeach;

                    foreach($data->lines as $line){

                        if(in_array($line->medicament,$medicaments)){

                            $line->medicament = $line->medicament;
                            $line->cout = $this->format_money($line->prix*$line->quantite);
                            $line->part_muscopci = 0;
                            $list_prestation = ListePrestation::find($line->prestation_id);

                            if($list_prestation->Prestation->exclusion==0){
                                $line->part_muscopci = $line->prix*$line->quantite*$data->Prestataire->baseCouverture/100;
                            }

                            $line->part_assure = $this->format_money(($line->prix*$line->quantite)-($line->part_muscopci));
                            $line->part_muscopci = $this->format_money($line->part_muscopci);
                            $medicament_data[] = [
                                'quantite'=>$line->quantite,
                                'medicament'=>$line->medicament,
                                'cout'=>$this->format_money($line->cout),
                                'part_assure'=>$this->format_money($line->part_assure),
                                'part_muscopci'=>$this->format_money($line->part_muscopci),
                            ];
                        }
                    }
                    $data->lines = $medicament_data;
                }else{
                    $data->lines = [[
                        'libelle'=>$data->libelle,
                        'cout'=>$this->format_money($data->cout),
                        'part_assure'=>$this->format_money($data->part_assure),
                        'part_muscopci'=>$this->format_money($data->part_muscopci),
                    ]];
                }

            endforeach;
            echo json_encode(["data"=>$actes,"success"=>true,"assures"=>$assures]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }

    function GetAssures(Request $request){

        $base = "http://gsante-muscopci.com/";
        $datas = [];

        if(is_null($request->search)){

            $mutualistes = Mutualiste::where('active', 1)->get();
            $ayantdroits = Ayantdroit::where('active', 1)->get();

        }else{

            $mutualistes = Mutualiste::where('active', 1)
                ->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('name', 'like', "%$search%")
                        ->orWhere('first_name', 'like', "%$search%")
                        ->orWhere('assurance_number', 'like', "%$search%");
                })
                ->get();

            $ayantdroits = Ayantdroit::where('active', 1)
                ->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('first_name', 'like', "%$search%")
                        ->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('assurance_number', 'like', "%$search%");
                })
                ->get();
        }

        $assures = $mutualistes->merge($ayantdroits);

        $assures = $assures->sortBy(function ($data) {
            $class = class_basename($data);
            return $class == 'Mutualiste' ? $data->first_name . " " . $data->name : $data->first_name . " " . $data->last_name;
        });

        $page = request()->get('page', 1);
        $perPage = 10;
        $total = $assures->count();
        $items = $assures->forPage($page, $perPage);

        $assuresPaginator = new \Illuminate\Pagination\LengthAwarePaginator($items, $total, $perPage, $page, [
            'path' => url()->current(),
            'query' => request()->query(),
        ]);

        foreach ($assuresPaginator as $data) {

            $class = class_basename($data);

            // var_dump($class);

            if($class == 'Mutualiste'){

                $mutualiste = Mutualiste::find($data->id);
                $beneficiaries = $mutualiste->Ayantdroits()->get();

                $presta_mutua_ayantdroit = PrestaMuta::whereIn('prestamutable_id',$beneficiaries->pluck('id'))
                ->where('prestamutable_type', 'App\Models\Ayantdroit')->where('tampon',0)->get();

                $sinistres = $mutualiste->presta_mutua()->where('tampon',0)->get()->merge($presta_mutua_ayantdroit);
            
                $sinistresM = $mutualiste->presta_mutua()->where('tampon',0)->get();

                $data->bareme = $mutualiste->Bareme;
                $conso_famille = Facture::whereIn('id', $sinistres->pluck('facture_id'))->get()->SUM('part_muscopci');
                $conso_assure = Facture::whereIn('id', $sinistresM->pluck('facture_id'))->get()->SUM('part_muscopci');

                // $data->type='ASSURE PRINCIPAL';

            }else{

                $bene = Ayantdroit::find($data->id);
                $parent = $bene->Mutualiste;
                $beneficiaries = $parent->Ayantdroits()->get();

                $presta_mutua_ayantdroit = PrestaMuta::whereIn('prestamutable_id',$beneficiaries->pluck('id'))
                ->where('prestamutable_type', 'App\Models\Ayantdroit')->where('tampon',0)->get();

                $sinistres = $parent->presta_mutua()->where('tampon',0)->get()->merge($presta_mutua_ayantdroit);
                $sinistresB = $bene->presta_mutua()->where('tampon',0)->get();
                

                $data->bareme = $bene->Bareme;
                $conso_famille = Facture::whereIn('id', $sinistres->pluck('facture_id'))->get()->SUM('part_muscopci');
                $conso_assure = Facture::whereIn('id', $sinistresB->pluck('facture_id'))->get()->SUM('part_muscopci');
                
                // $data->type='AYANT DROIT';
            }
            

            $data->fullname = $class == 'Mutualiste' ? $data->first_name . " " . $data->name : $data->first_name . " " . $data->last_name;
            $data->avatar = $base . ($class == 'Ayantdroit' ? 'Ayant_droit/' . $data->photo : 'mutualiste/' . $data->photo);
            // $data->avatar = $data->photo == '' ? 'assets/images/avatar-2.png' : $data->avatar;
            $data->type = $class == 'Ayantdroit' ? 'AYANT DROIT' : 'ASSURE';
            $data->conso_famille = $this->format_money(config("app.balanceF")-$conso_famille);
            $data->conso_assure = $this->format_money(config("app.balanceP")-$conso_assure);


            foreach($data->bareme as $bareme):
                $bareme->libelle_nature = $bareme->TypePrestation->libelle;
                $bareme->libelle_prestation = $bareme->Prestation ? $bareme->Prestation->libelle : $bareme->libelle;
                $bareme->consoBeneficiairePrestation = $this->format_money($bareme->consoBeneficiairePrestation);
                $bareme->consoFamillePrestation = $this->format_money($bareme->consoFamillePrestation);
            endforeach;

            $datas[]=$data;
        }

        echo json_encode(["data"=>$datas,"success"=>true,"next_page_url"=>$assuresPaginator->nextPageUrl(),"current_page"=>$assuresPaginator->currentPage(),"total"=>$assuresPaginator->total()]);


    }

    public function GetDemandesActesByPrestataire(Request $request)
    {
        $prestataire = null;
        $assure = null;

        if(!is_null($request->prestataire_id)){

            $prestataire = Prestataire::find($request->prestataire_id);
            $assures = ['AYANT DROIT','ASSURE'];

            $actes = PrestaMuta::where('prestataire_id', $request->prestataire_id)
            ->where('etat', 1)
            ->where('type','demande')
            ->orderByDesc('created_at')
            ->paginate(7);
            
        }else{

            $assures = [];

            if($request->assure_type=='ASSURE'){
                $assure = Mutualiste::find($request->assure_id);
                $actes = PrestaMuta::where('prestamutable_id', $request->assure_id)
                ->where('etat', 1)
                ->where('prestamutable_type','App\Models\Mutualiste')
                ->where('type','demande')
                ->orderByDesc('created_at')
                ->paginate(7);
            }else{
                $assure = Ayantdroit::find($request->assure_id);
                $actes = PrestaMuta::where('prestamutable_id', $request->assure_id)
                ->where('etat', 1)
                ->where('prestamutable_type','App\Models\Ayantdroit')
                ->where('type','demande')
                ->orderByDesc('created_at')
                ->paginate(7);
            }
            
        }

        $datas = [];
        $base = "http://gsante-muscopci.com/";

        if($prestataire || $assure){
            
            foreach($actes as $data):

                $data->Prestataire = $data->Prestataire;
                $data->typePrestation = $data->ListePrestation->Prestation->typePrestation;
                $data->date = $this->dateFr($data->date.' '.$data->heure);
                $data->is_ordonnance = $data->medecin_id==null ? false : true;

                $data->fullname = $data->prestamutable_type=='App\Models\Mutualiste' ? $data->assurer->first_name." ".$data->assurer->name : $data->assurer->first_name." ".$data->assurer->last_name;
                $data->avatar = $base.($data->prestamutable_type=='App\Models\Ayantdroit' ? 'Ayant_droit/'.$data->assurer->photo : 'mutualiste/'.$data->assurer->photo);
                $data->type = $data->prestamutable_type=='App\Models\Ayantdroit' ? 'AYANT DROIT' : 'ASSURE';

                if($data->is_ordonnance){
                    $data->lines = Ordonnance::find($data->medecin_id)->medicaments();
                    $medicament_data = [];
                    $medicaments = [];

                    foreach(explode(',',$data->description) as $line):
                        $medicaments[] = explode(' = ',explode('* ',$line)[1])[0];
                    endforeach;

                    foreach($data->lines as $line){

                        if(in_array($line->medicament,$medicaments)){

                            $line->medicament = $line->medicament;
                            $line->cout = $this->format_money($line->prix*$line->quantite);
                            $line->part_muscopci = 0;
                            $list_prestation = ListePrestation::find($line->prestation_id);

                            if($list_prestation->Prestation->exclusion==0){
                                $line->part_muscopci = $line->prix*$line->quantite*$data->Prestataire->baseCouverture/100;
                            }

                            $line->part_assure = $this->format_money(($line->prix*$line->quantite)-($line->part_muscopci));
                            $line->part_muscopci = $this->format_money($line->part_muscopci);
                            $medicament_data[] = [
                                'quantite'=>$line->quantite,
                                'medicament'=>$line->medicament,
                                'cout'=>$this->format_money($line->cout),
                                'part_assure'=>$this->format_money($line->part_assure),
                                'part_muscopci'=>$this->format_money($line->part_muscopci),
                            ];
                        }
                    }
                    $data->lines = $medicament_data;
                }else{
                    $data->lines = [[
                        'libelle'=>$data->libelle,
                        'cout'=>$this->format_money($data->cout),
                        'part_assure'=>$this->format_money($data->part_assure),
                        'part_muscopci'=>$this->format_money($data->part_muscopci),
                    ]];
                }

                $datas[]=$data;

            endforeach;
            echo json_encode(["data"=>$datas,"success"=>true,"assures"=>$assures,"next_page_url"=>$actes->nextPageUrl(),"current_page"=>$actes->currentPage(),"total"=>$actes->total()]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }   

    public function GetActesByPrestataire(Request $request)
    {
        $prestataire = null;
        $assure = null;

        if(!is_null($request->prestataire_id)){

            $prestataire = Prestataire::find($request->prestataire_id);
            $assures = ['AYANT DROIT','ASSURE'];

            $actes = PrestaMuta::where('prestataire_id', $request->prestataire_id)
            ->where('etat', 1)
            ->orderByDesc('created_at')
            ->paginate(7);
            
        }else{

            $assures = [];

            if($request->assure_type=='ASSURE'){
                $assure = Mutualiste::find($request->assure_id);
                $actes = PrestaMuta::where('prestamutable_id', $request->assure_id)
                ->where('etat', 1)
                ->where('prestamutable_type','App\Models\Mutualiste')
                ->orderByDesc('created_at')
                ->paginate(7);
            }else{
                $assure = Ayantdroit::find($request->assure_id);
                $actes = PrestaMuta::where('prestamutable_id', $request->assure_id)
                ->where('etat', 1)
                ->where('prestamutable_type','App\Models\Ayantdroit')
                ->orderByDesc('created_at')
                ->paginate(7);
            }
            
        }

        $datas = [];
        $base = "http://gsante-muscopci.com/";

        if($prestataire || $assure){

            foreach($actes as $data):

                $data->Prestataire = $data->Prestataire;
                $data->typePrestation = $data->ListePrestation->Prestation->typePrestation;
                $data->date = $this->dateFr($data->date.' '.$data->heure);
                $data->is_ordonnance = $data->medecin_id==null ? false : true;

                $data->fullname = $data->prestamutable_type=='App\Models\Mutualiste' ? $data->assurer->first_name." ".$data->assurer->name : $data->assurer->first_name." ".$data->assurer->last_name;
                $data->avatar = $base.($data->prestamutable_type=='App\Models\Ayantdroit' ? 'Ayant_droit/'.$data->assurer->photo : 'mutualiste/'.$data->assurer->photo);
                $data->type = $data->prestamutable_type=='App\Models\Ayantdroit' ? 'AYANT DROIT' : 'ASSURE';

                if($data->is_ordonnance && trim($data->description)!=''){
                    $data->lines = Ordonnance::find($data->medecin_id)->medicaments();
                    $medicament_data = [];
                    $medicaments = [];

                    foreach(explode(',',$data->description) as $line):
                        $medicaments[] = explode(' = ',explode('* ',$line)[1])[0];
                    endforeach;

                    foreach($data->lines as $line){

                        if(in_array($line->medicament,$medicaments)){

                            $line->medicament = $line->medicament;
                            $line->cout = $this->format_money($line->prix*$line->quantite);
                            $line->part_muscopci = 0;
                            $list_prestation = ListePrestation::find($line->prestation_id);

                            if($list_prestation->Prestation->exclusion==0){
                                $line->part_muscopci = $line->prix*$line->quantite*$data->Prestataire->baseCouverture/100;
                            }

                            $line->part_assure = $this->format_money(($line->prix*$line->quantite)-($line->part_muscopci));
                            $line->part_muscopci = $this->format_money($line->part_muscopci);
                            $medicament_data[] = [
                                'quantite'=>$line->quantite,
                                'medicament'=>$line->medicament,
                                'cout'=>$this->format_money($line->cout),
                                'part_assure'=>$this->format_money($line->part_assure),
                                'part_muscopci'=>$this->format_money($line->part_muscopci),
                            ];
                        }
                    }
                    $data->lines = $medicament_data;
                }else{
                    $data->lines = [[
                        'libelle'=>$data->libelle,
                        'cout'=>$this->format_money($data->cout),
                        'part_assure'=>$this->format_money($data->part_assure),
                        'part_muscopci'=>$this->format_money($data->part_muscopci),
                    ]];
                }

                $datas[]=$data;

            endforeach;
            echo json_encode(["data"=>$datas,"success"=>true,"assures"=>$assures,"next_page_url"=>$actes->nextPageUrl(),"current_page"=>$actes->currentPage(),"total"=>$actes->total()]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }  

    public function GetActesByAssure(Request $request)
    {
        $mutualiste = Mutualiste::where("registration_number",$request->matricule)->first();
        $assures = [];
        $assures_key = [];
        $datas = [];
        $assures[] = $mutualiste->first_name." ".$mutualiste->name;
        $assures_key[$mutualiste->id."_App\Models\Mutualiste"] = $mutualiste->first_name." ".$mutualiste->name;

        if($mutualiste){

            $actes = PrestaMuta::where('prestamutable_type','App\Models\Mutualiste')->where('prestamutable_id',$mutualiste->id)->where('etat',1)->get();

            foreach($mutualiste->Ayantdroits as $data):
                $actes = $actes->merge(PrestaMuta::where('prestamutable_type','App\Models\Ayantdroit')->where('prestamutable_id',$data->id)->where('etat',1)->get());
                $assures[] = $data->first_name." ".$data->last_name;
                $assures_key[$data->id."_App\Models\Ayantdroit"] = $data->first_name." ".$data->last_name;
            endforeach;

            $actes = $actes->sortByDesc('created_at');
            
            foreach($actes as $data):

                $data->Prestataire = $data->Prestataire;
                $data->typePrestation = $data->ListePrestation->Prestation->typePrestation;
                $data->date = $this->dateFr($data->date.' '.$data->heure);
                $data->is_ordonnance = $data->medecin_id==null ? false : true;

                $data->fullname = $assures_key[$data->prestamutable_id."_".$data->prestamutable_type];

                if($data->is_ordonnance){
                    $data->lines = Ordonnance::find($data->medecin_id)->medicaments();
                    $medicament_data = [];
                    $medicaments = [];

                    foreach(explode(',',$data->description) as $line):
                        $medicaments[] = explode(' = ',explode('* ',$line)[1])[0];
                    endforeach;

                    foreach($data->lines as $line){

                        if(in_array($line->medicament,$medicaments)){

                            $line->medicament = $line->medicament;
                            $line->cout = $this->format_money($line->prix*$line->quantite);
                            $line->part_muscopci = 0;
                            $list_prestation = ListePrestation::find($line->prestation_id);

                            if($list_prestation->Prestation->exclusion==0){
                                $line->part_muscopci = $line->prix*$line->quantite*$data->Prestataire->baseCouverture/100;
                            }

                            $line->part_assure = $this->format_money(($line->prix*$line->quantite)-($line->part_muscopci));
                            $line->part_muscopci = $this->format_money($line->part_muscopci);
                            $medicament_data[] = [
                                'quantite'=>$line->quantite,
                                'medicament'=>$line->medicament,
                                'cout'=>$this->format_money($line->cout),
                                'part_assure'=>$this->format_money($line->part_assure),
                                'part_muscopci'=>$this->format_money($line->part_muscopci),
                            ];
                        }
                    }
                    $data->lines = $medicament_data;
                }else{
                    $data->lines = [[
                        'libelle'=>$data->libelle,
                        'cout'=>$this->format_money($data->cout),
                        'part_assure'=>$this->format_money($data->part_assure),
                        'part_muscopci'=>$this->format_money($data->part_muscopci),
                    ]];
                }

                $datas[]=$data;

            endforeach;
            echo json_encode(["data"=>$datas,"success"=>true,"assures"=>$assures]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }   

    public function NotePrestataire(Request $request){
        $record = Note::firstOrNew(
            ['mutualiste_id' => $request->mutualiste_id, 'prestataire_id' => $request->prestataire_id]
        );
        $record->note = $request->note; 
        $record->save();
    }

    public function SuggestionPrestataire(Request $request){
        $record = new Suggestion;
        $record->mutualiste_id = $request->mutualiste_id; 
        $record->suggestion = $request->suggestion; 
        $record->save();
    }


    public function SignalPrestataire(Request $request){
        
        $record = new Signal;
        $record->mutualiste_id = $request->mutualiste_id; 
        $record->type = $request->type; 
        $record->type_id = $request->type_id; 
        $record->alert = $request->alert; 
        $record->save();

        return json_encode(["message"=>"Nous avons bien reçu votre alerte. Notre équipe prendra contact avec vous dans un délai de moins de 24 heures.","statut"=>"success"]);
    }

    

    public function GetOrdonnanceByAssure(Request $request)
    {
        $mutualiste = Mutualiste::where("registration_number",$request->matricule)->first();
        $assures = [];
        $datas = [];
        $assures_key = [];
        $assures[] = $mutualiste->first_name." ".$mutualiste->name;
        $assures_key[$mutualiste->id."_App\Models\Mutualiste"] = $mutualiste->first_name." ".$mutualiste->name;

        if($mutualiste){
            
            $ordonnances = Ordonnance::where('prescriptiontable_type','App\Models\Mutualiste')->where('prescriptiontable_id',$mutualiste->id)->get();
            foreach($mutualiste->Ayantdroits as $data):
                $ordonnances = $ordonnances->merge(Ordonnance::where('prescriptiontable_type','App\Models\Ayantdroit')->where('prescriptiontable_id',$data->id)->get());
                $assures_key[$data->id."_App\Models\Ayantdroit"] = $data->first_name." ".$data->last_name;
            endforeach;

            $ordonnances = $ordonnances->sortByDesc('created_at');

            foreach($ordonnances as $data):
                $data->medicaments = $data->medicaments();
                $data->Prestataire = $data->Prestataire;
                $data->code = $data->Code->code;
                $data->date = $this->dateFr($data->date.' '.$data->heure);
                $data->fullname = $assures_key[$data->prescriptiontable_id."_".$data->prescriptiontable_type];
                foreach($data->medicaments as $line){
                    $line->medicament = $line->medicament;
                    $line->cout = $this->format_money($line->prix*$line->quantite);
                    $line->part_muscopci = 0;
                    $list_prestation = ListePrestation::find($line->prestation_id);

                    if($list_prestation->Prestation->exclusion==0){
                        $line->part_muscopci = $line->prix*$line->quantite*$data->Prestataire->baseCouverture/100;
                    }
                    $line->part_assure = $this->format_money(($line->prix*$line->quantite)-($line->part_muscopci));
                    $line->part_muscopci = $this->format_money($line->part_muscopci);
                }
                $datas[]=$data;
            endforeach;
            echo json_encode(["data"=>$datas,"success"=>true]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }

    public function GetOrdonnanceByPrestataire(Request $request)
    {
        $prestataire = null;
        $assure = null;

        if(!is_null($request->prestataire_id)){

            $prestataire = Prestataire::find($request->prestataire_id);
            $assures = ['AYANT DROIT','ASSURE'];

            if($prestataire->typePrestataire->libelle=='PHARMACIE'){
                $ordonnances = Ordonnance::where('medicaments','like','%"prestataire_id":'.$request->prestataire_id.'}%')
                ->orderByDesc('created_at')
                ->paginate(7);
            }else{
                $ordonnances = Ordonnance::where('prestataire_id', $request->prestataire_id)
                ->orderByDesc('created_at')
                ->paginate(7);
            }
            
        }else{

            $assures = [];

            if($request->assure_type=='ASSURE'){
                $assure = Mutualiste::find($request->assure_id);
                $ordonnances = Ordonnance::where('prestataire_id', $request->assure_id)
                ->where('prescriptiontable_type','App\Models\Mutualiste')
                ->orderByDesc('created_at')
                ->paginate(7);
            }else{
                $assure = Ayantdroit::find($request->assure_id);
                $ordonnances = Ordonnance::where('prescriptiontable_id', $request->assure_id)
                ->where('prescriptiontable_type','App\Models\Ayantdroit')
                ->orderByDesc('created_at')
                ->paginate(7);
            }
            
        }

        $datas = [];
        $base = "http://gsante-muscopci.com/";

        if($prestataire || $assure){

            foreach($ordonnances as $data):

                $data->medicaments = $data->medicaments();
                $data->Prestataire = $data->Prestataire;
                $data->code = $data->Code->code;
                $data->date = $this->dateFr($data->date.' '.$data->heure);

                $data->fullname = $data->prescriptiontable_type=='App\Models\Mutualiste' ? $data->assurer->first_name." ".$data->assurer->name : $data->assurer->first_name." ".$data->assurer->last_name;
                $data->avatar = $base.($data->prescriptiontable_type=='App\Models\Ayantdroit' ? 'Ayant_droit/'.$data->assurer->photo : 'mutualiste/'.$data->assurer->photo);
                $data->type = $data->prescriptiontable_type=='App\Models\Ayantdroit' ? 'AYANT DROIT' : 'ASSURE';

                foreach($data->medicaments as $line){
                    $line->medicament = $line->medicament;
                    $line->cout = $this->format_money($line->prix*$line->quantite);
                    $line->part_muscopci = 0;
                    $list_prestation = ListePrestation::find($line->prestation_id);

                    if($list_prestation->Prestation->exclusion==0){
                        $line->part_muscopci = $line->prix*$line->quantite*$data->Prestataire->baseCouverture/100;
                    }
                    $line->part_assure = $this->format_money(($line->prix*$line->quantite)-($line->part_muscopci));
                    $line->part_muscopci = $this->format_money($line->part_muscopci);
                }
                $datas[]=$data;
            endforeach;
            echo json_encode(["data"=>$datas,"success"=>true,"assures"=>$assures,"next_page_url"=>$ordonnances->nextPageUrl(),"current_page"=>$ordonnances->currentPage(),"total"=>$ordonnances->total()]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }

    public function GetHospitalisationByAssure(Request $request)
    {
        $mutualiste = Mutualiste::where("registration_number",$request->matricule)->first();
        $assures = [];
        $assures_key = [];
        $assures[] = $mutualiste->first_name." ".$mutualiste->name;
        $assures_key[$mutualiste->id."_App\Models\Mutualiste"] = $mutualiste->first_name." ".$mutualiste->name;

        if($mutualiste){

            $hospitalisations = Hospitalisation::where('hospitalisationtable_type','App\Models\Mutualiste')->where('hospitalisationtable_id',$mutualiste->id)->get();
            foreach($mutualiste->Ayantdroits as $data):
                $hospitalisations = $hospitalisations->merge(Hospitalisation::where('hospitalisationtable_type','App\Models\Ayantdroit')->where('hospitalisationtable_id',$data->id)->get());
                $assures_key[$data->id."_App\Models\Ayantdroit"] = $data->first_name." ".$data->last_name;
                $assures[] = $mutualiste->first_name." ".$mutualiste->name;
            endforeach;

            $hospitalisations = $hospitalisations->sortByDesc('created_at');

            foreach($hospitalisations as $data):
                $data->facture = json_decode($data->facture);
                $data->code = $data->Code->code;
                $data->Prestataire = $data->Prestataire;
                $data->fullname = $assures_key[$data->hospitalisationtable_id."_".$data->hospitalisationtable_type];
                foreach($data->facture as $item):
                    $item->prix = $this->format_money($item->prix);
                    $item->part_assure = $this->format_money($item->part_assure);
                    $item->part_muscopci = $this->format_money($item->part_muscopci);
                endforeach;
                $data->date = $this->dateFr($data->date.' '.$data->heure);
                $datas[]=$data;
            endforeach;
            echo json_encode(["data"=>$datas,"success"=>true,"assures"=>$assures]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }

    public function GetHospitalisationByPrestataire(Request $request)
    {
        $prestataire = null;
        $assure = null;

        if(!is_null($request->prestataire_id)){

            $prestataire = Prestataire::find($request->prestataire_id);
            $assures = ['AYANT DROIT','ASSURE'];
            $hospitalisations = Hospitalisation::where('prestataire_id', $request->prestataire_id)
            ->orderByDesc('created_at')
            ->paginate(7);
            
        }else{

            $assures = [];

            if($request->assure_type=='ASSURE'){
                $assure = Mutualiste::find($request->assure_id);
                $hospitalisations = Hospitalisation::where('hospitalisationtable_id', $request->assure_id)
                ->where('hospitalisationtable_type','App\Models\Mutualiste')
                ->orderByDesc('created_at')
                ->paginate(7);
            }else{
                $assure = Ayantdroit::find($request->assure_id);
                $hospitalisations = Hospitalisation::where('hospitalisationtable_id', $request->assure_id)
                ->where('hospitalisationtable_type','App\Models\Ayantdroit')
                ->orderByDesc('created_at')
                ->paginate(7);
            }
            
        }

        $datas = [];
        $base = "http://gsante-muscopci.com/";

        if($prestataire || $assure){

            foreach($hospitalisations as $data):
                $data->facture = json_decode($data->facture);
                $data->code = $data->Code->code;
                $data->Prestataire = $data->Prestataire;


                $data->fullname = $data->hospitalisationtable_type=='App\Models\Mutualiste' ? $data->assurer->first_name." ".$data->assurer->name : $data->assurer->first_name." ".$data->assurer->last_name;
                $data->avatar = $base.($data->hospitalisationtable_type=='App\Models\Ayantdroit' ? 'Ayant_droit/'.$data->assurer->photo : 'mutualiste/'.$data->assurer->photo);
                $data->type = $data->hospitalisationtable_type=='App\Models\Ayantdroit' ? 'AYANT DROIT' : 'ASSURE';

                foreach($data->facture as $item):
                    $item->prix = $this->format_money($item->prix);
                    $item->part_assure = $this->format_money($item->part_assure);
                    $item->part_muscopci = $this->format_money($item->part_muscopci);
                endforeach;
                $data->date = $this->dateFr($data->date.' '.$data->heure);
                $datas[]=$data;
            endforeach;
            echo json_encode(["data"=>$datas,"success"=>true,"assures"=>$assures,"next_page_url"=>$hospitalisations->nextPageUrl(),"current_page"=>$hospitalisations->currentPage(),"total"=>$hospitalisations->total()]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }

    public function GetFactures(Request $request)
    {
        $prestataire = Prestataire::find($request->prestataire_id);
        $datas = [];

        if($prestataire){

            $factures = Facture::where('prestataire_id', $prestataire->id)
            ->where('payer', 0)
            ->orderByDesc('created_at')
            ->paginate(7);

            $assures = ['AYANT DROIT','ASSURE'];
            $datas = [];
            $base = "http://gsante-muscopci.com/";

            foreach($factures as $data):

                $data->Prestataire = $data->Prestataire;
                $data->date = $this->dateFr($data->date.' '.$data->heure);

                $prestamuta = $data->PrestaMutua->first();

                $data->fullname = $prestamuta->prestamutable_type=='App\Models\Mutualiste' ? $prestamuta->assurer->first_name." ".$prestamuta->assurer->name : $prestamuta->assurer->first_name." ".$prestamuta->assurer->last_name;
                $data->avatar = $base.($prestamuta->prestamutable_type=='App\Models\Ayantdroit' ? 'Ayant_droit/'.$prestamuta->assurer->photo : 'mutualiste/'.$prestamuta->assurer->photo);
                $data->type = $prestamuta->prestamutable_type=='App\Models\Ayantdroit' ? 'AYANT DROIT' : 'ASSURE';

                $datas[]=$data;
                
            endforeach;

            echo json_encode(["data"=>$datas,"success"=>true,"assures"=>$assures,"next_page_url"=>$factures->nextPageUrl(),"current_page"=>$factures->currentPage(),"total"=>$factures->total()]);

        }
    }

    public function GetInit(Request $request)
    {
        $mutualiste = Mutualiste::where("registration_number",$request->matricule)->first();
        
        if($mutualiste){

            $consultation = typePrestation::where('libelle','CONSULTATIONS')->first();
            $pharmacie = typePrestation::where('libelle','PHARMACIE')->first();
            $hospitalisation = typePrestation::where('libelle','HOSPITALISATION')->first();
            $liste_consultation = ListePrestation::whereIn('listeprestable_id',$consultation->Prestation->pluck('id'))->get();
            $liste_pharmacie = ListePrestation::whereIn('listeprestable_id',$pharmacie->Prestation->pluck('id'))->get();
            $liste_hospitalisation = ListePrestation::whereIn('listeprestable_id',$hospitalisation->Prestation->pluck('id'))->get();
            
            $consultation_stats = PrestaMuta::where('prestamutable_type','App\Models\Mutualiste')
                ->whereIn('liste_prestation_id',$liste_consultation->pluck('id'))
                ->where('prestamutable_id',$mutualiste->id)
                ->where('etat',1)->get()->sum('cout');

            $pharmacie_stats = PrestaMuta::where('prestamutable_type','App\Models\Mutualiste')
                ->whereIn('liste_prestation_id',$liste_pharmacie->pluck('id'))
                ->where('prestamutable_id',$mutualiste->id)
                ->where('etat',1)->get()->sum('cout');

            $hospitalisation_stats = PrestaMuta::where('prestamutable_type','App\Models\Mutualiste')
                ->whereIn('liste_prestation_id',$liste_hospitalisation->pluck('id'))
                ->where('prestamutable_id',$mutualiste->id)
                ->where('etat',1)->get()->sum('cout');

            foreach($mutualiste->Ayantdroits as $data):
                $consultation_stats += PrestaMuta::where('prestamutable_type','App\Models\Ayantdroit')
                    ->whereIn('liste_prestation_id',$liste_consultation->pluck('id'))
                    ->where('prestamutable_id',$data->id)
                    ->where('etat',1)->get()->sum('cout');

                $pharmacie_stats += PrestaMuta::where('prestamutable_type','App\Models\Ayantdroit')
                    ->whereIn('liste_prestation_id',$liste_pharmacie->pluck('id'))
                    ->where('prestamutable_id',$data->id)
                    ->where('etat',1)->get()->sum('cout');

                $hospitalisation_stats += PrestaMuta::where('prestamutable_type','App\Models\Ayantdroit')
                    ->whereIn('liste_prestation_id',$liste_hospitalisation->pluck('id'))
                    ->where('prestamutable_id',$data->id)
                    ->where('etat',1)->get()->sum('cout');
            endforeach;

            echo json_encode([
                "assurance_number" => $mutualiste->assurance_number,
                "hospitalisation_stats" => $hospitalisation_stats,
                "pharmacie_stats" => $pharmacie_stats,
                "consultation_stats" => $consultation_stats,
                "success"=>true
            ]);

        }else{
            echo json_encode(["success"=>false]);
        }
    }


    public function GetDemandeByAssure(Request $request)
    {
        $mutualiste = Mutualiste::where("registration_number",$request->matricule)->first();
        $datas=[];
        if($mutualiste){
            $demandes = Demande::where('type_assure','App\Models\Mutualiste')->where('id_assure',$mutualiste->id)->get();
            if($request->type_demande=="Renouvellement"){
                $demandes = $demandes->where("type_demande","Renouvellement");
            }else{
                $demandes = $demandes->where("type_acte",$request->type_demande);
            }
            foreach($demandes as $data):
                $data->assurer = $data->assurer;
                $data->fullname = $data->assurer->name." ".$data->assurer->first_name;
                $data->facture_numerique = json_decode($data->facture_numerique);
                $data->montant = $this->format_money(round($data->montant));
                $data->date_traitement = $this->dateFrNotime($data->date_traitement);
                $data->time = $this->dateFr($data->created_at);
                $datas[]=$data;
            endforeach;

            foreach($mutualiste->Ayantdroits as $data):

                if($request->type_demande=="Renouvellement"){
                    $demandes = Demande::where('type_assure','App\Models\Ayantdroit')->where('id_assure',$data->id)->where("type_demande","Renouvellement");
                }else{
                    $demandes = Demande::where('type_assure','App\Models\Ayantdroit')->where('id_assure',$data->id)->where("type_acte",$request->type_demande);
                }

                foreach($demandes->get() as $data):
                    $data->assurer = $data->assurer;
                    $data->fullname = $data->assurer->last_name." ".$data->assurer->first_name;
                    $data->facture_numerique = json_decode($data->facture_numerique);
                    $data->montant = $this->format_money(round($data->montant));
                    $data->date_traitement = $this->dateFrNotime($data->date_traitement);
                    $data->time = $this->dateFr($data->created_at);
                    $datas[]=$data;
                endforeach;
            endforeach;
            echo json_encode(["data"=>$datas,"success"=>true]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }
    

    public function AddRenouvellement(Request $request){


        $demande = new Demande;

        $demande->description = $request->description;
        $demande->type_demande = $request->type_demande;
        $demande->type_acte = $request->type_acte;
        $demande->type_assure = $request->type_assure;
        $demande->id_assure = $request->id_assure;
        $demande->statut = 'En attente';

        $demande->save();

        return json_encode(["message"=>"Demande de renouvellement envoyée avec succès une notification vous sera envoyez pour la validation","statut"=>"success"]);
    }


    public function AddDemandeAmbulatoire(Request $request){

        $demande = new Demande;

        $demande->centre = $request->centre;
        $demande->montant = $request->montant;
        $demande->date_traitement = date('Y-m-d',strtotime($request->date_traitement));
        $demande->type_demande = $request->type_demande;
        $demande->type_acte = $request->type_acte;
        $demande->type_assure = $request->type_assure;
        $demande->id_assure = $request->id_assure;
        $demande->description = $request->description;
        $demande->statut = 'En attente';
        $files = [];
        
        if($request->file('files')){
            foreach ($request->file('files') as $file) {
                $file_name = md5($file->getClientOriginalExtension().uniqid().time()).".".$file->getClientOriginalExtension();
                $file->move('factures-rapports',$file_name);
                $files[]=$file_name;
            }
        }

        $demande->facture_numerique = json_encode(["facture_normalisee"=>$files[0]]);

        $demande->save();

        return json_encode(["message"=>"Demande envoyée avec succès une notification vous sera envoyez pour la validation","statut"=>"success"]);
    }


    public function AddDemandeHospitalisation(Request $request){

        $demande = new Demande;

        $demande->centre = $request->centre;
        $demande->montant = $request->montant;
        $demande->date_traitement = date('Y-m-d',strtotime($request->date_traitement));
        $demande->type_demande = $request->type_demande;
        $demande->type_acte = $request->type_acte;
        $demande->type_assure = $request->type_assure;
        $demande->id_assure = $request->id_assure;
        $demande->description = $request->description;
        $demande->statut = 'En attente';
        $files = [];
        
        if($request->file('files')){
            foreach ($request->file('files') as $file) {
                $file_name = md5($file->getClientOriginalExtension().uniqid().time()).".".$file->getClientOriginalExtension();
                $file->move('factures-rapports',$file_name);
                $files[]=$file_name;
            }
        }

        $demande->facture_numerique = json_encode(["facture_normalisee"=>$files[0],"facture_detaille"=>$files[1],"rapport_medical"=>$files[2]]);
        $demande->save();

        return json_encode(["message"=>"Demande envoyée avec succès une notification vous sera envoyez pour la validation","statut"=>"success"]);
    }
    

    public function GetAssure(Request $request)
    {
        $mutualiste = Mutualiste::where("registration_number",$request->matricule)->first();
        $assures = [];

        if($mutualiste){
            $assures[] = ["id"=>$mutualiste->id,"fullname"=>$mutualiste->first_name." ".$mutualiste->name,"type"=>"App\Models\Mutualiste"];
            foreach($mutualiste->Ayantdroits as $data):
                $assures[] = ["id"=>$data->id,"fullname"=>$data->first_name." ".$data->last_name,"type"=>"App\Models\Ayantdroit"];
            endforeach;
            echo json_encode(["data"=>$assures,"success"=>true]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }


    public function GetAssureData(Request $request)
    {
        $mutualiste = Mutualiste::where("registration_number",$request->matricule)->first();
        $assures = [];

        if($mutualiste){
            foreach($mutualiste->Ayantdroits as $data):
                $assures[] = $data;
            endforeach;
            echo json_encode(["data"=>$assures,"success"=>true]);
        }else{
            echo json_encode(["data"=>[],"success"=>false]);
        }
    }

    function dateFr($date){

        setlocale(LC_TIME, "fr_FR");

        return utf8_encode(strftime(" %d %B %Y ",strtotime($date))).date('à H:i:s',strtotime(($date)));
    }

    function dateFrNotime($date){

        setlocale(LC_TIME, "fr_FR");
        return utf8_encode(strftime(" %d %B %Y ",strtotime($date)));
    }


    function format_money($money){
        return substr(strrev(chunk_split(strrev($money), 3, ' ')), 1);
    }

    function loginDoctor(Request $request){

        $user = User::where("Typeuser","Medecin")
            ->where("email",$request->email)
            ->where("mdp",$request->password)
            ->first();

        if($user){
            return json_encode(["success"=>true,"user"=>$user]);
        }else{
            return json_encode(["success"=>false]);
        }

    }

    function GetDemandes(){

        $datas = [];

        $hospitalisation = Hospitalisation::where('statut','En attente')->get();
        $acte = PrestaMuta::where('type','demande')->where('entente','En attente')->get();
        $renouvellement = Demande::where('statut','En attente')->where('type_demande','Renouvellement')->get();

        $base = "http://gsante-muscopci.com/";

        foreach ($hospitalisation as $data) {
            $datas[]=[
                "avatar"=> $base.($data->hospitalisationtable_type=='App\Models\Ayantdroit' ? 'Ayant_droit/'.$data->assurer->photo : 'mutualiste/'.$data->assurer->photo),
                "assurance_number"=>$data->assurer->assurance_number,
                "fullname"=> $data->assurer->first_name." ".($data->assurer->name ?? $data->assurer->last_name),
                "time"=>$this->dateFr($data->created_at),
                "centre"=>$data->Prestataire->rs,
                "type"=>'Hospitalisation',
                "data"=>$data
            ];
        }

        foreach ($renouvellement as $data) {
            $datas[]=[
                "avatar"=> $base.($data->type_assure=='App\Models\Ayantdroit' ? 'Ayant_droit/'.$data->assurer->photo : 'mutualiste/'.$data->assurer->photo),
                "assurance_number"=>$data->assurer->assurance_number,
                "fullname"=> $data->assurer->first_name." ".($data->assurer->name ?? $data->assurer->last_name),
                "time"=>$this->dateFr($data->created_at),
                "centre"=>'Médecin-conseil',
                "type"=>'Renouvellement',
                "data"=>$data
            ];
        }

        foreach ($acte as $data) {
            $datas[]=[
                "avatar"=> $base.($data->prestamutable_type=='App\Models\Ayantdroit' ? 'Ayant_droit/'.$data->assurer->photo : 'mutualiste/'.$data->assurer->photo),
                "assurance_number"=>$data->assurer->assurance_number,
                "fullname"=> $data->assurer->first_name." ".($data->assurer->name ?? $data->assurer->last_name),
                "time"=>$this->dateFr($data->created_at),
                "centre"=>$data->Prestataire->rs,
                "type"=>'Acte',
                "data"=>$data
            ];
        }

        return json_encode([
            "hospitalisation" => $hospitalisation->count(),
            "acte" => $acte->count(),
            "renouvellement" => $renouvellement->count(),
            "datas"=>$datas
        ]);
    }

    public function RejeterHospitalisation(Request $request){
        
        if(trim($request->motif)==''){
            return json_encode(['message'=>"Veuillez rensigner le motif s'il vous plait","success"=>false]);
        }

        $hospitalisation = Hospitalisation::findOrfail($request->id);

        if ($hospitalisation && $hospitalisation->statut =="En attente")  {

            $hospitalisation->statut='Rejetée';
            $hospitalisation->motif_status=$request->motif;
            $hospitalisation->medecin_id=$request->user_id;
            $hospitalisation->save();
        }

        $onesignal = new OneSignal;
        $onesignal->type = 'hospitalisation';

        if($hospitalisation->assurer->registration_number)
            $onesignal->message = "Votre demande d'hospitalisation a été rejétée";
        else
            $onesignal->message = "La demande d'hospitalisation pour ".$hospitalisation->assurer->first_name." ".$hospitalisation->assurer->last_name."  a été rejétée";

        $onesignal->matricule = $hospitalisation->assurer->registration_number ?? $hospitalisation->assurer->matriculeParent;
        $onesignal->execute();

        return json_encode(['message'=>"Demande d'hospitalisation rejétée","success"=>true]);
    }

    public function RejeterRenouvellement(Request $request){

        if(trim($request->motif)==''){
            return json_encode(['message'=>"Veuillez rensigner le motif s'il vous plait","success"=>false]);
        }

        $demande = Demande::find($request->id);

        if ($demande && $demande->statut=="En attente")  {

            $demande->statut='Rejetée';
            $demande->motif=$request->motif;
            $demande->medecin_id=$request->user_id;
            $demande->save();
        }

        $onesignal = new OneSignal;
        $onesignal->type = 'demande';

        if($demande->assurer->registration_number)
            $onesignal->message = "Votre demande de ".strtolower($demande->type_demande)." a été rejétée";
        else
            $onesignal->message = "La demande de ".strtolower($demande->type_demande)." pour ".$demande->assurer->first_name." ".$demande->assurer->last_name."  a été rejétée";

        $onesignal->matricule = $demande->assurer->registration_number ?? $demande->assurer->matriculeParent;
        $onesignal->execute();

        return json_encode(['message'=>"Demande de renouvellement rejétée","success"=>true]);
    }

    public function RejeterActe(Request $request){
        
        if(trim($request->motif)==''){
            return json_encode(['message'=>"Veuillez rensigner le motif s'il vous plait","success"=>false]);
        }

        $prestamuta = PrestaMuta::findOrfail($request->id);

        if ($prestamuta && $prestamuta->entente =="En attente")  {

            $prestamuta->entente='Rejetée';
            $prestamuta->motif_status=$request->motif;
            $prestamuta->medecin_id=$request->user_id;
            $prestamuta->save();
        }

        $onesignal = new OneSignal;
        $onesignal->type = 'acte';

        if($prestamuta->assurer->registration_number)
            $onesignal->message = "Votre demande d'acte ambulatoire a été rejétée";
        else
            $onesignal->message = "La demande d'acte ambulatoire pour ".$prestamuta->assurer->first_name." ".$prestamuta->assurer->last_name."  a été rejétée";

        $onesignal->matricule = $prestamuta->assurer->registration_number ?? $prestamuta->assurer->matriculeParent;
        $onesignal->execute();

        return json_encode(['message'=>"Demande d'acte ambulatoire rejétée","success"=>true]);
    }

    public function ValiderHospitalisation(Request $request){
        
        $hospitalisation = Hospitalisation::findOrfail($request->id);

        if ($hospitalisation && $hospitalisation->statut =="En attente")  {

            $hospitalisation->statut= $hospitalisation->nombre_de_jour==$request->nombre_de_jour ? 'Accordée' : 'Corrigée';
            $hospitalisation->nombre_de_jour= $request->nombre_de_jour;
            $hospitalisation->motif_status= $request->motif_status ?? '';
            $hospitalisation->medecin_id=$request->user_id;
            $hospitalisation->save();
        }

        $onesignal = new OneSignal;
        $onesignal->type = 'hospitalisation';

        if($hospitalisation->assurer->registration_number)
            $onesignal->message = "Votre demande d'hospitalisation a été accordée";
        else
            $onesignal->message = "La demande d'hospitalisation pour ".$hospitalisation->assurer->first_name." ".$hospitalisation->assurer->last_name."  a été accordée";

        $onesignal->matricule = $hospitalisation->assurer->registration_number ?? $hospitalisation->assurer->matriculeParent;
        $onesignal->execute();

        return json_encode(['message'=>"Demande d'hospitalisation accordée","success"=>true]);
    }

    public function ValiderRenouvellement(Request $request){

        $demande = Demande::find($request->id);

        if ($demande && $demande->statut=="En attente")  {

            $demande->statut= 'Accordée';
            $demande->medecin_id=$request->user_id;
            $demande->save();
        }

        $onesignal = new OneSignal;
        $onesignal->type = 'demande';

        if($demande->assurer->registration_number)
            $onesignal->message = "Votre demande de ".strtolower($demande->type_demande)." a été accordée";
        else
            $onesignal->message = "La demande de ".strtolower($demande->type_demande)." pour ".$demande->assurer->first_name." ".$demande->assurer->last_name."  a été accordée";

        $onesignal->matricule = $demande->assurer->registration_number ?? $demande->assurer->matriculeParent;
        $onesignal->execute();

        return json_encode(['message'=>"Demande de renouvellement accordée","success"=>true]);
    }


    public function ValiderActe(Request $request){
        
        $prestamuta = PrestaMuta::findOrfail($request->id);

        if ($prestamuta && $prestamuta->entente =="En attente")  {

            $prestamuta->entente='Accordée';
            $prestamuta->medecin_id=$request->user_id;
            $prestamuta->save();
        }

        $onesignal = new OneSignal;
        $onesignal->type = 'acte';

        if($prestamuta->assurer->registration_number)
            $onesignal->message = "Votre demande d'acte ambulatoire a été accordée";
        else
            $onesignal->message = "La demande d'acte ambulatoire pour ".$prestamuta->assurer->first_name." ".$prestamuta->assurer->last_name."  a été accordée";

        $onesignal->matricule = $prestamuta->assurer->registration_number ?? $prestamuta->assurer->matriculeParent;
        $onesignal->execute();

        return json_encode(['message'=>"Demande d'acte ambulatoire accordée","success"=>true]);
    }
}
