<?php

namespace App\Http\Controllers\Prestataire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Prestataire;
use App\Models\Mutualiste;
use App\Models\Ayantdroit;
use App\Models\ListePrestation;
use App\Models\Prestation;
use App\Models\typePrestataire;
use App\Models\PrestaMuta;
use App\Models\Facture;
use App\Models\Ordonnance;
use App\Models\Prescription;
use App\Models\Bareme;
use App\Models\Hospitalisation;
use App\Models\Code;
use App\Models\OneSignal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Ramsey\Uuid\Type\Integer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use PDF;

class PrestataireController extends Controller
{
    protected $user;
    protected $p;
    public function __construct()
    {
       
        $this->middleware('auth');
        $this->middleware('prestataire');
        
    } 

    public function PriseEnCharge()
    {
       
        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        
        return view('admin.prestataire.priseEnCharge',compact('p'));
    }

    public function GetPrescription(Request $request)
    {
        $code = Code::where('code',$request->code)->first();
        if($code){
            $prescription = $code->Prescription->first();
            if($prescription && $prescription->etat==0 && $prescription->tampon==1){

                return json_encode(['success'=>true,"id"=>$prescription->id,"libelle"=>$prescription->libelle]);
            }else{
                return json_encode(['success'=>false,"medicaments"=>null]);
            }
        }else{
            return json_encode(['success'=>false,"medicaments"=>null]);
        }
    }
    
    public function GetOrdornnance(Request $request)
    {
        $code = Code::where('code',$request->code)->first();
        if($code){
            $ordonnance = $code->Ordonnance->first();
            if($ordonnance && $ordonnance->etat==0){
                $data = [];
                foreach($ordonnance->medicaments() as $medicament){
                    $prestation = ListePrestation::find($medicament->prestation_id);
                    $medicament->support = $prestation->Prestation->exclusion==1 ? false : true;
                    if(is_null($medicament->prix))
                        $data[]=$medicament;
                }
                return json_encode(['success'=>true,"medicaments"=>$data,"ordonnance_id"=>$ordonnance->id]);
            }else{
                return json_encode(['success'=>false,"medicaments"=>null]);
            }
        }else{
            return json_encode(['success'=>false,"medicaments"=>null]);
        }
    }


    public function SearchAssureGet($id, $num){
        $id = $this->verifId($id);
       

        $numeAssure = htmlspecialchars($num);
        $verifMU = Mutualiste::where('assurance_number',$numeAssure)->get();
        $verifAD = Ayantdroit::where('assurance_number',$numeAssure)->get();

        $typeASS = "Mutualiste";

        if ($verifMU->count()>0) {
            $assure = $verifMU->first();
            $typeASS = "Mutualiste";
        }
            
        if ($verifAD->count()>0) {
            $assure = $verifAD->first();
            $typeASS = "Ayant-droit";
        }

        //dd($verifAD,$verifMU);
        if ($verifAD->count() == 0 and $verifMU->count() == 0) {
            return response()->json([
                'message' => "Le numéro de carte saisi innexistant",
                'title' => 'NUMERO DE CARTE',
                'statut' => 'error',
            ]); 
        }

        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        if($typeASS == "Mutualiste"){
            $retour = view('admin.prestataire.retourAjax.retourInfoCarteMutualiste',compact('p','assure','typeASS'))->render();
        }else{
            $retour = view('admin.prestataire.retourAjax.retourInfoCarte',compact('p','assure','typeASS'))->render();
        }
        if ($typeASS == "Mutualiste") {
            return response()->json([
                'message' => "$assure->first_name.' '. $assure->name",
                'title' => 'ASSURE',
                'statut' => 'success',
                'code' => $retour
            ]); 
           
        }else{
            return response()->json([
                'message' => "$assure->first_name.' '.$assure->last_name",
                'title' => 'ASSURE',
                'statut' => 'success',
                'code' => $retour
            ]); 
        }
        
        return redirect()->back();
        
    }

    public function SearchAssure(Request $request, $id){
        $id = $this->verifId($id);
        $request->validate([
            '_token' => 'required|string',
            'numeAssure' => 'required|string',
        ]);

        $numeAssure = htmlspecialchars($request->numeAssure);

        $verifMU = Mutualiste::where('assurance_number',$numeAssure)->get();
        $verifAD = Ayantdroit::where('assurance_number',$numeAssure)->get();

        $typeASS = "Mutualiste";

        if ($verifMU->count()>0) {
            $assure = $verifMU->first();
            $typeASS = "Mutualiste";
        }
            
        if ($verifAD->count()>0) {
            $assure = $verifAD->first();
            $typeASS = "Ayant-droit";
        }
        
        //dd($verifAD,$verifMU);
        if ($verifAD->count() == 0 and $verifMU->count() == 0) {
            return response()->json([
                'message' => "Le numéro de carte saisi innexistant",
                'title' => 'NUMERO DE CARTE',
                'statut' => 'error',
            ]); 
        }


        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();

        if($typeASS == "Mutualiste"){
            $retour = view('admin.prestataire.retourAjax.retourInfoCarteMutualiste',compact('p','assure','typeASS'))->render();
        }else{
            $retour = view('admin.prestataire.retourAjax.retourInfoCarte',compact('p','assure','typeASS'))->render();
        }

        if ($typeASS == "Mutualiste") {
            return response()->json([
                'message' => "$assure->first_name.' '. $assure->name",
                'title' => 'ASSURE',
                'statut' => 'success',
                'code' => $retour
            ]); 
           
        }else{
            return response()->json([
                'message' => "$assure->first_name.' '.$assure->last_name",
                'title' => 'ASSURE',
                'statut' => 'success',
                'code' => $retour
            ]); 
        }
        
        return redirect()->back();
        
    }

    public function GotoMakeActe($assure_id,$type){
        $id = $this->verifId($assure_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $prestataire = Prestataire::where('user_id', Auth::User()->principal_id)->first();
        $id_liste_prestation = [];
        $id_liste_prestation  = $prestataire->ListePrestation->where('hidden', 0)->pluck('listeprestable_id');
        $prestations = Prestation::whereIn('id', $id_liste_prestation)->where('etat', true)->orderBy('libelle','ASC')->get();

        $m = $assure;
        //dd($assure->bareme);
        $actes = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $prestataire->id],
            ['tampon', 1],
            ['etat', 1],
        ])->orderBy('id','desc')->get();

        $originalDate = Carbon::parse(date('Y-m-d'));
        $newDate = $originalDate->subDays(14);
        //dd($newDate);
        $actesM = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $prestataire->id],
            ['tampon', 0],
            ['etat', 1],
        ])
        ->where('date', '>=',$newDate)
        ->orderBy('id','desc')->Limit(3)->get();
        
        $pc = Prescription::where([
            ['presciptiontable_type', $prestamutable_type],
            ['presciptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire->id ],
            ['medicament_id',  null ],
            ['etat', 0 ],
            ['tampon', 1],
        ])->where('date', '>=',$newDate)->get();


        $po = Ordonnance::where([
            ['prescriptiontable_type', $prestamutable_type],
            ['prescriptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire->id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();


        $ph = Hospitalisation::where([
            ['hospitalisationtable_type', $prestamutable_type],
            ['hospitalisationtable_id',  $assure->id ],
            ['prestataire_id',  $prestataire->id ],
            ['etat', 0 ],
            ['statut','!=', 'Rejetée' ],
        ])->where('date', '>=',$newDate)->get();
        $p = $prestataire;

        $acteConsultation =  PrestaMuta::join('liste_prestations', 'presta_mutas.liste_prestation_id', '=', 'liste_prestations.id')
            ->join('prestations', 'prestations.id', '=', 'liste_prestations.listeprestable_id')
            ->join('type_prestations', 'type_prestations.id', '=', 'prestations.type_prestation_id')
            ->where('type_prestations.libelle', 'CONSULTATIONS')
            ->where('presta_mutas.prestataire_id', $prestataire->id)
            ->where('prestamutable_type', $prestamutable_type)
            ->where('prestamutable_id', $assure->id )
            ->where('presta_mutas.date', '>=',$newDate)
            ->where('presta_mutas.etat', 1)
            ->where('presta_mutas.tampon', 0)
            ->select('prestations.*', 'liste_prestations.*', 'presta_mutas.*')
        ->get();


        $user =  Auth::user();
        $prestataire = Prestataire::where('user_id',$user->principal_id)->first();

        if ($assure) {

            if($prestataire->typePrestataire->libelle=='PHARMACIE'){
                return view('admin.prestataire.actes.makeactepharmacie',compact('assure','prestataire','m','type','actesM'));
            }elseif($prestataire->typePrestataire->libelle=='LABORATOIRE'){
                return view('admin.prestataire.actes.makeexamen',compact('assure','prestataire','m','type','actesM','prestations'));
            }else{
                return view('admin.prestataire.actes.makeacte',compact('assure','prestations','prestataire','actes','m','type','actesM','pc','po','ph','p','acteConsultation'));
            }

        }else{
            session()->flash('messageErreur',"Nous contatons une activité suspecte, Veuillez contactez le service informatique svp ");
            Auth::logout();
            return redirect('login');
        }
    }


    public function AddInBill($id){
        $id = $this->verifId($id);
        $lp = ListePrestation::find($id);
        $retour =  view('admin.prestataire.actes.resteform',compact('lp'))->render();
        return response()->json([
            'statut' => 'success',
            'code' => $retour
        ]); 
    }

    public function verifId($id){
        $type = gettype($id);
        $id = htmlspecialchars($id);
        if ($id > 0 ) {
            return $id;
        }else{
            session()->flash('messageErreur',"Nous contatons une activité suspecte dans cette requette, Veuillez contactez le service informatique svp ");
            Auth::logout();
            return redirect('login');
        }
    }

    public function ViewActeTampon($acte_id, $m_id,$type){
        $a_id = $this->verifId($acte_id);
        $m_id = $this->verifId($m_id);
        $a = PrestaMuta::find($a_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $retour =  view('admin.prestataire.actes.dateilActe',compact('assure','a'))->render();
        return response()->json([
            'statut' => 'success',
            'code' => $retour
        ]); 
    }

    public function ViewFacture($id){

        $facture = Facture::find($id);
        $prestaMutua = $facture->PrestaMutua()->get();

        $retour =  view('admin.prestataire.facture.details',compact('facture','prestaMutua'))->render();

        return response()->json([
            'statut' => 'success',
            'code' => $retour
        ]); 
    }

    public function dashbordPrestataire(){
        return redirect('login');
    }

    public function printOrdonnance($presta_muta_id)
    {
        $prestataire = Prestataire::where('user_id', Auth::User()->principal_id)->first();
        $presta_muta = PrestaMuta::find($presta_muta_id);
        $ordonnance = Ordonnance::find($presta_muta->medecin_id);
        $facture = Facture::find($presta_muta->facture_id);
        $pdf = PDF::loadView('admin.prestataire.ordonnance.print', compact('presta_muta','ordonnance','prestataire','facture'));

        return $pdf->download($ordonnance->Code->code.'-'.$prestataire->rs.'.pdf');
    }


    public function printHospitalisation($hospitalisation_id)
    {
        $hospitalisation = Hospitalisation::find($hospitalisation_id);
        $prestataire = Prestataire::find($hospitalisation->prestataire_id);
        $pdf = PDF::loadView('admin.prestataire.hospitalisation.print', compact('prestataire','hospitalisation'));

        return $pdf->download($hospitalisation->Code->code.'-'.$prestataire->rs.'.pdf');
        // return view('admin.prestataire.hospitalisation.print',  compact('prestataire','hospitalisation'));
    }

    public function endHospitalisation($hospitalisation_id)
    {
        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        $hospitalisation = Hospitalisation::find($hospitalisation_id);
        $hospitalisation->tampon = 0;
        $hospitalisation->etat = 1;
        $hospitalisation->statut = 'Terminée';
        $factures = [];
        $actes = [];


        $liste_prestation_hospitalisation = ListePrestation::where('prestataire_id', $p->id)->where('libelle','Hospitalisation')->first();

        if(is_null($liste_prestation_hospitalisation)){
            return json_encode(["success"=>false,"message"=>"L'action hospitalisation ne fait pas partie de vos prestations"]);
        }


        $facture = new Facture();
        $facture->montant = 0;
        $facture->part_muscopci = 0;
        $facture->part_assure = 0;
        $facture->prestataire_id = $hospitalisation->prestataire_id ;
        $facture->date = date('Y-m-d');
        $facture->heure = date('H:i');
        $facture->payer = 0;
        $facture->etat = "tampon";
        $facture->save();
        $part_muscopci = 0;
        $part_assure = 0;

        foreach($hospitalisation->lignes_facture() as $ligne){

            if($ligne->part_muscopci>0)
            {
                
                $request_acte = new Request();
                $request_acte->merge(['commentaire' => '']);
                $request_acte->merge(['docteur' => '']);
                $request_acte->merge(['lpid' => $ligne->prestation_id]);

                $liste_prestation = ListePrestation::find($request_acte->lpid);
                $cout_acte = $liste_prestation->ParamlistePrestation->last()->cout ?? $ligne->prix;
                $part_assure_acte = $liste_prestation->ParamlistePrestation->last()->parAssure ?? $ligne->part_assure;
                $part_muscopci_acte   = $liste_prestation->ParamlistePrestation->last()->tierPayant ?? $ligne->part_muscopci;

                $part_muscopci += $part_muscopci_acte;
                $part_assure += $part_assure_acte;

                $request_acte->merge(['qte' => $ligne->quantite]);
                $request_acte->merge(['assure_id' => $hospitalisation->hospitalisationtable_id]);
                $request_acte->merge(['type' => $hospitalisation->hospitalisationtable_type]);
                $request_acte->merge(['cout' => $cout_acte]);
                $request_acte->merge(['part_muscopci' => $part_muscopci_acte]);
                $request_acte->merge(['part_muscopci' => $part_assure_acte]);

                $commentaire[]= $ligne->quantite.'* '.($ligne->prestation_name).' = '.($ligne->quantite * ($part_assure_acte+$part_muscopci_acte));

                $factures[]=[
                    "quantite"=>$ligne->quantite,
                    "prestation_id"=>$ligne->prestation_id,
                    "prestation_name"=>$ligne->prestation_name,
                    "part_assure"=>$part_muscopci_acte,
                    "part_muscopci"=>$part_assure_acte,
                    "prix"=>$cout_acte
                ];

                $actes[] = $this->AddActes($request_acte);
            }else{
                $factures[]=$ligne;
            }
        }


        $b = Bareme::where([

            ['baremetable_id', $hospitalisation->hospitalisationtable_id],
            ['baremetable_type', $hospitalisation->hospitalisationtable_type],

        ])->get();


        if ($hospitalisation->hospitalisationtable_type == 'App\Models\Ayantdroit') {
            $assure = Ayantdroit::find($hospitalisation->hospitalisationtable_id);
            $message = "Hospitalisation pour $assure->last_name $assure->first_name terminée avec succès";
            $matricule = $assure->Mutualiste->registration_number;
        }else{
            $assure = Mutualiste::find($hospitalisation->hospitalisationtable_id);
            $message = "Hospitalisation terminée avec succès";
            $matricule = $assure->registration_number;
        }

        foreach ($actes as $a) {

            $a->update([
                'facture_id' => $facture->id,
                'tampon' => 0,
                'type' => 'Acte',
            ]);

            $ligneBareme = $b->where('prestation_id',$a->ListePrestation->listeprestable_id)->last();

            $stockLigne = $ligneBareme;

            if (isset($stockLigne->firstdate)) {
                $firstdate = $stockLigne->firstdate;
            }else{
                $firstdate = date('Y-m-d');
            }
            if ($a->qte) {
                $qteAC = $a->qte;
            }else {
                $qteAC = 1;
            }

            if($ligneBareme){
                $ligneBareme->update([
                    'consoBeneficiairePrestation' => $stockLigne->consoBeneficiairePrestation + $a->part_muscopci,
                    'consoFamillePrestation' => $stockLigne->consoFamillePrestation  + $a->part_muscopci,
                    'qteUtilise' => $stockLigne->qteUtilise + $qteAC,
                    'firstdate' => $firstdate,
                    'lastedate' => date('Y-m-d'),
                ]);
            }

            $typeAss = $hospitalisation->hospitalisationtable_type;

            if ($typeAss == 'App\Models\Ayantdroit') {
                $assure = Ayantdroit::find($hospitalisation->hospitalisationtable_id);
                $prestamutable_type = 'App\Models\Ayantdroit';
                $type = "Ayant-droit";
            }else{
                $assure = Mutualiste::find($hospitalisation->hospitalisationtable_id);
                $prestamutable_type = 'App\Models\Mutualiste';
                $type = "Mutualiste";
            }


            DB::table('baremes')->where([

                ['baremetable_id', $assure->id],
                ['baremetable_type',$prestamutable_type],
    
            ])->update([
                
                'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci"),
                'plafondGeneraleBeneficiaire'=> DB::raw("plafondGeneraleBeneficiaire - $a->part_muscopci")
            ]);

            if ($typeAss == 'App\Models\Mutualiste') {
                if ($assure->Ayantdroits->count()>0) {
                    foreach ($assure->Ayantdroits as $ad) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();
                        
                        if($bad->last()){
                            $bad->last()->update([
                                'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                            ]);
                        }
                    }
                }
            }else{
                DB::table('baremes')
                ->where([

                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
        
                ])->update([
                    'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                ]);

                $bad = Bareme::where([
                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
                    ['prestation_id', $a->ListePrestation->listeprestable_id],
                ])->get();

                if($bad->last()){
                    $bad->last()->update([
                        'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                    ]);
                }
                $pere = $assure->Mutualiste;
                foreach ($pere->Ayantdroits as $ad) {
                    if ($ad->id != $assure->id) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();
                            
                        if($bad->count()>0){
                            if($bad->last()){
                                $bad->last()->update([
                                    'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                                ]);
                            }
                        }
                    }
                }
            }
            
            $a->delete();
        }

        $facture->update([
            'montant' => $part_muscopci + $part_assure,
            'part_muscopci' => $part_muscopci,
            'part_assure' => $part_assure,
            'prestataire_id' => $hospitalisation->prestataire_id,
            'date' => date('Y-m-d'),
            'heure' => date('H:i'),
            'payer' => 0,
            'etat' => "Validée"
        ]);
        
        $request_acte = new Request();
        $request_acte->merge(['commentaire' => implode(',',$commentaire)]);
        $request_acte->merge(['docteur' => 'Docteur']);
        $request_acte->merge(['lpid' => $liste_prestation_hospitalisation->id]);
        $request_acte->merge(['assure_id' => $hospitalisation->hospitalisationtable_id]);
        $request_acte->merge(['type' => $hospitalisation->hospitalisationtable_type]);

        $a = $this->AddActes($request_acte);
        
        $a->update([
            'facture_id' => $facture->id,
            'cout' => $part_muscopci + $part_assure,
            'part_assure' => $part_assure,
            'part_muscopci' => $part_muscopci,
            'description' => implode(',',$commentaire),
            'tampon' => 0,
            'medecin_id'=>$hospitalisation->id,
            'type' => 'Acte',
        ]);

        $hospitalisation->facture_id = $facture->id;
        $hospitalisation->save();

        $onesignal = new OneSignal;

        $onesignal->message = $message;
        $onesignal->matricule = $matricule;
        $onesignal->type = 'hospitalisation';
        $onesignal->execute();


        session()->flash('message',"Hospitalisation terminée avec succès");
        return redirect()->back();
    }


    public function AddActes(Request $request){

        $request->validate([
            'lpid' => 'required|integer',
            'assure_id' => 'required|integer',
            'type' => 'required|string',
        ]);

        if ($request->demande) {
            $typeAc = 'demande';
            $statut = 'En attente';
        }else{
            $typeAc = 'Acte';
            $statut = Null;
        }
        $typeAss = $request->type;

        if ($typeAss == 'Ayant-droit' || $typeAss == 'App\Models\Ayantdroit') {
            $assure = Ayantdroit::find($request->assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($request->assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }


        if ($assure->active) {

            $lp = ListePrestation::find($request->lpid);
            $prestataire = $lp->Prestataire;
            $cout = $lp->ParamlistePrestation->last()->cout ?? $request->cout ?? 0;
            $part_assure = $lp->ParamlistePrestation->last()->parAssure ?? $request->part_assure ?? 0;
            $part_muscopci   = $lp->ParamlistePrestation->last()->tierPayant ?? $request->part_muscopci ?? 0;
            $qte = $request->qte;
            //VERIFIER QUE LA QUANTITE N'EST PAS NULL.
            if ($request->qte) {
                if ($request->qte>1) {
                    $qte = $request->qte;
                    $cout = $cout * $request->qte;
                    $part_assure = $part_assure * $request->qte;
                    $part_muscopci = $part_muscopci * $request->qte;
                }
            }

            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            $ssumActesM = $SUMactes->SUM('cout') +  $cout;
            $pf = $assure->bareme->last()->plafondGeneraleFamillle ;
            $pb = $assure->bareme->last()->plafondGeneraleBeneficiaire;
            
            //VERIFIER LES PLAFONDS FAMILLE ET INDIVIDUEL
            if (($pf > $ssumActesM) and ($pb > $ssumActesM)) {

                if($pf>$pb){
                    $psup = $pf;
                    $pinf = $pb;
                }else{
                    $psup = $pb;
                    $pinf = $pf ;
                }
                $dif = $pinf  - $ssumActesM;

                if ($dif>0) {
                }else{
                    return response()->json([
                        'title' => 'PLAFOND GENERAL',
                        'statut' => 'error',
                        'message' => "Le montant des actes est supperieur au plafond FAMILLE OU BENEFIAIRE. Veuillez contacter la MUSCOP-CI pour la procedure d'enregistrement directe | Plafond Fammille: $pf Fcfa | Plafond individuel: $pb Fcfa | Cout Acte(s): $ssumActesM Fcfa",     
                    ]);  
                }
                
                
                //VERIFIER LA CONSOMATION PAR PRESTATION
                $qteP = $lp->listeprestable->ParamPrestation->last()->qte;
                $dureeP = $lp->listeprestable->ParamPrestation->last()->duree;
                $entite = $lp->listeprestable->ParamPrestation->last()->entite;
                $plafondP = $lp->listeprestable->ParamPrestation->last()->plafond;

                $baram = Bareme::where([
                    ['baremetable_id', $assure->id],
                    ['baremetable_type', $prestamutable_type],
                    ['prestation_id', $lp->listeprestable_id],
                ])->first();
                
                if ($qteP) {
                    if ($baram->qteUtilise >= $qteP) {
                        return response()->json([
                            'title' => 'LIMITE PRESTATION',
                            'statut' => 'error',
                            'message' => "Vous avez droit à $qteP fois cette prestation. Vous avez déjà consommé(e) $baram->qteUtilise", 
                        ]);
                    }
                }
                
                
                if ($plafondP) {
                    if ($entite) {
                        if ($entite == "Assuré") {
                            $resteAconsoSurP = $plafondP - $baram->consoBeneficiairePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 0;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    return $acte; 
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }else{
                            $resteAconsoSurP = $plafondP - $baram->consoFamillePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 0;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    return $acte; 
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond de la famille du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }
                    }
                }
                
                
                $acte = new PrestaMuta();
                $acte->libelle   = $lp->libelle;
                $acte->liste_prestation_id   = $lp->id;
                $acte->prestamutable_id   = $request->assure_id;
                $acte->prestataire_id   = $lp->prestataire_id;
                $acte->prestamutable_type   = $prestamutable_type;
                $acte->description   = $request->commentaire;
                $acte->cout   = $cout;
                $acte->part_assure   = $part_assure;
                $acte->part_muscopci   = $part_muscopci;
                $acte->docteur   = $request->docteur;
                $acte->tampon   = 0;
                $acte->entente   = $statut;
                $acte->type   = $typeAc;
                $acte->added_by   = Auth::User()->id;
                $acte->date   = date('Y-m-d');
                $acte->heure   = date('H:i');
                $acte->qte   = $qte;
                $acte->save();

                $actes = PrestaMuta::where([
                    ['prestamutable_id', $request->assure_id],
                    ['prestamutable_type',$prestamutable_type],
                    ['prestataire_id', $lp->prestataire_id],
                    ['tampon', 1],
                    ['etat', 1],
                ])->orderBy('id','desc')->get();

                return $acte;

            }else{
                return response()->json([
                    'statut' => 'success',
                    'code' => 'Le plafond (FAMILLE OU BENEFIAIRE) de cet assuré est inferieur à la sommes des actes',     
                ]);   
            }
        }else{
            return response()->json([
                'statut' => 'success',
                'code' => 'Cet assuré vient d\'être désactivé dans le système',     
            ]); 
        }
    }

    

    public function setarde(Request $request){
        $prestataire = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        $prestataire->garde = $prestataire->garde==1 ? 0 : 1;
        $message = $prestataire->garde==1 ? "Votre pharmacie n'assure plus de permanence" : "Votre pharmacie est maintenant de garde";
        $prestataire->save();

        return json_encode(["message"=>$message]);
    }

    public function setPrescription(Request $request){

        $actes = [];
        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();

        $facture = new Facture();
        $facture->montant = 0;
        $facture->part_muscopci = 0;
        $facture->part_assure = 0;
        $facture->prestataire_id = $p->id ;
        $facture->date = date('Y-m-d');
        $facture->heure = date('H:i');
        $facture->payer = 0;
        $facture->etat = "tampon";
        $facture->save();
        $part_assure = 0;
        $part_muscopci = 0;

        // prescription_id
        // type
        // type_id

        foreach($request->prestation_id as $data){

            $request_acte = new Request();
            $request_acte->merge(['commentaire' => '']);
            $request_acte->merge(['docteur' => '']);
            $request_acte->merge(['lpid' => $data]);

            $liste_prestation = ListePrestation::find($data);
            $cout_acte = $liste_prestation->ParamlistePrestation->last()->cout ?? 0;
            $part_assure_acte = $liste_prestation->ParamlistePrestation->last()->parAssure ?? 0;
            $part_muscopci_acte   = $liste_prestation->ParamlistePrestation->last()->tierPayant ?? 0;

            $part_muscopci += $part_muscopci_acte;
            $part_assure += $part_assure_acte;

            $request_acte->merge(['qte' => null]);
            $request_acte->merge(['assure_id' => $request->type_id]);
            $request_acte->merge(['type' => $request->type]);
            $request_acte->merge(['cout' => $cout_acte]);
            $request_acte->merge(['part_muscopci' => $part_muscopci_acte]);
            $request_acte->merge(['part_muscopci' => $part_assure_acte]);

            $actes[] = $this->AddActes($request_acte);
        }

        $typeAss = $request->type;

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($request->type_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
            $message = "Examen pour $assure->last_name $assure->first_name terminée";
            $matricule = $assure->Mutualiste->registration_number;
        }else{
            $assure = Mutualiste::find($request->type_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
            $message = "Examen terminée";
            $matricule = $assure->registration_number;
        }


        $b = Bareme::where([
            ['baremetable_id', $request->type_id],
            ['baremetable_type', $prestamutable_type]
        ])->get();

        foreach ($actes as $a) {

            $a->update([
                'facture_id' => $facture->id,
                'tampon' => 0,
                'type' => 'Acte',
            ]);

            $ligneBareme = $b->where('prestation_id',$a->ListePrestation->listeprestable_id)->last();

            $stockLigne = $ligneBareme;

            if (isset($stockLigne->firstdate)) {
                $firstdate = $stockLigne->firstdate;
            }else{
                $firstdate = date('Y-m-d');
            }
            if ($a->qte) {
                $qteAC = $a->qte;
            }else {
                $qteAC = 1;
            }

            $ligneBareme->update([
                'consoBeneficiairePrestation' => $stockLigne->consoBeneficiairePrestation + $a->part_muscopci,
                'consoFamillePrestation' => $stockLigne->consoFamillePrestation  + $a->part_muscopci,
                'qteUtilise' => $stockLigne->qteUtilise + $qteAC,
                'firstdate' => $firstdate,
                'lastedate' => date('Y-m-d'),
            ]);

            DB::table('baremes')->where([

                ['baremetable_id', $assure->id],
                ['baremetable_type',$prestamutable_type],
    
            ])->update([
                
                'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci"),
                'plafondGeneraleBeneficiaire'=> DB::raw("plafondGeneraleBeneficiaire - $a->part_muscopci")
            ]);

            if ($typeAss == 'Mutualiste') {
                if ($assure->Ayantdroits->count()>0) {
                    foreach ($assure->Ayantdroits as $ad) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();

                        $bad->last()->update([
                            'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                        ]);
                    }
                }
            }else{
                DB::table('baremes')
                ->where([

                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
        
                ])->update([
                    'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                ]);

                $bad = Bareme::where([
                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
                    ['prestation_id', $a->ListePrestation->listeprestable_id],
                ])->get();
                $bad->last()->update([
                    'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                ]);
                $pere = $assure->Mutualiste;
                foreach ($pere->Ayantdroits as $ad) {
                    if ($ad->id != $assure->id) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();
                            
                        if($bad->count()>0){
                            $bad->last()->update([
                                'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                            ]);
                        }
                    }
                }
            }
            
            
        }

        $facture->update([
            'montant' => $part_muscopci + $part_assure,
            'part_muscopci' => $part_muscopci,
            'part_assure' => $part_assure,
            'prestataire_id' => $p->id,
            'date' => date('Y-m-d'),
            'heure' => date('H:i'),
            'payer' => 0,
            'etat' => "Validée"
        ]);

        $prescription = Prescription::find($request->prescription_id);
        $prescription->facture_id = $facture->id;
        $prescription->tampon = 0;
        $prescription->etat = 1;
        $prescription->save();


        $onesignal = new OneSignal;

        $onesignal->message = $message;
        $onesignal->matricule = $matricule;
        $onesignal->type = 'prescription';
        $onesignal->execute();
        
        session()->flash('message',"Examen terminée avec succès");
        return response()->json([
            'statut' => 'success',
            'code' => "Examen terminée avec succès",     
        ]);   
        
    }
    
}
