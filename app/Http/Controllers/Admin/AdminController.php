<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mutualiste;
use App\Models\User;
use App\Models\Ayantdroit;
use App\Models\Bareme;
use App\Models\Menu;
use App\Models\Prestation;
use App\Models\Prestataire;
use App\Models\ParamPrestataire;
use App\Models\PrestaMuta;
use App\Models\Right;
use App\Models\UserMenu;
use App\Models\UserRight;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
    } 

    public function index()
    {
        
        // $m = Mutualiste::all();
        // $num = 1;
        // $i = 1;
        // foreach ($ad as $a) {
        //     $num2 = $num + 10;
        //    $a->update([
        //     'assurance_number' =>  date('y').'A'.$random = mt_rand($num , $num2).'D'.$i,
        //    ]);
        //    $num = $num2 + 1;
        //    $i++;
        // }
        // foreach ($m  as $a) {
        //    $num2 = $num + 10;
        //    $a->update([
        //     'assurance_number' =>  date('y').'M'.$random = mt_rand($num , $num2).'U'.$i,
        //    ]);
        //    $num = $num2 + 1;
        //    $i++;
        // }

        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();

        $actes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['tampon', 0],
            ['etat', 1],
        ])->orderBy('id','desc')->get();

        $acteparmois = DB::table('presta_mutas')->selectRaw("DATE_FORMAT(date, '%b') AS mois, COUNT(*) AS nbre")
        ->groupBy('mois')->whereYear('date', '=', date('Y'))
        ->where([
            ['prestataire_id', $p->id],
            ['tampon', 0],
        ])->get();

        
            
        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();
        

        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $acteinpayeparmois = DB::table('presta_mutas')->selectRaw("DATE_FORMAT(date, '%b') AS mois, COUNT(*) AS nbre")
        ->groupBy('mois')->whereYear('date', '=', date('Y'))
        ->where([
            ['prestataire_id', $p->id],
            ['tampon', 0],
        ])->whereIn('facture_id', $fi_id)->get();

        
        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();

        $actepayeparmois = DB::table('presta_mutas')->selectRaw("DATE_FORMAT(date, '%b') AS mois, COUNT(*) AS nbre")
        ->groupBy('mois')->whereYear('date', '=', date('Y'))
        ->where([
            ['prestataire_id', $p->id],
            ['tampon', 0],
        ])->whereIn('facture_id', $fp_id)->get();

       // dd($actepayeparmois,  $acteinpayeparmois);

        $pj = $p->Facture->where('date', date('Y-m-d'));

        $actepj = DB::table('presta_mutas')->select('facture_id','prestamutable_id','prestamutable_type')->where([
            ['prestataire_id', $p->id],
            ['tampon', 0],
            ['date', date('Y-m-d')],
        ])
        ->groupBy('facture_id','prestamutable_type','prestamutable_id')->get();
    
           
        $mutualiste =   Mutualiste::where('active',true)->orderBy('id','desc')->get();
        return view('admin.dashbord.index',compact('mutualiste','actes','p','actesi','actesp','demandes','pj','actepj','acteparmois','actepayeparmois','acteinpayeparmois'));
    }

    public function beneficiaires(){
        $mutualiste =   Mutualiste::select()->orderBy('name','asc')->get();
        $ayantdroit =   Ayantdroit::select()->orderBy('last_name','asc')->get();
        return view('admin.beneficiaire.benePage',compact('mutualiste','ayantdroit'));
    }

    public function GestionDesDroits(){
        $user = User::where([
            ['principal_id',Auth::user()->principal_id],
            ['active',1],
            ['Typeuser','Prestataire'],
        ])->get();
        $right = Right::where('type','Prestataire')->get();
        return view('admin.right.userRight',compact('user','right'));
    }

    public function AddUser() {
        $right = Right::where('type','Prestataire')->get();
        $menu = Menu::where('type','Prestataire')->get();
        $retour = view('admin.right.addUser',compact('menu','right'))->render();

        return response()->json([
            'status' => "success",
            'code' => $retour
        ]);
    }

    public function AddUserPrestataire(Request $request){
       
        $request->validate([
            '_token' => 'required|string',
            'name' => 'required|string',
            'contact' => 'required|string',
            'email' => 'required|email',
            'fonction' => 'required|string',
        ]);
        //dd($request->all());

        $verifEmail = User::where('email', $request->email)->get();
        if ($verifEmail->count()>0) {
            session()->flash('messageErreur',"Utilisateur email existe déjà");
            return redirect()->back();
        }

        $user = New User();
        $password = Str::random(6);
        $user->name = $request->name;
        $user->contact = $request->contact;
        $user->email = $request->email;
        $user->fonction = $request->fonction;
        $user->Typeuser = 'Prestataire';
        $user->typePrestataire = 'Admin Prestataire';
        $user->mdp = $password; 
        $user->active = 1; 
        $user->password = Hash::make($password);
        $user->principal_id = Auth::User()->principal_id;
        $user->save();

        $right = Right::where('type','Prestataire')->get();
        
            foreach ($right as $value) {
                if ( $value->libelle != 'All-Right-Prestataire' ) {
                    UserRight::create([
                        'user_id' => $user->id,
                        'right_id' => $value->id,
                        'libelle' => $value->libelle,
                    ]);
                }
            }
       
            foreach ($request->menu_id as $value) {
                $menu = Menu::find($value); 
                UserMenu::create([
                    'user_id' => $user->id,
                    'menu_id' => $value,
                    'libelle' => $menu->libelle,
                ]);
            }
            session()->flash('message',"Utilisateur enregistré avec succès");
            return redirect()->back();
       
    }

    public function updateUser($uid){
        $user_id = $this->verifId($uid);

        $user = User::find($user_id);
        $right = Right::where('type','Prestataire')->get();
        $menu = Menu::where('type','Prestataire')->get();

        $retour = view('admin.right.updateUser',compact('menu','right','user'))->render();

        return response()->json([
            'status' => "success",
            'code' => $retour
        ]);
    }

    public function UpdateUserPrestataire(Request $request, $id){
        $user_id = $this->verifId($id);
        $request->validate([
            '_token' => 'required|string',
            'name' => 'required|string',
            'contact' => 'required|string',
            'email' => 'required|email',
            'fonction' => 'required|string',
        ]);

        $user = User::find($user_id);
        $user->update([
            'name' => $request->name,
            'contact' => $request->contact,
            'email' => $request->email,
            'fonction' => $request->fonction,
        ]);
        $um = UserMenu::where('user_id', $user->id)->delete();
        if ($um) {
            foreach ($request->menu_id as $value) {
                $menu = Menu::find($value); 
                UserMenu::create([
                    'user_id' => $user->id,
                    'menu_id' => $value,
                    'libelle' => $menu->libelle,
                ]);
            }
        }

        session()->flash('message',"Utilisateur $user->name modifié avec succès");
        return redirect()->back();

    }

    public function verifId($id){
        $type = gettype($id);
        $id = htmlspecialchars($id);
        if ($id > 0 ) {
            return $id;
        }else{
            session()->flash('messageErreur',"Nous contatons une activité suspecte dans cette requette, Veuillez contactez le service informatique svp ");
            Auth::logout();
            return redirect('login');
        }
    }
    
}
