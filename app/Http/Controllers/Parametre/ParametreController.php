<?php

namespace App\Http\Controllers\Parametre;

use App\Http\Controllers\Controller;
use App\Models\Bloc;
use Illuminate\Http\Request;
use App\Models\Departement;
use App\Models\typePrestation;
use App\Models\Prestation;

class ParametreController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('parametre');
    } 

    public function Departement()
    {
       return view('parametre.departement');
    }

    public function CategoriePrestation(){
        $b = Bloc::where('etat',true)->orderBy('libelle')->get();
        $tP = typePrestation::where('etat', true)->get();
        $P = Prestation::where('etat', true)->get();
        return view('parametre.catePrestation', compact('tP','P','b'));
    }

    public function Bareme(){
        $b = Bloc::where('etat',true)->orderBy('libelle')->get();
        return view('parametre.bareme.bareme', compact('b'));
    }
}
