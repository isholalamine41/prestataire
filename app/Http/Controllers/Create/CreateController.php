<?php

namespace App\Http\Controllers\Create;

use App\Http\Controllers\Controller;
use App\Models\Ayantdroit;
use App\Models\Bareme;
use App\Models\Bloc;
use App\Models\Code;
use Illuminate\Http\Request;
use App\Models\Prestataire;
use App\Models\Prestation;
use App\Models\typePrestataire;
use App\Models\typePrestation;
use App\Models\Commune;
use App\Models\Facture;
use App\Models\User;
use Illuminate\Support\Str;
use App\Models\ListePrestation;
use App\Models\Mutualiste;
use App\Models\ParamlistePrestation;
use App\Models\ParamPrestation;
use App\Models\ParamtypePrestation;
use App\Models\UserProjet;
use App\Models\OneSignal;
use App\Models\ParamPrestataire;
use App\Models\Prescription;
use App\Models\PrestaMuta;
use App\Models\PrestataireTypePrestation;
use App\Models\TypePrescription;
use App\Models\Ordonnance;
use App\Models\Hospitalisation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CreateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('create');
    } 

    // public function verifyHospit(Request $request){

    //     $request_acte = new Request();
    //     $request_acte->merge(['commentaire' => '']);
    //     $request_acte->merge(['docteur' => '']);
    //     $request_acte->merge(['part_assure' => $request->part_assure]);
    //     $request_acte->merge(['part_muscopci' => $request->part_muscopci]);
    //     $request_acte->merge(['prestation_name' => $request->prestation_name]);
    //     $request_acte->merge(['lpid' => $request->prestation_id]);

    //     $request_acte->merge(['assure_id' => $request->assure_id]);
    //     $request_acte->merge(['type' => $request->type]);

    //     return $this->AddActesHospitalisation($request_acte);
    // }

    public function verifyOrdonnace(Request $request){

        $typeAss = $request->type;

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($request->assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($request->assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }


        if ($assure->active) {

            $lp = ListePrestation::where('libelle','Pharmacie')->first();
            $prestataire = $lp->Prestataire;
            $cout = $request->global;
            $part_assure = $request->payer;
            $part_muscopci = $request->assurance;
            $qte = null;
            
            //VERIFIER QUE LA QUANTITE N'EST PAS NULL.
            if ($request->qte) {
                if ($request->qte>1) {
                    $qte = $request->qte;
                    $cout = $cout * $request->qte;
                    $part_assure = $part_assure * $request->qte;
                    $part_muscopci = $part_muscopci * $request->qte;
                }
            }

            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            $ssumActesM = $SUMactes->SUM('cout') +  $cout;
            $pf = $assure->bareme->last()->plafondGeneraleFamillle ;
            $pb = $assure->bareme->last()->plafondGeneraleBeneficiaire;
            
            //VERIFIER LES PLAFONDS FAMILLE ET INDIVIDUEL
            if (($pf > $ssumActesM) and ($pb > $ssumActesM)) {

                if($pf>$pb){
                    $psup = $pf;
                    $pinf = $pb;
                }else{
                    $psup = $pb;
                    $pinf = $pf ;
                }
                $dif = $pinf  - $ssumActesM;

                if ($dif>0) {
                }else{
                    return response()->json([
                        'title' => 'PLAFOND GENERAL',
                        'statut' => 'error',
                        'message' => "Le montant des actes est supperieur au plafond FAMILLE OU BENEFIAIRE. Veuillez contacter la MUSCOP-CI pour la procedure d'enregistrement directe | Plafond Fammille: $pf Fcfa | Plafond individuel: $pb Fcfa | Cout Acte(s): $ssumActesM Fcfa",     
                    ]);  
                }
                
                
                //VERIFIER LA CONSOMATION PAR PRESTATION
                $qteP = $lp->listeprestable->ParamPrestation->last()->qte;
                $dureeP = $lp->listeprestable->ParamPrestation->last()->duree;
                $entite = $lp->listeprestable->ParamPrestation->last()->entite;
                $plafondP = $lp->listeprestable->ParamPrestation->last()->plafond;

                $baram = Bareme::where([
                    ['baremetable_id', $assure->id],
                    ['baremetable_type', $prestamutable_type],
                    ['prestation_id', $lp->listeprestable_id],
                ])->first();
                
                if ($qteP) {
                    if ($baram->qteUtilise >= $qteP) {
                        return response()->json([
                            'title' => 'LIMITE PRESTATION',
                            'statut' => 'error',
                            'message' => "Vous avez droit à $qteP fois cette prestation. Vous avez déjà consommé(e) $baram->qteUtilise", 
                        ]);
                    }
                }
                
                
                if ($plafondP) {
                    if ($entite) {
                        if ($entite == "Assuré") {
                            $resteAconsoSurP = $plafondP - $baram->consoBeneficiairePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    return response()->json([
                                        'statut' => 'success',
                                        'response' => '1',
                                    ]);  
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }else{
                            $resteAconsoSurP = $plafondP - $baram->consoFamillePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{

                                    return response()->json([
                                        'statut' => 'success',
                                        'response' => '2',
                                    ]); 
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond de la famille du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }
                    }
                }
                
                return response()->json([
                    'statut' => 'success',
                    'response' => '3',   
                ]); 

            }else{
                return response()->json([
                    'statut' => 'error',
                    'code' => 'Le plafond (FAMILLE OU BENEFIAIRE) de cet assuré est inferieur à la sommes des actes',     
                ]);   
            }
        }else{
            return response()->json([
                'statut' => 'error',
                'code' => 'Cet assuré vient d\'être désactivé dans le système',     
            ]); 
        }
    }

    public function AddActesReturn(Request $request){

        $request->validate([
            '_token' => 'required|string',
            'commentaire' => 'required|string',
            'docteur' => 'required|string',
            'lpid' => 'required|integer',
            'assure_id' => 'required|integer',
            'type' => 'required|string',
        ]);

        if ($request->demande) {
            $typeAc = 'demande';
            $statut = 'En attente';
        }else{
            $typeAc = 'tampon';
            $statut = Null;
        }
        $typeAss = $request->type;

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($request->assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($request->assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }


        if ($assure->active) {

            $lp = ListePrestation::find($request->lpid);
            $prestataire = $lp->Prestataire;
            $cout = $lp->ParamlistePrestation->last()->cout ?? 0;
            $part_assure = $lp->ParamlistePrestation->last()->parAssure ?? 0;
            $part_muscopci   = $lp->ParamlistePrestation->last()->tierPayant ?? 0;
            $qte = null;
            
            //VERIFIER QUE LA QUANTITE N'EST PAS NULL.
            if ($request->qte) {
                if ($request->qte>1) {
                    $qte = $request->qte;
                    $cout = $cout * $request->qte;
                    $part_assure = $part_assure * $request->qte;
                    $part_muscopci = $part_muscopci * $request->qte;
                }
            }

            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['liste_prestation_id', $lp->id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            if ($SUMactes->count()>0) {
                return response()->json([
                    'title' => 'PRESTATION EXISTANTE',
                    'statut' => 'error',
                    'message' => "La prestation $lp->libelle, existe déjà dans la liste des actes",     
                ]);  
            }

            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            $ssumActesM = $SUMactes->SUM('cout') +  $cout;
            $pf = $assure->bareme->last()->plafondGeneraleFamillle ;
            $pb = $assure->bareme->last()->plafondGeneraleBeneficiaire;
            
            //VERIFIER LES PLAFONDS FAMILLE ET INDIVIDUEL
            if (($pf > $ssumActesM) and ($pb > $ssumActesM)) {

                if($pf>$pb){
                    $psup = $pf;
                    $pinf = $pb;
                }else{
                    $psup = $pb;
                    $pinf = $pf ;
                }
                $dif = $pinf  - $ssumActesM;

                if ($dif>0) {
                }else{
                    return response()->json([
                        'title' => 'PLAFOND GENERAL',
                        'statut' => 'error',
                        'message' => "Le montant des actes est supperieur au plafond FAMILLE OU BENEFIAIRE. Veuillez contacter la MUSCOP-CI pour la procedure d'enregistrement directe | Plafond Fammille: $pf Fcfa | Plafond individuel: $pb Fcfa | Cout Acte(s): $ssumActesM Fcfa",     
                    ]);  
                }
                
                
                //VERIFIER LA CONSOMATION PAR PRESTATION
                $qteP = $lp->listeprestable->ParamPrestation->last()->qte;
                $dureeP = $lp->listeprestable->ParamPrestation->last()->duree;
                $entite = $lp->listeprestable->ParamPrestation->last()->entite;
                $plafondP = $lp->listeprestable->ParamPrestation->last()->plafond;

                $baram = Bareme::where([
                    ['baremetable_id', $assure->id],
                    ['baremetable_type', $prestamutable_type],
                    ['prestation_id', $lp->listeprestable_id],
                ])->first();
                
                if ($qteP) {
                    if ($baram->qteUtilise >= $qteP) {
                        return response()->json([
                            'title' => 'LIMITE PRESTATION',
                            'statut' => 'error',
                            'message' => "Vous avez droit à $qteP fois cette prestation. Vous avez déjà consommé(e) $baram->qteUtilise", 
                        ]);
                    }
                }
                
                
                if ($plafondP) {
                    if ($entite) {
                        if ($entite == "Assuré") {
                            $resteAconsoSurP = $plafondP - $baram->consoBeneficiairePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 1;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                                    return $acte;  
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }else{
                            $resteAconsoSurP = $plafondP - $baram->consoFamillePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 1;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    return $acte;
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond de la famille du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }
                    }
                }
                
                
                $acte = new PrestaMuta();
                $acte->libelle   = $lp->libelle;
                $acte->liste_prestation_id   = $lp->id;
                $acte->prestamutable_id   = $request->assure_id;
                $acte->prestataire_id   = $lp->prestataire_id;
                $acte->prestamutable_type   = $prestamutable_type;
                $acte->description   = $request->commentaire;
                $acte->cout   = $cout;
                $acte->part_assure   = $part_assure;
                $acte->part_muscopci   = $part_muscopci;
                $acte->docteur   = $request->docteur;
                $acte->tampon   = 1;
                $acte->entente   = $statut;
                $acte->type   = $typeAc;
                $acte->added_by   = Auth::User()->id;
                $acte->date   = date('Y-m-d');
                $acte->heure   = date('H:i');
                $acte->qte   = $qte;
                $acte->save();

                $actes = PrestaMuta::where([
                    ['prestamutable_id', $request->assure_id],
                    ['prestamutable_type',$prestamutable_type],
                    ['prestataire_id', $lp->prestataire_id],
                    ['tampon', 1],
                    ['etat', 1],
                ])->orderBy('id','desc')->get();

                $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                return $acte;

            }else{
                return response()->json([
                    'statut' => 'success',
                    'code' => 'Le plafond (FAMILLE OU BENEFIAIRE) de cet assuré est inferieur à la sommes des actes',     
                ]);   
            }
        }else{
            return response()->json([
                'statut' => 'success',
                'code' => 'Cet assuré vient d\'être désactivé dans le système',     
            ]); 
        }
    }


    public function AddActes(Request $request){

        $request->validate([
            '_token' => 'required|string',
            'commentaire' => 'required|string',
            'docteur' => 'required|string',
            'lpid' => 'required|integer',
            'assure_id' => 'required|integer',
            'type' => 'required|string',
        ]);

        if ($request->demande) {
            $typeAc = 'demande';
            $statut = 'En attente';
        }else{
            $typeAc = 'tampon';
            $statut = Null;
        }
        $typeAss = $request->type;

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($request->assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($request->assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }


        if ($assure->active) {

            $lp = ListePrestation::find($request->lpid);
            $prestataire = $lp->Prestataire;
            $cout = $lp->ParamlistePrestation->last()->cout ?? 0;
            $part_assure = $lp->ParamlistePrestation->last()->parAssure ?? 0;
            $part_muscopci   = $lp->ParamlistePrestation->last()->tierPayant ?? 0;
            $qte = null;
            
            //VERIFIER QUE LA QUANTITE N'EST PAS NULL.
            if ($request->qte) {
                if ($request->qte>1) {
                    $qte = $request->qte;
                    $cout = $cout * $request->qte;
                    $part_assure = $part_assure * $request->qte;
                    $part_muscopci = $part_muscopci * $request->qte;
                }
            }

            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['liste_prestation_id', $lp->id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            if ($SUMactes->count()>0) {
                return response()->json([
                    'title' => 'PRESTATION EXISTANTE',
                    'statut' => 'error',
                    'message' => "La prestation $lp->libelle, existe déjà dans la liste des actes",     
                ]);  
            }

            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            $ssumActesM = $SUMactes->SUM('cout') +  $cout;
            $pf = $assure->bareme->last()->plafondGeneraleFamillle ;
            $pb = $assure->bareme->last()->plafondGeneraleBeneficiaire;
            
            //VERIFIER LES PLAFONDS FAMILLE ET INDIVIDUEL
            if (($pf > $ssumActesM) and ($pb > $ssumActesM)) {

                if($pf>$pb){
                    $psup = $pf;
                    $pinf = $pb;
                }else{
                    $psup = $pb;
                    $pinf = $pf ;
                }
                $dif = $pinf  - $ssumActesM;

                if ($dif>0) {
                }else{
                    return response()->json([
                        'title' => 'PLAFOND GENERAL',
                        'statut' => 'error',
                        'message' => "Le montant des actes est supperieur au plafond FAMILLE OU BENEFIAIRE. Veuillez contacter la MUSCOP-CI pour la procedure d'enregistrement directe | Plafond Fammille: $pf Fcfa | Plafond individuel: $pb Fcfa | Cout Acte(s): $ssumActesM Fcfa",     
                    ]);  
                }
                
                
                //VERIFIER LA CONSOMATION PAR PRESTATION
                $qteP = $lp->listeprestable->ParamPrestation->last()->qte;
                $dureeP = $lp->listeprestable->ParamPrestation->last()->duree;
                $entite = $lp->listeprestable->ParamPrestation->last()->entite;
                $plafondP = $lp->listeprestable->ParamPrestation->last()->plafond;

                $baram = Bareme::where([
                    ['baremetable_id', $assure->id],
                    ['baremetable_type', $prestamutable_type],
                    ['prestation_id', $lp->listeprestable_id],
                ])->first();
                
                if ($qteP) {
                    if ($baram->qteUtilise >= $qteP) {
                        return response()->json([
                            'title' => 'LIMITE PRESTATION',
                            'statut' => 'error',
                            'message' => "Vous avez droit à $qteP fois cette prestation. Vous avez déjà consommé(e) $baram->qteUtilise", 
                        ]);
                    }
                }
                
                
                if ($plafondP) {
                    if ($entite) {
                        if ($entite == "Assuré") {
                            $resteAconsoSurP = $plafondP - $baram->consoBeneficiairePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 1;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                                    return response()->json([
                                        'statut' => 'success',
                                        'code' => $retour,     
                                    ]);  
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }else{
                            $resteAconsoSurP = $plafondP - $baram->consoFamillePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 1;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                                    return response()->json([
                                        'statut' => 'success',
                                        'code' => $retour,     
                                    ]); 
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond de la famille du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }
                    }
                }
                
                
                $acte = new PrestaMuta();
                $acte->libelle   = $lp->libelle;
                $acte->liste_prestation_id   = $lp->id;
                $acte->prestamutable_id   = $request->assure_id;
                $acte->prestataire_id   = $lp->prestataire_id;
                $acte->prestamutable_type   = $prestamutable_type;
                $acte->description   = $request->commentaire;
                $acte->cout   = $cout;
                $acte->part_assure   = $part_assure;
                $acte->part_muscopci   = $part_muscopci;
                $acte->docteur   = $request->docteur;
                $acte->tampon   = 1;
                $acte->entente   = $statut;
                $acte->type   = $typeAc;
                $acte->added_by   = Auth::User()->id;
                $acte->date   = date('Y-m-d');
                $acte->heure   = date('H:i');
                $acte->qte   = $qte;
                $acte->save();

                $actes = PrestaMuta::where([
                    ['prestamutable_id', $request->assure_id],
                    ['prestamutable_type',$prestamutable_type],
                    ['prestataire_id', $lp->prestataire_id],
                    ['tampon', 1],
                    ['etat', 1],
                ])->orderBy('id','desc')->get();

                $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                return response()->json([
                    'statut' => 'success',
                    'code' => $retour,     
                ]); 

            }else{
                return response()->json([
                    'statut' => 'success',
                    'code' => 'Le plafond (FAMILLE OU BENEFIAIRE) de cet assuré est inferieur à la sommes des actes',     
                ]);   
            }
        }else{
            return response()->json([
                'statut' => 'success',
                'code' => 'Cet assuré vient d\'être désactivé dans le système',     
            ]); 
        }
    }


    public function AddActesHospitalisation(Request $request){

        if ($request->demande) {
            $typeAc = 'demande';
            $statut = 'En attente';
        }else{
            $typeAc = 'tampon';
            $statut = Null;
        }
        $typeAss = $request->type;

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($request->assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($request->assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        if ($assure->active) {

            $lp = ListePrestation::find($request->lpid);
            $prestataire = $lp->Prestataire;
            $cout = $lp->ParamlistePrestation->last()->cout ?? $request->part_assure+$request->part_muscopci;
            $part_assure = $lp->ParamlistePrestation->last()->parAssure ?? $request->part_assure;
            $part_muscopci   = $lp->ParamlistePrestation->last()->tierPayant ?? $request->part_muscopci;
            $qte = null;
            
            //VERIFIER QUE LA QUANTITE N'EST PAS NULL.
            if ($request->qte) {
                if ($request->qte>1) {
                    $qte = $request->qte;
                    $cout = $cout * $request->qte;
                    $part_assure = $part_assure * $request->qte;
                    $part_muscopci = $part_muscopci * $request->qte;
                }
            }


            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            $ssumActesM = $SUMactes->SUM('cout') +  $cout;
            $pf = $assure->bareme->last()->plafondGeneraleFamillle ;
            $pb = $assure->bareme->last()->plafondGeneraleBeneficiaire;
            
            //VERIFIER LES PLAFONDS FAMILLE ET INDIVIDUEL
            if (($pf > $ssumActesM) and ($pb > $ssumActesM)) {

                if($pf>$pb){
                    $psup = $pf;
                    $pinf = $pb;
                }else{
                    $psup = $pb;
                    $pinf = $pf ;
                }
                $dif = $pinf  - $ssumActesM;

                if ($dif>0) {
                }else{
                    return response()->json([
                        'title' => 'PLAFOND GENERAL',
                        'statut' => 'error',
                        'message' => "Le montant des actes est supperieur au plafond FAMILLE OU BENEFIAIRE. Veuillez contacter la MUSCOP-CI pour la procedure d'enregistrement directe | Plafond Fammille: $pf Fcfa | Plafond individuel: $pb Fcfa | Cout Acte(s): $ssumActesM Fcfa",     
                    ]);  
                }
                
                
                //VERIFIER LA CONSOMATION PAR PRESTATION
                $qteP = $lp->listeprestable->ParamPrestation->last()->qte ?? null;
                $dureeP = $lp->listeprestable->ParamPrestation->last()->duree ?? null;
                $entite = $lp->listeprestable->ParamPrestation->last()->entite ?? null;
                $plafondP = $lp->listeprestable->ParamPrestation->last()->plafond ?? null;

                $baram = Bareme::where([
                    ['baremetable_id', $assure->id],
                    ['baremetable_type', $prestamutable_type],
                    ['prestation_id', $lp->listeprestable_id],
                ])->first();
                
                if ($qteP) {
                    if ($baram->qteUtilise >= $qteP) {
                        return response()->json([
                            'title' => 'LIMITE PRESTATION',
                            'statut' => 'error',
                            'message' => "Vous avez droit à $qteP fois cette prestation. Vous avez déjà consommé(e) $baram->qteUtilise", 
                        ]);
                    }
                }
                
                
                if ($plafondP) {
                    if ($entite) {
                        if ($entite == "Assuré") {
                            $resteAconsoSurP = $plafondP - $baram->consoBeneficiairePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $request->prestation_name ?? $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 1;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    return response()->json([
                                        'statut' => 'success',
                                        'id' => $acte->id,     
                                    ]);  
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }else{
                            $resteAconsoSurP = $plafondP - $baram->consoFamillePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $request->prestation_name;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 1;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                                    return response()->json([
                                        'statut' => 'success',
                                        'id' => $acte->id,     
                                    ]); 
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond de la famille du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }
                    }
                }
                
                
                $acte = new PrestaMuta();
                $acte->libelle   = $request->prestation_name;
                $acte->liste_prestation_id   = $lp->id;
                $acte->prestamutable_id   = $request->assure_id;
                $acte->prestataire_id   = $lp->prestataire_id;
                $acte->prestamutable_type   = $prestamutable_type;
                $acte->description   = $request->commentaire;
                $acte->cout   = $cout;
                $acte->part_assure   = $part_assure;
                $acte->part_muscopci   = $part_muscopci;
                $acte->docteur   = $request->docteur;
                $acte->tampon   = 1;
                $acte->entente   = $statut;
                $acte->type   = $typeAc;
                $acte->added_by   = Auth::User()->id;
                $acte->date   = date('Y-m-d');
                $acte->heure   = date('H:i');
                $acte->qte   = $qte;
                $acte->save();

                $actes = PrestaMuta::where([
                    ['prestamutable_id', $request->assure_id],
                    ['prestamutable_type',$prestamutable_type],
                    ['prestataire_id', $lp->prestataire_id],
                    ['tampon', 1],
                    ['etat', 1],
                ])->orderBy('id','desc')->get();

                $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                return response()->json([
                    'statut' => 'success',
                    'id' => $acte->id,      
                ]); 

            }else{
                return response()->json([
                    'statut' => 'success',
                    'code' => 'Le plafond (FAMILLE OU BENEFIAIRE) de cet assuré est inferieur à la sommes des actes',     
                ]);   
            }
        }else{
            return response()->json([
                'statut' => 'success',
                'code' => 'Cet assuré vient d\'être désactivé dans le système',     
            ]); 
        }
    }

    public function ValidateAllActes($p_id, $m_id , $typeAss){
        
        $p = Prestataire::find($p_id);

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
            $message = "$assure->last_name $assure->first_name a reçu des actes médicaux";
            $matricule = $assure->Mutualiste->registration_number;
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
            $message = "Vous avez reçu des actes médicaux";
            $matricule = $assure->registration_number;
        }
        

        $actes = PrestaMuta::where([

            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $p->id],
            ['tampon', 1],
            ['type', 'tampon'],
            ['etat', 1],
            ['entente', Null],

        ])->orderBy('id','desc')->get();
        
        //dd($actes, $assure);

        $b = Bareme::where([

            ['baremetable_id', $assure->id],
            ['baremetable_type',$prestamutable_type],

        ])->get();

        $facture = new Facture();
        $facture->montant = 0;
        $facture->part_muscopci = 0;
        $facture->part_assure = 0;
        $facture->prestataire_id = $p->id;
        $facture->date = date('Y-m-d');
        $facture->heure = date('H:i');
        $facture->payer = 0;
        $facture->etat = "tampon";
        $facture->save();

        foreach ($actes as $a) {

            $a->update([
                'facture_id' => $facture->id,
                'tampon' => 0,
                'type' => 'Acte',
            ]);

            $ligneBareme = $b->where('prestation_id',$a->ListePrestation->listeprestable_id)->last();
            //dd($ligneBareme )

            $stockLigne = $ligneBareme;

            if ($stockLigne->firstdate) {
                $firstdate = $stockLigne->firstdate;
            }else{
                $firstdate = date('Y-m-d');
            }
            if ($a->qte) {
                $qteAC = $a->qte;
            }else {
                $qteAC = 1;
            }

            $ligneBareme->update([
                'consoBeneficiairePrestation' => $stockLigne->consoBeneficiairePrestation + $a->part_muscopci,
                'consoFamillePrestation' => $stockLigne->consoFamillePrestation  + $a->part_muscopci,
                'qteUtilise' => $stockLigne->qteUtilise + $qteAC,
                'firstdate' => $firstdate,
                'lastedate' => date('Y-m-d'),
            ]);


            DB::table('baremes')->where([

                ['baremetable_id', $assure->id],
                ['baremetable_type',$prestamutable_type],
    
            ])->update([
                
                'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci"),
                'plafondGeneraleBeneficiaire'=> DB::raw("plafondGeneraleBeneficiaire - $a->part_muscopci")
            ]);

            if ($typeAss == 'Mutualiste') {
                if ($assure->Ayantdroits->count()>0) {
                    foreach ($assure->Ayantdroits as $ad) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();

                        $bad->last()->update([
                            'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                        ]);
                    }
                }
            }else{
                DB::table('baremes')
                ->where([

                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
        
                ])->update([
                    'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                ]);

                $bad = Bareme::where([
                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
                    ['prestation_id', $a->ListePrestation->listeprestable_id],
                ])->get();

                $bad->last()->update([
                    'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                ]);
                $pere = $assure->Mutualiste;
                foreach ($pere->Ayantdroits as $ad) {
                    if ($ad->id != $assure->id) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();

                        $bad->last()->update([
                            'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                        ]);
                    }
                }
            }
            
            
        }


        $facture->update([
            'montant' => $actes->SUM('part_muscopci') + $actes->SUM('part_assure'),
            'part_muscopci' => $actes->SUM('part_muscopci'),
            'part_assure' => $actes->SUM('part_assure'),
            'prestataire_id' => $p->id ,
            'date' => date('Y-m-d'),
            'heure' => date('H:i'),
            'payer' => 0,
            'etat' => "Validée" ,
        ]);

        $onesignal = new OneSignal;

        $onesignal->message = $message;
        $onesignal->matricule = $matricule;
        $onesignal->type = 'acte';
        $onesignal->execute();

        session()->flash('message',"Actes médicaux enregistrés avec succès");
        return redirect()->back();
                
    }

    public function ValidateAllActesPharmacie(Request $request){
        
        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();

        $pharamacie_data = typePrestation::where('libelle','PHARMACIE')->first();
        $pharamacie_prestaion = Prestation::where('libelle','pharmacie')->where('type_prestation_id',$pharamacie_data->id)->first();
        $liste_prestation_pharmacie = ListePrestation::where('prestataire_id', $p->id)->where('listeprestable_id',$pharamacie_prestaion->id)
        ->where('libelle','pharmacie')
        ->first();
        // $liste_prestation_pharmacie = ListePrestation::where('libelle','Pharmacie')->where('prestataire_id',$p->id)->first();
        $typeAss = $request->type;

        if(is_null($liste_prestation_pharmacie)){
            return json_encode(["success"=>false,"message"=>"L'action pharmacie ne fait pas partie de vos prestations"]);
        }

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($request->assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($request->assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $taux = round($p->baseCouverture/100,2);

        $b = Bareme::where([

            ['baremetable_id', $assure->id],
            ['baremetable_type',$prestamutable_type],

        ])->get();

        $facture = new Facture();
        $facture->montant = 0;
        $facture->part_muscopci = 0;
        $facture->part_assure = 0;
        $facture->prestataire_id = $p->id;
        $facture->date = date('Y-m-d');
        $facture->heure = date('H:i');
        $facture->payer = 0;
        $facture->etat = "tampon";
        $facture->save();

        $ordonnance = Ordonnance::find($request->ordonnance_id);
        $medicaments = [];

        $cout = 0;
        $part_assure = 0;
        $part_muscopci = 0;
        $commentaire = [];
        $actes = [];

        for ($i=0; $i < count($request->medicaments); $i++) { 

            foreach($ordonnance->medicaments() as $medicament){

                if($medicament->medicament==$request->medicaments[$i] && $medicament->etat==0){
                    
                    if($request->checkeds[$i]=="true"){

                        $medicament->prix = $request->prix_medicaments[$i];
                        $medicament->prestataire_id = $p->id;
                        $medicament->etat = 1;
                        $medicament->medicament = $request->propositions[$i]=='' ? $medicament->medicament :  $medicament->medicament." / ".$request->propositions[$i];

                        $cout += $medicament->quantite * $medicament->prix;
                        $cout_acte = $medicament->quantite * $medicament->prix;
                        $commentaire[]= $medicament->quantite.'* '.$medicament->medicament.' = '.($medicament->quantite * $medicament->prix);


                        $request_acte = new Request();
                        $request_acte->merge(['_token' =>  $request->_token]);
                        $request_acte->merge(['commentaire' => 'Achat de médicament']);
                        $request_acte->merge(['docteur' => 'Le pharmacien']);
                        // $liste_prestation_autre = ListePrestation::where('libelle','Autres')->where('prestataire_id',$p->id)->first();
                        $liste_prestation = ListePrestation::where('libelle','=',ListePrestation::find($request->prestation_ids[$i])->libelle)->where('prestataire_id',$p->id)->first();

                        if($liste_prestation->Prestation->exclusion){
                            $request_acte->merge(['lpid' => $liste_prestation->id]);
                            $part_muscopci_acte = 0;
                            $part_assure_acte = ($medicament->quantite * $medicament->prix) - $part_muscopci_acte;
                        }else{
                            
                            $request_acte->merge(['lpid' => $liste_prestation->id]);
                            $part_muscopci_acte = $medicament->quantite * $medicament->prix * $taux;
                            $part_assure_acte = ($medicament->quantite * $medicament->prix) - $part_muscopci_acte;
                        }

                        $part_assure += $part_assure_acte;
                        $part_muscopci += $part_muscopci_acte;

                        $request_acte->merge(['assure_id' => $request->assure_id]);
                        $request_acte->merge(['type' => $request->type]);


                        $a = $this->AddActesReturn($request_acte);

                        $a->update([
                            'facture_id' => $facture->id,
                            'cout' => $cout_acte,
                            'part_assure' => $part_assure_acte,
                            'part_muscopci' => $part_muscopci_acte,
                            'description' => implode(',',$commentaire),
                            'tampon' => 0,
                            'medecin_id'=>$ordonnance->id,
                            'type' => 'Acte',
                        ]);

                        $actes[]=$a;

                    }

                    $medicaments[]=$medicament;
                    break;
                }

                if($medicament->etat==1){
                    $medicaments[]=$medicament;
                }

            }
            
        }

        $ordonnance->medicaments = json_encode($medicaments);
        $ordonnance->setState();
        $ordonnance->save();

        if ($typeAss == 'Ayant-droit') {
            $message = "Ordonnance ".$ordonnance->Code->code." servie pour $assure->last_name $assure->first_name";
            $matricule = $assure->Mutualiste->registration_number;
        }else{
            $matricule = $assure->registration_number;
            $message = "Ordonnance ".$ordonnance->Code->code." servie";
        }

        foreach ($actes as $a) {

            $ligneBareme = $b->where('prestation_id',$a->ListePrestation->listeprestable_id)->last();
            $stockLigne = $ligneBareme;

            if (!is_null($stockLigne) && $stockLigne->firstdate) {
                $firstdate = $stockLigne->firstdate;
            }else{
                $firstdate = date('Y-m-d');
            }
            if ($a->qte) {
                $qteAC = $a->qte;
            }else {
                $qteAC = 1;
            }

            $ligneBareme->update([
                'consoBeneficiairePrestation' => $stockLigne->consoBeneficiairePrestation + $a->part_muscopci,
                'consoFamillePrestation' => $stockLigne->consoFamillePrestation  + $a->part_muscopci,
                'qteUtilise' => $stockLigne->qteUtilise + $qteAC,
                'firstdate' => $firstdate,
                'lastedate' => date('Y-m-d'),
            ]);

            
            DB::table('baremes')->where([

                ['baremetable_id', $assure->id],
                ['baremetable_type',$prestamutable_type],

            ])->update([
                
                'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci"),
                'plafondGeneraleBeneficiaire'=> DB::raw("plafondGeneraleBeneficiaire - $a->part_muscopci")
            ]);

            if ($typeAss == 'Mutualiste') {
                if ($assure->Ayantdroits->count()>0) {
                    foreach ($assure->Ayantdroits as $ad) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();

                        $bad->last()->update([
                            'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                        ]);
                    }
                }
            }else{
                DB::table('baremes')
                ->where([

                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
        
                ])->update([
                    'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                ]);

                $bad = Bareme::where([
                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
                    ['prestation_id', $a->ListePrestation->listeprestable_id],
                ])->get();
                
                $bad->last()->update([
                    'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                ]);
                $pere = $assure->Mutualiste;
                foreach ($pere->Ayantdroits as $ad) {
                    if ($ad->id != $assure->id) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();

                        $bad->last()->update([
                            'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                        ]);
                    }
                }
            }

            $a->delete();
        }
        
        $request_acte = new Request();
        $request_acte->merge(['_token' =>  $request->_token]);
        $request_acte->merge(['commentaire' => 'Achat de médicament']);
        $request_acte->merge(['docteur' => 'Le pharmacien']);
        $request_acte->merge(['lpid' => $liste_prestation_pharmacie->id]);
        $request_acte->merge(['assure_id' => $request->assure_id]);
        $request_acte->merge(['type' => $request->type]);

        $a = $this->AddActesReturn($request_acte);
        
        $a->update([
            'facture_id' => $facture->id,
            // 'libelle' => 'Pharmacie ['.implode(',',$commentaire).']',
            'cout' => $cout,
            'part_assure' => $part_assure,
            'part_muscopci' => $part_muscopci,
            'description' => implode(',',$commentaire),
            'tampon' => 0,
            'medecin_id'=>$ordonnance->id,
            'type' => 'Acte',
        ]);

        $facture->update([
            'montant' => $part_muscopci + $part_assure,
            'part_muscopci' => $part_muscopci,
            'part_assure' => $part_assure,
            'prestataire_id' => $p->id ,
            'date' => date('Y-m-d'),
            'heure' => date('H:i'),
            'payer' => 0,
            'etat' => "Validée"
        ]);


        $onesignal = new OneSignal;

        $onesignal->message = $message;
        $onesignal->matricule = $matricule;
        $onesignal->type = 'ordonnance';
        $onesignal->execute();

        session()->flash('message',"Actes médicaux enregistrés avec succès");
        return json_encode(["success"=>true]);
                
    }

    public function PrescriptionExam($acte_id, $m_id,$type){

        $a_id = $this->verifId($acte_id);
        $m_id = $this->verifId($m_id);
        $a = PrestaMuta::find($a_id);
        $typep = TypePrescription::all();

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }


        $pc = Prescription::where([
            ['presciptiontable_type', $prestamutable_type],
            ['presciptiontable_id',  $assure->id ],
            ['etat', 0 ],
            ['tampon', 1],
        ])->get();

        $retour = view('admin.prestataire.prescription.addexamen',compact('a','assure','type','typep','pc'))->render();
        return response()->json([
            'code' => $retour ,
        ]);
    }

    public function AddPrescriptionExam($m_id,$type){

        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        $m_id = $this->verifId($m_id);
        $typep = TypePrescription::all();

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $originalDate = Carbon::parse(date('Y-m-d'));
        $newDate = $originalDate->subDays(14);
        $actes = $assure->presta_mutua()->where('prestataire_id',$p->id)->where('etat',1)->where('tampon',0)->where('date', '>=',$newDate)->orderBy('id','desc')->Limit(3)->get();

        $retour = view('admin.prestataire.prescription.addprescription',compact('assure','type','typep','actes'))->render();
        return response()->json([
            'code' => $retour ,
        ]);
    }

    public function AddHospitalisationModal($m_id,$type){

        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        $m_id = $this->verifId($m_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $originalDate = Carbon::parse(date('Y-m-d'));
        $newDate = $originalDate->subDays(14);

        $actes =  PrestaMuta::join('liste_prestations', 'presta_mutas.liste_prestation_id', '=', 'liste_prestations.id')
            ->join('prestations', 'prestations.id', '=', 'liste_prestations.listeprestable_id')
            ->join('type_prestations', 'type_prestations.id', '=', 'prestations.type_prestation_id')
            ->where('type_prestations.libelle', 'CONSULTATIONS')
            ->where('presta_mutas.prestataire_id', $p->id)
            ->where('prestamutable_type', $prestamutable_type)
            ->where('prestamutable_id', $assure->id )
            ->where('presta_mutas.date', '>=',$newDate)
            ->where('presta_mutas.etat', 1)
            ->where('presta_mutas.tampon', 0)
            ->select('prestations.*', 'liste_prestations.*', 'presta_mutas.*')
        ->get();

        // $actes = $assure->presta_mutua()->where('prestataire_id',$p->id)->where('etat',1)->where('tampon',0)->where('date', '>=',$newDate)->orderBy('id','desc')->Limit(3)->get();

        $retour = view('admin.prestataire.hospitalisation.addhospitalisation',compact('assure','type','actes'))->render();
        return response()->json([
            'code' => $retour ,
        ]);
    }


    public function uploadFile(Request $request)
    {
        // Vérifier si le fichier a été correctement envoyé
        if ($request->hasFile('fichier_default') && $request->file('fichier_default')->isValid()) {
            $file = $request->file('fichier_default');
            
            $fileName = uniqid() . '_' . $file->getClientOriginalName();
            
            $file->move(public_path('/avis'), $fileName);
            
            return $fileName;
        }
        
        return '';
    }


    public function AddHospitalisation(Request $request){

        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        $liste_prestation = ListePrestation::where('libelle','Hospitalisation')->where('prestataire_id',$p->id)->first();
        $typeAss = $request->type;

        if(is_null($liste_prestation)){
           session()->flash('messageErreur',"L'action hospitalisation ne fait pas partie de vos prestations");
           return redirect()->back();
        }

        $m_id = $this->verifId($request->assure_id);
        $a = PrestaMuta::find($request->acte_id);
        
        $type = htmlspecialchars($request->type);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $hospitalisationtable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
            $ic =  "AD";
            $message = "Demande d'hopitalisation effectuée pour $assure->last_name $assure->first_name";
            $matricule = $assure->Mutualiste->registration_number;
        }else{
            $assure = Mutualiste::find($m_id);
            $hospitalisationtable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
            $ic =  "MU";
            $message = "Demande d'hopitalisation effectuée";
            $matricule = $assure->registration_number;
        }
        
        $codeR = date('y').$ic;
        $code = New Code();
        $code->code = $codeR;
        $code->etat   = 0;
        $code->tampon   = 1;
        $code->date   = date('Y-m-d');
        $code->heure   = date('H:i');
        $code->save();

        $path = $this->uploadFile($request);
    
        $hospitalisation = new Hospitalisation();
        $hospitalisation->hospitalisationtable_id =  $assure->id;
        $hospitalisation->nombre_de_jour =  $request->nombre_de_jour;
        $hospitalisation->motif_hospitalisation =  $request->motif_hospitalisation;
        $hospitalisation->fichier_default =  $path;
        $hospitalisation->hospitalisationtable_type =  $hospitalisationtable_type;
        $hospitalisation->description =  $request->description;
        $hospitalisation->docteur =  $request->docteur;
        $hospitalisation->tampon =  1;
        $hospitalisation->prestataire_id   = $a->prestataire_id;
        $hospitalisation->presta_muta_id =  $a->id;
        $hospitalisation->added_by   = Auth::User()->id;
        $hospitalisation->date   = date('Y-m-d');
        $hospitalisation->heure   = date('H:i');
        $hospitalisation->etat   = 0;
        $hospitalisation->fichier_update   = '';
        $hospitalisation->motif_status   = '';

        $hospitalisation->facture   = json_encode([[
            "quantite"=>1,
            "prestation_id"=>$a->ListePrestation->id,
            "prestation_name"=>$a->libelle,
            "part_assure"=>$a->ListePrestation->ParamlistePrestation->last()->parAssure,
            "part_muscopci"=>0,
            "prix"=>$a->ListePrestation->ParamlistePrestation->last()->cout
        ]]);
        $hospitalisation->code_id   = $code->id;
        $hospitalisation->save();

        $code->update([
            'code' => $codeR.$code->id,
        ]);


        $onesignal = new OneSignal;

        $onesignal->message = $message;
        $onesignal->matricule = $matricule;
        $onesignal->type = 'hospitalisation';
        $onesignal->execute();

        session()->flash('message',"Demande d'hospitalisation enregistrés avec succès");
        return redirect()->back();
    }

    
    public function PrescriptionOrdonnance($m_id,$type){

        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        $m_id = $this->verifId($m_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $originalDate = Carbon::parse(date('Y-m-d'));
        $newDate = $originalDate->subDays(14);
        $actes = $assure->presta_mutua()->where('prestataire_id',$p->id)->where('etat',1)->where('tampon',0)->where('date', '>=',$newDate)->orderBy('id','desc')->Limit(3)->get();


        $pharamacie_data = typePrestation::where('libelle','PHARMACIE')->first();
        $liste_prestations = ListePrestation::where('prestataire_id', $p->id)->whereIn('listeprestable_id',$pharamacie_data->Prestation->pluck('id'))
        ->get();

        $retour = view('admin.prestataire.ordonnance.addordonnance',compact('assure','type','actes','liste_prestations'))->render();
        return response()->json([
            'code' => $retour ,
        ]);
    }


    public function PrescriptionOrdonnanceByActe($m_id,$type,$acte_id){

        $m_id = $this->verifId($m_id);
        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $pharamacie_data = typePrestation::where('libelle','PHARMACIE')->first();
        $liste_prestations = ListePrestation::where('prestataire_id', $p->id)->whereIn('listeprestable_id',$pharamacie_data->Prestation->pluck('id'))
        ->get();

        $acte = PrestaMuta::find($acte_id);

        $retour = view('admin.prestataire.ordonnance.addordonnance',compact('assure','type','acte_id','acte','liste_prestations'))->render();
        return response()->json([
            'code' => $retour ,
        ]);
    }

    public function AddFactureHospitalisation($m_id,$type,$hospitalisation_id){

        $m_id = $this->verifId($m_id);
        $prestataire = Prestataire::where('user_id',Auth::user()->principal_id)->first();

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $hospitalisation = Hospitalisation::find($hospitalisation_id);
        // $hospitalisation_data = typePrestation::where('libelle','HOSPITALISATION')->first();
        // $liste_prestations = ListePrestation::where('prestataire_id', $prestataire->id)->whereIn('listeprestable_id',$hospitalisation_data->Prestation->pluck('id'))
        // ->get();
        $liste_prestations = ListePrestation::where('prestataire_id', $prestataire->id)->get();
        

        $prestationIds = $liste_prestations->pluck('listeprestable_id');

        $array_unique = array_unique($prestationIds->toArray());  

        $prestations = Prestation::whereIn('id', $array_unique);

        $retour = view('admin.prestataire.hospitalisation.addhospitalisationfacture',compact('assure','type','hospitalisation_id','hospitalisation','liste_prestations','prestations','prestataire'))->render();
        return response()->json([
            'code' => $retour ,
        ]);
    }

    public function AddExamen(Request $request, $acte_id, $ass_id, $type){

        $a_id = $this->verifId($acte_id);
        $m_id = $this->verifId($ass_id);
        $a = PrestaMuta::find($a_id);
        
        $type = htmlspecialchars($type);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $presciptiontable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
            $ic =  "AD";
            $message = "Nouvelle prescription pour $assure->last_name $assure->first_name";
            $matricule = $assure->Mutualiste->registration_number;
        }else{
            $assure = Mutualiste::find($m_id);
            $presciptiontable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
            $ic =  "MU";
            $message = "Vous avez une nouvelle prescription";
            $matricule = $assure->registration_number;
        }
        
        for ($i=0; $i < count($request->type_prescription_id); $i++) { 

            $codeR = date('y').$ic;
            $code = New Code();
            $code->code = $codeR;
            $code->etat   = 0;
            $code->tampon   = 1;
            $code->date   = date('Y-m-d');
            $code->heure   = date('H:i');
            $code->save();

        
            $p = new Prescription();
            $p->libelle =  $request->libelle[$i];
            $p->presciptiontable_id =  $assure->id;
            $p->presciptiontable_type =  $presciptiontable_type;
            $p->description =  $request->description[$i];
            $p->tampon =  1;
            $p->prestataire_id   = $a->prestataire_id;
            $p->type_prescription_id =  $request->type_prescription_id[$i];
            $p->presta_muta_id =  $a->id;
            $p->added_by   = Auth::User()->id;
            $p->date   = date('Y-m-d');
            $p->heure   = date('H:i');
            $p->etat   = 0;
            $p->code_id   = $code->id;
            $p->save();

            $code->update([
                'code' => $codeR.$code->id,
            ]);

        }


        $onesignal = new OneSignal;

        $onesignal->message = $message;
        $onesignal->matricule = $matricule;
        $onesignal->type = 'prescription';
        $onesignal->execute();

        session()->flash('message',"Prescription enregistrés avec succès");
        return redirect()->back();

    }

    public function AddOrdonnance(Request $request){

        $assure_id = $this->verifId($request->assure_id);
        $presta_muta = PrestaMuta::find($request->acte_id);
        
        $type = htmlspecialchars($request->type);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($assure_id);
            $prescriptiontable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
            $ic =  "AD";
            $message = "Nouvelle ordonnance pour $assure->last_name $assure->first_name";
            $matricule = $assure->Mutualiste->registration_number;
        }else{
            $assure = Mutualiste::find($assure_id);
            $prescriptiontable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
            $ic =  "MU";
            $message = "Vous avez une nouvelle ordonnance";
            $matricule = $assure->registration_number;
        }

        $medicaments = [];

        for ($i=0; $i < count($request->medicament); $i++) { 
            $medicaments[]=[
                    "prestation_id"=>$request->prestation_id[$i],
                    "medicament"=>$request->medicament[$i],
                    "quantite"=>$request->quantite[$i],
                    "prix"=>null,
                    "etat"=>0,
                    "prestataire_id"=>null,
            ];
        }

        $ordonnance = new Ordonnance;

        $codeR = date('y').$ic;
        $code = New Code();
        $code->code = $codeR;
        $code->etat   = 0;
        $code->tampon   = 1;
        $code->date   = date('Y-m-d');
        $code->heure   = date('H:i');
        $code->save();

        $ordonnance->medicaments = json_encode($medicaments);
        $ordonnance->prix = 0;
        $ordonnance->presta_muta_id = $presta_muta->id;
        $ordonnance->prescriptiontable_id = $assure->id;
        $ordonnance->prescriptiontable_type = $prescriptiontable_type;
        $ordonnance->prestataire_id = $presta_muta->prestataire_id;
        $ordonnance->added_by = Auth::User()->id;
        $ordonnance->date = date('Y-m-d');
        $ordonnance->heure = date('H:i');
        $ordonnance->code_id = $code->id;

        $code->update([
            'code' => $codeR.$code->id,
        ]);

        $ordonnance->save();

        $onesignal = new OneSignal;

        $onesignal->message = $message." CODE : $code->code";
        $onesignal->matricule = $matricule;
        $onesignal->type = 'ordonnace';
        $onesignal->execute();

        session()->flash('message',"Prescription d'ordonnance enregistrés avec succès CODE : $code->code");
        return redirect()->back();
    }

    public function AddHospitalisationFacture(Request $request){

        $hospitalisation = Hospitalisation::find($request->hospitalisation_id);
        $prestataire = Prestataire::where('user_id',Auth::user()->principal_id)->first();

        $facture = $hospitalisation->facture!='' ? json_decode($hospitalisation->facture) : [];

        for ($i=0; $i < count($request->quantite); $i++) { 

            $prestation = ListePrestation::find($request->prestation_id[$i]);


            if($prestation->libelle=='Pharmacie'){
                
                if($request->quantite[$i]>0 && $request->prestation_id[$i]!='' && $request->prestation_name[$i]!=''  && $request->prix[$i]!=''){

                    $part_muscopci = $request->prix[$i]*$prestataire->baseCouverture/100;
                    $part_assure = $request->prix[$i] - $part_muscopci;

                    $facture[]=[
                        "quantite"=>$request->quantite[$i],
                        "prestation_id"=>$request->prestation_id[$i],
                        "prestation_name"=>$request->prestation_name[$i],
                        "part_assure"=>$part_assure,
                        "part_muscopci"=>$part_muscopci,
                        "prix"=>$request->prix[$i]
                    ];
                }
                
            }else{

                if($request->quantite[$i]>0 && $request->prestation_id[$i]!='' && $request->prestation_name[$i]!=''  && $request->prix[$i]!=''){
                    $facture[]=[
                        "quantite"=>$request->quantite[$i],
                        "prestation_id"=>$request->prestation_id[$i],
                        "prestation_name"=>$request->prestation_name[$i],
                        "part_assure"=>$request->part_assure[$i],
                        "part_muscopci"=>$request->part_muscopci[$i],
                        "prix"=>$request->prix[$i]
                    ];
                }
            }
        }

        $hospitalisation->facture = json_encode($facture);
        $hospitalisation->save();
        
        session()->flash('message',"Ligne ajouter à l'hospitalisation avec succès");
        return redirect()->back();
    }

    public function AddPrescription(Request $request){

        $m_id = $this->verifId($request->assure_id);
        $a = PrestaMuta::find($request->acte_id);
        
        $type = htmlspecialchars($request->type);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $presciptiontable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
            $ic =  "AD";
            $message = "Nouvelle prescription pour $assure->last_name $assure->first_name";
            $matricule = $assure->Mutualiste->registration_number;
        }else{
            $assure = Mutualiste::find($m_id);
            $presciptiontable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
            $ic =  "MU";
            $message = "Vous avez une nouvelle prescription";
            $matricule = $assure->registration_number;
        }
        
        for ($i=0; $i < count($request->type_prescription_id); $i++) { 
            $codeR = date('y').$ic;
            $code = New Code();
            $code->code = $codeR;
            $code->etat   = 0;
            $code->tampon   = 1;
            $code->date   = date('Y-m-d');
            $code->heure   = date('H:i');
            $code->save();

        
            $p = new Prescription();
            $p->libelle =  $request->libelle[$i];
            $p->presciptiontable_id =  $assure->id;
            $p->presciptiontable_type =  $presciptiontable_type;
            $p->description =  $request->description[$i];
            $p->tampon =  1;
            $p->prestataire_id   = $a->prestataire_id;
            $p->type_prescription_id =  $request->type_prescription_id[$i];
            $p->presta_muta_id =  $a->id;
            $p->added_by   = Auth::User()->id;
            $p->date   = date('Y-m-d');
            $p->heure   = date('H:i');
            $p->etat   = 0;
            $p->code_id   = $code->id;
            $p->save();

            $code->update([
                'code' => $codeR.$code->id,
            ]);
        }


        $onesignal = new OneSignal;

        $onesignal->message = $message;
        $onesignal->matricule = $matricule;
        $onesignal->type = 'prescription';
        $onesignal->execute();

        session()->flash('message',"Prescription enregistrés avec succès");
        return redirect()->back();

    }


    public function verifId($id){
        $type = gettype($id);
        $id = htmlspecialchars($id);
        if ($id > 0 ) {
            return $id;
        }else{
            session()->flash('messageErreur',"Nous contatons une activité suspecte dans cette requette, Veuillez contactez le service informatique svp ");
            Auth::logout();
            return redirect('login');
        }
    }


}


