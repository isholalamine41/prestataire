<?php

namespace App\Http\Controllers\Detail;

use App\Http\Controllers\Controller;
use App\Models\Ayantdroit;
use Illuminate\Http\Request;
use App\Models\ProjetMutua;
use App\Models\User;
use App\Models\Mutualiste;
use App\Models\Versement;
use App\Models\Ligne;
use App\Models\Enregistrement;
use App\Models\ListePrestation;
use App\Models\PrestaMuta;
use App\Models\Prestataire;
use App\Models\Facture;
use App\Models\Prescription;
use App\Models\Ordonnance;
use App\Models\Hospitalisation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;


class DetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('detail');
    } 

    public function ProfilUser($id)
    {
       $mutualiste = Mutualiste::find($id);
       return view('mutualiste.profil', compact('mutualiste'));
    }

    public function DetailsPP($lp){
        $listP = ListePrestation::find($lp);
        return view('admin.prestataire.prestations.details', compact('listP'));
    }

    public function ActesMedicaux(){
       
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.actes_impaye', compact('actesi','actesp','p','demandes','ListePrestation'));

    }

    public function AllActesMedicaux(){
       
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();

        $dataQuery = PrestaMuta::query()->where('prestataire_id', $p->id)->get();
        $title = 'TOUT LES ACTES';

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.all_actes', compact('actesi','actesp','p','demandes','ListePrestation','dataQuery','title'));

    }


    public function ImpayerActesMedicaux(){
       
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();
        
        $dataQuery = $actesi;
        $title = 'TOUT LES ACTES IMPAYEES';

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.all_actes', compact('actesi','actesp','p','demandes','ListePrestation','dataQuery','title'));

    }


    public function PayerActesMedicaux(){
       
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();

        $dataQuery = $actesp;
        $title = 'TOUT LES ACTES PAYEES';

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.all_actes', compact('actesi','actesp','p','demandes','ListePrestation','dataQuery','title'));

    }

    public function DemandeActesMedicaux(){
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.demande', compact('p','demandes','ListePrestation'));
    }

    public function DemandeActesMedicauxAccordees(){
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();

        $query = PrestaMuta::query()->where('type','demande');

        $demandes = $query->where([
            ['prestataire_id', $p->id],
        ])->orderBy('id','desc')->get();

        $demandes_data = $query->where([
            ['prestataire_id', $p->id],
            ['entente', 'Accordée'],
        ])->orderBy('id','desc')->get();


        $ListePrestation = $p->ListePrestation()->get();
        $title = 'LISTE DES DEMANDES ACCORDEES';

        return view('admin.acte.all-demande', compact('p','demandes','demandes_data','ListePrestation','title'));
    }

    public function DemandeActesMedicauxRejetees(){
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $query = PrestaMuta::query()->where('type','demande');

        $demandes = $query->where([
            ['prestataire_id', $p->id],
        ])->orderBy('id','desc')->get();

        $demandes_data = $query->where([
            ['prestataire_id', $p->id],
            ['entente', 'Rejetée'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();
        $title = 'LISTE DES DEMANDES REJETEES';

        return view('admin.acte.all-demande', compact('p','demandes','demandes_data','ListePrestation','title'));
    }

    public function DemandeActesMedicauxAll(){
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $query = PrestaMuta::query()->where('type','demande');

        $demandes = $query->orderBy('id','desc')->get();
        $demandes_data = $demandes->get();

        $ListePrestation = $p->ListePrestation()->get();
        $title = 'LISTE DES DEMANDES REJETEES';

        return view('admin.acte.all-demande', compact('p','demandes','demandes_data','ListePrestation','title'));
    }

    public function DemandeActesMedicauxEnAttente(){
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $query = PrestaMuta::query()->where('type','demande');

        $demandes = $query->orderBy('id','desc')->get();
        $demandes_data = $query->where([
            ['prestataire_id', $p->id],
            ['entente', 'En attente'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();
        $title = 'LISTE DES DEMANDES REJETEES';

        return view('admin.acte.all-demande', compact('p','demandes','demandes_data','ListePrestation','title'));
    }

    public function FacturesActesMedicaux(){

        $prestataire = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $factures = Facture::where([
            ['prestataire_id', $prestataire->id],
            ['etat', 'Validée'],
        ])->orderBy('id','desc');
        
        $facturesMonth = $factures->whereMonth('date',date('m'))->get();
        
        $factures_all = $factures;
        $factures = $factures->get();
    

        return view('admin.facture.factures', compact('prestataire','factures','factures_all','facturesMonth'));
    }

    public function Prestations(){

        $prestataire = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        $prestations = ListePrestation::where([
            ['etat',1],
            ['hidden',0],
            ['prestataire_id',$prestataire->id],
            ['libelle','!=','Pharmacie'],
        ])->get();
        return view('admin.prestation.prestations', compact('prestataire','prestations'));
    }

    public function RechecheHospitalisation(Request $request){

        $user =  Auth::user();
        $prestataire = Prestataire::where('user_id',$user->principal_id)->first();
        $prestataire_id = $prestataire->id;

        $query = $prestataire->hospitalisations();

        if ($request->has('datedebut') && $request->input('datedebut')!='') {
            $query = $query->where('date', '>=', $request->input('datedebut'));
        }

        if ($request->has('datefin') && $request->input('datefin')!='') {
            $query = $query->where('date', '<=', $request->input('datefin'));
        }

        if ($request->has('type') && $request->input('type') !== 'all') {
            $query = $query->where('hospitalisationtable_type', $request->input('type'));
        }

        $resultats = $query->orderBy('id','desc')->get();

        if($resultats->count()>0): ?>

            <?php foreach($resultats as $hospitalisation): ?>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="ms-2 cat-name">
                                <p class="mb-0"><?=date("d/m/Y", strtotime($hospitalisation->date))?> à <?=$hospitalisation->heure?></p>
                            </div>	
                        </div>
                    </td>
                    <td><?=$hospitalisation->assurer->last_name ?? $hospitalisation->assurer->name?> <?=$hospitalisation->assurer->first_name?></td>
                    <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                        <?=$hospitalisation->Code->code?>
                    </td>
                    <td>Hospitalisation x <?=$hospitalisation->nombre_de_jour?> jour<?=$hospitalisation->nombre_de_jour>1 ? 's' : ''?> {!! $hospitalisation->etat==1 ? '<span class="badge badge-sm badge-success">Terminée</span>' : '' !!}</td>
                    <td class="text-center">
                        <a class="badge badge-primary lanceModal" href="<?=route('DetailsHospitalisation',[$hospitalisation->id])?>" class="lanceModal">
                            <?=count($hospitalisation->lignes_facture())?>
                        </a>
                    </td>
                    <td><?=$hospitalisation->amount()?></td>
                    <td><?=$hospitalisation->part_assure()?></td>
                    <td><?=$hospitalisation->part_muscopci()?></td>
                    <td><?= $hospitalisation->statut=='Accordée' || $hospitalisation->statut=='Corrigée' ? '<span class="badge badge-success">'.$hospitalisation->statut.'</span>' : ( $hospitalisation->statut=='Rejetée' ? '<span class="badge badge-danger">'.$hospitalisation->statut.'</span>' : '<span class="badge badge-primary">'.$hospitalisation->statut.'</span>') ?></td>
                    <td>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">ACTIONS</button>
                            <div class="dropdown-menu" style="">
                                <?php if($hospitalisation->statut=='Rejetée'): ?>
                                    <a class="dropdown-item text-dark lanceModal" href="<?=route('AddFactureHospitalisation',[$hospitalisation->hospitalisationtable_id, $hospitalisation->hospitalisationtable_type,$hospitalisation->id])?>" >AJOUTER DES ACTES</a>
                                    <a class="dropdown-item text-dark lanceModal" href="<?=route('DetailsHospitalisation',[$hospitalisation->id])?>" >LISTE DES ACTES</a>
                                    <a class="dropdown-item text-dark" href="<?=route('printHospitalisation',[$hospitalisation->id])?>" >FACTURE D'HOSPITALISATION</a>
                                <?php endif ?>
                                <?php if($hospitalisation->fichier_default!=''): ?>
                                    <a class="dropdown-item text-dark" href="<?=Storage::url($hospitalisation->fichier_default)?>" >AVIS D'HOSPITALISATION</a>
                                <?php endif ?>
                                <?php if($hospitalisation->statut=='Accordée' || $hospitalisation->statut=='Corrigée'): ?>
                                    <a class="dropdown-item text-dark" href="<?=route('endHospitalisation',[$hospitalisation->id])?>" >TERMINER L'HOSPITALISATION</a>
                                <?php endif ?>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach;

        endif;

    }

    public function RechecheOrdonnanceMedicaux(Request $request){

        $user =  Auth::user();
        $prestataire = Prestataire::where('user_id',$user->principal_id)->first();
        $prestataire_id = $prestataire->id;

        if($prestataire->typePrestataire->libelle=="PHARMACIE"){

            $query = Ordonnance::query()->where('medicaments','like','%"prestataire_id":'.$prestataire_id.'}%');

            if ($request->has('datedebut') && $request->input('datedebut')!='') {
                $query->where('date', '>=', $request->input('datedebut'));
            }

            if ($request->has('datefin') && $request->input('datefin')!='') {
                $query->where('date', '<=', $request->input('datefin'));
            }

            if ($request->has('type') && $request->input('type') !== 'all') {
                $query->where('prescriptiontable_type', $request->input('type'));
            }

            $resultats = $query->orderBy('id','desc')->get();

            if($resultats->count()>0): ?>

                <?php foreach($resultats as $ordonnance): ?>
                    <tr>
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="ms-2 cat-name">
                                    <p class="mb-0"><?=date("d/m/Y", strtotime($ordonnance->date))?> à <?=$ordonnance->heure?></p>
                                </div>	
                            </div>
                        </td>
                        <td><?=$ordonnance->assurer->last_name ?? $ordonnance->assurer->name?> <?=$ordonnance->assurer->first_name?></td>
                        <td><?=$ordonnance->presta_muta->libelle?></td>
                        <td class="text-center">
                            <div class="badge badge-primary">
                                <?=count($ordonnance->medicaments())?>
                            </div>
                        </td>
                        <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                            <?=$ordonnance->Code->code?>
                        </td>
                        <td>
                            <button href="<?=route('DetailsOrdonnanceParPrestatire',[$ordonnance->id])?>" class="btn btn-success lanceModal">
                                <i class="las la-eye"></i>
                            </button>
                        </td>
                    </tr>
                <?php endforeach;

            endif;
            
        }else{

            $query = $prestataire->ordonnances();

            if ($request->has('datedebut') && $request->input('datedebut')!='') {
                $query = $query->where('date', '>=', $request->input('datedebut'));
            }

            if ($request->has('datefin') && $request->input('datefin')!='') {
                $query = $query->where('date', '<=', $request->input('datefin'));
            }

            if ($request->has('type') && $request->input('type') !== 'all') {
                $query = $query->where('prescriptiontable_type', $request->input('type'));
            }

            $resultats = $query->orderBy('id','desc')->get();

            if($resultats->count()>0): ?>

                <?php foreach($resultats as $ordonnance): ?>
                    <tr>
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="ms-2 cat-name">
                                    <p class="mb-0"><?=date("d/m/Y", strtotime($ordonnance->date))?> à <?=$ordonnance->heure?></p>
                                </div>	
                            </div>
                        </td>
                        <td><?=$ordonnance->assurer->last_name ?? $ordonnance->assurer->name?> <?=$ordonnance->assurer->first_name?></td>
                        <td><?=$ordonnance->presta_muta->libelle?></td>
                        <td class="text-center">
                            <div class="badge badge-primary">
                                <?=count($ordonnance->medicaments())?>
                            </div>
                        </td>
                        <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                            <?=$ordonnance->Code->code?>
                        </td>
                        <td>
                            <button href="<?=route('DetailsOrdonnance',[$ordonnance->id])?>" class="btn btn-success lanceModal">
                                <i class="las la-eye"></i>
                            </button>
                        </td>
                    </tr>
                <?php endforeach;

            endif;
        }

    }

    public function RechecheActesMedicaux(Request $request){

        $prestataire = Prestataire::where('user_id', Auth::user()->principal_id)->first();

        $query = PrestaMuta::query()->where('prestataire_id', $prestataire->id);

        if ($request->has('datedebut') && $request->input('datedebut')!='') {
            $query->where('date', '>=', $request->input('datedebut'));
        }

        if ($request->has('datefin') && $request->input('datefin')!='') {
            $query->where('date', '<=', $request->input('datefin'));
        }

        if ($request->has('nature_id') && $request->input('nature_id') !== 'all') {
            $query->where('liste_prestation_id', $request->input('nature_id'));
        }

        if ($request->has('type') && $request->input('type') !== 'all') {
            $query->where('prestamutable_type', $request->input('type'));
        }

        if ($request->has('statut') && $request->input('statut')!='all') {
            $query->where('entente', $request->input('statut'));
        }

        if ($request->has('query')) {
            $query->where('type', 'demande');
        }

        $resultats = $query->orderBy('id','desc')->get();

        ?>

        <?php if($resultats->count()>0) : ?>
            <?php foreach($resultats as $ai): ?>
            <?php
                $data = $ai->prestamutable_type::find($ai->prestamutable_id);

                if($data->prestamutable_type == 'App\Models\Mutualiste'){
                    $typeAss = "Mutualiste";
                }else{
                    $typeAss = "Ayant-droit";
                }
            ?>
            <tr>
                <td class="txt-dark">
                    <div class="d-flex align-items-center">
                        <?php if(!$data->photo) : ?>
                            <img src="<?= \Illuminate\Support\Facades\URL::asset('erpfiles/dist/img/mock1.jpg') ?> " class="avatar avatar-md rounded-circle" alt="">
                        <?php else: ?>
                            <?php if($ai->prestamutable_type =='App\Models\Mutualiste'): ?>
                                <img src='<?= config("app.imgLink") ?><?= $data->photo ?> ' class="avatar avatar-md rounded-circle" alt="">
                            <?php else: ?>
                                <img src='<?= config("app.adLink") ?><?= $data->photo ?> ' class="avatar avatar-md rounded-circle" alt="">
                            <?php endif ?>
                        <?php endif ?>
                        <p class="mb-0 ms-2 txt-dark">
                            <a href="">
                                <?= $data->first_name ?>  <?= $data->last_name ?? $data->name ?> 
                            </a>
                        </p>	
                    </div>
                </td>
                <td class="txt-dark"><?= date('d/m/Y', strtotime($ai->date)) ?>  à <b><?= $ai->heure ?> </b></td>
                <td class="txt-dark"><?= $ai->libelle ?></td>
                
                <td class="txt-dark"><?= number_format($ai->part_assure+$ai->part_muscopci, 0, ',', ' ') ?>  FCFA</td>
                <td class="txt-dark"><b><?= number_format($ai->part_muscopci, 0, ',', ' ') ?>  FCFA</b></td>
                <td class="txt-dark"><b><?= number_format($ai->part_assure, 0, ',', ' ') ?> FCFA</b> </td>

                <?php if($request->has('query')): ?>
                    <td class="txt-dark">
                        <b><?php $ai->entente ?></b>
                    </td>
                <?php endif ?>
                
                <td>
                <button type="button" href="<?= route('ViewActeTampon', [$ai->id , $ai->prestamutable_id,$typeAss] ) ?> " class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
                </td>
            </tr>
            <?php endforeach ?>
        <?php endif ?>

        <?php

    }

    public function RechecheFacture(Request $request){

        $prestataire = Prestataire::where('user_id', Auth::user()->principal_id)->first();

        $query = Facture::query()->where('prestataire_id', $prestataire->id)
                                 ->where('etat', 'Validée');

        if ($request->has('datedebut') && $request->input('datedebut')!='') {
            $query->where('date', '>=', $request->input('datedebut'));
        }

        if ($request->has('datefin') && $request->input('datefin')!='') {
            $query->where('date', '<=', $request->input('datefin'));
        }

        if ($request->has('statut') && $request->input('statut')!='all') {
            $query->where('payer', $request->input('statut'));
        }

        $resultats = $query->orderBy('id','desc')->get();
        ?>
        <?php if($resultats->count()>0) : ?>
            <?php foreach($resultats as $facture): ?>
                
                <?php if($facture->PrestaMutua()->count()>0) : ?>
                <?php
                    $ajp = $facture->PrestaMutua()->first();
                    $data = $ajp->prestamutable_type::find($ajp->prestamutable_id);
                ?>
                <tr>
                    <td class="txt-dark">
                        <div class="d-flex align-items-center">
                            <?php if(!$data->photo): ?>
                                <img src="<?= \Illuminate\Support\Facades\URL::asset('erpfiles/dist/img/mock1.jpg')?>" class="avatar avatar-md rounded-circle" alt="">
                            <?php else: ?>
                                <?php if($ajp->prestamutable_type =='App\Models\Mutualiste'): ?>
                                    <img src='<?=config("app.imgLink")?><?=$data->photo?>' class="avatar avatar-md rounded-circle" alt="">
                                <?php else: ?>
                                    <img src='<?=config("app.adLink")?><?=$data->photo?>' class="avatar avatar-md rounded-circle" alt="">
                                <?php endif ?>
                            <?php endif ?>
                            <p class="mb-0 ms-2 txt-dark">
                                <a href="">
                                    <?=$data->first_name?> <?=$data->last_name ?? $data->name?>
                                </a>
                            </p>	
                        </div>
                    </td>
                    <td class="txt-dark"><?=date('d/m/Y', strtotime($facture->date))?> à <b><?=$facture->heure?></b></td>
                    <td class="txt-dark text-center" ><span class="badge badge-primary border-0"><?=$facture->PrestaMutua->count()?></span></td>
                    <td class="txt-dark"><?=number_format($facture->montant, 0, ',', ' ')?> FCFA</td>
                    <td class="txt-dark"><b><?=number_format($facture->part_muscopci, 0, ',', ' ')?> FCFA</b></td>
                    <td class="txt-dark"><b><?=number_format($facture->part_assure, 0, ',', ' ')?> FCFA</b> </td>
                    <td>
                        <button type="button" href="<?=route('ViewFacture', [$facture->id] )?>" class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
                    </td>
                </tr>
                <?php endif ?>
            <?php endforeach ?>
        <?php endif ?>

        <?php

    }

    public function UserPrescription($ass_id, $p_id, $type){
        $assure_id = $this->verifId($ass_id);
        $prestataire_id = $this->verifId($p_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }
        $originalDate = Carbon::parse(date('Y-m-d'));
        $newDate = $originalDate->subDays(14);

        $p = Prestataire::find($prestataire_id);
        $prestataire = $p;
        $pc = Prescription::where([
            ['presciptiontable_type', $prestamutable_type],
            ['presciptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['medicament_id',  null ],
            ['etat', 0 ],
            ['tampon', 1],
        ])->where('date', '>=',$newDate)->get();

        $po = Ordonnance::where([
            ['prescriptiontable_type', $prestamutable_type],
            ['prescriptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();

        $ph = Hospitalisation::where([
            ['hospitalisationtable_type', $prestamutable_type],
            ['hospitalisationtable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();
        
        
        $actesM = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $prestataire_id],
            ['tampon', 0],
            ['etat', 1],
        ])
        ->where('date', '>=',$newDate)
        ->orderBy('id','desc')->Limit(3)->get();


        $acteConsultation =  PrestaMuta::join('liste_prestations', 'presta_mutas.liste_prestation_id', '=', 'liste_prestations.id')
            ->join('prestations', 'prestations.id', '=', 'liste_prestations.listeprestable_id')
            ->join('type_prestations', 'type_prestations.id', '=', 'prestations.type_prestation_id')
            ->where('type_prestations.libelle', 'CONSULTATIONS')
            ->where('presta_mutas.prestataire_id', $prestataire->id)
            ->where('prestamutable_type', $prestamutable_type)
            ->where('prestamutable_id', $assure->id )
            ->where('presta_mutas.date', '>=',$newDate)
            ->where('presta_mutas.etat', 1)
            ->where('presta_mutas.tampon', 0)
            ->select('prestations.*', 'liste_prestations.*', 'presta_mutas.*')
        ->get();
        
        return view('admin.prestataire.prescription.userprescription',compact('pc','assure','type','p','po','ph','actesM','prestataire','acteConsultation'));

    }


    public function UserActesMedicaux($ass_id, $p_id, $type){
        
        $assure_id = $this->verifId($ass_id);
        $prestataire_id = $this->verifId($p_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }
        $originalDate = Carbon::parse(date('Y-m-d'));
        $newDate = $originalDate->subDays(14);

        $p = Prestataire::find($prestataire_id);
        $prestataire = $p;
        $pc = Prescription::where([
            ['presciptiontable_type', $prestamutable_type],
            ['presciptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['medicament_id',  null ],
            ['etat', 0 ],
            ['tampon', 1],
        ])->where('date', '>=',$newDate)->get();


        $ordonnances = Ordonnance::where([
            ['prescriptiontable_type', $prestamutable_type],
            ['prescriptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate);

        $ph = Hospitalisation::where([
            ['hospitalisationtable_type', $prestamutable_type],
            ['hospitalisationtable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();

        $actesM = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $prestataire_id],
            ['tampon', 0],
            ['etat', 1],
        ])
        ->where('date', '>=',$newDate)
        ->orderBy('id','desc')->Limit(3)->get();

        $po = Ordonnance::where([
            ['prescriptiontable_type', $prestamutable_type],
            ['prescriptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();

        $acteConsultation =  PrestaMuta::join('liste_prestations', 'presta_mutas.liste_prestation_id', '=', 'liste_prestations.id')
            ->join('prestations', 'prestations.id', '=', 'liste_prestations.listeprestable_id')
            ->join('type_prestations', 'type_prestations.id', '=', 'prestations.type_prestation_id')
            ->where('type_prestations.libelle', 'CONSULTATIONS')
            ->where('presta_mutas.prestataire_id', $prestataire->id)
            ->where('prestamutable_type', $prestamutable_type)
            ->where('prestamutable_id', $assure->id )
            ->where('presta_mutas.date', '>=',$newDate)
            ->where('presta_mutas.etat', 1)
            ->where('presta_mutas.tampon', 0)
            ->select('prestations.*', 'liste_prestations.*', 'presta_mutas.*')
        ->get();

        return view('admin.prestataire.actes.useracte',compact('pc','assure','type','p','ordonnances','ph','actesM','prestataire','po','acteConsultation'));
    }

    public function UserOrdonnance($ass_id, $p_id, $type){
        
        $assure_id = $this->verifId($ass_id);
        $prestataire_id = $this->verifId($p_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }
        $originalDate = Carbon::parse(date('Y-m-d'));
        $newDate = $originalDate->subDays(14);

        $p = Prestataire::find($prestataire_id);
        $prestataire = $p;
        $pc = Prescription::where([
            ['presciptiontable_type', $prestamutable_type],
            ['presciptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['medicament_id',  null ],
            ['etat', 0 ],
            ['tampon', 1],
        ])->where('date', '>=',$newDate)->get();


        $ordonnances = Ordonnance::where([
            ['prescriptiontable_type', $prestamutable_type],
            ['prescriptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate);

        $ph = Hospitalisation::where([
            ['hospitalisationtable_type', $prestamutable_type],
            ['hospitalisationtable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();


        $po = Ordonnance::where([
            ['prescriptiontable_type', $prestamutable_type],
            ['prescriptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();


        $actesM = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $prestataire_id],
            ['tampon', 0],
            ['etat', 1],
        ])
        ->where('date', '>=',$newDate)
        ->orderBy('id','desc')->Limit(3)->get();


        $acteConsultation =  PrestaMuta::join('liste_prestations', 'presta_mutas.liste_prestation_id', '=', 'liste_prestations.id')
            ->join('prestations', 'prestations.id', '=', 'liste_prestations.listeprestable_id')
            ->join('type_prestations', 'type_prestations.id', '=', 'prestations.type_prestation_id')
            ->where('type_prestations.libelle', 'CONSULTATIONS')
            ->where('presta_mutas.prestataire_id', $prestataire->id)
            ->where('prestamutable_type', $prestamutable_type)
            ->where('prestamutable_id', $assure->id )
            ->where('presta_mutas.date', '>=',$newDate)
            ->where('presta_mutas.etat', 1)
            ->where('presta_mutas.tampon', 0)
            ->select('prestations.*', 'liste_prestations.*', 'presta_mutas.*')
        ->get();

        return view('admin.prestataire.ordonnance.userordonnance',compact('pc','assure','type','p','ordonnances','ph','actesM','prestataire','po','acteConsultation'));
    }


    public function UserHospitalisation($ass_id, $p_id, $type){
        
        $assure_id = $this->verifId($ass_id);
        $prestataire_id = $this->verifId($p_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }
        $originalDate = Carbon::parse(date('Y-m-d'));
        $newDate = $originalDate->subDays(14);

        $p = Prestataire::find($prestataire_id);
        $prestataire = $p;
        $pc = Prescription::where([
            ['presciptiontable_type', $prestamutable_type],
            ['presciptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['medicament_id',  null ],
            ['etat', 0 ],
            ['tampon', 1],
        ])->where('date', '>=',$newDate)->get();


        $ordonnances = Ordonnance::where([
            ['prescriptiontable_type', $prestamutable_type],
            ['prescriptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate);

        $ph = Hospitalisation::where([
            ['hospitalisationtable_type', $prestamutable_type],
            ['hospitalisationtable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();


        $actesM = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $prestataire_id],
            ['tampon', 0],
            ['etat', 1],
        ])
        ->where('date', '>=',$newDate)
        ->orderBy('id','desc')->Limit(3)->get();


        $po = Ordonnance::where([
            ['prescriptiontable_type', $prestamutable_type],
            ['prescriptiontable_id',  $assure->id ],
            ['prestataire_id',  $prestataire_id ],
            ['etat', 0 ],
        ])->where('date', '>=',$newDate)->get();

        $actesM = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $prestataire_id],
            ['tampon', 0],
            ['etat', 1],
        ])
        ->where('date', '>=',$newDate)
        ->orderBy('id','desc')->Limit(3)->get();

        $acteConsultation =  PrestaMuta::join('liste_prestations', 'presta_mutas.liste_prestation_id', '=', 'liste_prestations.id')
            ->join('prestations', 'prestations.id', '=', 'liste_prestations.listeprestable_id')
            ->join('type_prestations', 'type_prestations.id', '=', 'prestations.type_prestation_id')
            ->where('type_prestations.libelle', 'CONSULTATIONS')
            ->where('presta_mutas.prestataire_id', $prestataire->id)
            ->where('prestamutable_type', $prestamutable_type)
            ->where('prestamutable_id', $assure->id )
            ->where('presta_mutas.date', '>=',$newDate)
            ->where('presta_mutas.etat', 1)
            ->where('presta_mutas.tampon', 0)
            ->select('prestations.*', 'liste_prestations.*', 'presta_mutas.*')
        ->get();
        
        return view('admin.prestataire.hospitalisation.userhospitalisation',compact('pc','assure','type','p','ordonnances','ph','actesM','prestataire','po','actesM','acteConsultation'));
    }

    
    public function verifId($id){
        $type = gettype($id);
        $id = htmlspecialchars($id);
        if ($id > 0 ) {
            return $id;
        }else{
            session()->flash('messageErreur',"Nous contatons une activité suspecte dans cette requette, Veuillez contactez le service informatique svp ");
            Auth::logout();
            return redirect('login');
        }
    }


    public function DetailsOrdonnance($id){

        $ordonnance = Ordonnance::find($id);

        $retour = view('admin.prestataire.ordonnance.details-ordonnance',compact('ordonnance'))->render();

        return response()->json([
            'code' => $retour ,
        ]);

    }

    public function DetailsHospitalisation($id){

        $hospitalisation = Hospitalisation::find($id);

        $retour = view('admin.prestataire.hospitalisation.details-hospitalisation-prestataire',compact('hospitalisation'))->render();

        return response()->json([
            'code' => $retour ,
        ]);

    }


    public function DetailsOrdonnanceParPrestatire($id){

        $user =  Auth::user();
        $prestataire = Prestataire::where('user_id',$user->principal_id)->first();
        $ordonnance = Ordonnance::find($id);

        $retour = view('admin.prestataire.ordonnance.details-ordonnance-prestataire',compact('ordonnance','prestataire'))->render();

        return response()->json([
            'code' => $retour ,
        ]);

    }

    public function OrdonnanceMedicaux(){

        $user =  Auth::user();
        $prestataire = Prestataire::where('user_id',$user->principal_id)->first();
        $prestataire_id = $prestataire->id;

        if($prestataire->typePrestataire->libelle=="PHARMACIE"){

            $data = Ordonnance::where('medicaments','like','%"prestataire_id":'.$prestataire_id.'}%');
            $data_en_ligne = Ordonnance::where('medicaments','like','%"prestataire_id":'.$prestataire_id.'}%')->where('etat',1)->count();
            $data_traiter = Ordonnance::where('medicaments','like','%"prestataire_id":'.$prestataire_id.'}%')->where('etat',0)->count();
            $prestations = $prestataire->presta_mutua()->where("libelle","pharmacie");
            $total_prestation = $prestations->sum('cout');
            $total_muscopci = $prestations->sum('part_muscopci');
            $total_assure = $prestations->sum('part_assure');

            return view('admin.prestataire.ordonnance.liste-ordonnance', compact('total_prestation','total_muscopci','total_assure','data','data_en_ligne','data_traiter'));
            
        }else{
            
            $data = $prestataire->ordonnances();
            $data_en_ligne = $prestataire->ordonnances()->where('etat',1)->count();
            $data_traiter = $prestataire->ordonnances()->where('etat',0)->count();
            return view('admin.prestataire.ordonnance.liste-ordonnance-prestataire', compact('data','data_en_ligne','data_traiter'));
        }
        
    }

    public function Hospitalisations(){

        $user =  Auth::user();
        $prestataire = Prestataire::where('user_id',$user->principal_id)->first();
        $prestataire_id = $prestataire->id;

        $data = $prestataire->hospitalisations();
        $en_cours = $prestataire->hospitalisations()->where('tampon',1)->count();
        $traiter = $prestataire->hospitalisations()->where('etat',1)->count();

        return view('admin.prestataire.hospitalisation.liste-hospitalisation-prestataire', compact('data','en_cours','traiter'));
        
    }
    

}
