<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Right;
use App\Models\User;
use App\Models\UserMenu;
use Illuminate\Support\Facades\Auth;

class Prestataire
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (Auth::user()->Typeuser === 'Prestataire'  and Auth::user()->active == 1) {
            $user =  Auth::user();
            foreach (Auth::user()->UserMenu as $Up) {
                if ($Up->libelle === 'All-Menu-Prestataire' or $Up->libelle === 'PRISE EN CHARGE') {
                    return $next($request);
                }
            }
            return response()->json([
            'message' => '
                <div class="modal fade bd-example-modal-sm ModalAcces">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header" style="background-color:#c52727; font-weight:bold; font-size:20px;">
                            <h5 class="modal-title" id="exampleModalLabel" style="color: #fff;"><i style="font-size:32px;" class="las la-sign-in-alt"></i>&ensp; &ensp; DROIT D\'ACCES</h5>
                        </div>
                        <div class="modal-body" style="color:#000; font-size: 16px;">Vous n\'avez pas les droits d\'accès à cette page. Merci de bien vouloir contacter l\'administrateur général</div>
                        
                        </div>
                    </div>
                </div>
            ',
            'statut' => 'messageErreur',
            'title' => 'DROITS D\'ACCES',
            'type' => 'error',
            ]);  
            return redirect()->back();
       }else{
         //dd(Auth::user());
         session()->flash('messageErreur', 'Vous n\'êtes pas habilité à consulter cette page.');
         return redirect()->back();
       } 
    }
}
