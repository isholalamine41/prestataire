<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Right;
use App\Models\User;
use App\Models\Userright;

use Illuminate\Support\Facades\Auth;

class Create
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user =  Auth::user();
        //dd($user->Userright);
        if($user->active and Auth::user()->Typeuser === 'Prestataire'){
            foreach ($user->UserRight as $Ur) {
                if ($Ur->libelle === 'All-Right-Prestataire' or $Ur->libelle === 'Create-Right-Prestataire') {
                    return $next($request);
                }
            }
            return response()->json([
            'message' => 'Vous n\'avez pas les droits d\'ajout. Merci de bien vouloir contacter l\'administrateur général',
            'statut' => 'messageErreur',
            'title' => 'DROITS D\'ACCES',
            'type' => 'error',
            ]);
        }
        
    }
}
   