<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatehospitalisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitalisations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('facture_id')->nullable();
            $table->unsignedBigInteger('prestataire_id')->nullable();
            $table->unsignedBigInteger('hospitalisationtable_id');
            $table->unsignedBigInteger('presta_muta_id')->nullable();
            $table->string('hospitalisationtable_type', 191);
            $table->unsignedBigInteger('added_by');
            $table->bigInteger('code_id');
            $table->bigInteger('nombre_de_jour');
            $table->longText('motif_hospitalisation');
            $table->longText('motif_status');
            $table->string('docteur', 191)->default('Dr');
            $table->string('fichier_default', 191);
            $table->string('fichier_update', 191);
            $table->longText('facture')->nullable();
            $table->longText('facture_modifier')->nullable();
            $table->longText('facture_finale')->nullable();
            $table->longText('description')->nullable();
            $table->tinyInteger('tampon')->default(1);
            $table->tinyInteger('etat')->default(1);
            $table->date('date');
            $table->string('heure', 191);
            $table->timestamps();

            




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitalisations');
    }
}
