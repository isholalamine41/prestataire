<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    { 
       

        Schema::create('liste_prestations', function (Blueprint $table) {
            $table->id();
            $table->integer('listeprestable_id');
            $table->string('listeprestable_type');
            $table->integer('param_prestation_id')->nullable();
            $table->string('libelle');
            $table->integer('prestataire_id');
            $table->integer('paramliste_prestation_id')->nullable();
            $table->integer('added_by');
            $table->integer('cout')->nullable();
            $table->integer('qte')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('etat')->default(true);
            $table->boolean('ententePrealable')->default(false);
            $table->boolean('entente')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('liste_prestations');
    }
};
