<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prestations', function (Blueprint $table) {
            $table->id();
            $table->integer('type_prestation_id');
            $table->integer('CoefiLettre')->nullable();
            $table->integer('taux')->nullable();
            $table->integer('plafond')->nullable();
            $table->string('libelle');
            $table->integer('added_by');
            $table->boolean('exclusion')->default(false);
            $table->longText('description')->nullable();
            $table->boolean('etat')->default(true);
            $table->boolean('ententePrealable')->default(false);
            $table->boolean('entente')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prestations');
    }
};
