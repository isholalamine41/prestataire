<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('baremes', function (Blueprint $table) {
            $table->id();
            $table->string('libelle');
            $table->integer('baremetable_id');
            $table->integer('type_prestation_id');
            $table->string('baremetable_type');
            $table->integer('prestation_id');
            $table->date('lastedate')->nullable();

            $table->integer('consoBeneficiairePrestation')->nullable();
            $table->integer('consoFamillePrestation')->nullable();
            $table->integer('plafondBeneficiairePrestation')->nullable();
            $table->integer('plafondFamillePrestation')->nullable();
            $table->integer('qteUtilise')->nullable();
            $table->integer('plafondGeneraleFamillle')->nullable();
            $table->integer('plafondGeneraleBeneficiaire')->nullable();

            $table->string('description')->nullable();
            $table->date('datedebut')->nullable();
            $table->date('datefin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('baremes');
    }
};
