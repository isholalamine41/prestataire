<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('medecins', function (Blueprint $table) {
            
            $table->id();
            $table->integer('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username');

            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->string('sex')->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->string('photo')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('birth_place')->nullable();
            $table->string('bio')->nullable();
            $table->string('residence')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('cv')->nullable();
            $table->integer('added_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medecins');
    }
};
