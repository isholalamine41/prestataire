<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('paramptps', function (Blueprint $table) {
            $table->id();
            $table->string('libelle');
            $table->string('lettre')->nullable();
            $table->integer('valeurLettre')->nullable();
            $table->integer('cout')->nullable();
            $table->date('datedebut')->nullable();
            $table->date('datefin')->nullable();
            $table->integer('annee')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('paramptps');
    }
};
