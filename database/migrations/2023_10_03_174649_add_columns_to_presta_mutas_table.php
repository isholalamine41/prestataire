<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToPrestaMutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('presta_mutas', function (Blueprint $table) {
            $table->integer('nombre_de_jour')->nullable();
            $table->string('motif_hospitalisation')->nullable();
            $table->string('motif_status')->nullable();
            $table->string('fichier_default')->nullable();
            $table->string('fichier_update')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('presta_mutas', function (Blueprint $table) {
            $table->dropColumn('nombre_de_jour');
            $table->dropColumn('motif_hospitalisation');
            $table->dropColumn('motif_status');
            $table->dropColumn('fichier_default');
            $table->dropColumn('fichier_update');
        });
    }
}
