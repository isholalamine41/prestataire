<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdonnancesTable extends Migration
{
    public function up()
    {
        Schema::create('ordonnances', function (Blueprint $table) {
            $table->id();
            $table->longText('medicaments');
            $table->decimal('prix', 10, 2);
            $table->unsignedBigInteger('presta_muta_id');
            $table->integer('nb_renouvellement')->default(0);
            $table->boolean('etat')->default(0);
            $table->unsignedBigInteger('prescriptiontable_id');
            $table->string('prescriptiontable_type');
            $table->unsignedBigInteger('prestataire_id');
            $table->unsignedBigInteger('added_by');
            $table->date('date');
            $table->time('heure');
            $table->unsignedBigInteger('code_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ordonnances');
    }
}
