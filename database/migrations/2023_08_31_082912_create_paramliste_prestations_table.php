<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('paramliste_prestations', function (Blueprint $table) {
            $table->id();
            $table->string('libelle');
            $table->integer('cout');
            $table->integer('prestataire_id');
            $table->integer('tierPayant'); 
            $table->integer('parAssure');
            $table->integer('liste_prestation_id'); 
            $table->integer('added_by');

            $table->string('fourmulecout')->nullable(); 
            $table->string('fourmuletp')->nullable(); 
            $table->integer('CoefiLettre')->nullable(); 
            $table->integer('annee')->nullable();
            $table->string('description')->nullable();
            $table->date('datedebut')->nullable();
            $table->date('datefin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('paramliste_prestations');
    }
};
