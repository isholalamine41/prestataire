<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('medicaments', function (Blueprint $table) {
            $table->id();
            $table->integer('codePro');
            $table->string('libelle');
            $table->string('groupe')->nullable();
            $table->string('dci')->nullable();
            $table->string('categorie')->nullable();
            $table->string('type')->nullable();
            $table->string('prix');
            $table->string('regime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medicaments');
    }
};
