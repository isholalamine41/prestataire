<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandesTable extends Migration
{
    public function up()
    {
        Schema::create('demandes', function (Blueprint $table) {
            $table->id(); // Clé primaire auto-incrémentée
            $table->string('facture_numerique');
            $table->date('date_traitement');
            $table->string('type_demande');
            $table->string('type_assure');
            $table->unsignedBigInteger('id_assure'); // Clé étrangère pour relier à la table d'assurés
            $table->decimal('montant', 10, 2); // Vous pouvez ajuster la précision et l'échelle en fonction de vos besoins
            $table->text('description')->nullable(); // La description est facultative

            $table->timestamps(); // Champs de date de création et de mise à jour
        });
    }

    public function down()
    {
        Schema::dropIfExists('demandes');
    }
}
