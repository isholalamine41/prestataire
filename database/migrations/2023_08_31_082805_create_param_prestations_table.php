<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
       
        
        Schema::create('param_prestations', function (Blueprint $table) {
            $table->id();
            $table->integer('CoefiLettre')->nullable();
            $table->integer('taux')->nullable();
            $table->integer('plafond')->nullable();
            $table->integer('prestation_id');
            $table->string('libelle');
            $table->integer('added_by');
            $table->integer('qte')->nullable();
            $table->integer('duree')->nullable();
            $table->string('entite')->nullable();
            $table->integer('annee')->nullable();
            $table->string('description')->nullable();
            $table->date('datedebut')->nullable();
            $table->date('datefin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('param_prestations');
    }
};
