<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('presta_mutas', function (Blueprint $table) {
            $table->id();
            $table->integer('liste_prestation_id');
            $table->string('libelle');
            $table->integer('medecin_id')->nullable();
            $table->integer('facture_id')->nullable();
            $table->integer('prestataire_id');
            $table->integer('prestamutable_id');
            $table->string('prestamutable_type');
            $table->integer('added_by');
            $table->integer('cout')->default(0);
            $table->integer('part_assure')->default(0);
            $table->integer('part_muscopci')->default(0);
            $table->string('docteur')->default('Dr');
            $table->longText('description')->nullable();
            $table->string('entente')->nullable();
            $table->boolean('tampon')->default(1);
            $table->string('type')->nullable();
            $table->boolean('etat')->default(1);
            $table->date('date');
            $table->string('heure');
            $table->string('qte')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('presta_mutas');
    }
};
