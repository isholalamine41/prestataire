<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('prestataire_type_prestations', function (Blueprint $table) {
            $table->id();
            $table->string('libelle');
            $table->integer('type_prestation_id');
            $table->integer('prestataire_id');

            $table->string('lettre')->nullable();
            $table->integer('valeurLettre')->nullable();
            $table->integer('plafondMutuelle')->nullable();
            $table->integer('valeurLettreMutuelle')->nullable();

            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prestataire_type_prestations');
    }
};
