<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    { 
   
        Schema::create('prestataires', function (Blueprint $table) {
            $table->id();
            $table->integer('commune_id');
            $table->integer('user_id');
            $table->integer('type_prestataire_id');
            $table->string('rs');
            $table->integer('baseCouverture');
            $table->integer('added_by');

            $table->string('situation_geo')->nullable();
            $table->string('rib')->nullable();
            $table->string('banque')->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->string('logo')->nullable();

            $table->string('whathsapp')->nullable();
            $table->string('nomDG')->nullable();
            $table->string('emailDG')->nullable();
            $table->string('nomComptable')->nullable();
            $table->string('contactComptable')->nullable();
            $table->string('emailComptable')->nullable();
            $table->string('nomRespo')->nullable();
            $table->string('emailRespo')->nullable();
            $table->boolean('etat')->default(true);
           
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prestataires');
    }
};
