<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

#get 
Route::get('/reseau-de-soins', 'App\Http\Controllers\Api\ApiController@ReseauDeSoins');
Route::get('/type-prestation', 'App\Http\Controllers\Api\ApiController@TypePrestation');
Route::get('/reseau-de-pharmacie', 'App\Http\Controllers\Api\ApiController@ReseauDePharmacie');
Route::get('/get-actes', 'App\Http\Controllers\Api\ApiController@GetActesByAssure');
Route::get('/get-actes-by-prestataire', 'App\Http\Controllers\Api\ApiController@GetActesByPrestataire');
Route::get('/get-demandes-actes', 'App\Http\Controllers\Api\ApiController@GetDemandesActesByAssure');
Route::get('/get-demandes-actes-by-prestataire', 'App\Http\Controllers\Api\ApiController@GetDemandesActesByPrestataire');
Route::get('/get-ordonnances', 'App\Http\Controllers\Api\ApiController@GetOrdonnanceByAssure');
Route::get('/get-ordonnances-by-prestataire', 'App\Http\Controllers\Api\ApiController@GetOrdonnanceByPrestataire');
Route::get('/get-hospitalisations-by-prestataire', 'App\Http\Controllers\Api\ApiController@GetHospitalisationByPrestataire');
Route::get('/get-hospitalisations', 'App\Http\Controllers\Api\ApiController@GetHospitalisationByAssure');
Route::get('/get-demandes', 'App\Http\Controllers\Api\ApiController@GetDemandeByAssure');
Route::get('/get-assures', 'App\Http\Controllers\Api\ApiController@GetAssure');
Route::get('/get-assures-datas', 'App\Http\Controllers\Api\ApiController@GetAssureData');
Route::get('/get-init', 'App\Http\Controllers\Api\ApiController@GetInit');
Route::get('/get-demandes-doctor', 'App\Http\Controllers\Api\ApiController@GetDemandes');
Route::get('/get-factures', 'App\Http\Controllers\Api\ApiController@GetFactures');
Route::get('/get-assures-all', 'App\Http\Controllers\Api\ApiController@GetAssures');

#post
Route::post('/add-renouvellement', 'App\Http\Controllers\Api\ApiController@AddRenouvellement');
Route::post('/note', 'App\Http\Controllers\Api\ApiController@NotePrestataire');
Route::post('/suggestion', 'App\Http\Controllers\Api\ApiController@SuggestionPrestataire');
Route::post('/signal', 'App\Http\Controllers\Api\ApiController@SignalPrestataire');
Route::post('/add-demande-ambulatoire', 'App\Http\Controllers\Api\ApiController@AddDemandeAmbulatoire');
Route::post('/add-demande-hospitalisation', 'App\Http\Controllers\Api\ApiController@AddDemandeHospitalisation');
Route::post('/login-doctor', 'App\Http\Controllers\Api\ApiController@loginDoctor');
Route::post('/rejeter-hospitalisation', 'App\Http\Controllers\Api\ApiController@RejeterHospitalisation');
Route::post('/rejeter-renouvellement', 'App\Http\Controllers\Api\ApiController@RejeterRenouvellement');
Route::post('/rejeter-acte', 'App\Http\Controllers\Api\ApiController@RejeterActe');
Route::post('/valider-hospitalisation', 'App\Http\Controllers\Api\ApiController@ValiderHospitalisation');
Route::post('/valider-renouvellement', 'App\Http\Controllers\Api\ApiController@ValiderRenouvellement');
Route::post('/valider-acte', 'App\Http\Controllers\Api\ApiController@ValiderActe');





