<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

//GESTION MODULE ADMIN
Route::group(['prefix' => 'sante/muscop-ci/prestataire' , 'middleware' => 'auth'], function(){
    Route::group(['namespace' => 'App\Http\Controllers\Admin' , 'middleware' => 'admin'], function () {
        Route::get('/administrateur', 'AdminController@index')->name('dashbord');
        Route::get('/gestion-des-droits', 'AdminController@GestionDesDroits')->name('GestionDesDroits');
        Route::get('/add-user', 'AdminController@AddUser')->name('AddUser');
        Route::get('/messagerie', 'AdminController@Messagerie')->name('Messagerie');
        Route::get('/update-users/{id}', 'AdminController@updateUser')->name('updateUser');
        Route::post('/ajouter-user-prestataire', 'AdminController@AddUserPrestataire')->name('AddUserPrestataire');
        Route::post('/update-user-prestataire/{id}', 'AdminController@UpdateUserPrestataire')->name('UpdateUserPrestataire');
    });
});



//Update
Route::group(['prefix' => 'dashbord/update' , 'middleware' => 'auth'], function(){
    
    Route::group(['namespace' => 'App\Http\Controllers\Update' , 'middleware' => 'update'], function () {
        
        
    });

});

//Delete
Route::group(['prefix' => 'dashbord/delete' , 'middleware' => 'auth'], function(){
    Route::group(['namespace' => 'App\Http\Controllers\Delete' , 'middleware' => 'delete'], function () {
        Route::get('/delete-acte-tampon/{id}/{mid}/{pype}', 'DeleteController@DeleteActeTampon')->name('DeleteActeTampon');
        Route::get('/delete-all-acte-tampon/{pid}/{mid}/{type}', 'DeleteController@deleteALLACTETamPon')->name('deleteALLACTETamPon');
    });

});

//VIEWS
Route::group(['prefix' => 'dashbord/details' , 'middleware' => 'auth'], function(){
        Route::group(['namespace' => 'App\Http\Controllers\Detail' , 'middleware' => 'detail'], function () {
            //get
        // Route::get('/voir-acte-tampon/{acte_id}/{m_id}', 'DetailController@ViewActeTampon')->name('ViewActeTampon');
        Route::get('/liste-des-actes-medicaux', 'DetailController@ActesMedicaux')->name('ActesMedicaux');
        Route::get('/tous-des-actes-medicaux', 'DetailController@AllActesMedicaux')->name('AllActesMedicaux');
        Route::get('/actes-medicaux-impayes', 'DetailController@ImpayerActesMedicaux')->name('ImpayerActesMedicaux');
        Route::get('/actes-medicaux-payes', 'DetailController@PayerActesMedicaux')->name('PayerActesMedicaux');
        Route::get('/demande-actes', 'DetailController@DemandeActesMedicaux')->name('DemandeActesMedicaux');
        Route::get('/demande-all', 'DetailController@DemandeActesMedicauxAll')->name('DemandeActesMedicauxAll');
        Route::get('/demande-accordees', 'DetailController@DemandeActesMedicauxAccordees')->name('DemandeActesMedicauxAccordees');
        Route::get('/demande-rejetees', 'DetailController@DemandeActesMedicauxRejetees')->name('DemandeActesMedicauxRejetees');
        Route::get('/demande-en-attente', 'DetailController@DemandeActesMedicauxEnAttente')->name('DemandeActesMedicauxEnAttente');
        Route::get('/factures', 'DetailController@FacturesActesMedicaux')->name('FacturesActesMedicaux');
        Route::get('/prestations', 'DetailController@Prestations')->name('Prestations');
        Route::get('/ordonnances', 'DetailController@OrdonnanceMedicaux')->name('OrdonnanceMedicaux');
        Route::get('/hospitalisations', 'DetailController@Hospitalisations')->name('Hospitalisations');
        Route::get('/prescription-assure/{a}/{p}/{type}', 'DetailController@UserPrescription')->name('UserPrescription');
        Route::get('/ordonnance-assure/{a}/{p}/{type}', 'DetailController@UserOrdonnance')->name('UserOrdonnance');
        Route::get('/actes-medicaux-assure/{a}/{p}/{type}', 'DetailController@UserActesMedicaux')->name('UserActesMedicaux');
        Route::get('/hospitalisation-assure/{a}/{p}/{type}', 'DetailController@UserHospitalisation')->name('UserHospitalisation');
        Route::get('/add-prescription-assure/{a}/{p}/{type}', 'DetailController@AddUserPrescription')->name('AddUserPrescription');
        Route::get('/details-ordonnance/{id}', 'DetailController@DetailsOrdonnance')->name('DetailsOrdonnance'); 
        Route::get('/details-hospitalisation/{id}', 'DetailController@DetailsHospitalisation')->name('DetailsHospitalisation'); 
        Route::get('/details-ordonnance-prestatire/{id}', 'DetailController@DetailsOrdonnanceParPrestatire')->name('DetailsOrdonnanceParPrestatire'); 
        
        #recherche ajax
        Route::get('/recherche-ordonnance', 'DetailController@RechecheOrdonnanceMedicaux')->name('RechecheOrdonnanceMedicaux');
        Route::get('/recherche-hospitalisation', 'DetailController@RechecheHospitalisation')->name('RechecheHospitalisation');
        Route::get('/recherche-actes', 'DetailController@RechecheActesMedicaux')->name('RechecheActesMedicaux');
        Route::get('/recherche-facture', 'DetailController@RechecheFacture')->name('RechecheFacture');

        // Post
       
    });
});

//CREATE
Route::group(['prefix' => 'dashbord/create' , 'middleware' => 'auth'], function(){
    Route::group(['namespace' => 'App\Http\Controllers\Create' , 'middleware' => 'create'], function () {
        //poste
        Route::post('/add-acte', 'CreateController@AddActes')->name('AddActes'); 
        Route::get('/validation-acte/{pid}/{mid}/{type}', 'CreateController@ValidateAllActes')->name('ValidateAllActes'); 
        Route::get('/prescriptio-examen/{pid}/{mid}/{type}', 'CreateController@PrescriptionExam')->name('PrescriptionExam');
        Route::get('/prescription-examen-add/{mid}/{type}', 'CreateController@AddPrescriptionExam')->name('AddPrescriptionExam'); 
        Route::get('/add-hospitalisation/{mid}/{type}', 'CreateController@AddHospitalisationModal')->name('AddHospitalisationModal'); 
        Route::get('/prescription-ordonnance-add/{mid}/{type}', 'CreateController@PrescriptionOrdonnance')->name('PrescriptionOrdonnance'); 
        Route::get('/verify-ordonnance/{mid}/{type}', 'CreateController@verifyOrdonnace')->name('verifyOrdonnace'); 
        Route::get('/verify-hospit/{mid}/{type}', 'CreateController@verifyHospit')->name('verifyHospit'); 
        Route::get('/prescription-ordonnance-add/{mid}/{type}/{acte_id}', 'CreateController@PrescriptionOrdonnanceByActe')->name('PrescriptionOrdonnanceByActe'); 
        Route::get('/hospitalisations-add/{mid}/{type}/{hospitalisation_id}', 'CreateController@AddFactureHospitalisation')->name('AddFactureHospitalisation'); 
        Route::post('/add-examen/{act_id}/{ass_id}/{type}', 'CreateController@AddExamen')->name('AddExamen'); 
        Route::post('/add-hospitalisation', 'CreateController@AddHospitalisation')->name('AddHospitalisation'); 
        Route::post('/add-prescription-examen', 'CreateController@AddPrescription')->name('AddPrescription'); 
        Route::post('/add-prescription-ordonnance', 'CreateController@AddOrdonnance')->name('AddOrdonnance');         
        Route::post('/add-hospitalisation-ligne', 'CreateController@AddHospitalisationFacture')->name('AddHospitalisationFacture');         
        Route::post('/validation-acte-pharmacie', 'CreateController@ValidateAllActesPharmacie')->name('ValidateAllActesPharmacie'); 
    });
});



//PRESTATAIRES
Route::group(['prefix' => 'dashbord/prestataire' , 'middleware' => 'auth'], function(){
    Route::group(['namespace' => 'App\Http\Controllers\Prestataire' , 'middleware' => 'prestataire'], function () {
        //get
        Route::get('/pages/prise-encharge', 'PrestataireController@PriseEnCharge')->name('PriseEnCharge');
        Route::get('/search-assure/{id}/{nume}', 'PrestataireController@SearchAssureGet')->name('PriseEnChargeGet');
        Route::get('/go-to-make-actes/{id}/{typeAss}', 'PrestataireController@GotoMakeActe')->name('GotoMakeActe');
        Route::get('/add-in-bill/liste-prestation/{id}', 'PrestataireController@AddInBill');
        Route::get('/voir-acte-tampon/{acte_id}/{m_id}/{pype}', 'PrestataireController@ViewActeTampon')->name('ViewActeTampon');
        Route::get('/voir-facture/{facture_id}', 'PrestataireController@ViewFacture')->name('ViewFacture');
        Route::get('/home', 'PrestataireController@dashbordPrestataire')->name('dashbordPrestataire');
        Route::get('/search-ordornnance', 'PrestataireController@GetOrdornnance')->name('GetOrdornnance'); 
        Route::get('/search-prescription', 'PrestataireController@GetPrescription')->name('GetPrescription'); 
        Route::get('/print-ordornnance/{id}', 'PrestataireController@printOrdonnance')->name('printOrdonnance'); 
        Route::get('/print-hospitalisation/{id}', 'PrestataireController@printHospitalisation')->name('printHospitalisation'); 
        Route::get('/end-hospitalisation/{id}', 'PrestataireController@endHospitalisation')->name('endHospitalisation'); 
        Route::get('/set-prescription', 'PrestataireController@setPrescription')->name('setPrescription'); 
        Route::get('/set-garde', 'PrestataireController@setarde')->name('setGarde'); 
        
        //POST
        Route::post('/search-assure/{id}', 'PrestataireController@SearchAssure')->name('SearchAssure'); 
       
    });
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

