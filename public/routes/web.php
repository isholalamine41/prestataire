<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

//GESTION MODULE ADMIN
Route::group(['prefix' => 'sante/muscop-ci/prestataire' , 'middleware' => 'auth'], function(){
    Route::group(['namespace' => 'App\Http\Controllers\Admin' , 'middleware' => 'admin'], function () {
        Route::get('/administrateur', 'AdminController@index')->name('dashbord');
        Route::get('/gestion-des-droits', 'AdminController@GestionDesDroits')->name('GestionDesDroits');
        Route::get('/add-user', 'AdminController@AddUser')->name('AddUser');
        Route::get('/messagerie', 'AdminController@Messagerie')->name('Messagerie');
        Route::get('/update-users/{id}', 'AdminController@updateUser')->name('updateUser');




        Route::post('/ajouter-user-prestataire', 'AdminController@AddUserPrestataire')->name('AddUserPrestataire');
        Route::post('/update-user-prestataire/{id}', 'AdminController@UpdateUserPrestataire')->name('UpdateUserPrestataire');
    });
});



//Update
Route::group(['prefix' => 'dashbord/update' , 'middleware' => 'auth'], function(){
    
    Route::group(['namespace' => 'App\Http\Controllers\Update' , 'middleware' => 'update'], function () {
        
        
    });

});

//Delete
Route::group(['prefix' => 'dashbord/delete' , 'middleware' => 'auth'], function(){
    Route::group(['namespace' => 'App\Http\Controllers\Delete' , 'middleware' => 'delete'], function () {
        Route::get('/delete-acte-tampon/{id}/{mid}/{pype}', 'DeleteController@DeleteActeTampon')->name('DeleteActeTampon');
        Route::get('/delete-all-acte-tampon/{pid}/{mid}/{type}', 'DeleteController@deleteALLACTETamPon')->name('deleteALLACTETamPon');
    });

});

//VIEWS
Route::group(['prefix' => 'dashbord/details' , 'middleware' => 'auth'], function(){
        Route::group(['namespace' => 'App\Http\Controllers\Detail' , 'middleware' => 'detail'], function () {
            //get
        // Route::get('/voir-acte-tampon/{acte_id}/{m_id}', 'DetailController@ViewActeTampon')->name('ViewActeTampon');
        Route::get('/liste-des-actes-medicaux', 'DetailController@ActesMedicaux')->name('ActesMedicaux');
        Route::get('/tous-des-actes-medicaux', 'DetailController@AllActesMedicaux')->name('AllActesMedicaux');
        Route::get('/actes-medicaux-impayes', 'DetailController@ImpayerActesMedicaux')->name('ImpayerActesMedicaux');
        Route::get('/actes-medicaux-payes', 'DetailController@PayerActesMedicaux')->name('PayerActesMedicaux');
        Route::get('/demande-actes', 'DetailController@DemandeActesMedicaux')->name('DemandeActesMedicaux');
        Route::get('/demande-accordees', 'DetailController@DemandeActesMedicauxAccordees')->name('DemandeActesMedicauxAccordees');
        Route::get('/demande-rejetees', 'DetailController@DemandeActesMedicauxRejetees')->name('DemandeActesMedicauxRejetees');
        Route::get('/factures', 'DetailController@FacturesActesMedicaux')->name('FacturesActesMedicaux');
        Route::get('/prestations', 'DetailController@Prestations')->name('Prestations');
        Route::get('/ordonnances', 'DetailController@OrdonnanceMedicaux')->name('OrdonnanceMedicaux');
        
        
        #recherche ajax
        Route::get('/recherche-actes', 'DetailController@RechecheActesMedicaux')->name('RechecheActesMedicaux');
        Route::get('/recherche-facture', 'DetailController@RechecheFacture')->name('RechecheFacture');

        // Post
       
    });
});

//CREATE
Route::group(['prefix' => 'dashbord/create' , 'middleware' => 'auth'], function(){
    Route::group(['namespace' => 'App\Http\Controllers\Create' , 'middleware' => 'create'], function () {
        //poste
        Route::post('/add-acte', 'CreateController@AddActes')->name('AddActes'); 
        Route::get('/validation-acte/{pid}/{mid}/{type}', 'CreateController@ValidateAllActes')->name('ValidateAllActes'); 
       

        
    });
});



//PRESTATAIRES
Route::group(['prefix' => 'dashbord/prestataire' , 'middleware' => 'auth'], function(){
    Route::group(['namespace' => 'App\Http\Controllers\Prestataire' , 'middleware' => 'prestataire'], function () {
        //get
        Route::get('/pages/prise-encharge', 'PrestataireController@PriseEnCharge')->name('PriseEnCharge');
        Route::get('/search-assure/{id}/{nume}', 'PrestataireController@SearchAssureGet')->name('PriseEnChargeGet');
        Route::get('/go-to-make-actes/{id}/{typeAss}', 'PrestataireController@GotoMakeActe')->name('GotoMakeActe');
        Route::get('/add-in-bill/liste-prestation/{id}', 'PrestataireController@AddInBill');
        Route::get('/voir-acte-tampon/{acte_id}/{m_id}/{pype}', 'PrestataireController@ViewActeTampon')->name('ViewActeTampon');
        Route::get('/voir-facture/{facture_id}', 'PrestataireController@ViewFacture')->name('ViewFacture');
        Route::get('/home', 'PrestataireController@dashbordPrestataire')->name('dashbordPrestataire');
        
        //POST
        Route::post('/search-assure/{id}', 'PrestataireController@SearchAssure')->name('SearchAssure'); 
       
    });
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

