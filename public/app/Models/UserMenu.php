<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMenu extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'menu_id',
        'user_id','type','libelle','date'
    ];

    public function Menu()
    {
    	return $this->belongsTo(Menu::class);
    }
    
    public function User()
    {
    	return $this->belongsTo(User::class);
    }
}
