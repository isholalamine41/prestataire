<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'montant',
        'part_muscopci',
        'part_assure',

        'prestataire_id',
        'date',
        'heure',
        'payer',
        'etat',

        'qte',
        'pu',
    ];

    public function PrestaMutua()
    {
        return $this->hasMany(PrestaMuta::class);
    }

    public function Prestataire()
    {
        return $this->belongsTo(Prestataire::class);
    }
}
