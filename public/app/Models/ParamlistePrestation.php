<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParamlistePrestation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'cout',
        'liste_prestation_id',
        'prestataire_id',
        'CoefiLettre',

        'tierPayant',
        'parAssure',
        'fourmulecout',
        'fourmuletp',
        
        'valeurLettre',
        'paramptp_id',
        'description',
        'datedebut',
        'datefin',
        'annee',
        'added_by',
    ];

     
   
    
    public function ListePrestation()
    {
       return $this->belongsTo(ListePrestation::class);
    }

    public function Paramptp()
    {
       return $this->belongsTo(Paramptp::class);
    }

    public function Prestataire()
    {
       return $this->belongsTo(Prestataire::class);
    }
}
