<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class ListePrestation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'listeprestable_id',
        'listeprestable_type',
        'prestataire_id',
        'param_prestation_id',
        'ententePrealable',
        'entente',
        'qte',
        'etat',
        'added_by',
        'description',
    ];

        

    public function listeprestable(): MorphTo
    {
        return $this->morphTo();
    }
    
    public function ParamPrestation()
    {
        return $this->belongsTo(ParamPrestation::class);
    }

    public function ParamlistePrestation()
    {
        return $this->hasMany(ParamlistePrestation::class);
    }

    public function Prestataire()
    {
        return $this->belongsTo(Prestataire::class);
    }

    public function typePrestation()
    {
        return $this->belongsTo(typePrestation::class);
    }
    
    public function Prestation()
    {
       return $this->belongsTo(Prestation::class,'listeprestable_id');
    }
    
    public function PrestaMutua()
    {
        return $this->hasMany(PrestaMuta::class);
    }

    

    
    
}
