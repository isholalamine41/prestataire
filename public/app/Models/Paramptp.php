<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paramptp extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'lettre',
        'valeurLettre',
        'cout',
        'added_by',
        'annee',
        'datedebut',
        'datefin',
    ];

    public function PrestataireTypePrestation(){
        
        return $this->hasMany(PrestataireTypePrestation::class);
    }

    public function ParamlistePrestation()
    {
       return $this->hasMany(ParamlistePrestation::class);
    }
}
