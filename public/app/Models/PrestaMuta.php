<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class PrestaMuta extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'listePrestation_id',
        'medecin_id',
        'facture_id',
        'prestamutable_id',
        'prestamutable_type',
        'description',
        'cout',
        'part_assure',
        'part_muscopci',
        'entente',
        'docteur',
        'tampon',
        'type',
        'added_by',
        'date',
        'heure',
        'qte',
        'etat',
        'prestataire_id',
    ];

    public function prestamutable(): MorphTo
    {
        return $this->morphTo();
    }
    
    public function ListePrestation()
    {
        return $this->belongsTo(ListePrestation::class);
    }

    public function Medecin()
    {
        return $this->belongsTo(Medecin::class);
    }

    public function Facture()
    {
        return $this->belongsTo(Facture::class);
    }

    public function Prestataire()
    {
        return $this->belongsTo(Prestataire::class);
    }

    // public function Mutualiste()
    // {
    //     return $this->belongsTo(Mutualiste::class);
    // }

    // public function Ayantdroits()
    // {
    //     return $this->belongsTo(Ayantdroit::class);
    // }
}
