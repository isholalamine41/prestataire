<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRight extends Model
{
    use HasFactory;
    Protected $fillable = ['id','right_id','user_id','libelle'];

    public function Right()
    {
    	return $this->belongsTo(Right::class);
    }

    public function User()
    {
    	return $this->belongsTo(User::class);
    }
}
