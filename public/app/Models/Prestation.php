<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Prestation extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id',
        'libelle',
        'type_prestation_id',
        'description',
        'entente',
        'etat',
        'ententePrealable',
        'added_by',
        'exclusion'
    ];

    public function ListePrestation(): MorphMany
    {
        return $this->morphMany(ListePrestation::class, 'listeprestable');
    }

    public function typePrestation()
    {
        return $this->belongsTo(typePrestation::class);
    }

    public function paramPrestation()
    {
        return $this->hasMany(paramPrestation::class);
    }

    public function Bareme()
    {
         return $this->hasMany(Bareme::class);
    }


}
