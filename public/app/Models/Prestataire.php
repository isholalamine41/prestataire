<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prestataire extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'commune_id',
        'user_id',
        'type_prestataire_id',
        'nom',
        'rs',
        'situation_geo',
        'rib',
        'banque',
        'email',
        'telephone',
        'contact',
        'whathsapp',
        'description',
        'logo',
        'nomDG',
        'contactDG',
        'emailDG',
        'nomComptable',
        'contactComptable',
        'emailComptable',
        'nomRespo',
        'contactRespo',
        'emailRespo',
        'baseCouverture',
        'etat',
        'added_by',
        'principal_id',
    ];

    public function Commune()
    {
        return $this->belongsTo(Commune::class);
    }

    public function typePrestataire()
    {
        return $this->belongsTo(typePrestataire::class);
    }

    public function ListePrestation()
    {
        return $this->hasMany(ListePrestation::class);
    }

    public function AddedBy()
    {
        return $this->belongsTo(User::class, 'added_by');
    }

    public function PrestataireTypePrestation()
    {
        return $this->hasMany(ListePrestPrestataireTypePrestationation::class);
    }

    public function ParamPrestataire()
    {
        return $this->hasMany(ParamPrestataire::class);
    }

    public function PrestaMutua()
    {
        return $this->hasMany(PrestaMuta::class);
    }

    public function Facture()
    {
        return $this->hasMany(Facture::class);
    }
    
    
}
