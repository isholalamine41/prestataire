<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medecin extends Model
{
    use HasFactory;
    Protected $fillable = [
        'first_name','last_name','username','phone','city','sex','active','email','birth_date',
        'birth_place','bio','residence','marital_status','photo','user_id','cv','added_by',
    ];
    
    public function PrestaMutua()
    {
        return $this->hasMany(PrestaMuta::class);
    }
}
