<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Medicament extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'type_prestation_id',
        'description',
        'baseRemboussement',
        'entente',
        'etat',
        'ententePrealable',
        'added_by',
    ];

    public function presta_mutua(): MorphMany
    {
        return $this->morphMany(ListePrestation::class, 'listeprestable');
    }
}
