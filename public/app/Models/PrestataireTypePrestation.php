<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrestataireTypePrestation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'libelle',
        'lettre',
        'valeurLettre',
        'plafondMutuelle',
        'valeurLettreMutuelle',
        'paramptp_id',
        'description',
        'type_prestation_id',
        'prestataire_id',
    ];

    public function typePrestation()
    {
        return $this->belongsTo(typePrestation::class);
    }

    public function Prestataire()
    {
        return $this->belongsTo(Prestataire::class);
    }

    public function Paramptp()
    {
        return $this->belongsTo(Paramptp::class);
    }
}
