<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;


class Administration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {        
        if (Auth::user()->Typeuser === 'Prestataire'  and Auth::user()->active == 1 and Auth::user()->typePrestataire ==='Super Prestataire') {
             return $next($request);
        }else{
          return response()->json([
            'message' => 'Vous n\'avez pas les droits d\'acces à cette page. Merci de bien vouloir contacter l\'administrateur général',
            'statut' => 'messageErreur',
            'title' => 'DROITS D\'ACCES',
            'type' => 'error',
            ]);
        } 
    }
}
