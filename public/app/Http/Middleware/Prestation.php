<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Right;
use App\Models\User;
use App\Models\UserMenu;
use Illuminate\Support\Facades\Auth;

class Prestation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
     {
        if ((Auth::user()->Typeuser === 'Admin' or Auth::user()->Typeuser === 'Super Admin') and Auth::user()->active == 1) {
            $user =  Auth::user();
            foreach (Auth::user()->UserMenu as $Up) {
                if ($Up->libelle === 'All' or $Up->libelle === 'PRESTATION') {
                    return $next($request);
                }
            }
            return response()->json([
            'message' => 'Vous n\'avez pas les droits d\'accès à cette page. Merci de bien vouloir contacter l\'administrateur général',
            'statut' => 'messageErreur',
            'title' => 'DROITS D\'ACCES',
            'type' => 'error',
            ]);  
            return redirect()->back();
       }else{
         //dd(Auth::user());
         session()->flash('messageErreur', 'Vous n\'êtes pas habilité à consulter cette page.');
         return redirect()->back();
       } 
    }
}
