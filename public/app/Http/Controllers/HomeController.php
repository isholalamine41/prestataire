<?php

namespace App\Http\Controllers;

use App\Models\Prestataire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\Auth\Authenticatable;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $user;
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = auth()->user();
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        if (Auth::user()->Typeuser == 'Prestataire' and Auth::user()->active == 1 and Auth::user()->typePrestataire == 'Super Prestataire') {
            return redirect()->route('dashbord');
        }
        if (Auth::user()->Typeuser == 'Prestataire' and Auth::user()->active == 1 and Auth::user()->typePrestataire == 'Admin Prestataire') {

            if (Auth::user()->UserMenu->where('libelle',"ACTES MEDICAUX")->count()>0) {
                return redirect()->route('ActesMedicaux');
            }
            if (Auth::user()->UserMenu->where('libelle',"NOS PRESTATIONS")->count()>0) {
                return redirect()->route('Prestations');
            }
            if (Auth::user()->UserMenu->where('libelle',"DEMANDES")->count()>0) {
                return redirect()->route('DemandeActesMedicaux');
            }
            if (Auth::user()->UserMenu->where('libelle',"PRISE EN CHARGE")->count()>0) {
                return redirect()->route('PriseEnCharge');
            }
            if (Auth::user()->UserMenu->where('libelle',"FACTURES")->count()>0) {
                return redirect()->route('FacturesActesMedicaux');
            }
            if (Auth::user()->UserMenu->where('libelle',"ORDONNANCES")->count()>0) {
                return redirect()->route('OrdonnanceMedicaux');
            }
            if (Auth::user()->UserMenu->where('libelle',"ORDONNANCES")->count()>0) {
                return redirect()->route('OrdonnanceMedicaux');
            }

        }else{
            Auth::logout();
            session()->flash('messageErreur',"Vous n'etes autorisé a consulter cette page");
        }
    }
}
