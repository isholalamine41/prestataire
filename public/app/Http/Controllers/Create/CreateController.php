<?php

namespace App\Http\Controllers\Create;

use App\Http\Controllers\Controller;
use App\Models\Ayantdroit;
use App\Models\Bareme;
use App\Models\Bloc;
use Illuminate\Http\Request;
use App\Models\Prestataire;
use App\Models\Prestation;
use App\Models\typePrestataire;
use App\Models\typePrestation;
use App\Models\Commune;
use App\Models\Facture;
use App\Models\User;
use Illuminate\Support\Str;
use App\Models\ListePrestation;
use App\Models\Mutualiste;
use App\Models\ParamlistePrestation;
use App\Models\ParamPrestation;
use App\Models\ParamtypePrestation;
use App\Models\UserProjet;
use App\Models\ParamPrestataire;
use App\Models\PrestaMuta;
use App\Models\PrestataireTypePrestation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CreateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('create');
    } 



    public function AddActes(Request $request){

        $request->validate([
            '_token' => 'required|string',
            'commentaire' => 'required|string',
            'docteur' => 'required|string',
            'lpid' => 'required|integer',
            'assure_id' => 'required|integer',
            'type' => 'required|string',
        ]);

        if ($request->demande) {
            $typeAc = 'demande';
            $statut = 'En attente';
        }else{
            $typeAc = 'tampon';
            $statut = Null;
        }
        $typeAss = $request->type;

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($request->assure_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($request->assure_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }


        if ($assure->active) {

            $lp = ListePrestation::find($request->lpid);
            $prestataire = $lp->Prestataire;
            $cout = $lp->ParamlistePrestation->last()->cout;
            $part_assure = $lp->ParamlistePrestation->last()->parAssure;
            $part_muscopci   = $lp->ParamlistePrestation->last()->tierPayant;
            $qte = null;
            
            //VERIFIER QUE LA QUANTITE N'EST PAS NULL.
            if ($request->qte) {
                if ($request->qte>1) {
                    $qte = $request->qte;
                    $cout = $cout * $request->qte;
                    $part_assure = $part_assure * $request->qte;
                    $part_muscopci = $part_muscopci * $request->qte;
                }
            }

            
            
            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['liste_prestation_id', $lp->id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            if ($SUMactes->count()>0) {
                return response()->json([
                    'title' => 'PRESTATION EXISTANTE',
                    'statut' => 'error',
                    'message' => "La prestation $lp->libelle, existe déjà dans la liste des actes",     
                ]);  
            }

            $SUMactes = PrestaMuta::where([
                ['prestamutable_id', $request->assure_id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $lp->prestataire_id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();

            $ssumActesM = $SUMactes->SUM('cout') +  $cout;
            $pf = $assure->bareme->last()->plafondGeneraleFamillle ;
            $pb = $assure->bareme->last()->plafondGeneraleBeneficiaire;
            
            //VERIFIER LES PLAFONDS FAMILLE ET INDIVIDUEL
            if (($pf > $ssumActesM) and ($pb > $ssumActesM)) {

                if($pf>$pb){
                    $psup = $pf;
                    $pinf = $pb;
                }else{
                    $psup = $pb;
                    $pinf = $pf ;
                }
                $dif = $pinf  - $ssumActesM;

                if ($dif>0) {
                }else{
                    return response()->json([
                        'title' => 'PLAFOND GENERAL',
                        'statut' => 'error',
                        'message' => "Le montant des actes est supperieur au plafond FAMILLE OU BENEFIAIRE. Veuillez contacter la MUSCOP-CI pour la procedure d'enregistrement directe | Plafond Fammille: $pf Fcfa | Plafond individuel: $pb Fcfa | Cout Acte(s): $ssumActesM Fcfa",     
                    ]);  
                }
                
                
                //VERIFIER LA CONSOMATION PAR PRESTATION
                $qteP = $lp->listeprestable->ParamPrestation->last()->qte;
                $dureeP = $lp->listeprestable->ParamPrestation->last()->duree;
                $entite = $lp->listeprestable->ParamPrestation->last()->entite;
                $plafondP = $lp->listeprestable->ParamPrestation->last()->plafond;

                $baram = Bareme::where([
                    ['baremetable_id', $assure->id],
                    ['baremetable_type', $prestamutable_type],
                    ['prestation_id', $lp->listeprestable_id],
                ])->first();
                
                if ($qteP) {
                    if ($baram->qteUtilise >= $qteP) {
                        return response()->json([
                            'title' => 'LIMITE PRESTATION',
                            'statut' => 'error',
                            'message' => "Vous avez droit à $qteP fois cette prestation. Vous avez déjà consommé(e) $baram->qteUtilise", 
                        ]);
                    }
                }
                
                
                if ($plafondP) {
                    if ($entite) {
                        if ($entite == "Assuré") {
                            $resteAconsoSurP = $plafondP - $baram->consoBeneficiairePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 1;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                                    return response()->json([
                                        'statut' => 'success',
                                        'code' => $retour,     
                                    ]);  
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }else{
                            $resteAconsoSurP = $plafondP - $baram->consoFamillePrestation;
                            if ($resteAconsoSurP>1000) {
                                if($cout>$resteAconsoSurP){
                                    $part_muscopci = $resteAconsoSurP;
                                    $part_assure = $cout - $part_muscopci;
                                    $cout =  $resteAconsoSurP;
                                }else{
                                    $acte = new PrestaMuta();
                                    $acte->libelle   = $lp->libelle;
                                    $acte->liste_prestation_id   = $lp->id;
                                    $acte->prestataire_id   = $lp->prestataire_id;
                                    $acte->prestamutable_id   = $request->assure_id;
                                    $acte->prestamutable_type   = $prestamutable_type;
                                    $acte->description   = $request->commentaire;
                                    $acte->cout   = $cout;
                                    $acte->part_assure   = $part_assure;
                                    $acte->part_muscopci   = $part_muscopci;
                                    $acte->docteur   = $request->docteur;
                                    $acte->tampon   = 1;
                                    $acte->entente   = $statut;
                                    $acte->type   = $typeAc;
                                    $acte->added_by   = Auth::User()->id;
                                    $acte->date   = date('Y-m-d');
                                    $acte->heure   = date('H:i');
                                    $acte->qte   = $qte;
                                    $acte->save();

                                    $actes = PrestaMuta::where([
                                        ['prestamutable_id', $request->assure_id],
                                        ['prestamutable_type',$prestamutable_type],
                                        ['prestataire_id', $lp->prestataire_id],
                                        ['tampon', 1],
                                        ['etat', 1],
                                    ])->orderBy('id','desc')->get();

                                    $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                                    return response()->json([
                                        'statut' => 'success',
                                        'code' => $retour,     
                                    ]); 
                                }
                            }else{
                                return response()->json([
                                    'title' => 'PLAFOND PRESTATION',
                                    'statut' => 'error',
                                    'message' => "Le plafond de la famille du mutualiste pour cette prestion est de: $resteAconsoSurP FCFA, nous ne pouvons pas enregistrer cet acte", 
                                ]); 
                            }
                        }
                    }
                }
                
                
                $acte = new PrestaMuta();
                $acte->libelle   = $lp->libelle;
                $acte->liste_prestation_id   = $lp->id;
                $acte->prestamutable_id   = $request->assure_id;
                $acte->prestataire_id   = $lp->prestataire_id;
                $acte->prestamutable_type   = $prestamutable_type;
                $acte->description   = $request->commentaire;
                $acte->cout   = $cout;
                $acte->part_assure   = $part_assure;
                $acte->part_muscopci   = $part_muscopci;
                $acte->docteur   = $request->docteur;
                $acte->tampon   = 1;
                $acte->entente   = $statut;
                $acte->type   = $typeAc;
                $acte->added_by   = Auth::User()->id;
                $acte->date   = date('Y-m-d');
                $acte->heure   = date('H:i');
                $acte->qte   = $qte;
                $acte->save();

                $actes = PrestaMuta::where([
                    ['prestamutable_id', $request->assure_id],
                    ['prestamutable_type',$prestamutable_type],
                    ['prestataire_id', $lp->prestataire_id],
                    ['tampon', 1],
                    ['etat', 1],
                ])->orderBy('id','desc')->get();

                $retour = view('admin.prestataire.actes.actesTampon',compact('lp','actes','assure','prestataire','typeAss'))->render();

                return response()->json([
                    'statut' => 'success',
                    'code' => $retour,     
                ]); 

            }else{
                return response()->json([
                    'statut' => 'success',
                    'code' => 'Le plafond (FAMILLE OU BENEFIAIRE) de cet assuré est inferieur à la sommes des actes',     
                ]);   
            }
        }else{
            return response()->json([
                'statut' => 'success',
                'code' => 'Cet assuré vient d\'être désactivé dans le système',     
            ]); 
        }
    }

    public function ValidateAllActes($p_id, $m_id , $typeAss){
        
        $p = Prestataire::find($p_id);

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }
        

        $actes = PrestaMuta::where([

            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $p->id],
            ['tampon', 1],
            ['type', 'tampon'],
            ['etat', 1],
            ['entente', Null],

        ])->orderBy('id','desc')->get();
        
        //dd($actes, $assure);

        $b = Bareme::where([

            ['baremetable_id', $assure->id],
            ['baremetable_type',$prestamutable_type],

        ])->get();

        $facture = new Facture();
        $facture->montant = 0;
        $facture->part_muscopci = 0;
        $facture->part_assure = 0;
        $facture->prestataire_id = $p->id;
        $facture->date = date('Y-m-d');
        $facture->heure = date('H:i');
        $facture->payer = 0;
        $facture->etat = "tampon";
        $facture->save();

        foreach ($actes as $a) {

            $a->update([
                'facture_id' => $facture->id,
                'tampon' => 0,
                'type' => 'Acte',
            ]);

            $ligneBareme = $b->where('prestation_id',$a->ListePrestation->listeprestable_id)->last();
            //dd($ligneBareme )

            $stockLigne = $ligneBareme;

            if ($stockLigne->firstdate) {
                $firstdate = $stockLigne->firstdate;
            }else{
                $firstdate = date('Y-m-d');
            }
            if ($a->qte) {
                $qteAC = $a->qte;
            }else {
                $qteAC = 1;
            }

            $ligneBareme->update([
                'consoBeneficiairePrestation' => $stockLigne->consoBeneficiairePrestation + $a->part_muscopci,
                'consoFamillePrestation' => $stockLigne->consoFamillePrestation  + $a->part_muscopci,
                'qteUtilise' => $stockLigne->qteUtilise + $qteAC,
                'firstdate' => $firstdate,
                'lastedate' => date('Y-m-d'),
            ]);


            DB::table('baremes')->where([

                ['baremetable_id', $assure->id],
                ['baremetable_type',$prestamutable_type],
    
            ])->update([
                
                'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci"),
                'plafondGeneraleBeneficiaire'=> DB::raw("plafondGeneraleBeneficiaire - $a->part_muscopci")
            ]);

            if ($typeAss == 'Mutualiste') {
                if ($assure->Ayantdroits->count()>0) {
                    foreach ($assure->Ayantdroits as $ad) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();

                        $bad->last()->update([
                            'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                        ]);
                    }
                }
            }else{
                DB::table('baremes')
                ->where([

                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
        
                ])->update([
                    'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                ]);

                $bad = Bareme::where([
                    ['baremetable_id', $assure->Mutualiste->id],
                    ['baremetable_type','App\Models\Mutualiste'],
                    ['prestation_id', $a->ListePrestation->listeprestable_id],
                ])->get();

                $bad->last()->update([
                    'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                ]);
                $pere = $assure->Mutualiste;
                foreach ($pere->Ayantdroits as $ad) {
                    if ($ad->id != $assure->id) {
                        DB::table('baremes')
                        ->where([

                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                
                        ])->update([
                            'plafondGeneraleFamillle' => DB::raw("plafondGeneraleFamillle - $a->part_muscopci")
                        ]);

                        $bad = Bareme::where([
                            ['baremetable_id', $ad->id],
                            ['baremetable_type','App\Models\Ayantdroit'],
                            ['prestation_id', $a->ListePrestation->listeprestable_id],
                        ])->get();

                        $bad->last()->update([
                            'consoFamillePrestation' => $bad->last()->consoFamillePrestation + $a->part_muscopci,
                        ]);
                    }
                }
            }
            
            
        }


        $facture->update([
            'montant' => $actes->SUM('part_muscopci') + $actes->SUM('part_assure'),
            'part_muscopci' => $actes->SUM('part_muscopci'),
            'part_assure' => $actes->SUM('part_assure'),
            'prestataire_id' => $p->id ,
            'date' => date('Y-m-d'),
            'heure' => date('H:i'),
            'payer' => 0,
            'etat' => "Validée" ,
        ]);

        session()->flash('message',"Actes médicaux enregistrés avec succès");
        return redirect()->back();
                
    }

}


