<?php

namespace App\Http\Controllers\Prestataire;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Prestataire;
use App\Models\Mutualiste;
use App\Models\Ayantdroit;
use App\Models\ListePrestation;
use App\Models\Prestation;
use App\Models\typePrestataire;
use App\Models\PrestaMuta;
use App\Models\Facture;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Ramsey\Uuid\Type\Integer;

class PrestataireController extends Controller
{
    protected $user;
    protected $p;
    public function __construct()
    {
       
        $this->middleware('auth');
        $this->middleware('prestataire');
      
        
        
    } 

    public function PriseEnCharge()
    {
       
        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        
        return view('admin.prestataire.priseEnCharge',compact('p'));
    }



    public function SearchAssureGet($id, $num){
        $id = $this->verifId($id);
       

        $numeAssure = htmlspecialchars($num);
        $verifMU = Mutualiste::where('assurance_number',$numeAssure)->get();
        $verifAD = Ayantdroit::where('assurance_number',$numeAssure)->get();

        $typeASS = "Mutualiste";

        if ($verifMU->count()>0) {
            $assure = $verifMU->first();
            $typeASS = "Mutualiste";
        }
            
        if ($verifAD->count()>0) {
            $assure = $verifAD->first();
            $typeASS = "Ayant-droit";
        }

        //dd($verifAD,$verifMU);
        if ($verifAD->count() == 0 and $verifMU->count() == 0) {
            return response()->json([
                'message' => "Le numéro de carte saisi innexistant",
                'title' => 'NUMERO DE CARTE',
                'statut' => 'error',
            ]); 
        }

        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        $retour = view('admin.prestataire.retourAjax.retourInfoCarte',compact('p','assure','typeASS'))->render();
        if ($typeASS == "Mutualiste") {
            return response()->json([
                'message' => "$assure->first_name.' '. $assure->name",
                'title' => 'ASSURE',
                'statut' => 'success',
                'code' => $retour
            ]); 
           
        }else{
            return response()->json([
                'message' => "$assure->first_name.' '.$assure->last_name",
                'title' => 'ASSURE',
                'statut' => 'success',
                'code' => $retour
            ]); 
        }
        
        return redirect()->back();
        
    }

    public function SearchAssure(Request $request, $id){
        $id = $this->verifId($id);
        $request->validate([
            '_token' => 'required|string',
            'numeAssure' => 'required|string',
        ]);

        $numeAssure = htmlspecialchars($request->numeAssure);

        $verifMU = Mutualiste::where('assurance_number',$numeAssure)->get();
        $verifAD = Ayantdroit::where('assurance_number',$numeAssure)->get();

        $typeASS = "Mutualiste";

        if ($verifMU->count()>0) {
            $assure = $verifMU->first();
            $typeASS = "Mutualiste";
        }
            
        if ($verifAD->count()>0) {
            $assure = $verifAD->first();
            $typeASS = "Ayant-droit";
        }
        
        //dd($verifAD,$verifMU);
        if ($verifAD->count() == 0 and $verifMU->count() == 0) {
            return response()->json([
                'message' => "Le numéro de carte saisi innexistant",
                'title' => 'NUMERO DE CARTE',
                'statut' => 'error',
            ]); 
        }


        $p = Prestataire::where('user_id',Auth::user()->principal_id)->first();
        $retour = view('admin.prestataire.retourAjax.retourInfoCarte',compact('p','assure','typeASS'))->render();
        if ($typeASS == "Mutualiste") {
            return response()->json([
                'message' => "$assure->first_name.' '. $assure->name",
                'title' => 'ASSURE',
                'statut' => 'success',
                'code' => $retour
            ]); 
           
        }else{
            return response()->json([
                'message' => "$assure->first_name.' '.$assure->last_name",
                'title' => 'ASSURE',
                'statut' => 'success',
                'code' => $retour
            ]); 
        }
        
        return redirect()->back();
        
    }

    public function GotoMakeActe($assure_id,$type){
        $id = $this->verifId($assure_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $prestataire = Prestataire::where('user_id', Auth::User()->principal_id)->first();
        $id_liste_prestation = [];
        $id_liste_prestation  = $prestataire->ListePrestation->pluck('listeprestable_id');
        $prestations = Prestation::whereIn('id', $id_liste_prestation)->where('etat', true)->orderBy('libelle','ASC')->get();
        
        $m = $assure;
        //dd($assure->bareme);
        $actes = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $prestataire->id],
            ['tampon', 1],
            ['etat', 1],
        ])->orderBy('id','desc')->get();
        
        if ($assure) {
            return view('admin.prestataire.actes.makeacte',compact('assure','prestations','prestataire','actes','m','type'));
        }else{
            session()->flash('messageErreur',"Nous contatons une activité suspecte, Veuillez contactez le service informatique svp ");
            Auth::logout();
            return redirect('login');
        }
    }

    public function AddInBill($id){
        $id = $this->verifId($id);
        $lp = ListePrestation::find($id);
        $retour =  view('admin.prestataire.actes.resteform',compact('lp'))->render();
        return response()->json([
            'statut' => 'success',
            'code' => $retour
        ]); 
    }

    public function verifId($id){
        $type = gettype($id);
        $id = htmlspecialchars($id);
        if ($id > 0 ) {
            return $id;
        }else{
            session()->flash('messageErreur',"Nous contatons une activité suspecte dans cette requette, Veuillez contactez le service informatique svp ");
            Auth::logout();
            return redirect('login');
        }
    }

    public function ViewActeTampon($acte_id, $m_id,$type){
        $a_id = $this->verifId($acte_id);
        $m_id = $this->verifId($m_id);
        $a = PrestaMuta::find($a_id);

        if ($type == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }

        $retour =  view('admin.prestataire.actes.dateilActe',compact('assure','a'))->render();
        return response()->json([
            'statut' => 'success',
            'code' => $retour
        ]); 
    }

    public function ViewFacture($id){

        $facture = Facture::find($id);
        $prestaMutua = $facture->PrestaMutua()->get();

        $retour =  view('admin.prestataire.facture.details',compact('facture','prestaMutua'))->render();

        return response()->json([
            'statut' => 'success',
            'code' => $retour
        ]); 
    }

    public function dashbordPrestataire(){
        return redirect('login');
    }

    
}
