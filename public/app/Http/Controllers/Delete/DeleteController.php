<?php

namespace App\Http\Controllers\Delete;

use App\Http\Controllers\Controller;
use App\Models\Ayantdroit;
use App\Models\Bloc;
use App\Models\ListePrestation;
use App\Models\Mutualiste;
use App\Models\PrestaMuta;
use App\Models\Prestataire;
use App\Models\typePrestation;
use App\Models\Prestation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('delete');
    }

    public function DeleteActeTampon($id, $mid, $typeAss){
        $acte = PrestaMuta::find($id);
        $prestataire = $acte->Prestataire;
        $acte = PrestaMuta::find($id);

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($mid);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($mid);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }
        
        if ( $acte) {
            $acte->delete();

            $actes = PrestaMuta::where([
                ['prestamutable_id', $assure->id],
                ['prestamutable_type',$prestamutable_type],
                ['prestataire_id', $acte->Prestataire->id],
                ['tampon', 1],
                ['etat', 1],
            ])->orderBy('id','desc')->get();
           

            $retour = view('admin.prestataire.actes.actesTampon',compact('actes','assure','prestataire','typeAss'))->render();

            return response()->json([
                'title' => "RETRAIT D'ACTE",
                'message' => 'Acte médical retiré avec succès',
                'statut' => 'success',
                'code' => $retour,     
            ]); 
        }
    }

    public function deleteALLACTETamPon($p_id, $m_id, $typeAss){
        $p = Prestataire::find($p_id);

        if ($typeAss == 'Ayant-droit') {
            $assure = Ayantdroit::find($m_id);
            $prestamutable_type = 'App\Models\Ayantdroit';
            $type = "Ayant-droit";
        }else{
            $assure = Mutualiste::find($m_id);
            $prestamutable_type = 'App\Models\Mutualiste';
            $type = "Mutualiste";
        }


        $actes = PrestaMuta::where([
            ['prestamutable_id',  $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $p->id],
            ['tampon', 1],
        ])->orderBy('id','desc')->get();

        foreach ($actes as $a) {
            if ($a) {
               $a->delete();
            }
        }
        $actes = PrestaMuta::where([
            ['prestamutable_id', $assure->id],
            ['prestamutable_type',$prestamutable_type],
            ['prestataire_id', $p->id],
            ['tampon', 1],
        ])->orderBy('id','desc')->get();

        $retour = view('admin.prestataire.actes.actesTampon',compact('actes','assure','typeAss'))->render();

        return response()->json([
            'title' => "RETRAIT D'ACTE",
            'message' => 'Acte médical retiré avec succès',
            'statut' => 'success',
            'code' => $retour,     
        ]); 

    }

    public function ViewActeTampon(){
        
    }
    
    
}
