<?php

namespace App\Http\Controllers\Update;

use App\Http\Controllers\Controller;
use App\Models\Bloc;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Commune;
use App\Models\ListePrestation;
use App\Models\ParamlistePrestation;
use Illuminate\Http\Request;
use App\Models\Prestataire;
use App\Models\typePrestataire;
use App\Models\typePrestation;
use App\Models\User;
use App\Models\ParamtypePrestation;
use App\Models\ParamPrestation;
use App\Models\Prestation;
use App\Models\ParamPrestataire;
use Illuminate\Support\Facades\Auth;


class UpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('update');
    } 

    public function ModalUpPrestataire($id){
        $idp = htmlspecialchars($id);
        $p = Prestataire::find($idp);
        $type = typePrestataire::all();
        $commune  = Commune::all();
        if ($p) {
            return view('admin.prestataire.update.uprestataire',compact('p','type','commune'));
        }
        
    }

    public function UPrestataire(Request $request, $id){
      
        $request->validate([
            'logo' => 'image|mimes:jpeg,png,jpg|max:2048',
            '_token' => 'required|string',
            'type_prestataire' => 'required|integer',
            'base' => 'required|integer',
            'commune' => 'required|integer',
            'email' => 'email|max:255|string',
            'contact' => 'max:50|string',
            'rs' => 'required|string',
            
        ]);

        $idp = htmlspecialchars($id);
        $p = Prestataire::find($idp);
        
        $verifName = Prestataire::where([
            ['rs', $request->rs],
        ])->get();

        $verifRIB =  Prestataire::where([
            ['rib', $request->rib],
        ])->get();
        
        foreach ($verifName as $value) {
           if ($value->id != $idp) {
                session()->flash('messageErreur',"Le prestataire $request->rs existe déjà");
                return redirect()->back();
           }
        }

      
        $puser = User::find($p->user_id);

        if (request()->file('logo')) {

            $file = request()->file('logo');
            $path = storage_path('media/');

            if($file = $request->file('logo')) {
                $fileData = $this->uploads($file,$path); 
                if ($fileData) {
                    $puser->update(['photo' => $fileData['filenamewri']]);
                    $p->update(['logo' =>  $fileData['filenamewri']]);
                }
            }
        }

        $puser->update([
            'name' => $request->rs,
            'email' =>$request->email ?? $puser->email
        ]);
        $p->update([
            'rs' => $request->rs,
            'type_prestataire' => $request->type_prestataire,
            'baseCouverture' => $request->base,
            'commune_id' => $request->commune,
            'email' => $request->email,
            'contact' => $request->contact,
            'situation_geo' => $request->situation_geo,
            'banque' => $request->banque,
            'rib' => $request->rib,
            'nomDG' => $request->nomDG,
            'contactDG' => $request->contactd,
            'emailDG' => $request->emaild,
            'nomComptable' => $request->nomComptable,
            'contactComptable' => $request->contactComptable,
            'description' => $request->description,
        ]);

        $ParamPrestataire = new ParamPrestataire();
        $ParamPrestataire->rs = $p->rs;
        $ParamPrestataire->prestataire_id =  $p->id;
        $ParamPrestataire->taux = $p->baseCouverture;
        $ParamPrestataire->lastedate = date('Y-m-d');
        $ParamPrestataire->save();

        session()->flash('message',"Prestataire $request->rs modifié avec succès.");
        return redirect()->back();
    }


    
    public function uploads($file, $path)
    {
        if($file) {
            $fileName = md5($file->getClientOriginalExtension().time()).".".$file->getClientOriginalExtension();
            Storage::disk('public')->put('media/' . $fileName, File::get($file));
            $file_name  = $file->getClientOriginalName();
            $file_type  = $file->getClientOriginalExtension();
            $filenamewri   = $fileName;

            return $file = [
                'fileName' => $file_name,
                'fileType' => $file_type,
                'filenamewri' => $filenamewri,
            ];
            
        }
    }

    public function UpdateBloc($id){
        $idb = htmlspecialchars($id);
        $bloc = Bloc::find($idb);
        if ($bloc) {
            return view('parametre.modal.updateBloc', compact('bloc'));
        }else{
            return redirect()->back();
        }
    }

    public function UpdatingBLOC(Request $request, $id){
        $idb = htmlspecialchars($id);
        $bloc = Bloc::where('libelle',$request->libelle)->get();
       
        if ($bloc->count()>0) {
            foreach ($bloc as $value) {
                if ($value->id != $idb) {
                    session()->flash('messageErreur',"Le bloc $request->libelle existe déjà");
                    return redirect()->back();
                }
            }
        }
        $bloc = Bloc::find($idb);
        $bloc->update(['libelle' => $request->libelle]);
        session()->flash('message',"Bloc $request->libelle modifié avec succès.");
        return redirect()->back();
    }

    public function UpdateTypeP($id){
        $idb = htmlspecialchars($id);
        $typeP = typePrestation::find($idb);
        $bloc = Bloc::where('etat',true)->get();
        if ($typeP) {
            return view('parametre.modal.updateType', compact('typeP','bloc'));
        }else{
            return redirect()->back();
        }
    }

    public function UpCATPres(Request $request, $id){

        $request->validate([
            '_token' => 'required|string',
            'libelle' => 'required|string',
            'bloc_ic' => 'required|integer',
        ]);
        //dd($request->all(), $id);
        $tp = typePrestation::find($id);
        $tp->update([
            'libelle' => htmlspecialchars($request->libelle),
            'bloc_id' => $request->bloc_ic,
            'added_by' => Auth::user()->id,
            'description' => $request->description,
        ]);
        $tp->ParamtypePrestation->last()->update([
            'libelle' => htmlspecialchars($request->libelle),
            'added_by' => Auth::user()->id,
        ]);

        $ptp = new ParamtypePrestation();
        

        if ($request->plafond and ($request->plafond != $tp->ParamtypePrestation->last()->plafond)) {
        
            $ptp->libelle = htmlspecialchars($request->libelle);
            $ptp->plafond = $request->plafond; 
            $ptp->taux = $request->taux;
            $ptp->lettre = $request->lettre;
            $ptp->valeurLettre = $request->valeur;
            $ptp->added_by = Auth::user()->id;
            $ptp->type_prestation_id = $tp->id;
            $ptp->save();
            session()->flash('message',"Categorie $request->libelle modifiée avec succès.");
            return redirect()->back();
        }
        if($request->taux != $tp->ParamtypePrestation->last()->taux){
            
            $ptp->libelle = htmlspecialchars($request->libelle);
            $ptp->taux = $request->taux;
            $ptp->plafond = $request->plafond;
            $ptp->lettre = $request->lettre;
            $ptp->valeurLettre = $request->valeur;
            $ptp->added_by = Auth::user()->id;
            $ptp->type_prestation_id = $tp->id;
            $ptp->save();
            session()->flash('message',"Categorie $request->libelle modifiée avec succès.");
            return redirect()->back();
        }
        if($request->lettre and ($request->lettre != $tp->ParamtypePrestation->last()->lettre)){
            
            $ptp->libelle = htmlspecialchars($request->libelle);
            $ptp->lettre = $request->lettre;
            $ptp->valeurLettre = $request->valeur;
            $ptp->taux = $request->taux;
            $ptp->plafond = $request->plafond;
            $ptp->added_by = Auth::user()->id;
            $ptp->type_prestation_id = $tp->id;
            $ptp->save();
            session()->flash('message',"Categorie $request->libelle modifiée avec succès.");
            return redirect()->back();
        }
        if($request->lettre and ($request->valeur != $tp->ParamtypePrestation->last()->valeurLettre)){
            
            $ptp->libelle = htmlspecialchars($request->libelle);
            $ptp->lettre = $request->lettre;
            $ptp->valeurLettre = $request->valeur;
            $ptp->taux = $request->taux;
            $ptp->plafond = $request->plafond;
            $ptp->added_by = Auth::user()->id;
            $ptp->type_prestation_id = $tp->id;
            $ptp->save();
            session()->flash('message',"Categorie $request->libelle modifiée avec succès.");
            return redirect()->back();
        }
        session()->flash('message',"Categorie $request->libelle modifiée avec succès.");
        return redirect()->back();
        
    }

    public function UpdatePrestation($id)
    {
        $idp = htmlspecialchars($id);
        $p = Prestation::find($idp);
        $tp = typePrestation::where('etat',true)->get();
        return view('parametre.modal.updatePrestation',compact('p','tp'));
    }

    public function AddingPRES(Request $request, $id){
        
        $request->validate([
            '_token' => 'required|string',
            'prestation' => 'required|string',
            'type_prestation_id' => 'required|integer',
        ]);

        $p = Prestation::find($id);

        if ($request->ententePrealable == "NON") {
            $enttent = 0;
         }else{
             $enttent = 1;
         }

        $p->update([
            'libelle' => htmlspecialchars($request->prestation),
            'bloc_id' => $request->bloc_ic,
            'added_by' => Auth::user()->id,
            'ententePrealable' => $enttent,
            'description' => $request->description,
            'exclusion' => $request->exclusion,
        ]);

        $p->ParamPrestation->last()->update([
            'libelle' => htmlspecialchars($request->prestation),
            'added_by' => Auth::user()->id,
            'duree' => $request->duree,
            'qte' => $request->qte,
            'entite' => $request->entite,
        ]);

        $pp = new ParamPrestation();
        if ($request->CoefiLettre and ($request->CoefiLettre != $p->ParamPrestation->last()->CoefiLettre)) {
            $request->validate([
                'CoefiLettre' => 'required|integer',
            ]);
            $pp->libelle = $request->prestation; 
            $pp->CoefiLettre = $request->CoefiLettre; 
            $pp->taux = $request->taux;
            $pp->plafond = $request->plafond;
            $pp->prestation_id = $p->id;
            $pp->added_by = Auth::user()->id;
            $pp->duree = $request->duree;
            $pp->qte = $request->qte;
            $pp->entite = $request->entite;
            
            $pp->save();
            session()->flash('message',"prestation $request->prestation modifiée avec succès.");
            return redirect()->back();
        }
        if ($request->taux and ($request->taux != $p->ParamPrestation->last()->taux)) {
            $request->validate([
                'taux' => 'required|integer',
            ]);
            $pp->libelle = $request->prestation; 
            $pp->CoefiLettre = $request->CoefiLettre; 
            $pp->taux = $request->taux;
            $pp->plafond = $request->plafond;
            $pp->prestation_id = $p->id;
            $pp->added_by = Auth::user()->id;
            $pp->duree = $request->duree;
            $pp->qte = $request->qte;
            $pp->entite = $request->entite;
            
            $pp->save();
            session()->flash('message',"prestation $request->prestation modifiée avec succès.");
            return redirect()->back();
        }
        if ($request->plafond and ($request->plafond != $p->ParamPrestation->last()->plafond)) {
            $request->validate([
                'plafond' => 'required|integer',
            ]);
            $pp->libelle = $request->prestation; 
            $pp->CoefiLettre = $request->CoefiLettre; 
            $pp->taux = $request->taux;
            $pp->plafond = $request->plafond;
            $pp->prestation_id = $p->id;
            $pp->added_by = Auth::user()->id;
            $pp->duree = $request->duree;
            $pp->qte = $request->qte;
            $pp->entite = $request->entite;
            
            $pp->save();
            session()->flash('message',"PRESTATION $request->prestation modifiée avec succès.");
            return redirect()->back();
        }
        session()->flash('message',"prestation $request->prestation modifiée avec succès.");
        return redirect()->back();
    }

    public function UpdatePP($id){
        $lp = ListePrestation::find($id);
        $tp = typePrestation::where('etat',true)->get();
        return view('admin.prestataire.update.updatePrestation',compact('lp','tp'));

    }

    public function UpdatingPRESP(Request $request, $id){
        $lp = ListePrestation::find($id);
      
        $tierPayant = Null;
        $parAssure = Null;
        $cout = Null;
        $fourmulecout = null ;
        $fourmuletp = null;

        $lp->update([
            'libelle' => $request->libelle,
            'description' => $request->description,
        ]);
        $lp->ParamlistePrestation->last()->update([
            'libelle' => $request->libelle,
            'description' => $request->description,
        ]);

        if($request->valeurLettre or $request->CoefiLettre){
            //dd($request->all());
            $cout= $request->CoefiLettre*$request->valeurLettre;
            $tierPayant = ($cout*$lp->Prestataire->ParamPrestataire->last()->taux)/100;
            if ($lp->listeprestable->typePrestation->ParamtypePrestation->last()->valeurLettre>0) {
                $couttp= $request->CoefiLettre*$lp->listeprestable->typePrestation->ParamtypePrestation->last()->valeurLettre;
                $tierPayant = ($couttp*$lp->Prestataire->ParamPrestataire->last()->taux)/100;
            }
            $parAssure = $cout - $tierPayant;
        }else{
            $cout = $request->cout;
            $tierPayant = ($cout*$lp->Prestataire->ParamPrestataire->last()->taux)/100;
            $parAssure = $cout - $tierPayant;
        }

        if ($lp->ParamPrestation->plafond) {
            if ($tierPayant > $lp->ParamPrestation->plafond ) {
                $tierPayant = $lp->ParamPrestation->plafond;
                $parAssure = $cout - $tierPayant;
            }
        }

        $plp = new ParamlistePrestation();
        if ($request->CoefiLettre and ($request->CoefiLettre != $lp->ParamlistePrestation->last()->CoefiLettre)) {
            $request->validate([
                'CoefiLettre' => 'required|integer',
            ]);

            $plp->libelle = $request->libelle; 
            $plp->valeurLettre = $request->valeurLettre;
            $plp->CoefiLettre = $request->CoefiLettre; 
            $plp->cout = $cout; 
            $plp->tierPayant = $tierPayant;
            $plp->parAssure = $parAssure; 
            $plp->liste_prestation_id = $lp->id;
            $plp->prestataire_id = $lp->Prestataire->id;
            $plp->added_by = Auth::user()->id;
            $plp->save();
            session()->flash('message',"prestation $request->libelle modifiée avec succès.");
            return redirect()->back();
        }
        if ($request->valeurLettre and ($request->valeurLettre != $lp->ParamlistePrestation->last()->valeurLettre)) {
            $request->validate([
                'valeurLettre' => 'required|integer',
            ]);
            $plp->libelle = $request->libelle; 
            $plp->valeurLettre = $request->valeurLettre;
            $plp->CoefiLettre = $request->CoefiLettre; 
            $plp->cout = $cout; 
            $plp->tierPayant = $tierPayant;
            $plp->parAssure = $parAssure; 
            $plp->liste_prestation_id = $lp->id;
            $plp->prestataire_id = $lp->Prestataire->id;
            $plp->added_by = Auth::user()->id;
            $plp->save();
            session()->flash('message',"prestation $request->libelle modifiée avec succès.");
            return redirect()->back();
        }
        if ($request->cout and ($request->cout != $lp->ParamlistePrestation->last()->cout)) {
            $request->validate([
                'cout' => 'required|integer',
            ]);
            $plp->libelle = $request->libelle; 
            $plp->valeurLettre = $request->valeurLettre;
            $plp->CoefiLettre = $request->CoefiLettre; 
            $plp->cout = $cout; 
            $plp->tierPayant = $tierPayant;
            $plp->parAssure = $parAssure; 
            $plp->prestataire_id = $lp->Prestataire->id;
            $plp->liste_prestation_id = $lp->id;
            $plp->added_by = Auth::user()->id;
            $plp->save();
            session()->flash('message',"prestation $request->libelle modifiée avec succès.");
            return redirect()->back();
        }
        session()->flash('message',"prestation $request->libelle modifiée avec succès.");
        return redirect()->back();
    }

}