<?php

namespace App\Http\Controllers\Detail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProjetMutua;
use App\Models\User;
use App\Models\Mutualiste;
use App\Models\Versement;
use App\Models\Ligne;
use App\Models\Enregistrement;
use App\Models\ListePrestation;
use App\Models\PrestaMuta;
use App\Models\Prestataire;
use App\Models\Facture;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;


class DetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('detail');
    } 

    public function ProfilUser($id)
    {
       $mutualiste = Mutualiste::find($id);
       return view('mutualiste.profil', compact('mutualiste'));
    }

    public function DetailsPP($lp){
        $listP = ListePrestation::find($lp);
        return view('admin.prestataire.prestations.details', compact('listP'));
    }

    public function ActesMedicaux(){
       
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.actes_impaye', compact('actesi','actesp','p','demandes','ListePrestation'));

    }

    public function AllActesMedicaux(){
       
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();

        $dataQuery = PrestaMuta::query()->where('prestataire_id', $p->id);
        $title = 'TOUT LES ACTES';

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.all_actes', compact('actesi','actesp','p','demandes','ListePrestation','dataQuery','title'));

    }


    public function ImpayerActesMedicaux(){
       
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();
        
        $dataQuery = $actesi;
        $title = 'TOUT LES ACTES IMPAYEES';

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.all_actes', compact('actesi','actesp','p','demandes','ListePrestation','dataQuery','title'));

    }


    public function PayerActesMedicaux(){
       
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $fi = $p->Facture->where('payer',0);
        $fi_id = [];
        $fi_id = $fi->pluck('id');
        $actesi = PrestaMuta::whereIn('facture_id',$fi_id)->get();

        $fp = $p->Facture->where('payer',1);
        $fp_id = [];
        $fp_id = $fp->pluck('id');
        $actesp = PrestaMuta::whereIn('facture_id',$fp_id)->get();

        $dataQuery = $actesp;
        $title = 'TOUT LES ACTES PAYEES';

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.all_actes', compact('actesi','actesp','p','demandes','ListePrestation','dataQuery','title'));

    }

    public function DemandeActesMedicaux(){
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['type', 'demande'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();

        return view('admin.acte.demande', compact('p','demandes','ListePrestation'));
    }

    public function DemandeActesMedicauxAccordees(){
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();

        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['entente', 'Accordée'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();
        $title = 'LISTE DES DEMANDES ACCORDEES';

        return view('admin.acte.all-demande', compact('p','demandes','ListePrestation','title'));
    }

    public function DemandeActesMedicauxRejetees(){
        $p = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $demandes = PrestaMuta::where([
            ['prestataire_id', $p->id],
            ['entente', 'Rejetée'],
        ])->orderBy('id','desc')->get();

        $ListePrestation = $p->ListePrestation()->get();
        $title = 'LISTE DES DEMANDES REJETEES';

        return view('admin.acte.all-demande', compact('p','demandes','ListePrestation','title'));
    }

    public function FacturesActesMedicaux(){

        $prestataire = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        
        $factures = Facture::where([
            ['prestataire_id', $prestataire->id],
            ['etat', 'Validée'],
        ])->orderBy('id','desc');
        
        $facturesMonth = $factures->whereMonth('date',date('m'))->get();
        
        $factures_all = $factures;
        $factures = $factures->get();
    

        return view('admin.facture.factures', compact('prestataire','factures','factures_all','facturesMonth'));
    }

    public function Prestations(){

        $prestataire = Prestataire::where('user_id', Auth::user()->principal_id)->first();
        $prestations = ListePrestation::where([
            ['etat',1],
            ['prestataire_id',$prestataire->id],
        ])->get();
        return view('admin.prestation.prestations', compact('prestataire','prestations'));
    }

    public function RechecheActesMedicaux(Request $request){

        $prestataire = Prestataire::where('user_id', Auth::user()->principal_id)->first();

        $query = PrestaMuta::query()->where('prestataire_id', $prestataire->id);

        if ($request->has('datedebut') && $request->input('datedebut')!='') {
            $query->where('date', '>=', $request->input('datedebut'));
        }

        if ($request->has('datefin') && $request->input('datefin')!='') {
            $query->where('date', '<=', $request->input('datefin'));
        }

        if ($request->has('nature_id') && $request->input('nature_id') !== 'all') {
            $query->where('liste_prestation_id', $request->input('nature_id'));
        }

        if ($request->has('type') && $request->input('type') !== 'all') {
            $query->where('prestamutable_type', $request->input('type'));
        }

        if ($request->has('statut') && $request->input('statut')!='all') {
            $query->where('entente', $request->input('statut'));
        }

        if ($request->has('query')) {
            $query->where('type', 'demande');
        }

        $resultats = $query->orderBy('id','desc')->get();

        ?>

        <?php if($resultats->count()>0) : ?>
            <?php foreach($resultats as $ai): ?>
            <?php
                $data = $ai->prestamutable_type::find($ai->prestamutable_id);

                if($data->prestamutable_type == 'App\Models\Mutualiste'){
                    $typeAss = "Mutualiste";
                }else{
                    $typeAss = "Ayant-droit";
                }
            ?>
            <tr>
                <td class="txt-dark">
                    <div class="d-flex align-items-center">
                        <?php if(!$data->photo) : ?>
                            <img src="<?= \Illuminate\Support\Facades\URL::asset('erpfiles/dist/img/mock1.jpg') ?> " class="avatar avatar-md rounded-circle" alt="">
                        <?php else: ?>
                            <?php if($ai->prestamutable_type =='App\Models\Mutualiste'): ?>
                                <img src='<?= config("app.imgLink") ?><?= $data->photo ?> ' class="avatar avatar-md rounded-circle" alt="">
                            <?php else: ?>
                                <img src='<?= config("app.adLink") ?><?= $data->photo ?> ' class="avatar avatar-md rounded-circle" alt="">
                            <?php endif ?>
                        <?php endif ?>
                        <p class="mb-0 ms-2 txt-dark">
                            <a href="">
                                <?= $data->first_name ?>  <?= $data->last_name ?? $data->name ?> 
                            </a>
                        </p>	
                    </div>
                </td>
                <td class="txt-dark"><?= date('d/m/Y', strtotime($ai->date)) ?>  à <b><?= $ai->heure ?> </b></td>
                <td class="txt-dark"><?= $ai->libelle ?></td>
                
                <td class="txt-dark"><?= number_format($ai->ListePrestation->ParamlistePrestation->last()->cout, 0, ',', ' ') ?>  FCsssFA</td>
                <td class="txt-dark"><?= number_format($ai->part_assure, 0, ',', ' ') ?>  FCFA</td>
                <td class="txt-dark"><b><?= number_format($ai->part_muscopci, 0, ',', ' ') ?>  FCFA</b></td>

                <?php if($request->has('query')): ?>
                    <td class="txt-dark">
                        <b><?php $ai->entente ?></b>
                    </td>
                <?php endif ?>
                
                <td>
                <button type="button" href="<?= route('ViewActeTampon', [$ai->id , $ai->prestamutable_id,$typeAss] ) ?> " class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
                </td>
            </tr>
            <?php endforeach ?>
        <?php endif ?>

        <?php

    }

    public function RechecheFacture(Request $request){

        $prestataire = Prestataire::where('user_id', Auth::user()->principal_id)->first();

        $query = Facture::query()->where('prestataire_id', $prestataire->id)
                                 ->where('etat', 'Validée');

        if ($request->has('datedebut') && $request->input('datedebut')!='') {
            $query->where('date', '>=', $request->input('datedebut'));
        }

        if ($request->has('datefin') && $request->input('datefin')!='') {
            $query->where('date', '<=', $request->input('datefin'));
        }

        if ($request->has('statut') && $request->input('statut')!='all') {
            $query->where('payer', $request->input('statut'));
        }

        $resultats = $query->orderBy('id','desc')->get();
        ?>
        <?php if($resultats->count()>0) : ?>
            <?php foreach($resultats as $facture): ?>
                <?php
                    $ajp = $facture->PrestaMutua()->first();
                    $data = $ajp->prestamutable_type::find($ajp->prestamutable_id);
                ?>
                <tr>
                    <td class="txt-dark">
                        <div class="d-flex align-items-center">
                            <?php if(!$data->photo): ?>
                                <img src="<?= \Illuminate\Support\Facades\URL::asset('erpfiles/dist/img/mock1.jpg')?>" class="avatar avatar-md rounded-circle" alt="">
                            <?php else: ?>
                                <?php if($ajp->prestamutable_type =='App\Models\Mutualiste'): ?>
                                    <img src='<?=config("app.imgLink")?><?=$data->photo?>' class="avatar avatar-md rounded-circle" alt="">
                                <?php else: ?>
                                    <img src='<?=config("app.adLink")?><?=$data->photo?>' class="avatar avatar-md rounded-circle" alt="">
                                <?php endif ?>
                            <?php endif ?>
                            <p class="mb-0 ms-2 txt-dark">
                                <a href="">
                                    <?=$data->first_name?> <?=$data->last_name ?? $data->name?>
                                </a>
                            </p>	
                        </div>
                    </td>
                    <td class="txt-dark"><?=date('d/m/Y', strtotime($facture->date))?> à <b><?=$facture->heure?></b></td>
                    <td class="txt-dark text-center" ><span class="badge badge-primary border-0"><?=$facture->PrestaMutua->count()?></span></td>
                    <td class="txt-dark"><?=number_format($facture->montant, 0, ',', ' ')?> FCFA</td>
                    <td class="txt-dark"><?=number_format($facture->part_assure, 0, ',', ' ')?> FCFA</td>
                    <td class="txt-dark"><b><?=number_format($facture->part_muscopci, 0, ',', ' ')?> FCFA</b></td>
                    <td>
                        <button type="button" href="<?=route('ViewFacture', [$facture->id] )?>" class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>

        <?php

    }

}
