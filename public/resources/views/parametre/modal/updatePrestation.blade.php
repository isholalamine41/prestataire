<div class="modal fade affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #ff2a00;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge" style="color:#fff !important;">MODIFIER {{$p->libelle}}</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('AddingPRES',$p->id)}}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Catégorie*</label>
                    <select name="type_prestation_id" class="form-control changeCat" required="" id="">
                        <option selected value="{{$p->TypePrestation->id}}">{{$p->TypePrestation->libelle}}</option>
                        @if($tp->count()>0)
                            @foreach($tp as $t)
                                <option value="{{$t->id}}">{{$t->libelle}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Prestation *</label>
                    <input type="text" class="form-control" name="prestation" value="{{$p->libelle}}"  required="">
                </div>
            </div>
            <div class="row retouronchange" >
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Accord préable avant la prestation ? * </label>
                    <select name="ententePrealable" class="form-control" required="" id="">
                        @if($p->ententePrealable)
                            <option selected value="OUI">OUI</option>
                            <option value="NON">NON</option>
                         @else
                            <option selected value="NON">NON</option>
                            <option value="OUI">OUI</option>
                        @endif
                        
                        
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Plafond de Rembourssement </label>
                    @if(!is_null($p->ParamPrestation->last()->plafond))
                        <input type="number" class="form-control" value="{{$p->ParamPrestation->last()->plafond}}" name="plafond">
                        @else
                        <input type="number" class="form-control" name="plafond">
                    @endif
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Taux de couverture *</label>
                    <select name="taux" class="form-control"  required="" id="">
                        <option></option>
                            @if(!is_null($p->ParamPrestation->last()->taux))
                                <option selected  value="{{$p->ParamPrestation->last()->taux}}"> <span style="color:brown; font-weight:500; height: 40px;">{{$p->ParamPrestation->last()->taux}}%</span></option>
                            @endif
                        
                        <option value="70">70%</option>
                        <option value="80">80%</option>
                        <option value="90">90%</option>
                        <option value="100">100%</option>
                    </select>
                </div>
                @if(!is_null($p->typePrestation->ParamtypePrestation->last()->lettre))
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Coéficient Lettre <span style="font-weight: bold; color:brown">{{$p->typePrestation->ParamtypePrestation->last()->lettre}}</span> pour cette prestation</label>
                    <input type="number" placeholder="{{$p->typePrestation->ParamtypePrestation->last()->lettre}} ?" value="{{$p->ParamPrestation->last()->CoefiLettre}}" name="CoefiLettre" class="form-control">
                </div>
                @else
                
                @endif
            </div>
            <div class="row" >
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Duree</label>
                    <select name="duree" class="form-control" id="">
                        <option selected value="{{$p->ParamPrestation->last()->duree}}">{{$p->ParamPrestation->last()->duree}} </option>
                        <option></option>
                        <option value="1">1 an</option>
                        <option value="2">2 ans</option>
                        <option value="3">3 ans</option>
                    </select>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Assuré/famille</label>
                    <select name="entite" class="form-control" id="">
                        <option selected value="{{$p->ParamPrestation->last()->entite}}">{{$p->ParamPrestation->last()->entite}}</option>
                        <option></option>
                        <option value="Assuré">Assuré</option>
                        <option value="Famille">Famille</option>
                    </select>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> <br>
                    <label for="" class="txt-dark">Quantité</label>
                    <input type="number" name="qte" class="form-control" value="{{$p->ParamPrestation->last()->qte}}" id="">
                </div>
            </div>
            <div class="row"><br>
                <div class="col-md-12">
                    <label for="" class="txt-dark">Commentaire</label>
                    <textarea name="description" class="form-control" id="" cols="30" rows="2">{{$p->description}}</textarea>
                </div>
            </div>
            <div class="row"><br>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <select name="exclusion" class="form-control" id="">
                        @if($p->exclusion)
                        <option selected value="1">EXCLUS</option>
                        <option  value="0">PAS EXCLUS</option>
                        @else
                        <option selected value="0">PAS EXCLUS</option>
                        <option value="1">EXCLUS</option>
                        @endif
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" align="right">
                    <button type="submit" style=" width:100%" class="btn btn-danger"> <i class="fa fa-save"></i> MODIFIER</button>  
                </div>
            </div>
         
        </form>   
      </div>
    </div>
  </div>
</div>