    
    @if(!is_null($p->ParamPrestation->last()->plafond))
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Plafond de Rembourssement </label>
        <input type="number" class="form-control" value="{{$p->ParamPrestation->last()->plafond}}" name="plafond">
    </div>
    @endif
    @if(!is_null($p->ParamPrestation->last()->taux))
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Taux de couverture *</label>
        <select name="taux" class="form-control"  required="" id="">
            <option selected  value="{{$p->ParamPrestation->last()->taux}}"> <span style="color:brown; font-weight:500; height: 40px;">{{$p->ParamPrestation->last()->taux}}%</span></option>
        </select>
    </div>
    @endif
    @if(!is_null($p->typePrestation->ParamtypePrestation->last()->lettre))
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Coéficient Lettre* <span style="font-weight: bold; color:brown">{{$p->typePrestation->ParamtypePrestation->last()->lettre}}</span> pour cette prestation</label>
        <input type="number" required="" placeholder="{{$p->typePrestation->ParamtypePrestation->last()->lettre}} ?" name="CoefiLettre" class="form-control">
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Valeur de la Lettre <span style="font-weight: bold; color:brown">{{$p->typePrestation->ParamtypePrestation->last()->Lettre}}</span></label>
        <input type="number" required="" placeholder="{{$p->typePrestation->ParamtypePrestation->last()->valeurLettre}} FCFA ?" name="valeurLettre" class="form-control">
    </div>
    @else
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Coût du prestation</label>
        <input type="number" required="" name="cout" class="form-control">
    </div>
    @endif