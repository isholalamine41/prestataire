    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Accord préable avant la prestation ? * </label>
        <select name="ententePrealable" class="form-control" required="" id="">
            <option></option>
            <option value="NON">NON</option>
            <option value="OUI">OUI</option>
        </select>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Plafond de Rembourssement </label>
        @if(!is_null($tp->ParamtypePrestation->last()->plafond))
            <input type="number" class="form-control" value="{{$tp->ParamtypePrestation->last()->plafond}}" name="plafond">
            @else
            <input type="number" class="form-control" name="plafond">
        @endif
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Taux de couverture *</label>
        <select name="taux" class="form-control"  required="" id="">
            <option></option>
            @if(!is_null($tp->ParamtypePrestation->last()->taux))
                <option selected  value="{{$tp->ParamtypePrestation->last()->taux}}"> <span style="color:brown; font-weight:500; height: 40px;">{{$tp->ParamtypePrestation->last()->taux}}%</span></option>
            @endif
            <option value="70">70%</option>
            <option selected value="80">80%</option>
            <option value="90">90%</option>
            <option value="100">100%</option>
        </select>
    </div>
    @if(!is_null($tp->ParamtypePrestation->last()->lettre))
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <br>
        <label for="" class="txt-dark">Coéficient Lettre <span style="font-weight: bold; color:brown">{{$tp->ParamtypePrestation->last()->lettre}}</span> pour cette prestation</label>
        <input type="number" placeholder="{{$tp->ParamtypePrestation->last()->lettre}} ?" name="CoefiLettre" class="form-control">
    </div>
    @else
    
    @endif