<div class="modal fade affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#01c853;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge" style="color:#fff !important;">AJOUTER UN BLOC</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('AddingBLOC')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"> 
               <label for="" class="txt-dark">LIBELLE *</label>
               <input type="text" class="form-control" name="libelle" required="">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> <br>
            <button style="background:#01c853" type="submit" style="margin-top: 1.5em;" class="btn btn-success"><i class="fa fa-save"></i> AJOUTER</button>  
            </div>
           
          </div>
         
        </form>   
      </div>
    </div>
  </div>
</div>