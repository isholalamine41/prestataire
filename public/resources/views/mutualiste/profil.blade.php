@extends('layouts.admin.master')

@section('content')
<!-- Main Content -->
<!-- Main Content -->
<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					
					<div class="col-lg-9 col-xs-12">
                        <!-- Row -->
						<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="panel panel-default card-view pa-0">
									<div class="panel-wrapper collapse in">
										<div class="panel-body pa-0">
											<div class="sm-data-box bg-blue">
												<div class="container-fluid">
													<div class="row">
														<a href="">
														<div class="col-xs-8 text-center pl-0 pr-0 data-wrap-left">
															<span class="txt-light block counter" style="font-size:16px;">CONSOMMATIONS TOTAL</span> <br>
															<span style="font-size: 22px;" class="weight-500 uppercase-font txt-light block"><span>
															{{number_format(100000, 0, ',', ' ')}} 
															</span> FCFA</span>
														</div>
														<div class="col-xs-4 text-center  pl-0 pr-0 data-wrap-right">
															<i class="fa fa-money txt-light data-right-rep-icon"></i>
														</div>
														</a>
													</div>  
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="panel panel-default card-view pa-0">
									<div class="panel-wrapper collapse in">
										<div class="panel-body pa-0">
											<div class="sm-data-box bg-green">
												<div class="container-fluid">
													<div class="row">
														<a href="">
														<div class="col-xs-8 text-center pl-0 pr-0 data-wrap-left">
														<span class="txt-light block counter" style="font-size:16px;">BENEFICIAIRES FAMILLE</span> <br>
														<span style="font-size: 22px;" class="weight-500 uppercase-font txt-light block"><span> 
														{{$mutualiste->Ayantdroits->count() + 1}}
														</div>
														<div class="col-xs-4 text-center  pl-0 pr-0 data-wrap-right">
														<i class="fa fa-users txt-light data-right-rep-icon"></i>
														</div>
														</a>
													</div>  
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
								<div class="panel panel-default card-view pa-0">
									<div class="panel-wrapper collapse in">
										<div class="panel-body pa-0">
											<div class="sm-data-box bg-red">
												<div class="container-fluid">
													<div class="row">
														<a href="#">
														<div class="col-xs-8 text-center pl-0 pr-0 data-wrap-left">
															<span class=" txt-light block counter" style="font-size: 16px;">TOTAL SINISTRES</span> <br>
															<span style="font-size: 22px;" class="weight-500 uppercase-font txt-light block"><span> 
																{{number_format(300, 0, ',', ' ')}} 
															
														</div>
														<div class="col-xs-4 text-center  pl-0 pr-0 data-wrap-right">
														<i class="fa fa-ambulance txt-light data-right-rep-icon"></i>
														</div>
														</a>
													</div>  
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						
							
						</div>
                		<!-- /Row -->
						<!-- end row-->
						
						<div class="row">
							<div class="col-md-12">
								@if(session()->has('messageErreur'))
									<div class="alert alert-danger alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><strong>Erreur ! </strong> {{ Session::get('messageErreur') }}</p>
										<div class="clearfix"></div>
									</div>
									@endif
									@if(session()->has('message'))
									<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><strong>Félicitations ! </strong> {{ Session::get('message') }}</p> 
										<div class="clearfix"></div>
									</div>
								@endif
							</div>
						</div>
						<!-- Row -->
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="profile_tab_8" role="tab" href="#profile_8" aria-expanded="false"><span> <b style="font-weight:bold;">ASSURE PRINCIPAL</b></span></a></li>
											
											<li role="presentation" class=""><a  data-toggle="tab" id="photos_tab_8" role="tab" href="#earnings_9" aria-expanded="false"><span><b style="font-weight:bold;">SINISTRES (1)</b></span></a></li>

											
											<li role="presentation" class="next"><a aria-expanded="true"  data-toggle="tab" role="tab" id="follo_tab_8" href="#follo_8"><span><b style="font-weight:bold;">AYANTS DROITS ({{$mutualiste->Ayantdroits->count()}})</b></span></a></li>
											
											<li role="presentation" class=""><a  data-toggle="tab" id="photos_tab_8" role="tab" href="#photos_8" aria-expanded="false"><span><b style="font-weight:bold;">DEMANDES (0)</b></span></a></li>
											<li role="presentation" class=""><a  data-toggle="tab" id="photos_tab_8" role="tab" href="#photos_9" aria-expanded="false"><span><b style="font-weight:bold;">STATISTIQUES</b></span></a></li>

											
										
										</ul>
										<div class="tab-content" id="myTabContent_8">
										<div  id="profile_8" class="tab-pane fade  active in" role="tabpanel">
												<div class="col-lg-12 col-xs-12">
													<div class="pt-20">
														<div class="streamline user-activity">
															<div class="row">
																<div class="col-md-6">
																	<h5 class="block mt-10 mb-5 weight-500 capitalize-font txt-dark">{{$mutualiste->name}} {{$mutualiste->first_name}}</h5>
																	<h6 class="block capitalize-font pb-20"><span class="weight-500">Matricule: </span>{{$mutualiste->registration_number}} <br>
																	<b class="weight-500">Contact:</b> {{$mutualiste->phone}} <br>
																	<b class="weight-500">Naissance:</b> {{$mutualiste->birth_date}} à {{$mutualiste->birth_place}}<br>
																	<b class="weight-500">Position Statutaire:</b> {{$mutualiste->statutory_position}} <br>
																	
																	</h6>
																	
																	</h6>
																</div>
																<div class="col-md-6">
																<h6><b class="weight-500">Fonction:</b> {{$mutualiste->fonction}}<br>
																<b class="weight-500">Grade:</b>  {{$mutualiste->grade}}<br>
																<b class="weight-500">Echelon:</b>  {{$mutualiste->echelon}}<br>
																<b class="weight-500">Service:</b>  {{$mutualiste->role}}<br>
																<b class="weight-500">Nbre Sinistre:</b> <span class="label label-danger">12</span> 
																</h6>    
																<h6>

																</h6><br>
																	 
																</div>
																
																
															</div>
														</div>
													</div>
												</div>
											</div>
											<div  id="earnings_9" class="tab-pane fade" role="tabpanel">
												<!-- Row -->
												<div class="col-lg-12 col-xs-12">
												<div class="row">
													<div class="col-sm-12">
														<div class="panel panel-default card-view">
															<div class="panel-heading">
																<div class="pull-left">
																	<h6 class="panel-title txt-dark">SINISTRES FAMILLE <b style="font-weight: bolder;">{{$mutualiste->name}} {{$mutualiste->first_name}} (0)</b></h6>
																</div>
																<div class="pull-right">
																	<button title="EXPORTER LA LISTE DES SINISTRES EN EXCEL" class="btn btn-primary btn-anim"><span class="btn-text"> EXPORTATER EN EXCEL </span><i style="color: #fff;" class="fa fa-file-excel-o fa-x2"></i></button>
																</div>
																<div class="clearfix"></div>
															</div>
															<div class="panel-wrapper collapse in">
																<div class="panel-body">
																	<div class="table-wrap">
																		<div class="table-responsive">
																			<table id="datable_1" class="table table-hover display  pb-30" >
																				<thead>
																					<tr>
																						<th>DATE</th>
																						<th>ASSURE</th>
																						<th>MOUVEMENT</th>
																						<th>PRESTATAIRE</th>
																						<th>COUT</th>
																						<th>STATUT</th>
																						<th>ACTIONS</th>
																					</tr>
																				</thead>
																				
																				<tbody>
																					<tr>
																						<td>2011/04/25</td>
																						<td>ZODI BORENE ADINGRA</td>
																						<td>System Architect</td>
																						<td>Edinburgh</td>
																						<td class="txt-dark">61 000 FCFA</td>
																						<td><span class="label label-success">Accordé</span></td>
																						<td><button href="" title="Voir les Détails" class="btn btn-success btn-icon-anim btn-square DetailView"><i class="fa fa-eye"></i></button></td>
																					</tr>
																					<tr>
																						<td>2011/04/25</td>
																						<td>ZODI BORENE ADINGRA</td>
																						<td>System Architect</td>
																						<td>Edinburgh</td>
																						<td class="txt-dark">61 000 FCFA</td>
																						<td><span class="label label-danger">refusé</span></td>
																						<td><button href="" title="Voir les Détails" class="btn btn-success btn-icon-anim btn-square DetailView"><i class="fa fa-eye"></i></button></td>
																					</tr>
																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
															</div>
														</div>	
													</div>
												</div>
												<!-- /Row -->
												</div>
											</div>
											
											

											
											
											<div  id="follo_8" class="tab-pane fade" role="tabpanel">
												
													<div class="col-lg-12 col-xs-12"><br>
														<div class="table-responsive">
															<table class="table table-hover mb-0">
																<thead>
																	<tr>
																		<th>Image</th>
																		<th>Nom complet</th>
																		<th>Type</th>
																		<th>Contact</th>
																		<th>N° Police</th>
																		<th>SINISTRES</th>
																		<th>Actions</th>
																	</tr>
																</thead>
																<tbody>
																	@if($mutualiste->Ayantdroits)
																		@foreach($mutualiste->Ayantdroits as $mad)
																		<tr>
																			<td style="height: 70px;">
																				<img class="inline-block mb-10" style="height: 70px;" src='{{config("app.adLink")}}{{$mad->photo}}'/>
																			</td>
																			<td>
																				<span class="txt-dark weight-500">{{$mad->last_name}} {{$mad->first_name}}</span>
																			</td>
																			<td>{{ ($mad->Typedroit) ? $mad->Typedroit->libelle : "" }}</td>
																			<td>{{$mad->contact}}</td>
																			<!-- <td><span class="txt-success"><i class="zmdi zmdi-caret-up mr-10 font-20"></i><span>2.43%</span></span></td> -->
																			<td>
																				<span class="txt-dark weight-500">{{$mad->num_pocile}}</span>
																			</td>
																			<td>
																				<span class="label label-danger">12</span>
																			</td>
																			<td>
																			<button href="" title="Voir les Détails" class="btn btn-success btn-icon-anim btn-square DetailView"><i class="fa fa-eye"></i></button>

																			</td>
																		</tr>
																		@endforeach
																		
																		
																	@endif

																</tbody>
															</table>
														</div>	
													</div>
											</div>

											<div  id="photos_8" class="tab-pane fade" role="tabpanel">
												<div class="col-lg-12 col-xs-12">
													Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea officia ipsum facilis sequi minus voluptatum iste dolorem! Sit explicabo voluptas distinctio quidem praesentium officiis nesciunt exercitationem vero eaque culpa? Tempore.
												</div>
											</div>
											<div  id="photos_9" class="tab-pane fade" role="tabpanel">
											 	<div class="col-lg-12 col-xs-12">

													<div class="row">
														<div class="col-md-3" >
															<div class="panel panel-default card-view" style="border: solid 2px #2879ff;">
																<div class="panel-wrapper collapse in">
																	<div class="panel-body sm-data-box-1" >
																		<div class="sm-data-box-1">
																			<span class="weight-500 font-12 block text-center txt-dark ">
																				<b style="font-weight:bold;">CONSO ASSURE PRINCIPAL</b> 
																			</span> 
																			<div class="weight-500 text-center txt-dark">
																				<span style="font-size:12px;">1 000 000</span><span  class="font-10"> FCFA</span>
																			</div>
																		</div> 
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="panel panel-default card-view" style="border: solid 2px  orange">
																<div class="panel-wrapper collapse in">
																	<div class="panel-body sm-data-box-1">
																		<a href="">
																			<div class="sm-data-box-1">
																				<span class="weight-500 font-12 block text-center txt-dark ">
																					<b style="font-weight:bold;">CONSO AYANTS-DROITS</b> 
																				</span> 
																				<div class="weight-500 text-center txt-dark">
																					<span style="font-size:12px;">2 000 000</span><span  class="font-10"> FCFA</span>
																				</div>
																			</div> 
																		</a>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="panel panel-default card-view" style="border: solid 2px  red">
																<div class="panel-wrapper collapse in">
																	<div class="panel-body sm-data-box-1">
																		<a href="">
																			<div class="sm-data-box-1">
																				<span class="weight-500 font-12 block text-center txt-dark ">
																					<b style="font-weight:bold;">CONSO GLOBALE</b> 
																				</span> 
																				<div class="weight-500 text-center txt-dark">
																					<span style="font-size:12px;">2 000 000</span><span  class="font-10"> FCFA</span>
																				</div>
																			</div> 
																		</a>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-3">
															<div class="panel panel-default card-view" style="border: solid 2px  green">
																<div class="panel-wrapper collapse in">
																	<div class="panel-body sm-data-box-1">
																		<div class="sm-data-box-1">
																			<span class="weight-500 font-12 block text-center txt-dark ">
																				<b style="font-weight:bold;">BALANCE FAMILLE</b> 
																			</span> 
																			<div class="weight-500 text-center txt-dark">
																				<span style="font-size:12px;">2 000 000</span><span  class="font-10"> FCFA</span>
																			</div>
																		</div> 
																	</div>
																</div>
															</div>
														</div>
													</div>
													
													<div class="row">
														<div class="col-lg-8">
															<div class="panel panel-default card-view">
																<div class="panel-heading">
																	<div class="pull-left">
																		<h6 class="panel-title txt-dark">CONSOMMATION FAMILLE PAR MOIS</h6>
																	</div>
																	<div class="clearfix"></div>
																</div>
																<div class="panel-wrapper collapse in">
																	<div class="panel-body">
																		<div id="morris_bar_chart" class="morris-chart" style="position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="390" version="1.1" width="560" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.333374px; top: -0.666687px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="41.8125" y="328.7236372780501" text-anchor="end" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="3.999993975132611" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#878787" d="M54.3125,328.7236372780501H535" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="41.8125" y="252.79272795853757" text-anchor="end" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="3.999992858745088" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">500</tspan></text><path fill="none" stroke="#878787" d="M54.3125,252.79272795853757H535" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="41.8125" y="176.86181863902505" text-anchor="end" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="3.9999917423575653" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">1,000</tspan></text><path fill="none" stroke="#878787" d="M54.3125,176.86181863902505H535" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="41.8125" y="100.93090931951252" text-anchor="end" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="3.9999982553645737" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">1,500</tspan></text><path fill="none" stroke="#878787" d="M54.3125,100.93090931951252H535" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="41.8125" y="25" text-anchor="end" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="3.9999990463256836" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2,000</tspan></text><path fill="none" stroke="#878787" d="M54.3125,25H535" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="494.9427083333333" y="341.2236372780501" text-anchor="middle" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(0.8192,-0.5736,0.5736,0.8192,-125.305,364.9757)"><tspan dy="3.999993975132611" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">iPhone 5</tspan></text><text x="414.828125" y="341.2236372780501" text-anchor="middle" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(0.8192,-0.5736,0.5736,0.8192,-142.7776,321.1856)"><tspan dy="3.999993975132611" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">iPhone 4S</tspan></text><text x="334.7135416666667" y="341.2236372780501" text-anchor="middle" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(0.8192,-0.5736,0.5736,0.8192,-154.2821,273.072)"><tspan dy="3.999993975132611" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">iPhone 4</tspan></text><text x="254.59895833333334" y="341.2236372780501" text-anchor="middle" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(0.8192,-0.5736,0.5736,0.8192,-175.1019,231.6281)"><tspan dy="3.999993975132611" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">iPhone 3GS</tspan></text><text x="174.484375" y="341.2236372780501" text-anchor="middle" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(0.8192,-0.5736,0.5736,0.8192,-186.6083,183.5134)"><tspan dy="3.999993975132611" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">iPhone 3G</tspan></text><text x="94.36979166666666" y="341.2236372780501" text-anchor="middle" font-family="Roboto" font-size="12px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Roboto; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(0.8192,-0.5736,0.5736,0.8192,-193.8693,132.6082)"><tspan dy="3.999993975132611" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">iPhone</tspan></text><rect x="64.32682291666666" y="308.0704299431427" width="60.0859375" height="20.653207334907393" rx="0" ry="0" fill="#fec107" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="144.44140624999997" y="307.9185681245037" width="60.0859375" height="20.805069153546413" rx="0" ry="0" fill="#fec107" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="224.55598958333331" y="286.9616371523182" width="60.0859375" height="41.7620001257319" rx="0" ry="0" fill="#fec107" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="304.6705729166667" y="271.0161461952206" width="60.0859375" height="57.707491082829506" rx="0" ry="0" fill="#fec107" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="384.78515625" y="229.25414606948868" width="60.0859375" height="99.46949120856141" rx="0" ry="0" fill="#fec107" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="464.8997395833333" y="90.14872019614177" width="60.0859375" height="238.57491708190832" rx="0" ry="0" fill="#fec107" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect></svg><div class="morris-hover morris-default-style" style="left: 45.3698px; top: 170px; display: none;"><div class="morris-hover-row-label">iPhone</div><div class="morris-hover-point" style="color: #fec107">
																			Geekbench:
																			136
																		</div>
																	</div>
																</div>
															</div>
														</div>
													   </div>
														</div>
														<div class="col-lg-4">
															<div class="panel panel-default card-view">
																<div class="panel-heading">
																	<div class="pull-left">
																		<h6 class="panel-title txt-dark">CONSOMMATION PAR BENEFICIAIRE</h6>
																	</div>
																	<div class="clearfix"></div>
																</div>
																<div class="panel-wrapper collapse in">
																	<div class="panel-body">
																		<div id="morris_donut_chart" class="morris-chart donut-chart"><svg height="390" version="1.1" width="259" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.552124px; top: -0.666672px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#2879ff" d="M129.5,277.6666666666667A79.66666666666667,79.66666666666667,0,0,0,204.90828697510662,223.69762699662644" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#2879ff" stroke="#ffffff" d="M129.5,280.6666666666667A82.66666666666667,82.66666666666667,0,0,0,207.74792958086377,224.66532006344502L237.87969278639798,234.9336187169087A114.5,114.5,0,0,1,129.5,312.5Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#e91e63" d="M204.90828697510662,223.69762699662644A79.66666666666667,79.66666666666667,0,0,0,58.053581934140865,162.75532489387572" stroke-width="2" opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#e91e63" stroke="#ffffff" d="M207.74792958086377,224.66532006344502A82.66666666666667,82.66666666666667,0,0,0,55.36313104463153,161.4281195551514L22.330372901211305,145.1329873408136A119.5,119.5,0,0,1,242.61243046265992,236.54644049493967Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#fec107" d="M58.053581934140865,162.75532489387572A79.66666666666667,79.66666666666667,0,0,0,129.4749719789381,277.66666273527426" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#fec107" stroke="#ffffff" d="M55.36313104463153,161.4281195551514A82.66666666666667,82.66666666666667,0,0,0,129.47402950115753,280.66666258723023L129.4640287647081,312.4999943496515A114.5,114.5,0,0,1,26.814457717060208,147.3449962386875Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="129.5" y="188" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="15px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 15px; font-weight: 800;" font-weight="800" transform="matrix(1.2425,0,0,1.2425,-31.4279,-48.182)" stroke-width="0.8048116878153058"><tspan dy="5.9999895095825195" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">In-Store Sales</tspan></text><text x="129.5" y="208" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="14px" stroke="none" fill="#878787" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 14px;" transform="matrix(1.7319,0,0,1.7319,-94.779,-146.6207)" stroke-width="0.5774058816822004"><tspan dy="4.9999895095825195" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30</tspan></text></svg></div>
																	</div>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						
							
					</div>
					<div class="col-lg-3 col-xs-12">
						<div class="panel panel-default card-view  pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body  pa-0">
									<div class="profile-box">
										<div class="profile-cover-pic" style="background:#09b76e;">
											<div class="profile-image-overlay"></div>
										</div>
										<div class="profile-info text-center">
											<div class="profile-img-wrap">
												@if(!$mutualiste->photo)
												<img href="{{route('ProfilUser',$mutualiste->id)}}" class="inline-block mb-10 DetailView" src="{{URL::asset('erpfiles/dist/img/mock1.jpg')}}" alt="user"/>
                                                    @else
                                                <img  href="{{route('ProfilUser',$mutualiste->id)}}" class="inline-block mb-10" src='{{config("app.imgLink")}}{{$mutualiste->photo}}'/>
                                                @endif
												
											</div>	
											<h5 href="{{route('ProfilUser',$mutualiste->id)}}" class="block mt-10 mb-5 weight-500 capitalize-font txt-danger DetailView">{{$mutualiste->first_name}} {{$mutualiste->name}}</h5>
											<h6 class="block capitalize-font pb-20">Matricule: {{$mutualiste->registration_number}} <br>
                                            Contact : {{$mutualiste->phone}}
                                        	</h6>
											<button class="btn btn-danger btn-block btn-anim mt-30" title="Suspendre ce mutualiste de l'assurance maladie" data-toggle="modal" data-target="#myModal"><i class="fa  fa-unlock-alt"></i><span class="btn-text">BLOQUER CE MUTUALISTE</span></button>

										</div>	
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
				
				<!-- Row -->
			
			
			</div>
            @include('layouts.admin._footer')
			<!-- /Footer -->
			
		</div>
<!-- /Main Content -->
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')

	
@endpush


@push('script.footer1')

@endpush


@section('title')
MUTUALISTES
@endsection

@section('beneficiaire')
active
@endsection


@push('script.footer2')
 <!-- jQuery -->
 <script src="{{URL::asset('erpfiles/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Counter Animation JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>

<!-- Data table JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/productorders-data.js')}}"></script>
	<!-- Data table JavaScript -->
	<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/dist/js/dataTables-data.js')}}"></script>
<!-- Owl JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Switchery JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/jquery.slimscroll.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{URL::asset('erpfiles/dist/js/dropdown-bootstrap-extended.js')}}"></script>

<!-- Sparkline JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/morris.js/morris.min.js')}}"></script>

<!-- Chartist JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/chartist/dist/chartist.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/chartist-data.js')}}"></script>

<!-- ChartJS JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/chart.js/Chart.min.js')}}"></script>

<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<!-- Sweet-Alert  -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
		
<script src="{{URL::asset('erpfiles/dist/js/sweetalert-data.js')}}"></script>
<!-- Init JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/init.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/dashboard3-data.js')}}"></script>

@endpush



