
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>ERP MUSCOP-CI</title>
  <style>
    body{
      font-family: 'Arial';
    }
      .header{
          width: 100%;
          text-align: center;
          position: fixed;
          top: 0px;
          
      }
      table {
        width: 100%;
        border-collapse: collapse;
      }

      td{
        height: 28px;
        font-size: 12.5px;
      }
      


    th, td{
      border: 0.5px solid black;
      padding: 2px;
    }


  </style>
</head>
<body>
  <div>
        
        <div class="panel-body">
        <table style="border: 1px solid #000;">
                  <thead>
                    <tr align="center">
                     <td colspan="3" style="margin: auto; height: 100px !important; width: 100%;" >
                        <img src="{{URL::asset('erpfiles/dist/img/logo.png')}}" alt="LOGO MUSCOP-CI"> 
                            <h1>RECU DE VERSEMENT</h1>
                    </td>
                    </tr>
                  </thead>
                  <tbody class="retour">
                    
                    <tr>
                      <td><b>Reçu :</b> <span style="color: blue;"> MUCOP-{{$rglmt->id}}</span></td>

                      <td><b>De M :</b> <span style="color: blue;">{{$rglmt->Mutualiste->name}}</span></td>

                      <td><b>Contact :</b> <span style="color: blue;">{{$rglmt->Mutualiste->phone}}</span></td>
                      
                    </tr> 
                    <tr>
                      <td>
                        <b>Projet: </b> <span style="color: blue;"> : {{$rglmt->Projet->libelle}} </td>

                      <td><b>Date de souscription  :</b> <span style="color: blue;">{{(new DateTime($rglmt->ProjetMutua->date))->format('d/m/Y')}}</span></td>

                      <td><b>Montant souscription :</b> <span style="color: blue;">
                             {{number_format($rglmt->ProjetMutua->montantSouscription, 0, ',', ' ')}} FCFA</span>
                      </td>
                      
                    </tr>

                    <tr>
                      <td colspan="3"> 
                        @if($rglmt->ProjetMutua->fraisdossier)
                        <b>Frais de dossier pour la caisse de solidarité</b>
                        @else
                        <b>{{$rglmt->ProjetMutua->descriptioncommande}}</b>
                        @endif
                          
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2"><b>Montant versé :</b> <span style="color: blue;">{{number_format($rglmt->valeur, 0, ',', ' ')}} FCFA</span></td>

                      <td><b>Date de versement :</b> <span style="color: blue;">{{(new DateTime($rglmt->date))->format('d/m/Y')}}</span></td>

                     

                      
                    </tr>
                    

                    <tr>
                      <td colspan="3"><b>Mode versement :</b> <span style="color: blue;">{{$rglmt->mode_reglement}}</span></td>
                      
                    </tr>
                    @if(!$rglmt->frais_dossier)
                     <tr>
                      <td colspan="3">
                        <b>Nouveau solde :</b> <span style="color: blue;">
                        {{number_format($rglmt->reste_a_payer, 0, ',', ' ')}} FCFA</span>
                      </td>
                    </tr>
                    @endif

                    <tr>
                      <td colspan="3">
                        <p align="right" style="margin-right: 10px;">SIGNATURE ET CACHET</p>
                        <p><b>Date d’émission :</b> <span style="color: blue;"> {{(new DateTime($rglmt->date))->format('d/m/Y')}}</span></p>
                        <p><b>Date de tirage :</b> <span style="color: blue;"> {{date('d/m/Y')}}</span></p>
                      </td>
                    </tr>
                    

                    <tr>
                      <td  colspan="3"  align="center" > <span style="color: red;"> NB : Ce reçu de paiement vous engage de facto à l’observation de nos conditions d’acquisition (Voir fiche de souscription).</span> </td>
                    </tr>
                    
                  </tbody>
        </table>  
        <span align="right" style="font-size: 10px;"> <b>Date d'impression :{{date('d/m/Y')}} | Heure : {{date("H:i:s")}}</b></span>
          <br><hr style="width: 100%; color: gray;"><br>
             
          <table style="border: 1px solid #000;">
                  <thead>
                    <tr align="center">
                     <td colspan="3" style="margin: auto; height: 100px !important; width: 100%;" >
                     <img src="{{URL::asset('erpfiles/dist/img/logo.png')}}" alt="LOGO MUSCOP-CI">
                            <h1>RECU DE VERSEMENT</h1>
                    </td>
                    </tr>
                  </thead>
                  <tbody class="retour">
                    
                    <tr>
                      <td><b>Reçu :</b> <span style="color: blue;"> MUCOP-{{$rglmt->id}}</span></td>

                      <td><b>De M :</b> <span style="color: blue;">{{$rglmt->Mutualiste->name}}</span></td>

                      <td><b>Contact :</b> <span style="color: blue;">{{$rglmt->Mutualiste->phone}}</span></td>
                      
                    </tr> 
                    <tr>
                      <td>
                        <b>Projet: </b> <span style="color: blue;"> : {{$rglmt->Projet->libelle}} </td>

                      <td><b>Date de souscription  :</b> <span style="color: blue;">{{(new DateTime($rglmt->ProjetMutua->date))->format('d/m/Y')}}</span></td>

                      <td><b>Montant souscription :</b> <span style="color: blue;">
                             {{number_format($rglmt->ProjetMutua->montantSouscription, 0, ',', ' ')}} FCFA</span>
                      </td>
                      
                    </tr>

                    <tr>
                      <td colspan="3"> 
                        @if($rglmt->ProjetMutua->fraisdossier)
                        <b>Frais de dossier pour la caisse de solidarité</b>
                        @else
                        <b>{{$rglmt->ProjetMutua->descriptioncommande}}</b>
                        @endif
                          
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2"><b>Montant versé :</b> <span style="color: blue;">{{number_format($rglmt->valeur, 0, ',', ' ')}} FCFA</span></td>

                      <td><b>Date de versement :</b> <span style="color: blue;">{{(new DateTime($rglmt->date))->format('d/m/Y')}}</span></td>

                     

                      
                    </tr>
                    

                    <tr>
                      <td colspan="3"><b>Mode versement :</b> <span style="color: blue;">{{$rglmt->mode_reglement}}</span></td>
                      
                    </tr>
                    @if(!$rglmt->frais_dossier)
                     <tr>
                      <td colspan="3">
                        <b>Nouveau solde :</b> <span style="color: blue;">
                        {{number_format($rglmt->reste_a_payer, 0, ',', ' ')}} FCFA</span>
                      </td>
                    </tr>
                    @endif

                    <tr>
                      <td colspan="3">
                        <p align="right" style="margin-right: 10px;">SIGNATURE ET CACHET</p>
                        <p><b>Date d’émission :</b> <span style="color: blue;"> {{(new DateTime($rglmt->date))->format('d/m/Y')}}</span></p>
                        <p><b>Date de tirage :</b> <span style="color: blue;"> {{date('d/m/Y')}}</span></p>
                      </td>
                    </tr>
                    

                    <tr>
                      <td  colspan="3"  align="center" > <span style="color: red;"> NB : Ce reçu de paiement vous engage de facto à l’observation de nos conditions d’acquisition (Voir fiche de souscription).</span> </td>
                    </tr>
                    
                  </tbody>
        </table>  
                <span align="right" style="font-size: 10px;"> <b>Date d'impression :{{date('d/m/Y')}} | Heure : {{date("H:i:s")}}</b></span>
        </div>
        <div style="position: absolute; bottom: 8px; font-size: 12px; width: 100%;" align="center"> 
          <hr style="width: 100%;">  
        La <b>M</b>utuelle <b>S</b>ociale du <b>C</b>orps <b>P</b>réfectoral de <b>C</b>ôte d'<b>I</b>voire - Abidjan Cocody DGAT - BPV 192 <br> 
        Email: muscopci2020@gmail.com  N° Immatriculation <b>1C/0482022/CI</b>
        </div>










    </div>

</div>
</body>
</html>