<div class="modal fade example-modal-lg affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge">AJOUTER UN MUTUALISTE</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('AddNewMutualiste')}}" method="post" enctype="multipart/form-data">
          @csrf                              
            <div class="row">
                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="name">Nom et prenoms <span class="text-danger">*</span></label>
                        <input id="name" type="text" class="form-control " name="name" value="" required="" autocomplete="name">
                    </div>
                </div>
                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="phone">
                            téléphonique
                        </label>
                        <input id="phone" type="text" class="form-control " name="phone" autocomplete="phone">
                    </div>
                </div>
                
                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="statutory_position">Position statutaire</label>
                        <select name="statutory_position" id="statutory_position" class="form-control">
                            <option></option>
                            <option value="Souscripteur externe">Souscripteur externe</option>
                            <option value="En activité">En activité</option>
                            <option value="À la retraite">À la retraite</option>
                            <option value="En détachement">En détachement</option>
                            <option value="En disponibilité">En disponibilité</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="birth_date">Date de naissance</label>
                        <input type="text" name="birth_date" class="form-control" placeholder="jj/mm/aaaa">
                    </div>
                </div>
                
                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="birth_place">
                            Lieu de naissance
                        </label>
                        <input type="text" name="birth_place" id="" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="role">Fonction </label>
                        <select class="form-control" name="role" id="" value="">
                            <option value=""></option>  
                            <option value="Directeur de Cabinet">Directeur de Cabinet</option>
                            <option value="Directeur de Cabinet Adjoint">Directeur de Cabinet Adjoint</option>
                            <option value="Conseiller">Conseiller</option>
                            <option value="Inspecteur Général"> Inspecteur Général</option>
                            <option value="Inspecteur"> Inspecteur</option>
                            <option value="Directeur Général"> Directeur Général</option>
                            <option value="Directeur"> Directeur</option>
                            <option value=" Sous-directeur"> Sous-directeur</option>
                            <option value="Préfet de Région"> Préfet de Région</option>
                            <option value="Préfet de Département"> Préfet de Département</option>
                            <option value=" Secrétaire Général de Préfecture"> Secrétaire Général de Préfecture
                            </option>
                            <option value="Sous-préfet">Sous-préfet</option>
                            <option value="autres">autres </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="city">Lieu de résidence habituelle</label>
                        <input type="text" name="residence" id="" class="form-control">
                    </div>
                </div>

                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="registration_number">Numéro matricule </label>
                        <span class="text-danger">*</span>
                        <input type="text" required="" name="registration_number" class="form-control" style="width: 100%;">
                    </div>
                </div>
                
                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <label for="city">Email</label>
                        <input type="email" class="form-control email" name="email">
                    </div>
                </div>

                <div class="col-md-4 col-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-warning text-white" style="width:100%; margin-top:1em;">
                            ENREGISTRER
                        </button>
                    </div>
                </div>
            </div>
        </form>  
      </div>
    </div>
  </div>
</div>