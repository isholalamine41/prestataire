<div class="modal fade example-modal-lg affiche" aria-hidden="true" aria-labelledby="exampleOptionalLarge"
  role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title txt-dark" id="exampleOptionalLarge">VERSEMENT SOUSCRIPTEUR</h4>
      <div class="modal-body">
        <form action="{{route('VersmtSouscription',['id' => $mensualite->id])}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-md-6">
                <label for="">Selectionner le projet</label>
                <select name="projet_mutua_id" class="form-control" id="" required="">
                    <option></option>
                        @if($mensualite->ProjetMutua)
                            @foreach($mensualite->ProjetMutua as $pm)
                                <option value="{{$pm->id}}">
                                    <b>{{$pm->Projet->libelle}}:</b> 
                                    @if($pm->fiche_projet_id)
                                        {{number_format($pm->FicheProjet->montant_souscription, 0, ',', ' ')}} FCFA
                                        @else
                                        @if($pm->libelle == 'TERRAIN YAKRO')
                                        Lot: {{$pm->lot}} | 
                                        @endif
                                        {{number_format($pm->montantSouscription, 0, ',', ' ')}} FCFA

                                    @endif
                                </option>
                            @endforeach
                        @endif
                </select>
            </div>
            <div class="col-md-6">
               <label for="">Date du versement</label>
               <input type="date" class="form-control" name="date" required="">
            </div>
          </div><br>

          <div class="row">
            <div class="col-md-6">
                <label for="">Mode de versement</label>
                <select class="form-control" name="mode" required="">
                    <option></option>
                    <option value="CHEQUE">CHEQUE</option>
                    <option value="VIREMENT">VIREMENT</option>
                    <option value="ESPECE">ESPECE</option>
                    <option value="MOBILE MONEY">MOBILE MONEY</option>
                </select>
            </div>
            <div class="col-md-6">
             <label for="">Montant versement</label>
               <input type="number" class="form-control" name="montant" required="">
            </div>
          </div> <br>
          <div class="row">
            <div class="col-md-6">  
                <label for="">Référence</label>
                <input type="text" class="form-control" name="ref" required="">
            </div>
            <div class="col-md-6" align="center">  
              <button type="submit" style="width:250px; margin-top: 1.5em;" class="btn btn-success"><i class="fa fa-save"></i> ENREGISTRER </button>
            </div>
          </div>
            
        </form>   
      </div>
    </div>
  </div>
</div>