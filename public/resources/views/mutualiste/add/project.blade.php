@extends('layouts.admin.master')

@section('content')
<!-- Main Content -->
<!-- Main Content -->
<div class="page-wrapper">
            <div class="container-fluid pt-25">
					
				<!-- Row -->
				<div class="row">
					<div class="col-lg-3 col-xs-12">
						<div class="panel panel-default card-view  pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body  pa-0">
									<div class="profile-box">
										<div class="profile-cover-pic" style="background:#09b76e;">
											<div class="profile-image-overlay"></div>
										</div>
										<div class="profile-info text-center">
											<div class="profile-img-wrap">
                                                @if(!$mutualiste->photo)
												<a href="{{route('ProfilUser',$mutualiste->id)}}" class="DetailView"><img  class="inline-block mb-10" src="{{URL::asset('erpfiles/dist/img/mock1.jpg')}}" alt="user"/></a>
                                                    @else
													<a href="{{route('ProfilUser',$mutualiste->id)}}" class="DetailView"><img class="DetailView inline-block mb-10" src="{{URL::asset('Media/'.$mutualiste->photo)}}"/></a>
                                                @endif
											</div>	
											<a href="{{route('ProfilUser',$mutualiste->id)}}" class="DetailView"><h5 class="block mt-10 mb-5 weight-500 capitalize-font txt-danger">{{$mutualiste->name}}</h5></a>
											<h6 class="block capitalize-font pb-20">Matricule: {{$mutualiste->registration_number}} <br>
                                            Contact :{{$mutualiste->phone}}
                                        	</h6>
										</div>	
										<div class="social-info">
											<div class="row">
												<div class="col-xs-3 text-center">
                                                    <span class="counts block head-font" style="font-size: 12px;"><b>POSITION</b>  </span>
                                                    <span class="counts block head-font"><span>{{$mutualiste->statutory_position}}</span></span>
												</div>
												<div class="col-xs-3 text-center">
                                                    <span class="counts block head-font" style="font-size: 12px;"><b>PROJET(S)</b> </span>
													<span class="counts block head-font"><span class="counter-anim">{{$mutualiste->ProjetMutua->count()}}</span></span>
												</div>
												<div class="col-xs-6 col-md-4 text-center">
													<span class="counts block head-font" style="font-size: 12px;"><span ><b>ASSURANCE</b> </span></span>
													<span class="counts block head-font" style="font-size: 12px;">
														@if($mutualiste->ProjetMutua)
															@foreach($mutualiste->ProjetMutua as $pm)
																@if($pm->Projet->libelle == "ASSURANCE MALADIE")
																 <b style="font-weight:bold ;">{{$pm->FicheProjet->offre}}</b>
																@endif
															@endforeach
														@endif
													</span>
												</div>
											</div>
											<button href="{{route('ProfilUser',$mutualiste->id)}}" class="DetailView btn btn-warning btn-block mt-30"><i class="fa fa-angle-left"></i> <span class="btn-text"> RETOURNER SUR LE PROFIL</span></button>
											<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
															<h5 class="modal-title" id="myModalLabel">MODIFIER LES INFOS</h5>
														</div>
														<div class="modal-body">
															<!-- Row -->
															<div class="row">
																<div class="col-lg-12">
																	<div class="">
																		<div class="panel-wrapper collapse in">
																			<div class="panel-body pa-0">
																				<div class="col-sm-12 col-xs-12">
																					<div class="form-wrap">
																						<form action="#">
																							<div class="form-body overflow-hide">
																								<div class="form-group">
																									<label class="control-label mb-10" for="exampleInputuname_1">Name</label>
																									<div class="input-group">
																										<div class="input-group-addon"><i class="icon-user"></i></div>
																										<input type="text" class="form-control" id="exampleInputuname_1" placeholder="willard bryant">
																									</div>
																								</div>
																								<div class="form-group">
																									<label class="control-label mb-10" for="exampleInputEmail_1">Email address</label>
																									<div class="input-group">
																										<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																										<input type="email" class="form-control" id="exampleInputEmail_1" placeholder="xyz@gmail.com">
																									</div>
																								</div>
																								<div class="form-group">
																									<label class="control-label mb-10" for="exampleInputContact_1">Contact number</label>
																									<div class="input-group">
																										<div class="input-group-addon"><i class="icon-phone"></i></div>
																										<input type="email" class="form-control" id="exampleInputContact_1" placeholder="+102 9388333">
																									</div>
																								</div>
																								<div class="form-group">
																									<label class="control-label mb-10" for="exampleInputpwd_1">Password</label>
																									<div class="input-group">
																										<div class="input-group-addon"><i class="icon-lock"></i></div>
																										<input type="password" class="form-control" id="exampleInputpwd_1" placeholder="Enter pwd" value="password">
																									</div>
																								</div>
																								<div class="form-group">
																									<label class="control-label mb-10">Gender</label>
																									<div>
																										<div class="radio">
																											<input type="radio" name="radio1" id="radio_1" value="option1" checked="">
																											<label for="radio_1">
																											M
																											</label>
																										</div>
																										<div class="radio">
																											<input type="radio" name="radio1" id="radio_2" value="option2">
																											<label for="radio_2">
																											F
																											</label>
																										</div>
																									</div>
																								</div>
																								<div class="form-group">
																									<label class="control-label mb-10">Country</label>
																									<select class="form-control" data-placeholder="Choose a Category" tabindex="1">
																										<option value="Category 1">USA</option>
																										<option value="Category 2">Austrailia</option>
																										<option value="Category 3">India</option>
																										<option value="Category 4">UK</option>
																									</select>
																								</div>
																							</div>
																							<div class="form-actions mt-10">			
																								<button type="submit" class="btn btn-success mr-10 mb-30">Update profile</button>
																							</div>				
																						</form>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-success waves-effect" data-dismiss="modal">Save</button>
															<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<!-- /.modal-content -->
												</div>
												<!-- /.modal-dialog -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-9 col-xs-12">

					       @if(session()->has('messageErreur'))
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><strong>Erreur ! </strong> {{ Session::get('messageErreur') }}</p>
									<div class="clearfix"></div>
								</div>
								@endif
								@if(session()->has('message'))
								<div class="alert alert-success alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><strong>Félicitations ! </strong> {{ Session::get('message') }}</p> 
									<div class="clearfix"></div>
								</div>
							@endif
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="profile_tab_8" role="tab" href="#profile_8" aria-expanded="false"><span><b style="font-size:20px; font-weight:bold;">SOUSCRIPTION PROJET MUSCOP-CI</b></span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">
											<div  id="profile_8" class="tab-pane fade active in" role="tabpanel">
												<div class="col-md-12">
													<div class="pt-20">
														<div class="streamline user-activity">
														<form action="{{route('ProjetMutualiste',['id' =>$mutualiste->id])}}" method="post">@csrf
															<div class="row">
																<div class="col-md-4">
																	<p><b class="txt-dark" style="font-size:14px; font-weight:bold;"><u>PROJET</u></b></p>	<br>
																	<div class="form-group">
																		<label for="" class="txt-dark">Sélectionnez le projet svp <br></label> 
																		<select name="projet_id" id="" class="form-control projet_id" required="">
																		<option value=""></option>
																			@if($project->count()>0)
																				@foreach($project as $p)
																				<option value="{{$p->id}}">{{$p->libelle}}</option>
																				@endforeach
																			@endif
																		</select>
																	</div> <br>
																	<div class="form-group">
																		<label for="" class="txt-dark">Date de souscription<br></label> 
																		<input type="date" name="date" class="form-control date " required="">
																	</div>
																</div>
																<div class="col-md-8">
																		<p><b class="txt-dark" style="font-size:14px; font-weight:bold;"><u>FICHE PROJET</u></b></p>	<br>
																		<div class="row fiche_projet">
																			
																		</div>
																	
																</div>
															</div>
															</form>
														</div>
													</div>
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						
							
					</div>
				</div>
				<!-- /Row -->
				
			
			
			</div>
            @include('layouts.admin._footer')
			<!-- /Footer -->
			
		</div>
<!-- /Main Content -->
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')

	
@endpush


@push('script.footer1')

@endpush


@section('title')
MUTUALISTES
@endsection

@section('mutualiste')
active
@endsection


@push('script.footer2')
 <!-- jQuery -->
 <script src="{{URL::asset('erpfiles/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Counter Animation JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>

<!-- Data table JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/productorders-data.js')}}"></script>
	<!-- Data table JavaScript -->
	<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/dist/js/dataTables-data.js')}}"></script>
<!-- Owl JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Switchery JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/jquery.slimscroll.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{URL::asset('erpfiles/dist/js/dropdown-bootstrap-extended.js')}}"></script>

<!-- Sparkline JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/morris.js/morris.min.js')}}"></script>

<!-- Chartist JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/chartist/dist/chartist.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/chartist-data.js')}}"></script>

<!-- ChartJS JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/chart.js/Chart.min.js')}}"></script>

<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

<!-- Init JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/init.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/dashboard3-data.js')}}"></script>
<script>
        $(document).on("change",".projet_id", function(){
          var optionSelected = $("option:selected", this);
          var valueSelected = this.value;
          $.ajax({
            method: 'get',
            url: "/dashbord/create/fiche/projet/" + valueSelected,
            success : function(response){
              console.log(response);
              $('.fiche_projet').html(response.code);
            }
          })
        });
		$(document).on("change",".superficie", function(){
        	var vsuperficie = parseInt($(".superficie").val(), 10);
        	var vprix500 = parseInt($(".prix500").val(), 10);
			
			var montant = ((vsuperficie-500)*10000) + vprix500;
		    $(".montant").val(montant);
          
        });

		$(document).on("change",".Moption_id", function(){
        	var optionSelected = parseInt($(".Moption_id").val(), 10);
		
		  if(optionSelected >= 500000 && optionSelected <= 1000000){
			var fd = 25000;
		  }
		  if(optionSelected > 1000000 && optionSelected <= 2000000){
			var fd = 50000;
		  }
		  if(optionSelected > 2000000 && optionSelected <= 3000000){
			var fd = 75000;
		  }
		  $(".Frais_dossier").text(fd);
          
        });


		$(document).on("change",".montantV", function(){
        	var m = parseInt($(".montantV").val(), 10);
			var e = parseInt($(".nbmois").val(), 10);
			
			var fd = m/e
			//alert(fd);
		 
		 $(".mensualite").val(fd);
          
        });
        
</script>
<script>
        $(document).on("change",".option_id", function(){
          var optionSelected = $("option:selected", this);
          var valueSelected = this.value;
          $.ajax({
            method: 'get',
            url: "/dashbord/create/fiche/projet/option/" + valueSelected,
            success : function(response){
              console.log(response);
              $('.montant').html(response.code);
            }
          })
        });
        
</script>
@endpush



