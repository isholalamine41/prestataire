<div class="row">
    <div class="col-md-4">
        <label class="txt-dark" for="">VALEUR 500²</label> 
        <input name="prix500" type="text" value="4500000" class="form-control prix500" required="">
    </div>
    <div class="col-md-4">
        <label class="txt-dark" for="">ZONE</label> 
        <input name="zone" type="text" class="form-control zone">
    </div> 
    <div class="col-md-4">
        <label class="txt-dark" for="">ILOT</label> 
        <input name="ilot" type="number" class="form-control ilot">
    </div>
    <div class="col-md-4"><br>
        <label class="txt-dark" for="">LOT</label> 
        <input name="lot" type="number" class="form-control lot">
    </div>
    <div class="col-md-4"><br>
        <label class="txt-dark" for="">SUPERFICIE</label> 
        <input name="superficie" type="number" class="form-control superficie" required="">
    </div>
    <div class="col-md-4"><br>
        <label class="txt-dark" for="">OPTION</label> 
        <input name="misevaleur" type="text" class="form-control option">
    </div>

    <div class="col-md-4"><br>
        <label class="txt-dark" for="">MONTANT TERRAIN</label> 
        <input name="montant" type="number" class="form-control montant" required="">
    </div>

    <div class="col-md-4"><br>
        <label class="txt-dark" for="">AVANCE</label> 
        <input name="avance" type="number" class="form-control avance" required="">
    </div> 
    <div class="col-md-4"><br>
        <label class="txt-dark" for="">ECHEANCE</label> 
        <input name="nbmois" type="number" class="form-control nbmois" required="">
    </div>
    <div class="col-md-12"><br>
        <label for="" class="txt-dark">DESCRIPTION BIEN</label>
        <textarea name="commande" required="" id="" class="form-control" required="" cols="30" rows="3"></textarea>
    </div>
</div>
<div class="row">
    <div class="col-md-12"> <br>
        <button type="submit" style="width:100%;" class="btn btn-warning waves-effect" data-dismiss="modal">ENREGISTRER</button> <br>
    </div>
</div>