
<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-success">
                <h5 class="modal-title white" style="color: #fff; font-size: 16px;">MODIFIER : {{$user->name}}</h5>
                <button style="font-size: 18px; color:#fff; border:#fff 1px solid;" type="button" class="btn btn-default" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card overflow-hidden border-success">
                        <div class="card-body custome-tooltip p-3">
                            <div class="row">
                                <div class="col-md-6 txt-dark"><b>Email :</b>  {{$user->email}}  </div>
                                <div class="col-md-6 txt-dark"><b>Mot de passe :</b>  {{$user->mdp}} </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($user->typePrestataire !="Super Prestataire")
            <form action="{{Route('UpdateUserPrestataire',$user->id)}}" method="post" enctype="multipart/form-data">@csrf  
            @endif                            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <label for="name">Nom et prenoms <span class="text-danger">*</span></label>
                        <input id="name" type="text" class="form-control border-success " name="name" value="{{$user->name}}" required="" >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group txt-dark ">
                        <label for="phone">Contact *</label>
                        <input id="phone" type="text" value="{{$user->contact}}" class="form-control border-success" name="contact"  required="" >
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <label for="birth_date">Email*</label>
                        <input type="email" value="{{$user->email}}" class="form-control border-success" required="" name="email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <label for="birth_date">Role / Fonction *</label>
                        <input type="text" name="fonction" value="{{$user->fonction}}"  required="" class="form-control border-success">
                    </div>
                </div>
            </div><br>
            <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <label for="registration_number">Les Menus *</label>
                        <span class="text-danger">*</span>
                        <select name="menu_id[]" id="" multiple class="form-control border-success" style="height:100px;"  required="">
                            @foreach($user->UserMenu  as $um)
                                @if($um->libelle =='All-Menu-Prestataire')
                                <option selected value="{{$um->Menu->id}}">Tous les menus</option>
                                @else
                                <option selected value="{{$um->Menu->id}}">{{$um->Menu->libelle}}</option>
                                @endif
                            @endforeach
                            @foreach($menu->whereNotIn('id', $user->UserMenu->pluck('menu_id')) as $m)
                                @if($m->libelle =='All-Menu-Prestataire' or $m->libelle =='GESTION DES DROITS')
                                @else
                                <option value="{{$m->id}}">{{$m->libelle}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <br><br>
                        @if($user->typePrestataire !="Super Prestataire")
                        <button align="left" type="submit" class="btn btn-success text-white" style="width:250px; margin-top:1em;">
                            MODIFIER
                        </button>
                        @endif
                    </div>
                </div>
                <br>
            </div>
            <div class="row">
        @if($user->typePrestataire !="Super Prestataire")
        </form>  
        @endif
            </div>
        </div>
    </div>
</div>
