@extends('layouts.admin.master')
@section('content')

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0">Nos prestations ({{$prestations->count()}})</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table id="table" class="display table" style="min-width: 845px">
                                                <thead>
                                                    <tr>
                                                        <th>LIBELLE</th>
                                                        <th>CATEGORIE</th>
                                                        <th>MONTANT</th>
                                                        <th>PART ASSURE</th>
                                                        <th>PART MUSCOPCI</th>
                                                    </tr>
                                                </thead>
                                                @if($prestations->count()>0)
                                                <tbody id="tbody" >
                                                    @foreach($prestations as $lp)
                                                    @if($lp->listeprestable)
                                                    <tr>
													    <td class="txt-dark">{{$lp->libelle}}</b></td>
													    <td class="txt-dark">{{$lp->listeprestable->libelle}}</b></td>
													    <td class="txt-dark">{{$lp->ParamlistePrestation->last()->cout ??4}} <small>FCFA</small></b></td>
													    <td class="txt-dark">{{$lp->ParamlistePrestation->last()->parAssure ??4}} <small>FCFA</small></b></td>
													    <td class="txt-dark">{{$lp->ParamlistePrestation->last()->tierPayant ??4}} <small>FCFA</small></b></td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')
@endpush


@push('script.footer1')
@endpush

@section('title')
NOS PRESTATIONS
@endsection


@section('dd')
active
@endsection



@push('script.footer2')

<script>
    $('#table').DataTable();
</script>
@endpush





