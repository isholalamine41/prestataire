                        @if(!is_null($assure))
                        
                            <div class="basic-form"><br>
                                @if($typeASS == "Mutualiste")
                                <img class="inline-block mb-10 ImgCarte" src='{{config("app.imgLink")}}{{$assure->photo}}'/>
                                @else
                                <img class="inline-block mb-10 ImgCarte" src='{{config("app.adLink")}}{{$assure->photo}}'/>
                                @endif
                                    <div style="margin-top: -2.5em;" >
                                        @if($assure->active)
                                            <p style="color: green;">
                                               ASSURE(E) ACTIVE(E)
                                            </p>
                                            @else
                                            
                                            <p align="center"  class="headerP" style="color: red; font-size:46px; font-weight:bold;  z-index:9999; position:absolute; width:460px; bottom:330px;left:95px; ">
                                                ASSURE(E) BLOQUE(E)
                                            </p>
                                        @endif
                                    </div>
                                <div  align="center" style="text-align: center; vertical-align:middle; height:400px;">
                                    <p class="assurance_number">
                                        {{$assure->assurance_number}}
                                    </p>
                                    <p class="assureName">{{$assure->first_name}} {{$assure->name ?? $assure->last_name }}</p>
                                    <p class="assureAyantDroits">
                                        @if($typeASS == "Mutualiste")
                                            @if($assure->Ayantdroits->Count()>9)
                                                {{$assure->Ayantdroits->Count()}}
                                            @else
                                                0{{$assure->Ayantdroits->Count()}}
                                            @endif
                                        @else
                                            Ayant droit
                                        @endif
                                    </p>
                                    @if($typeASS == "Mutualiste")
                                    <p class="dateNaisse">{{$assure->birth_date}}</p>
                                    @else
                                    <p class="dateNaisse">{{$assure->date_naissance}}</p>
                                    @endif
                                    <p class="assureSexe">
                                    @if($typeASS == "Mutualiste")
                                        @if($assure->sex == "M")
                                            Masculin
                                            @else
                                            Feminin
                                        @endif
                                        @else
                                        @if($assure->sexe == "M")
                                            Masculin
                                            @else
                                            Feminin
                                        @endif
                                    @endif
                                    </p>
                                    
                                    <p class="assurePolice" >{{$assure->num_pocile}}</p>
                                    <img class="headerP CartePosition" src="{{URL::asset('erpfiles/images/Carte.png')}}" style="height:350px;" alt="">
                                </div>
                            </div>
                            
                            @else
                            <div class="row" >
                                <div  align="center" style="text-align: center;">
                               
                                    <img src="{{URL::asset('erpfiles/images/g4.gif')}}" style="max-height: 400px; max-width: 250px;" alt="">
                                </div>
                            </div>
                            @endif

                            <!-- <div class="row" style="position:relative; margin-top:-1em;">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
										<div class="widget-stat card  bg-primary">
											<div class="card-body">
												<div class="media">
													<span class="me-1">
                                                    <i class="las la-money-check-alt"></i>
													</span>
													<div class="media-body text-white">
														<p class="mb-1">RESTE PLAFOND INDIVIDUEL</p>
														<h3 class="text-white">{{number_format($assure->bareme->last()->plafondGeneraleBeneficiaire, 0, ',', ' ')}}</h3>
														 <span>FCFA</span>
													</div>
												</div>
											</div>
										</div>
									</div>
                                
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
										<div class="widget-stat card  bg-warning">
											<div class="card-body">
												<div class="media">
													<span class="me-1">
                                                        <i class="las la-money-check-alt"></i>
													</span>
													<div class="media-body text-white">
														<p class="mb-1">RESTE PLAFOND FAMILLE</p>
                                                        <h3 class="text-white">{{number_format($assure->bareme->last()->plafondGeneraleFamillle, 0, ',', ' ')}}</h3>
														<span>FCFA</span>
													</div>
												</div>
											</div>
										</div>
									</div>
                                    
                            </div> -->
                            @if($assure->active)
                                @if($assure->bareme->last()->plafondGeneraleBeneficiaire > 0 and $assure->bareme->last()->plafondGeneraleFamillle > 0)
                                <div align="center">
                                    <button type="submit" href="{{route('GotoMakeActe',[$assure->id, $typeASS])}}" class="btn btn-outline-warning DetailView" style="width: 320px; font-size: 25px;">  SUIVANT &ensp; <i style="font-weight:bold;" class="las la-hand-point-right"></i></button>
                                </div>
                                @endif
                            @endif
                            
                            