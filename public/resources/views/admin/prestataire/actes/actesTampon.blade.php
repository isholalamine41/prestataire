                    <div class="card-header bg bg-info">
                            <h4 class="card-title text-white">FACTURE </h4>
                        </div>
                        @if($actes->count()>0)
                        <div class="card-header" style="width: 100%;">
                            <div class="row" style="width: 100%;">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" >
                                    <h4 class="heading mb-0">MONTANT GLOBAL : {{number_format($actes->SUM("part_muscopci") + $actes->SUM("part_assure"), 0, ',', ' ')}} FCFA  </h4>
                                    <h4 class="heading mb-0">PART ASSURANCE : {{number_format($actes->SUM("part_muscopci"), 0, ',', ' ')}} FCFA  </h4>
                                    <h4 class="heading mb-0" style="color: brown;">MONTANT A PAYER : {{number_format($actes->SUM("part_assure"), 0, ',', ' ')}} FCFA  </h4>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    @if($actes->where('entente','En attente')->count()>0)
                                    <button type="button" class="btn  btn-square btn-primary ValidateAllInDemandeActes"><i class="fa fa-save"></i> VALIDER</button>
                                    @else
                                    <button type="button" href="{{route('ValidateAllActes', [$prestataire->id , $assure->id, $typeAss] )}}" class="btn  btn-square btn-success ViewPage"><i class="fa fa-save"></i> VALIDER</button>
                                    @endif
                                    <button type="button" href="{{route('deleteALLACTETamPon', [$prestataire->id , $assure->id, $typeAss] )}}" class="btn  btn-square btn-warning deleteACTETamPon"> <i class="fa fa-trash"></i> ANNULER</button>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="card-body" >
                            <div class="loaderFacture" align="center" ><img style="max-height: 100px; vertical-align:middle;" src="{{URL::asset('erpfiles/images/loader.gif')}}" alt=""></div>
                            
                        <div class="card-body p-0">
                            <div class="table-responsive p-0">
                                <table class="table card-table border-no success-tbl p-0">
                                    <thead >
                                        <tr >
                                            <th>PRESTATION</th>
                                            <th>MONTANT</th>
                                            <th>TIER PAYANT</th>
                                            <th>STATUT</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($actes as $a)	
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="ms-2 cat-name">
                                                        <p class="mb-0">{{$a->libelle}}</p>
                                                    </div>	
                                                </div>
                                            </td>
                                            <td>{{number_format($a->ListePrestation->ParamlistePrestation->last()->cout, 0, ',', ' ')}} FCFA</td>
                                            <td>
                                                {{number_format($a->part_muscopci, 0, ',', ' ')}} FCFA  
                                            </td>

                                            <td>
                                                @if($a->type == "demande")
                                                    @if($a->entente == 'En attente')
                                                    <span class="badge badge-warning border-0">{{$a->entente}}</span>
                                                    @endif
                                                    @if($a->entente == 'Accordée')
                                                    <span class="badge badge-success border-0">{{$a->entente}}</span>
                                                    @endif
                                                    @if($a->entente == 'Rejetée')
                                                    <span class="badge badge-danger border-0">{{$a->entente}}</span>
                                                    @endif
                                                    @else
                                                    Brouillon
                                                @endif
                                                
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" href="{{route('ViewActeTampon', [$a->id , $assure->id, $typeAss] )}}"  class="btn btn-success btn-icon-xs lanceModal"><i class="fa fa-eye"></i></button>
                                                    <button type="button" href="{{route('DeleteActeTampon', [$a->id , $assure->id, $typeAss] )}}" class="btn btn-warning btn-icon-xs DeleteActeTamponAjax"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                
                                </table>
                            </div>
                        </div>
                        </div>
                        </div>