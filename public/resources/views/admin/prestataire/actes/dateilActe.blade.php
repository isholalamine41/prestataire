<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-success">
                <!-- {{$a->ListePrestation->listeprestable->typePrestation->libelle}}    -->
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">{{$a->ListePrestation->listeprestable->libelle}} - {{$a->ListePrestation->libelle}} </h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <h4>{{$a->ListePrestation->Prestataire->rs}}</h4>
                <table class="table card-table border-no success-tbl p-0 headerP">
                    <thead>
                        <tr>
                            <th>PRESTATION</th>
                            <th>MONTANT</th>
                            <th>TIER PAYANT</th>
                            <th>PART ASSURE</th>
                            <th>QTE</th>
                            <th>STATUT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="ms-2 cat-name">
                                        <p class="mb-0">{{$a->libelle}}</p>
                                    </div>	
                                </div>
                            </td>
                            <td class="txt-dark">{{number_format($a->ListePrestation->ParamlistePrestation->last()->cout, 0, ',', ' ')}} FCFA</td>
                            <td class="txt-dark">
                                {{number_format($a->part_muscopci, 0, ',', ' ')}} FCFA  
                            </td>

                            <td  class="txt-dark">
                                {{number_format($a->part_assure, 0, ',', ' ')}} FCFA
                            </td>
                            <td  class="txt-dark">
                                {{$a->qte}}
                            </td>
                            <td>
                                @if($a->type == "demande")
                                    @if($a->entente == 'En attente')
                                    <span class="badge badge-warning border-0">{{$a->entente}}</span>
                                    @endif
                                    @if($a->entente == 'Accordée')
                                    <span class="badge badge-success border-0">{{$a->entente}}</span>
                                    @endif
                                    @if($a->entente == 'Rejetée')
                                    <span class="badge badge-danger border-0">{{$a->entente}}</span>
                                    @endif
                                    @else 
                                    {{$a->entente}}
                                @endif
                            </td>
                        </tr>
                    </tbody>
                
                </table>
                <p class="txt-dark weight-bold">Commentaire / Description</p>
                <div class="row " style="padding: 15px; margin-top:-2em;">
                
                    <div class="col-md-12 border-success">
                        {{$a->description}}
                    </div>
               </div>
                <br><br>

                <h4>INFOS BAREME MUSCOP-CI</h4>
                <table class="table card-table border-no success-tbl p-0 headerP">
                    <thead>
                        <tr>
                            <th>NATURE</th>
                            <th>PLAFOND</th>
                            <th>TAUX</th>
                            <th>QTE</th>
                            <th>ACCORD</th>
                            <th>DUREE</th>
                            <th>ENTITE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="txt-dark">
                               <p class="mb-0"><b>{{$a->ListePrestation->listeprestable->libelle}}</b></p>
                            </td>
                            <td class="txt-dark">
                                @if($a->ListePrestation->listeprestable->ParamPrestation->last()->plafond)
                                    <span class="badge badge-danger border-0">{{number_format($a->ListePrestation->listeprestable->ParamPrestation->last()->plafond, 0, ',', ' ')}} FCFA</span>
                                @else
                                 Aucun
                                @endif
                            </td>
                            <td class="txt-dark">
                                {{$a->ListePrestation->listeprestable->ParamPrestation->last()->taux}} %
                            </td>
                            <td>
                                {{$a->ListePrestation->listeprestable->ParamPrestation->last()->qte}} 
                            </td>
                            <td class="txt-dark">
                                @if($a->ListePrestation->listeprestable->ententePrealable)
                                <span class="badge badge-primary border-0">OUI</span>
                                @else
                                <span class="badge badge-success border-0">NON</span>
                                @endif
                            </td>
                            
                            <td class="txt-dark">
                                Par {{$a->ListePrestation->listeprestable->ParamPrestation->last()->duree}} an(s)
                            </td>
                            <td class="txt-dark">
                                {{$a->ListePrestation->listeprestable->ParamPrestation->last()->entite}} 

                            </td>
                            
                        </tr>
                    </tbody>
                
                </table>
                <br><br>
                <!-- 
                <h4>INFOS CONSOMMTION ASSURE</h4>
                <table class="table card-table border-no success-tbl p-0 headerP">
                    <thead>
                        <tr>
                            <th>NATURE ACTE</th>
                            <th>CONSOMMATION ASSURE(E)</th>
                            <th>CONSOMMATION FAMILLE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                               <p class="txt-dark"><b>{{$a->ListePrestation->listeprestable->libelle}}</b></p>
                            </td>
                            <td>
                               <p class="txt-dark"><b>{{$a->prestamutable->bareme->where('prestation_id',$a->ListePrestation->listeprestable->id)->last()->consoBeneficiairePrestation}} FCFA</b></p>
                            </td>
                            <td>
                                <p class="txt-dark"><b>{{$a->prestamutable->bareme->where('prestation_id',$a->ListePrestation->listeprestable->id)->last()->consoFamillePrestation}} FCFA</b></p>
                            </td>
                        </tr>
                    </tbody>
                
                </table> -->

            </div>
        </div>
    </div>
</div>
