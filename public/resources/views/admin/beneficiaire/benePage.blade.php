@extends('layouts.admin.master')

@section('content')
<!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid pt-25">
                 <!-- Row -->
                 <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body pa-0">
									<div class="sm-data-box">
										<div class="container-fluid">
											<div class="row" style="border: solid 2px #2879ff;">
												<div class="col-xs-9 text-center pl-0 pr-0 data-wrap-left">
													<span class="txt-dark block counter"><span style="font-weight:bold;">{{number_format($ayantdroit->count()+$mutualiste->count(), 0, ',', ' ')}}</span></span>
													<span class="weight-500 uppercase-font txt-dark block font-20" >BENEFICIAIRES</span>
												</div>
												<div class="col-xs-3 text-center  pl-0 pr-0 data-wrap-right">
                                                    <i style="color: #2879ff;"  class="zmdi zmdi-male-female data-right-rep-icon"></i>

												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body pa-0">
									<div class="sm-data-box">
										<div class="container-fluid">
											<div class="row" style="border: solid 2px #01c853;">
												<div class="col-xs-9 text-center pl-0 pr-0 data-wrap-left">
													<span class=" txt-dark block counter font-20" style="font-weight:bold;"><span >{{number_format($mutualiste->count(), 0, ',', ' ')}}</span>
													<span class="weight-500 uppercase-font txt-dark block font-20" >MUTUALISTES</span>
												</div>
												<div class="col-xs-3 text-center  pl-0 pr-0 pt-25  data-wrap-right  txt-blue">
                                                   <i style="color: #01c853;"  class="zmdi zmdi-male-female data-right-rep-icon"></i>
												</div> 
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    
					
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body pa-0">
									<div class="sm-data-box">
										<div class="container-fluid">
											<div class="row" style="border: solid 2px #fec107;">
												<div class="col-xs-9 text-center pl-0 pr-0 data-wrap-left">
													<span class="txt-dark block counter"><span class="counter" style="font-weight:bold;">{{number_format($ayantdroit->count(), 0, ',', ' ')}}</span>
													<span class="weight-500 uppercase-font txt-dark block font-20">AYANTS DROITS</span>
												</div>
												<div class="col-xs-3 text-center  pl-0 pr-0 data-wrap-right">
                                                    <i style="color: #fec107;"  class="zmdi zmdi-male-female data-right-rep-icon"></i>
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
              
                <br><br>
				<div class="col-sm-12">
								<div class="panel panel-default">
                                    
									<div class="button-list" align="right">
                                      
                                        <select class="form-controll" style="width: 250px; height:40px;">
                                            <option>---------- Tous les bénéficiaires ----------</option>
                                            <option value="MUTUALISTES" style="height: 30px;">MUTUALISTES</option>
                                            <option value="AYANTS-DROITS" style="height: 30px;">AYANTS-DROITS</option>
                                        </select>
										<button href="{{route('ModalAddPrestataire')}}" class="btn btn-primary lanceModal btn-outline btn-icon right-icon"><i class="fa fa-edit"></i> <span style="font-weight: bolder;">AJOUTER UN BENEFICIAIRES</span> </button>
										<button href="" class="btn btn-success lanceModal btn-outline btn-icon right-icon"><i class="fa fa-file-excel-o"></i> <span style="font-weight: bolder;">EXPORTER EN EXCEL</span> </button>
                                        <button href="" class="btn btn-warning lanceModal btn-outline btn-icon right-icon"><i class="fa fa-file-pdf-o"></i> <span style="font-weight: bolder;">EXPORTER EN PDF</span> </button><br>
									</div>
								</div>
							</div>
				<br>
                <!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark"><b style="font-size:15px; font-weight:bold;">LISTE DES MUTUALISTES</b></h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
                                        <table id="datable_1" class="table table-hover display  pb-30" >
												<thead>
													<tr>
														<th>Image</th>
														<th>Matricule</th>
														<th>Nom Complet</th>
														<th>Fonction</th>
														<th>Service</th>
														<th>AYANTS DROITS</th>
														<th>Actions</th>
													</tr>
												</thead>
												
												<tbody>
													@if($mutualiste->count()>0)
													@foreach($mutualiste as $Mutua)
													<tr>
														<td style="height: 70px;">
															<div class="profile-img-wrap">
																@if(!$Mutua->photo)
																<img href="{{route('ProfilUser',$Mutua->id)}}" style="height: 70px;" class="inline-block mb-10 DetailView" src="{{URL::asset('erpfiles/dist/img/mock1.jpg')}}" alt="user"/>
																	@else
																<img  href="{{route('ProfilUser',$Mutua->id)}}" style="height: 70px;" class="inline-block mb-10" src='{{config("app.imgLink")}}{{$Mutua->photo}}'/>
																@endif
															</div>
														</td>
														<td>{{$Mutua->registration_number}}</td>
														<td>{{$Mutua->first_name}} {{$Mutua->name}} </td>
														<td>{{$Mutua->role ?? 'pas d\'infos'}}</td>
														<td>{{$Mutua->statutory_position ?? 'pas d\'infos'}}</td>
														
														<td> <span class="label label-danger"> {{$Mutua->Ayantdroits->count()}}</span></td>
														<td style="width:180px;">
															
														<button href="{{route('ProfilUser',$Mutua->id)}}" title="Voir les Détails" class="btn btn-success btn-icon-anim btn-square DetailView"><i class="fa fa-eye"></i></button>
														<button title="Modifier" class="btn btn-warning btn-icon-anim btn-square"><i class="fa fa-edit"></i></button>
														
														<button href="{{route('DeleteMutua',$Mutua->id)}}" title="Supprimer" class="btn btn-danger btn-icon-anim btn-square DeleteRedirect"><i class="fa fa-trash"></i></button>
														</td>
													</tr>
													@endforeach
													@endif
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->


            </div>
            @include('layouts.admin._footer')
        </div>
<!-- /Main Content -->
@endsection


@section('body')
wrapper theme-4-active pimary-color-red
@endsection


@push('style.hearder')
@endpush


@push('script.footer1')
@endpush

@section('title')
BENEFICIAIRES ASSURANCES
@endsection


@section('beneficiaire')
active
@endsection



@push('script.footer2')
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Data table JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/jquery.slimscroll.js')}}"></script>

<!-- simpleWeather JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/simpleweather-data.js')}}"></script>

<!-- Progressbar Animation JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{URL::asset('erpfiles/dist/js/dropdown-bootstrap-extended.js')}}"></script>

<!-- Sparkline JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- Owl JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- ChartJS JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/chart.js/Chart.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/morris.js/morris.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

<!-- Switchery JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>



<!-- Data table JavaScript -->
<script src="{{URL::asset('erpfiles/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/dataTables-data.js')}}"></script>
<!-- Init JavaScript -->
<script src="{{URL::asset('erpfiles/dist/js/init.js')}}"></script>
<script src="{{URL::asset('erpfiles/dist/js/dashboard-data.js')}}"></script>

@endpush