?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mutualiste extends Model
{
    use HasFactory;
    Protected $fillable = [
        'registration_number','name','username','phone','role',
        'grade','echelon','city','sex','active','email','birth_date',
        'birth_place','spouse_name','spouse_phone','mother_name','father_name',
        'subscription_date','statutory_position','residence','marital_status','photo','user_id'
    ];

    public function ProjetMutua()
    {
        return $this->hasMany('App\Models\ProjetMutua');
    }
    
    public function Versement()
    {
    	return $this->hasMany(Versement::class);
    }
}
