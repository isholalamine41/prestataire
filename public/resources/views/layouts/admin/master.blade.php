<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>{{config("app.name")}} | @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="IC-DIGITALTRANS: +225 27 222 327 57">

    <link rel="shortcut icon" type="image/png" href="images/favicon.png">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <link href="{{URL::asset('erpfiles/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('erpfiles/vendor/swiper/css/swiper-bundle.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('erpfiles/vendor/swiper/css/swiper-bundle.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/14.6.4/nouislider.min.css">
    <link href="{{URL::asset('erpfiles/vendor/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="{{URL::asset('erpfiles/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
	
    <link rel="stylesheet" href="{{URL::asset('erpfiles/vendor/toastr/css/toastr.min.css')}}">
    <!-- Custom Stylesheet -->
	  <link href="{{URL::asset('erpfiles/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <!-- tagify-css -->
    <link href="{{URL::asset('erpfiles/vendor/tagify/dist/tagify.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('erpfiles/vendor/select2/css/select2.min.css')}}">
	<link href="{{URL::asset('erpfiles/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <!-- Style css -->
    @stack('style.hearder')
      <link href="{{URL::asset('erpfiles/css/style.css')}}" rel="stylesheet">
      <style>
      .dataTables_paginate .paginate_button.previous, .dataTables_wrapper .dataTables_paginate .paginate_button.next {
        font-size: 13px;
        width: auto !important;
      }
      .border-success{
        border: #3a9b94 solid 1px;
      }
      .bd-p{
        border: #3a9b94 solid 1px;
      }
      .ImgCarte{
        height: 160px; 
        width: 136px; 
        top: 220px; 
        Left:56px; 
        position:absolute; 
        z-index:9000; 
        border-radius:8px;
      }
      .assureName{
        top: 225px; 
        Left:355px; 
        position:absolute; 
        z-index:9000; 
        color:black;
        font-weight: bold;
      }
      .assureAyantDroits{
        top: 255px; 
        Left:355px; 
        position:absolute; 
        z-index:9000; 
        color:black;
        font-weight: bold;
      }
      .dateNaisse{
        top: 285px; 
        Left:357px; 
        position:absolute; 
        z-index:9000; 
        color:black;
        font-weight: bold;
      }
      .assureSexe{
        top: 317px; 
        Left:355px; 
        position:absolute; 
        z-index:9000; 
        color:black;
        font-weight: bold;
      }
      .assurePolice{
        top: 350px; 
        Left:355px; 
        position:absolute; 
        z-index:9000; 
        color:black;
        font-weight: bold;
      }

      .assurance_number{
        top: 185px; 
        Left:420px; 
        position:absolute; 
        z-index:9000; 
        color:red;
        font-size: 16px;
        font-weight: bold;
      }

      .CartePosition{
        position:absolute;
        Left:28px; 
        top: 96px; 
      }

      .headerP{
        border: 0.07em #ccc solid; box-shadow:  -0.1rem 0 .4rem ;
      }
      .txt-dark{
        color:black;
      }
      .weight-bold{
        font-weight: bold;
      }
      .weight-500{
        font-weight:500;
      }
     
    </style>
	
</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
		<div>
			<img src="images/pre.gif" alt=""> 
		</div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <div id="main-wrapper">
        @include('layouts.admin._header')
        @include('layouts.admin._sidebarleft')
        @yield('content')
        @include('layouts.admin._footer')
	  </div>

    <div class="retour_modal"></div>

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
  <script src="{{URL::asset('erpfiles/vendor/global/global.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/chart.js/Chart.bundle.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/apexchart/apexchart.js')}}"></script>
	
	<!-- Dashboard 1 -->
	<script src="{{URL::asset('erpfiles/js/dashboard/dashboard-1.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/draggable/draggable.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/swiper/js/swiper-bundle.min.js')}}"></script>
	
	
	<!-- tagify -->
	<script src="{{URL::asset('erpfiles/vendor/tagify/dist/tagify.js')}}"></script>
	 
	<script src="{{URL::asset('erpfiles/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/datatables/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/datatables/js/buttons.html5.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/datatables/js/jszip.min.js')}}"></script>
	<script src="{{URL::asset('erpfiles/js/plugins-init/datatables.init.js')}}"></script>
   
	<!-- Apex Chart -->
	
	<script src="{{URL::asset('erpfiles/vendor/bootstrap-datetimepicker/js/moment.js')}}"></script>
	<script src="{{URL::asset('erpfiles/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
	

	<!-- Vectormap -->
    <script src="{{URL::asset('erpfiles/vendor/jqvmap/js/jquery.vmap.min.js')}}"></script>
    <script src="{{URL::asset('erpfiles/vendor/jqvmap/js/jquery.vmap.world.js')}}"></script>
    <script src="{{URL::asset('erpfiles/vendor/jqvmap/js/jquery.vmap.usa.js')}}"></script>
    <script src="{{URL::asset('erpfiles/js/custom.js')}}"></script>
	  <script src="{{URL::asset('erpfiles/js/deznav-init.js')}}"></script>
   
    <script src="{{URL::asset('erpfiles/vendor/toastr/js/toastr.min.js')}}"></script>
    <!-- All init script -->
    <script src="{{URL::asset('erpfiles/js/plugins-init/toastr-init.js')}}"></script>

    <script src="{{URL::asset('erpfiles/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script src="{{URL::asset('erpfiles/vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{URL::asset('erpfiles/js/plugins-init/select2-init.js')}}"></script>
    
    <script>
          $("#loader").hide() 
          $(document).on("click","#SearchAssureForm", function(e){
            e.preventDefault();
            var _token = $('input[name="_token"]').val();
            var typeUssuer = $('.typeUssuer').val();
            var numeAssure = $('.numeAssure').val();
            var prandor = $('.prandor').val();
            var url = "/dashbord/prestataire/search-assure/" + prandor;

            var formData = {
              typeUssuer: typeUssuer,
              numeAssure: numeAssure,
              _token: _token
            };

            if(numeAssure !=''){
              $("#loader").show() 
              $.ajax({
                method: 'POST',
                url: '/dashbord/prestataire/search-assure/' + prandor,
                data: formData,
                dataType:"json",
                success : function(response){
                  console.log(response.statut);
                  if (response.statut == 'success') {
                    $('.retour_CarteAssure').html(response.code);
                    $('#loader').modal("hide");
                  }else{
                      $('#loader').modal("hide");
                      toastr.warning(response.message, response.title, {
                        positionClass: "toast-top-right",
                        timeOut: 5e3,
                        closeButton: !0,
                        debug: !1,
                        newestOnTop: !0,
                        progressBar: !0,
                        preventDuplicates: !0,
                        onclick: null,
                        showDuration: "500",
                        hideDuration: "1000",
                        extendedTimeOut: "1000",
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut",
                        tapToDismiss: !1
                    });
                    $('#loader').html(response.message);
                  }
                }
              });
            }else{
                alert("Veuillez renseigner tous les champs Svp.");
            }
        });
       $(document).on("click",".DetailView", function(e){
          e.preventDefault();
          var a=$(this);
          $('.retour_modal').text("");
          $.ajax({
            method: 'get',
            url: a.attr("href"),
            success : function(response){
              if (response.statut == 'messageErreur') {
                $('.retour_modal').html(response.message);
                $('.ModalAcces').modal("show");
              }else{
                location.href=a.attr("href");
              }
            }
          })
        });

        $(document).on("click",".DetailView2", function(e){
          e.preventDefault();
          var a=$(this);
          $('.retour_modal').text("");
          $.ajax({
            method: 'get',
            url: a.attr("href"),
            success : function(response){
              if (response.statut == 'messageErreur') {
                toastr.warning(response.message, response.title, {
                    positionClass: "toast-top-right",
                    timeOut: 5e3,
                    closeButton: !0,
                    debug: !1,
                    newestOnTop: !0,
                    progressBar: !0,
                    preventDuplicates: !0,
                    onclick: null,
                    showDuration: "500",
                    hideDuration: "1000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                    tapToDismiss: !1
                });
              }else{
                location.href=a.attr("href");
              }
            }
          })
        });


        
        $(document).on("click",".ViewPage", function(e){
          e.preventDefault();
          var a=$(this);
          location.href=a.attr("href");
        });

        $(document).on("click",".DeleteModale", function(e){
          e.preventDefault();
          var a=$(this);
          //location.href=a.attr("href");
          swal({   
            title: "SUPPRESSION",   
            text: "ETES-VOUS SURE DE VOULOIR SUPPRIMER",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#fec107",   
            confirmButtonText: "OUI, SUPPRIMER",   
            cancelButtonText: "NON, ANNULER",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {
               
              $.ajax({
                method: 'get',
                url: a.attr("href"),
                success : function(response){
                  if (response.statut == 'error') {
                    $.toast({
                        heading: "SUPPRESSION",
                        text: response.message,
                        position: 'top-right',
                        loaderBg:'#fec107',
                        icon: 'error',
                        hideAfter: 7000, 
                        stack: 6
                    });
                    $('.affiche').modal('hide');
                  }else{
                    swal("SUPPRESSION", response.message, "success");
                    $.toast({
                        heading: "SUPPRESSION",
                        text: response.message,
                        position: 'top-right',
                        loaderBg:'#fec107',
                        icon: 'success',
                        hideAfter: 7000, 
                        stack: 6
                    });
                    $('.affiche').modal('hide');
                  }
                }
              }) 
                 
                  
            } else {     
              swal("ANNULATION", "Suppression Annulée :)", "error");   
            } 
        });
        });

        $(document).on("click",".DeleteRedirect", function(e){
          //alert("lorem lorem lorem lorem lorem lorem lorem lorem");
          e.preventDefault();
          var a=$(this);
          swal({   
            title: "SUPPRESSION",   
            text: "ETES-VOUS SURE DE VOULOIR SUPPRIMER",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#fec107",   
            confirmButtonText: "OUI, SUPPRIMER",   
            cancelButtonText: "NON, ANNULER",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {
              location.href=a.attr("href");
              swal("SUPPRESSION", "En cour de suppression", "success");
            } else {     
              swal("ANNULATION", "Suppression Annulée :)", "error");   
            } 
        });
        });



      
    </script>
	
	@stack('script.footer1')
    @stack('script.footer2')
	
</body>
</html>