

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{config("app.name")}} | CONNEXION</title>
	
	<!-- FAVICONS ICON -->
	<link rel="shortcut icon" type="image/png" href="images/favicon.png">
    <link href="{{URL::asset('erpfiles/css/style.css')}}" rel="stylesheet">
    <style>
        .border-primary{
            border: #222B40 solid 1px;
        }
        
        
    </style>

</head>

<body class="vh-100" style="background-image:url({{URL::asset('erpfiles/images/bgP.jpg')}}); background-size:100%; background-repeat:no-repeat; background-position:center;">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form headerP">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(session()->has('messageErreur'))
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><strong>Erreur ! </strong> {{ Session::get('messageErreur') }}</p>
                                                    <div class="clearfix"></div>
                                                </div>
                                                @endif
                                                @if(session()->has('message'))
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><strong>Félicitations ! </strong> {{ Session::get('message') }}</p> 
                                                    <div class="clearfix"></div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

									<div class="text-center mb-3">
										<img src="{{URL::asset('erpfiles/images/logo.png')}}" alt="Muscop-ci Logo">
									</div>
                                    <h4 class="text-center mb-4">CONNEXION</h4>
                                    <form method="POST" action="{{ route('login') }}">@csrf
                                        <div class="mb-3">
                                            <label class="mb-1 txt-dark" style="color:black"><strong>Email</strong></label>
                                            <input type="email" name="email" class="form-control border-primary" placeholder="hello@example.com">
                                        </div>
                                        <div class="mb-3">
                                            <label class="mb-1 txt-dark"  style="color:black"><strong>Mot de passe</strong></label>
                                            <input type="password" name="password" class="form-control border-primary">
                                        </div>
                                        <div class="text-center mt-4">
                                            <button type="submit" class="btn btn-primary btn-block">CONNEXION</button>
                                        </div>
                                    </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--**********************************
	Scripts
***********************************-->
<!-- Required vendors -->
<script src="{{URL::asset('erpfiles/vendor/global/global.min.js')}}"></script>
<script src="{{URL::asset('erpfiles/js/custom.js')}}"></script>
<script src="{{URL::asset('erpfiles/js/deznav-init.js')}}"></script>
</body>
</html>