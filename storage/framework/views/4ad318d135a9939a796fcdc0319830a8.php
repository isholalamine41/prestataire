<?php $__env->startSection('content'); ?>

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="row">
									
                                    <div class="col-xl-3 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card ">
                                                <div class="depostit-card-media d-flex justify-content-between style-1">
                                                    <div>
                                                        <h6>TOTAL ORDONNANCE</h6>
                                                        <h3><?php echo e(number_format($data->count(), 0, ',', ' ')); ?></h3>
                                                    </div>
                                                    <div class="icon-box bg-primary">
                                                        <i style="color: #fff;" class="las la-notes-medical"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card ">
                                                <div class="depostit-card-media d-flex justify-content-between style-1">
                                                    <div>
                                                        <h6>MONTANT TOTAL</h6>
                                                        <h3><?php echo e(number_format($total_prestation, 0, ',', ' ')); ?> FCFA</h3>
                                                    </div>
                                                    <div class="icon-box bg-info">
                                                        <i style="color: #fff;" class="las la-file-invoice-dollar"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card p-0">
                                                <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                    <div>
                                                        <h6>MONTANT MUSCOP CI</h6>
                                                        <h3><?php echo e(number_format($total_muscopci, 0, ',', ' ')); ?> FCFA</h3>
                                                    </div>
                                                    <div class="icon-box bg-success">
                                                        <i class="las la-file-invoice-dollar" style="color: #fff;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <div class="card same-card headerP">
                                            <div class="card-body depostit-card">
                                                <div class="depostit-card-media d-flex justify-content-between style-1">
                                                    <div>
                                                        <h6>MONTANT ASSURE</h6>
                                                        <h3><?php echo e(number_format($total_assure, 0, ',', ' ')); ?> FCFA</h3>
                                                    </div>
                                                    <div class="icon-box bg-warning">
														<i class="las la-file-invoice-dollar" style="color: #fff;"></i>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP" style="padding: 15px;">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date début</label>
                                            <input type="date" id="datedebut" name="datedebut" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date fin</label>
                                            <input type="date" id="datefin" name="datefin" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Type d'assuré</label>
                                            <select name="nature_id" id="type" class="form-control border-success">
                                                <option value="all">Tous les assurés</option>
                                                <option value="App\Models\Mutualiste">Assuré principal</option>
                                                <option value="App\Models\Ayantdroit">Ayants-droit</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <button class="btn btn-success btn-icon right-icon" id="search" style="margin-top: 2em; width:100%"><span>RECHERCHEZ </span> <i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                               
                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0" id="HeaderTitle">Total ordonnance (<?php echo e(number_format($data->count(), 0, ',', ' ')); ?>)</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table class="table card-table border-no success-tbl p-0" id="table">
                                                <thead>
                                                    <tr>
                                                        <th>DATE : HEURE </th>
                                                        <th>ASSURE</th>
                                                        <th>ACTE</th>
                                                        <th class="text-center">NOMBRE DE MEDICAMENT </th>
                                                        <th>CODE</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody">
                                                    <?php if($data->count()>0): ?>
                                                        <?php $__currentLoopData = $data->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ordonnance): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="ms-2 cat-name">
                                                                        <p class="mb-0"><?php echo e(date("d/m/Y", strtotime($ordonnance->date))); ?> à <?php echo e($ordonnance->heure); ?></p>
                                                                    </div>	
                                                                </div>
                                                            </td>
                                                            <td><?php echo e($ordonnance->assurer->last_name ?? $ordonnance->assurer->name); ?> <?php echo e($ordonnance->assurer->first_name); ?></td>
                                                            <td><?php echo e($ordonnance->presta_muta->libelle); ?></td>
                                                            <td class="text-center">
                                                                <div class="badge badge-primary">
                                                                    <?php echo e(count($ordonnance->medicaments())); ?>

                                                                </div>
                                                            </td>
                                                            <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                                                                <?php echo e($ordonnance->Code->code); ?>

                                                            </td>
                                                            <td>
                                                                <button href="<?php echo e(route('DetailsOrdonnanceParPrestatire',[$ordonnance->id])); ?>" class="btn btn-success lanceModal">
                                                                    <i class="las la-eye"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>
wrapper theme-4-active pimary-color-red
<?php $__env->stopSection(); ?>


<?php $__env->startPush('style.hearder'); ?>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('script.footer1'); ?>
<script>

    var table = $('#table').DataTable();

    $('#search').on('click',()=>{

        $('#search').html('<div class="spinner-border" role="status"></div>');

        let datedebut = $('#datedebut').val();
        let datefin = $('#datefin').val();
        let type = $('#type').val();
        
        table.destroy();

        $.ajax({
            method: 'GET',
            data : {
                datedebut : datedebut,
                datefin : datefin,
                type : type,
            },
            url: "<?php echo e(route('RechecheOrdonnanceMedicaux')); ?>",
            success : function(response){

                $("#tbody").html(response);
                table = $('#table').DataTable();
                $('#search').html('<span>RECHERCHEZ </span> <i class="fa fa-search"></i>');
                $('#HeaderTitle').html('<h4 id="HeaderTitle" class="heading mb-0 HeaderTitle">RESULTATS : ('+table.rows().count()+') </h4>');

            }
        });

    });

    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
</script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title'); ?>
ORDONNANCES
<?php $__env->stopSection(); ?>


<?php $__env->startSection('dd'); ?>
active
<?php $__env->stopSection(); ?>



<?php $__env->startPush('script.footer2'); ?>

<?php $__env->stopPush(); ?>






<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/ordonnance/liste-ordonnance.blade.php ENDPATH**/ ?>