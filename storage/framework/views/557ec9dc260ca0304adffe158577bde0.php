
<?php if($lp->listeprestable->typePrestation->libelle == "HOSPITALISATION"): ?>
<div class="col-md-12"><br>
    <span class="">Quantité / nombre de Jours</span>
    <input type="number" value="1" class="form-control border-success qte" name="qte">
</div>
<?php endif; ?>
<div class="col-md-12"><br>
    <span>Motif *</span>
    <textarea name="" id="" cols="30" class="form-control border-success commentaire" rows="2"></textarea>
</div>
<div class="col-md-12"><br>
    <span class="">Nom Docteur / Medecin *</span>
    <input type="text" class="form-control border-success docteur" name="docteur">
</div>
<?php if($lp->listeprestable->ententePrealable): ?>
    <div class="col-md-12"><br>
        <input type="hidden" class="form-control border-success demande" value="demande" name="demande">
        <p class="txt-dark weight-bold" style="color:brown">Cette Prestation nécessite un accord du medecin conseil de la MUSCOP-CI </p>
        <button type="button" style="margin-top: 1.5em; width:100%" class="btn btn-warning" id="AddActes"><i class="fa-brands fa-accusoft me-2"></i>Faire une demande</button>
    </div>
    <?php else: ?>
    <div class="col-md-12"><br>
        <button type="button" style="margin-top: 1.5em; width:100%" class="btn btn-success AddActes" id="AddActes"><i class="fa-brands fa-accusoft me-2"></i>Ajouter</button>
    </div>
<?php endif; ?><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/actes/resteform.blade.php ENDPATH**/ ?>