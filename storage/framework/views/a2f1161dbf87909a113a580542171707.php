<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-success">
                <!-- <?php echo e($a->ListePrestation->listeprestable->typePrestation->libelle); ?>    -->
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;"><?php echo e($a->ListePrestation->listeprestable->libelle); ?> - <?php echo e($a->ListePrestation->libelle); ?> </h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <h4><?php echo e($a->ListePrestation->Prestataire->rs); ?></h4>
                <table class="table card-table border-no success-tbl p-0 headerP">
                    <thead>
                        <tr>
                            <th>PRESTATION</th>
                            <th>MONTANT</th>
                            <th>TIER PAYANT</th>
                            <th>PART ASSURE</th>
                            <th>QTE</th>
                            <th>STATUT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="ms-2 cat-name">
                                        <p class="mb-0"><?php echo e($a->libelle); ?></p>
                                    </div>	
                                </div>
                            </td>
                            <td class="txt-dark"><?php echo e(number_format($a->part_muscopci+$a->part_assure, 0, ',', ' ')); ?> FCFA</td>
                            <td class="txt-dark">
                                <?php echo e(number_format($a->part_muscopci, 0, ',', ' ')); ?> FCFA  
                            </td>

                            <td  class="txt-dark">
                                <?php echo e(number_format($a->part_assure, 0, ',', ' ')); ?> FCFA
                            </td>
                            <td  class="txt-dark">
                                <?php echo e($a->qte); ?>

                            </td>
                            <td>
                                <?php if($a->type == "demande"): ?>
                                    <?php if($a->entente == 'En attente'): ?>
                                    <span class="badge badge-warning border-0"><?php echo e($a->entente); ?></span>
                                    <?php endif; ?>
                                    <?php if($a->entente == 'Accordée'): ?>
                                    <span class="badge badge-success border-0"><?php echo e($a->entente); ?></span>
                                    <?php endif; ?>
                                    <?php if($a->entente == 'Rejetée'): ?>
                                    <span class="badge badge-danger border-0"><?php echo e($a->entente); ?></span>
                                    <?php endif; ?>
                                    <?php else: ?> 
                                    <?php echo e($a->entente); ?>

                                <?php endif; ?>
                            </td>
                        </tr>
                    </tbody>
                
                </table>
                <p class="txt-dark weight-bold">Commentaire / Description</p>
                <div class="row " style="padding: 15px; margin-top:-2em;">
                
                    <div class="col-md-12 border-success">
                        <?php echo e($a->description); ?>

                    </div>
               </div>
                <br><br>

                <?php if($a->Ordonnance->count()>0): ?>

                    <h4>LISTE DES ORDONNANCES (<?php echo e($a->Ordonnance->count()); ?>)</h4>
                    <table class="table card-table border-no success-tbl p-0 headerP">
                        <thead>
                            <tr>
                                <th>DATE : HEURE </th>
                                <th>ACTE </th>
                                <th>MEDICAMENT</th>
                                <th>CODE</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $a->Ordonnance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ordonnance): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-2 cat-name">
                                            <p class="mb-0"><?php echo e(date("d/m/Y"), strtotime($ordonnance->date)); ?> à <?php echo e($ordonnance->heure); ?></p>
                                        </div>	
                                    </div>
                                </td>
                                <td><?php echo e($ordonnance->presta_muta->libelle); ?></td>
                                <td class="text-center">
                                    <ul>
                                        <?php $__currentLoopData = $ordonnance->medicaments(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($data->quantite); ?> x <?php echo e($data->medicament); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </td>
                                <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                                <?php echo e($ordonnance->Code ? $ordonnance->Code->code : ''); ?> 
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    
                    </table>
                    <br>
                
                <?php endif; ?>

                <?php if($a->Prescription->count()>0): ?>

                    <h4>LISTE DES PRESCRIPTIONS DEJA EFFECTUEES (<?php echo e($a->Prescription->count()); ?>)</h4>
                    <table class="table card-table border-no success-tbl p-0 headerP">
                        <thead>
                            <tr>
                                <th>DATE : HEURE </th>
                                <th>NATURE </th>
                                <th>PRESCRIPTION</th>
                                <th>CODE</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $a->Prescription; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-2 cat-name">
                                            <p class="mb-0"><?php echo e(date("d/m/Y"), strtotime($p->date)); ?> à <?php echo e($p->heure); ?></p>
                                        </div>	
                                    </div>
                                </td>
                                <td class="txt-dark"><?php echo e($p->TypePrescription->libelle); ?></td>
                                <td class="txt-dark"><?php echo e($p->libelle); ?></td>

                                <td  class="txt-dark weight-bold font-16" style="font-size: 16px;">
                                    <?php echo e($p->Code->code); ?>

                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    
                    </table>
                <?php endif; ?>

                

            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/santemv/www/resources/views/admin/prestataire/actes/dateilActe.blade.php ENDPATH**/ ?>