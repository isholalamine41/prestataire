
<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-success">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">Détails actes facture</h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <table class="table card-table border-no success-tbl p-0 headerP">
                    <thead>
                        <tr>
                            <th>PRESTATION</th>
                            <th>MONTANT</th>
                            <th>PART ASSURANCE</th>
                            <th>PART ASSURE</th>
                            <th>QTE</th>
                            <th>STATUT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $prestaMutua; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-2 cat-name">
                                            <p class="mb-0"><?php echo e($a->libelle); ?></p>
                                        </div>	
                                    </div>
                                </td>
                                <td class="txt-dark"><?php echo e(number_format($a->part_muscopci + $a->part_assure, 0, ',', ' ')); ?> FCFA</td>
                                <td class="txt-dark">
                                    <?php echo e(number_format($a->part_muscopci, 0, ',', ' ')); ?> FCFA  
                                </td>

                                <td  class="txt-dark">
                                    <?php echo e(number_format($a->part_assure, 0, ',', ' ')); ?> FCFA
                                </td>
                                <td  class="txt-dark">
                                    <?php echo e($a->qte); ?>

                                </td>
                                <td>
                                    <?php if($a->type == "demande"): ?>
                                        <?php if($a->entente == 'En attente'): ?>
                                        <span class="badge badge-warning border-0"><?php echo e($a->entente); ?></span>
                                        <?php endif; ?>
                                        <?php if($a->entente == 'Accordée'): ?>
                                        <span class="badge badge-success border-0"><?php echo e($a->entente); ?></span>
                                        <?php endif; ?>
                                        <?php if($a->entente == 'Rejetée'): ?>
                                        <span class="badge badge-danger border-0"><?php echo e($a->entente); ?></span>
                                        <?php endif; ?>
                                        <?php else: ?>
                                        Brouillon
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                
                </table>
                

            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/santemv/www/resources/views/admin/prestataire/facture/details.blade.php ENDPATH**/ ?>