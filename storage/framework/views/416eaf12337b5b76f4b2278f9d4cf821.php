<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">
                    Hospitalisation de <?php echo e($hospitalisation->assurer->first_name); ?>

                </h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="text-danger">Code : <?php echo e($hospitalisation->Code->code); ?></h3>
                    </div>
                    <div class="col-md-3">
                        <a href="<?php echo e(route('printHospitalisation',[$hospitalisation->id])); ?>" class="btn btn-warning btn-block">Imprimer <i class="fa fa-print"></i></a>
                    </div>
                </div>
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <table class="table table-striped">
                        <thead>
                            <th>Quantité</th>
                            <th>Acte</th>
                            <th>Montant</th>
                            <th>Part assuré</th>
                            <th>Part MUSCOP CI</th>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $hospitalisation->lignes_facture(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ligne): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr style="text-align: left;">
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($ligne->quantite); ?></td>
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($ligne->prestation_name); ?></td>
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($ligne->quantite*$ligne->prix); ?></td>
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($ligne->part_assure); ?></td>
                                    <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($ligne->part_muscopci); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/hospitalisation/details-hospitalisation-prestataire.blade.php ENDPATH**/ ?>