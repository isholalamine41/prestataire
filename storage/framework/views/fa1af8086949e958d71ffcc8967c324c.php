
<div class="headerP border-success row" style="margin: 5px;padding: 5px;border-radius: 5px;background: white;">
    <div class="col-xl-4 col-lg-4 col-md-4">
        <a href="<?php echo e(route('GotoMakeActe',[$assure->id, $type])); ?>">
            <div class="card-body" style="border:1px solid #452b90; border-radius:5px;" >
                <div  class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3  col-xs-3 ">
                        <div class="crd-bx-img" align="left">
                            <?php if($type == "Mutualiste"): ?>
                            <img src='<?php echo e(config("app.imgLink")); ?><?php echo e($assure->photo); ?>' style="height:60px;" class="rounded-circle" alt="">
                            <?php else: ?>
                            <img src='<?php echo e(config("app.adLink")); ?><?php echo e($assure->photo); ?>' style="height:60px;" class="rounded-circle" alt="">
                            <?php endif; ?>
                            <div class="active"></div>
                        </div>
                    </div>

                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9  col-xs-9">
                        <div class="card__text" align="left" style="margin-left: 6px;">
                            <h4 class="mb-0 txt-dark"><?php echo e($assure->first_name); ?> <?php echo e($assure->name ?? $assure->last_name); ?></h4>
                            <p class="mb-0"><?php echo e($assure->role ?? $assure->assurance_number); ?></p><p class="mb-0" style="margin-top: -0.5em; padding:0px;"><b><?php echo e($assure->birth_date ?? $assure->date_naissance); ?></b></p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div> 

    <div class="col-xl-8">
        <div class="">
            <br>
            <div class="card-body custome-tooltip p-3 text-right" align="right" >
                <a href="<?php echo e(route('AddHospitalisationModal',[$assure->id, $type])); ?>" class="<?php echo e(($acteConsultation->count()>0 && $ph->count()==0) ? 'lanceModal' : 'block'); ?>" aria-expanded="false">
                    <button <?php echo e(($acteConsultation->count()==0 || $ph->count()>0) ? 'disabled' : ''); ?> type="button" class="btn btn-danger"><span class="nav-text weight-bold"> <i style="font-size: 16px;" class="las la-notes-medical"></i> &ensp; DEMANDE D'HOSPITALISATION</span></button>
                </a>
                <a href="<?php echo e(route('AddPrescriptionExam',[$assure->id, $type])); ?>" class="<?php echo e($actesM->count()>0 ? 'lanceModal' : 'block'); ?>" aria-expanded="false">
                    <button <?php echo e($actesM->count()==0 ? 'disabled' : ''); ?>  type="button" class="btn btn-success"><span class="nav-text weight-bold"> <i style="font-size: 16px;" class="las la-notes-medical"></i> &ensp; AJOUTER UNE PRESCRIPTION</span></button>
                </a>
                <a href="<?php echo e(route('PrescriptionOrdonnance',[$assure->id, $type])); ?>" class="<?php echo e($actesM->count()>0 ? 'lanceModal' : 'block'); ?>" aria-expanded="false">
                    <button <?php echo e($actesM->count()==0 ? 'disabled' : ''); ?>  type="button" class="btn btn-primary"><span class="nav-text weight-bold"> <i style="font-size: 16px;" class="las la-stethoscope"></i> &ensp; AJOUTER UNE ORDONNANCE</span></button>
                </a>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
        <a href="<?php echo e(route('UserActesMedicaux',[$assure, $p->id, $type])); ?>">
        <div class="card same-card same-card headerP">
            <div class="card-body depostit-card p-0">
                <div class="depostit-card-media d-flex justify-content-between pb-0">
                    <div>
                        <h6>ACTES MEDICAUX</h6>
                        <h3><?php echo e($actesM->count()); ?></h3>
                    </div>
                    <div class="icon-box bg-danger">
                    <i class="las la-stethoscope" style="color: #fff;"></i>

                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
        <a href="<?php echo e(route('UserHospitalisation',[$assure, $prestataire->id, $type])); ?>">
        <div class="card same-card same-card headerP">
            <div class="card-body depostit-card p-0">
                <div class="depostit-card-media d-flex justify-content-between pb-0">
                    <div>
                        <h6>HOSPITALISATIONS</h6>
                        <h3><?php echo e($ph->count()); ?></h3>
                    </div>
                    <div class="icon-box bg-danger">
                    <i class="las la-notes-medical" style="color: #fff;"></i>

                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
        <a href="<?php echo e(route('UserPrescription',[$assure, $prestataire->id, $type])); ?>">
        <div class="card same-card headerP">
            <div class="card-body depostit-card">
                <div class="depostit-card-media d-flex justify-content-between style-1">
                    <div>
                        <h6>PRESCRIPTIONS EN LIGNE </h6>
                        <h3><?php echo e($pc->count()); ?></h3>
                    </div>
                    <div class="icon-box bg-success">
                        <i class="las la-notes-medical" style="color: #fff;"></i>
                    </div>
                </div>

            </div>
        </div>
        </a>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
        <a href="<?php echo e(route('UserOrdonnance',[$assure, $prestataire->id, $type])); ?>">
        <div class="card same-card same-card headerP">
            <div class="card-body depostit-card p-0">
                <div class="depostit-card-media d-flex justify-content-between pb-0">
                    <div>
                        <h6>ORDONNANCES EN LIGNE</h6>
                        <h3><?php echo e($po->count()); ?></h3>
                    </div>
                    <div class="icon-box bg-primary">
                    <i class="las la-stethoscope" style="color: #fff;"></i>

                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
</div>





<?php $__env->startPush('script.footer2'); ?>
    <script>
        $('.block').on('click',function(e){
            e.preventDefault();
        });
    </script>
<?php $__env->stopPush(); ?><?php /**PATH /home/santemv/www/resources/views/layouts/admin/block-assure.blade.php ENDPATH**/ ?>