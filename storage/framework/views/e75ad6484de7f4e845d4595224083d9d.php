<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config("app.name")); ?> | <?php echo $__env->yieldContent('title'); ?></title>

    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="<?php echo e(URL::asset('erpfiles/images/favicon.png')); ?>" type="image/x-icon">

    <link href="<?php echo e(URL::asset('erpfiles/css/style.css')); ?>" rel="shortcut icon" type="text/css">
    <link rel="shortcut icon" type="image/png" href="images/favicon.png">
    <script type="text/javascript" src="//www.turnjs.com/lib/turn.min.js "></script>
</head>
<body class="vh-100" style="background-image:url('images/bg.png'); background-position:center;">

            <?php echo $__env->yieldContent('content'); ?>
       



        <script src="<?php echo e(URL::asset('erpfiles/vendor/global/global.min.js')); ?>"></script>
        <script src="<?php echo e(URL::asset('erpfiles/js/custom.js')); ?>"></script>
        <script src="<?php echo e(URL::asset('erpfiles/js/deznav-init.js')); ?>"></script>
</body>
</html>



<?php /**PATH /home/santemv/www/resources/views/layouts/app.blade.php ENDPATH**/ ?>