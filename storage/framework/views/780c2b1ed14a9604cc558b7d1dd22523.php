<?php $__env->startSection('content'); ?>

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0">Nos prestations (<?php echo e($prestations->count()); ?>)</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table id="table" class="display table" style="min-width: 845px">
                                                <thead>
                                                    <tr>
                                                        <th>LIBELLE</th>
                                                        <th>CATEGORIE</th>
                                                        <th>MONTANT</th>
                                                        <th>PART ASSURE</th>
                                                        <th>PART MUSCOPCI</th>
                                                    </tr>
                                                </thead>
                                                <?php if($prestations->count()>0): ?>
                                                <tbody id="tbody" >
                                                    <?php $__currentLoopData = $prestations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($lp->listeprestable): ?>
                                                    <tr>
													    <td class="txt-dark"><?php echo e($lp->libelle); ?></b></td>
													    <td class="txt-dark"><?php echo e($lp->listeprestable->libelle); ?></b></td>
													    <td class="txt-dark"><?php echo e($lp->ParamlistePrestation->last()->cout ?? 0); ?> <small>FCFA</small></b></td>
													    <td class="txt-dark"><?php echo e($lp->ParamlistePrestation->last()->parAssure ?? 0); ?> <small>FCFA</small></b></td>
													    <td class="txt-dark"><?php echo e($lp->ParamlistePrestation->last()->tierPayant ?? 0); ?> <small>FCFA</small></b></td>
                                                    </tr>
                                                    <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                                <?php endif; ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>
wrapper theme-4-active pimary-color-red
<?php $__env->stopSection(); ?>


<?php $__env->startPush('style.hearder'); ?>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('script.footer1'); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title'); ?>
NOS PRESTATIONS
<?php $__env->stopSection(); ?>


<?php $__env->startSection('dd'); ?>
active
<?php $__env->stopSection(); ?>



<?php $__env->startPush('script.footer2'); ?>

<script>
    $('#table').DataTable();
</script>
<?php $__env->stopPush(); ?>






<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/santemv/www/resources/views/admin/prestation/prestations.blade.php ENDPATH**/ ?>