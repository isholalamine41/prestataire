                    <div class="card-header bg bg-info">
                            <h4 class="card-title text-white">FACTURE </h4>
                        </div>
                        <?php if($actes->count()>0): ?>
                        <div class="card-header" style="width: 100%;">
                            <div class="row" style="width: 100%;">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" >
                                    <h4 class="heading mb-0">MONTANT GLOBAL : <?php echo e(number_format($actes->SUM("part_muscopci") + $actes->SUM("part_assure"), 0, ',', ' ')); ?> FCFA  </h4>
                                    <h4 class="heading mb-0">PART ASSURANCE : <?php echo e(number_format($actes->SUM("part_muscopci"), 0, ',', ' ')); ?> FCFA  </h4>
                                    <h4 class="heading mb-0" style="color: brown;">MONTANT A PAYER : <?php echo e(number_format($actes->SUM("part_assure"), 0, ',', ' ')); ?> FCFA  </h4>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <?php if($actes->where('entente','En attente')->count()>0): ?>
                                    <button type="button" class="btn  btn-square btn-primary ValidateAllInDemandeActes"><i class="fa fa-save"></i> VALIDER</button>
                                    <?php else: ?>
                                    <button type="button" href="<?php echo e(route('ValidateAllActes', [$prestataire->id , $assure->id, $typeAss] )); ?>" class="btn  btn-square btn-success ViewPage"><i class="fa fa-save"></i> VALIDER</button>
                                    <?php endif; ?>
                                    <button type="button" href="<?php echo e(route('deleteALLACTETamPon', [$prestataire->id , $assure->id, $typeAss] )); ?>" class="btn  btn-square btn-warning deleteACTETamPon"> <i class="fa fa-trash"></i> ANNULER</button>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="card-body" >
                            <div class="loaderFacture" align="center" ><img style="max-height: 100px; vertical-align:middle;" src="<?php echo e(URL::asset('erpfiles/images/loader.gif')); ?>" alt=""></div>
                            
                        <div class="card-body p-0">
                            <div class="table-responsive p-0">
                                <table class="table card-table border-no success-tbl p-0">
                                    <thead >
                                        <tr >
                                            <th>PRESTATION</th>
                                            <th>MONTANT</th>
                                            <th>TIER PAYANT</th>
                                            <th>STATUT</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $actes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="ms-2 cat-name">
                                                        <p class="mb-0"><?php echo e($a->libelle); ?></p>
                                                    </div>	
                                                </div>
                                            </td>
                                            <td><?php echo e(number_format($a->ListePrestation->ParamlistePrestation->last()->cout ?? 0, 0, ',', ' ')); ?> FCFA</td>
                                            <td>
                                                <?php echo e(number_format($a->part_muscopci, 0, ',', ' ')); ?> FCFA  
                                            </td>

                                            <td>
                                                <?php if($a->type == "demande"): ?>
                                                    <?php if($a->entente == 'En attente'): ?>
                                                    <span class="badge badge-warning border-0"><?php echo e($a->entente); ?></span>
                                                    <?php endif; ?>
                                                    <?php if($a->entente == 'Accordée'): ?>
                                                    <span class="badge badge-success border-0"><?php echo e($a->entente); ?></span>
                                                    <?php endif; ?>
                                                    <?php if($a->entente == 'Rejetée'): ?>
                                                    <span class="badge badge-danger border-0"><?php echo e($a->entente); ?></span>
                                                    <?php endif; ?>
                                                    <?php else: ?>
                                                    Brouillon
                                                <?php endif; ?>
                                                
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" href="<?php echo e(route('ViewActeTampon', [$a->id , $assure->id, $typeAss] )); ?>"  class="btn btn-success btn-icon-xs lanceModal"><i class="fa fa-eye"></i></button>
                                                    <button type="button" href="<?php echo e(route('DeleteActeTampon', [$a->id , $assure->id, $typeAss] )); ?>" class="btn btn-warning btn-icon-xs DeleteActeTamponAjax"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                
                                </table>
                            </div>
                        </div>
                        </div>
                        </div><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/actes/actesTampon.blade.php ENDPATH**/ ?>