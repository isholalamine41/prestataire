<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">AJOUTER A L'HOSPITALISATION</h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body"> 
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <form action="<?php echo e(route('AddHospitalisationFacture')); ?>" method="post"><br>
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <input type="hidden" value="<?php echo e($assure->id); ?>" name="assure_id">
                            <input type="hidden" value="<?php echo e($type); ?>" name="type">
                        </div>
                        <h4>Hospitalisation : <?php echo e($hospitalisation->Code->code); ?></h4>
                        <input type="hidden" value="<?php echo e($hospitalisation_id); ?>" name="hospitalisation_id">
                        <div class="row line">
                            <div class="col-xl-9 col-xxl-9 col-md-9 col-lg-9">
                                <label for="">Prestations</label>
                                <select style="border: 1px solid #452a90;" id="prestation_id" class="form-control">
                                    <option value=""></option>
                                    <?php $__currentLoopData = $prestations->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $_prestation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <optgroup label="<?php echo e($_prestation->libelle); ?>" <?php echo e($_prestation->exclusion==true ? 'disable' : ''); ?>>
                                            <?php $__currentLoopData = $_prestation->ListePrestation()->where('libelle','!=','Hospitalisation')->where('prestataire_id',$prestataire->id)->where('hidden',0)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prestation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option 
                                                    data-prix="<?php echo e($prestation->ParamlistePrestation->last()->cout); ?>"
                                                    data-part_assure="<?php echo e($prestation->ParamlistePrestation->last()->parAssure); ?>"
                                                    data-part_muscopci="<?php echo e($prestation->ParamlistePrestation->last()->tierPayant); ?>"
                                                    data-id="<?php echo e($prestation->id); ?>"
                                                ><?php echo e($prestation->libelle); ?>

                                                </option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </optgroup>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="" style="visibility:hidden">Prestations</label>
                                <button class="btn btn-primary btn-block" id="add-button" type="button">Ajouter <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <br>
                        <table class="table table-striped">
                            <tbody id="tbody-hospit" >
                            </tbody>
                        </table>
                        <button type="submit" style="width: 250px;float:right;background:black" class="btn btn-warning title hidden submit">AJOUTER</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .line{
        margin-bottom: 15px !important;
    }
    .td{
        color:black !important;
    }
    .hidden{
        display: none;
    }
    .fa-times{
        cursor: pointer;
    }
</style>
<script>

    $('.submit').on('click',function(){
        $('.submit').addClass('hidden');
    });

    var data = $('#tbody-hospit');

    $('#add-button').on('click',function(){

        let prestation = $('#prestation_id :checked');
        let prestation_text = prestation.text().trim();

        in_array = false;

        $('.prestation_text').each(function(index, element) {

            if($(element).val()==prestation_text && prestation_text!='Pharmacie')
                in_array = true;
        });

        if(in_array){
            alert("Prestation déjà présent dans la liste");
            return;
        }

        if(prestation_text!='' && prestation_text!='Pharmacie'){
            data.append(`
                <tr>
                    <td class="td" style="width: 100px;"><input name="quantite[]" type="number" class="form-control" placeholder="Quantité" min="1" value="1"></td>
                    <td class="td">
                        <input type="hidden"  name="prestation_id[]"  value="${prestation.data('id')}" >
                        <input type="hidden" name="part_assure[]" value="${prestation.data('part_assure')}" >
                        <input type="hidden" name="part_muscopci[]" value="${prestation.data('part_muscopci')}" >
                        <input name="prestation_name[]" type="text" readonly class="form-control prestation_text" value="${prestation_text}" >
                    </td>
                    <td class="td" style="width: 170px;" ><input name="prix[]" type="number" readonly class="form-control"  min="10"  value="${prestation.data('prix')}"></td>
                    <td class="td"  class="text-center"><i onclick="delete_line(this)" class="fa fa-trash text-danger"></i></td>
                </tr>
            `);
       }else if(prestation_text=='Pharmacie'){

            data.append(`
                <tr>
                    <td class="td" style="width: 100px;"><input name="quantite[]" type="number" class="form-control" placeholder="Quantité" min="1" value="1"></td>
                    <td class="td">
                        <input type="hidden" class="prestation_id"  name="prestation_id[]" value="${prestation.data('id')}" >
                        <input type="hidden" name="part_assure[]" value="0" >
                        <input type="hidden" name="part_muscopci[]" value="0" >
                        <input name="prestation_name[]" type="text" class="form-control prestation_text" placeholder="Saisir le médicament" >
                    </td>
                    <td class="td" style="width: 170px;" ><input name="prix[]" type="number" class="form-control"  min="10" placeholder="Saisir le prix"></td>
                    <td class="td" class="text-center"><i onclick="delete_line(this)" class="fa fa-trash text-danger"></i></td>
                </tr>
            `);
       }

       $('#prestation_id').val('');
        console.log(data.html());
        console.log(10);

       if(data.html().trim()!=''){
            $('.title').removeClass('hidden');
       }

    });

    function delete_line(self){

        $(self).parent().parent().remove();

        console.log(data.html());

       if(data.html().trim()==''){
            $('.title').addClass('hidden');
       }

    }

</script><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/hospitalisation/addhospitalisationfacture.blade.php ENDPATH**/ ?>