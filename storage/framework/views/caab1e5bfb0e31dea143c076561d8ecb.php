<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">
                    ORDONNANCE de <?php echo e($ordonnance->assurer->name ?? $ordonnance->assurer->first_name); ?> <?php echo e($ordonnance->assurer->last_name); ?>

                </h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="text-danger">Code : <?php echo e($ordonnance->Code->code); ?></h3>
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <table class="table table-striped">
                        <thead>
                            <th>Quantité</th>
                            <th>Médicament</th>
                            <th>Prix</th>
                            <th>Etat</th>
                            <th>Pharmacie</th>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $ordonnance->medicaments(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $medicament): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($medicament->quantite); ?> x </td>
                                    <td><?php echo e($medicament->medicament); ?></td>
                                    <td><?php echo e($medicament->prix ?? 'Non défini'); ?></td>
                                    <td><?php echo $medicament->etat==1 ? '<span class="badge-success badge">Validé</span>' : '<span class="badge-warning badge">En attente</span>'; ?></td>
                                    <td><?php echo e(App\Models\Prestataire::find($medicament->prestataire_id)->rs ?? 'Non défini'); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/ordonnance/details-ordonnance.blade.php ENDPATH**/ ?>