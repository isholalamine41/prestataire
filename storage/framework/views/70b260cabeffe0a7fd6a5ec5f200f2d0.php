
<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title white" style="color: #fff; font-size: 16px;">AJOUTER UN UTILISATEUR</h5>
                <button style="font-size: 18px; color:#fff;border:#fff 1px solid;" type="button" class="btn btn-default" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
            <form action="<?php echo e(Route('AddUserPrestataire')); ?>" method="post" enctype="multipart/form-data"><?php echo csrf_field(); ?>                              
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <label for="name">Nom et prenoms <span class="text-danger">*</span></label>
                        <input id="name" type="text" class="form-control border-primary " name="name" value="" required="" >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group txt-dark ">
                        <label for="phone">Contact *</label>
                        <input id="phone" type="text" class="form-control border-primary" name="contact"  required="" >
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <label for="birth_date">Email*</label>
                        <input type="email" class="form-control border-primary" required="" name="email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <label for="birth_date">Role / Fonction *</label>
                        <input type="text" name="fonction"  required="" class="form-control border-primary">
                    </div>
                </div>
            </div><br>
            <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <label for="registration_number">Les Menus *</label>
                        <span class="text-danger">*</span>
                        <select name="menu_id[]" id="" multiple class="form-control border-primary" style="height:100px;"  required="">
                            <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($m->libelle =='All-Menu-Prestataire' or $m->libelle =='GESTION DES DROITS'): ?>
                                <?php else: ?>
                                <option value="<?php echo e($m->id); ?>"><?php echo e($m->libelle); ?></option>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group txt-dark">
                        <br><br>
                        <button align="left" type="submit" class="btn btn-primary text-white" style="width:250px; margin-top:1em;">
                            ENREGISTRER
                        </button>
                    </div>
                </div>
                <br>
            </div>
            <div class="row">
           
        </form>  
            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/santemv/www/resources/views/admin/right/addUser.blade.php ENDPATH**/ ?>