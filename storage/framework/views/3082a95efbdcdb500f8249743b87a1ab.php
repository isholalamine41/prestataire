
<?php $__env->startSection('content'); ?>

   

    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card overflow-hidden border-primary">
                        <div class="card-body custome-tooltip p-3 text-right" align="right" >
                            <a href="<?php echo e(route('AddUser')); ?>" class="lanceModal" aria-expanded="false">
                                <button type="button" class="btn btn-outline-primary"><span class="nav-text weight-bold"> <i style="font-size: 16px;" class="la la-user"></i> &ensp; AJOUTER UN UTILISATEUR</span></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php if(session()->has('messageErreur')): ?>
                        
                        <div class="alert alert-danger solid alert-dismissible fade show">
                            <svg viewBox="0 0 24 24" width="24 " height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="me-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                            <strong>Erreur ! </strong> <?php echo e(Session::get('messageErreur')); ?>

                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"><span><i class="fa-solid fa-xmark"></i></span>
                            </button>
                        </div>
                        
                        <?php endif; ?>
                        <?php if(session()->has('message')): ?>
                        
                        <div class="alert alert-success solid alert-dismissible fade show">
                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="me-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                            <strong>Félicitations ! </strong> <?php echo e(Session::get('message')); ?>

                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close">
                            <span><i class="fa-solid fa-xmark"></i></span>
                            </button>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-xxl-12">

                    <div class="row">
                        <div class="col-xl-12">
                            <div class="row">
                            
                                

                                <div class="col-xl-12">
                                    <div class="card overflow-hidden headerP">
                                        <div class="card-header border-0 pb-0 flex-wrap">
                                            <h4 class="heading mb-0">LISTE DES UTILISATEURS (<?php echo e($user->count()); ?>)</h4>
                                        </div>
                                        <div class="card-body custome-tooltip p-0">
                                            <div class="table-responsive" style="padding: 15px;">
                                                <table id="example" class="display table" style="min-width: 845px">
                                                    <thead>
                                                        <tr>
                                                            <th>NOM & PRENOMS</th>
                                                            <th>FONCTION</th>
                                                            <th>CONTACT</th>
                                                            <th>DROITS</th>
                                                            <th>MENU</th>
                                                            <th>ACTIONS</th>
                                                        </tr>
                                                    </thead>
                                                    <?php if($user->count()>0): ?>
                                                    <tbody>
                                                        <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        
                                                        <tr>
                                                            <td class="txt-dark">
                                                                <?php echo e($u->name); ?>

                                                            </td>
                                                            <td class="txt-dark"><span class="badge badge-primary border-0"><?php echo e($u->fonction); ?></span></td>
                                                        
                                                            <td class="txt-dark text-center" ><?php echo e($u->contact); ?></td>
                                                            <td class="txt-dark">
                                                                <?php if($u->typePrestataire=='Super Prestataire'): ?>
                                                                <select name="" class="form-control" id="">
                                                                    <?php if($u->UserRight): ?>
                                                                        <?php $__currentLoopData = $u->UserRight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php if($fur->libelle == 'All-Right-Prestataire'): ?>
                                                                            <option>TOUS LES DROITS</option>
                                                                            <?php endif; ?>
                                                                            <?php if($fur->libelle == 'Create-Right-Prestataire'): ?>
                                                                            <option>AJOUT D'ACTE MEDICAL</option>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php endif; ?>
                                                                </select>
                                                                <?php else: ?>
                                                                    MENUs ATTRIBUES
                                                                <?php endif; ?>
                                                            </td>
                                                           
                                                            <td class="txt-dark">
                                                                <select name="" class="form-control" id="">
                                                                    <?php if($u->UserMenu): ?>
                                                                        <?php $__currentLoopData = $u->UserMenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php if($fur->libelle == 'All-Menu-Prestataire'): ?>
                                                                                <option>TOUS LES PAGES</option>
                                                                                <?php else: ?>
                                                                                <option><?php echo e($fur->libelle); ?></option>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php endif; ?> 
                                                                </select>
                                                            </td>
                                                            <td>
                                                                
                                                                <button type="button" href="<?php echo e(route('updateUser',$u->id)); ?>" class="btn btn-rounded btn-success lanceModal"><i class="fa fa-edit"></i> Voir </button>
                                                                
                                                            </td>
                                                        </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </tbody>
                                                    <?php endif; ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>
wrapper theme-4-active pimary-color-red
<?php $__env->stopSection(); ?>


<?php $__env->startPush('style.hearder'); ?>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('script.footer1'); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title'); ?>
GESTION DES DROITS
<?php $__env->stopSection(); ?>


<?php $__env->startSection('dd'); ?>
active
<?php $__env->stopSection(); ?>



<?php $__env->startPush('script.footer2'); ?>
<script>
    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
</script>
<?php $__env->stopPush(); ?>






<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/santemv/www/resources/views/admin/right/userRight.blade.php ENDPATH**/ ?>