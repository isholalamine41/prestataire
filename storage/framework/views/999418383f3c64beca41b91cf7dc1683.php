<?php $__env->startSection('content'); ?>

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="row">
									
                                  
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="<?php echo e(route('DemandeActesMedicauxAll')); ?>">
                                            <div class="card same-card headerP">
                                                <div class="card-body depostit-card p-0">
                                                    <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                        <div>
                                                            <h6>TOUTES LES DEMANDES</h6>
                                                            <h3><?php echo e(number_format($demandes->count(), 0, ',', ' ')); ?></h3>
                                                        </div>
                                                        <div class="icon-box bg-success">
                                                            <i class="las la-file-invoice-dollar" style="color: #fff;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="<?php echo e(route('DemandeActesMedicauxEnAttente')); ?>">
                                            <div class="card same-card headerP">
                                                <div class="card-body depostit-card">
                                                    <div class="depostit-card-media d-flex justify-content-between style-1">
                                                        <div>
                                                            <h6>DEMANDES EN ATTENTES</h6>
                                                            <h3><?php echo e(number_format($demandes->where('entente','En attente')->count(), 0, ',', ' ')); ?></h3>
                                                        </div>
                                                        <div class="icon-box bg-warning">
                                                            <i class="las la-praying-hands" style="color: #fff;"></i>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="<?php echo e(route('DemandeActesMedicauxAccordees')); ?>">
                                            <div class="card same-card headerP">
                                                <div class="card-body depostit-card">
                                                    <div class="depostit-card-media d-flex justify-content-between style-1">
                                                        <div>
                                                            <h6>DEMANDES ACCORDEES</h6>
                                                            <h3><?php echo e(number_format($demandes->where('entente','Accordée')->count(), 0, ',', ' ')); ?></h3>
                                                        </div>
                                                        <div class="icon-box bg-warning">
                                                            <i class="las la-praying-hands" style="color: #fff;"></i>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-3 col-sm-6">
                                        <a href="<?php echo e(route('DemandeActesMedicauxRejetees')); ?>">
                                            <div class="card same-card same-card headerP">
                                                <div class="card-body depostit-card p-0">
                                                    <div class="depostit-card-media d-flex justify-content-between pb-0">
                                                        <div>
                                                            <h6>DEMANDES REJETEES</h6>
                                                            <h3><?php echo e(number_format($demandes->where('entente','Rejetée')->count(), 0, ',', ' ')); ?></h3>
                                                        </div>
                                                        <div class="icon-box bg-danger">
                                                        <i class="las la-praying-hands" style="color: #fff;"></i>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP" style="padding: 15px;">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Date début</label>
                                            <input type="date" id="datedebut" name="datedebut" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Date fin</label>
                                            <input type="date" id="datefin" name="datefin" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Type d'assuré</label>
                                            <select name="nature_id" id="type" class="form-control border-success">
                                                <option value="all">Tous les assurés</option>
                                                <option value="App\Models\Mutualiste">Assuré principal</option>
                                                <option value="App\Models\Ayantdroit">Ayants-droit</option>
                                            </select>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Nature d'Acte</label>
                                            <select name="nature_id" id="nature_id" class="form-control border-success">
                                                <option value="all">Tout</option>
                                                <?php $__currentLoopData = $ListePrestation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($data->id); ?>"><?php echo e($data->libelle); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label for="" class="txt-dark">Statut</label>
                                            <select name="statut" id="statut" class="form-control border-success">
                                                <option value="all">Tout</option>
                                                <option value="En attente">En attente</option>
                                                <option value="Accordée">Accordé</option>
                                                <option value="Rejété">Rejété</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <button class="btn btn-success btn-icon right-icon" id="search" style="margin-top: 2em; width:100%"><span>RECHERCHEZ </span> <i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                               

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0" id="HeaderTitle"><?php echo e($title); ?></h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table id="table" class="display table" style="min-width: 845px">
                                                <thead>
                                                    <tr>
                                                        <th>ASSURE</th>
                                                        <th>DATE : HEURE</th>
                                                        <th>ACTES</th>
                                                        <th>MONTANT</th>
                                                        <th>TIER PAYANT</th>
                                                        <th>PART ASSURE</th>
                                                        <th>STATUT </th>
                                                        <th>ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                <?php if($demandes_data->count()>0): ?>
                                                <tbody id="tbody">
                                                    <?php $__currentLoopData = $demandes_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php
                                                            $data = $ai->prestamutable_type::find($ai->prestamutable_id);
                                                            if($ai->prestamutable_type =='App\Models\Mutualiste'){
                                                                $type = "Mutualiste";
                                                            }else{
                                                                $type = "Ayant-droit";
                                                            }
                                                        ?>
                                                    <tr>
                                                        <td class="txt-dark">
                                                            <div class="d-flex align-items-center">
                                                                <?php if(!$data->photo): ?>
                                                                    <img src="<?php echo e(URL::asset('erpfiles/dist/img/mock1.jpg')); ?>" class="avatar avatar-md rounded-circle" alt="">
                                                                    <?php else: ?>
                                                                        <?php if($ai->prestamutable_type =='App\Models\Mutualiste'): ?>
                                                                            <img src='<?php echo e(config("app.imgLink")); ?><?php echo e($data->photo); ?>' class="avatar avatar-md rounded-circle" alt="">
                                                                        <?php else: ?>
                                                                            <img src='<?php echo e(config("app.adLink")); ?><?php echo e($data->photo); ?>' class="avatar avatar-md rounded-circle" alt="">
                                                                        <?php endif; ?>
                                                                <?php endif; ?>
                                                                <p class="mb-0 ms-2 txt-dark">
                                                                    <a href="">
                                                                        <?php echo e($data->first_name); ?> <?php echo e($data->last_name ?? $data->name); ?>

                                                                    </a>
                                                                </p>	
                                                            </div>
                                                        </td>
													    <td class="txt-dark"><?php echo e(date('d/m/Y', strtotime($ai->date))); ?> à <b><?php echo e($ai->heure); ?></b></td>
													    <td class="txt-dark"><?php echo e($ai->libelle); ?></td>
                                                        
                                                        <td class="txt-dark"><?php echo e(number_format($ai->part_assure+$ai->part_muscopci, 0, ',', ' ')); ?> FCFA</td>
                                                        <td class="txt-dark"><?php echo e(number_format($ai->part_assure, 0, ',', ' ')); ?> FCFA</td>
                                                        <td class="txt-dark"><b><?php echo e(number_format($ai->part_muscopci, 0, ',', ' ')); ?> FCFA</b></td>
                                                       
                                                        <td class="txt-dark">
                                                            <b><?php echo e($ai->entente); ?></b>
                                                        </td>
                                                        <td>
														<button type="button" href="<?php echo e(route('ViewActeTampon', [$ai->id , $ai->prestamutable_id, $type] )); ?>" class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
														</td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                                <?php endif; ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>
wrapper theme-4-active pimary-color-red
<?php $__env->stopSection(); ?>


<?php $__env->startPush('style.hearder'); ?>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('script.footer1'); ?>

<script>
    
    var table = $('#table').DataTable();

    $('#search').on('click',()=>{

        $('#search').html('<div class="spinner-border" role="status"></div>');

        let datedebut = $('#datedebut').val();
        let datefin = $('#datefin').val();
        let type = $('#type').val();
        let nature_id = $('#nature_id').val();
        let statut = $('#statut').val();
        
        table.destroy();

        $.ajax({
            method: 'GET',
            data : {
                datedebut : datedebut,
                datefin : datefin,
                type : type,
                nature_id : nature_id,
                statut : statut,
                query: 'demande'
            },
            url: "<?php echo e(route('RechecheActesMedicaux')); ?>",
            success : function(response){

                $("#tbody").html(response);
                table = $('#table').DataTable();
                $('#search').html('<span>RECHERCHEZ </span> <i class="fa fa-search"></i>');
                $('#HeaderTitle').html('<h4 id="HeaderTitle" class="heading mb-0 HeaderTitle">RESULTATS : ('+table.rows().count()+') </h4>');
            }
        });

    });
</script>
<script>
    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
</script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title'); ?>
<?php echo e($title); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('dd'); ?>
active
<?php $__env->stopSection(); ?>



<?php $__env->startPush('script.footer2'); ?>

<?php $__env->stopPush(); ?>






<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/santemv/www/resources/views/admin/acte/all-demande.blade.php ENDPATH**/ ?>