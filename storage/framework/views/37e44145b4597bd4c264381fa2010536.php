<?php $__env->startSection('content'); ?>

<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12 col-xxl-12">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">

                            
                            <div class="row">
                                
                                <div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
                                    <div class="widget-stat card  bg-success headerP">
                                        <div class="card-body">
                                            <div class="media">
                                                <span class="me-1">
                                                    <i class="la la-users"></i>
                                                </span>
                                                <div class="media-body text-white">
                                                    <p class="mb-1">MONTANT GLOBAL</p>
                                                    <h4 class="text-white"><?php echo e(number_format($prestataire->Facture->SUM('part_muscopci'), 0, ',', ' ')); ?> </h4>
                                                    <span>FCFA</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
                                    <div class="widget-stat card  bg-warning headerP">
                                        <div class="card-body">
                                            <div class="media">
                                                <span class="me-1">
                                                    <i class="la la-users"></i>
                                                </span>
                                                <div class="media-body text-white">
                                                    <p class="mb-1">MONTANT GLOBAL REGLE</p>
                                                    <h4 class="text-white"><?php echo e(number_format($prestataire->Facture->where('payer',1)->SUM('part_muscopci'), 0, ',', ' ')); ?> </h4>
                                                    <span>FCFA</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4  col-lg-4 col-md-4  col-sm-6 col-xs-6">
                                    <div class="widget-stat card  bg-danger headerP">
                                        <div class="card-body">
                                            <div class="media">
                                                <span class="me-1">
                                                    <i class="las la-file-invoice-dollar"></i>
                                                </span>
                                                <div class="media-body text-white">
                                                    <p class="mb-1">FACTURE EN COURS</p>
                                                    <h4 class="text-white"><?php echo e(number_format($prestataire->Facture->where('payer',0)->SUM('part_muscopci'), 0, ',', ' ')); ?></h4>
                                                    <span>FCFA</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              


                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP" style="padding: 15px;">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date début</label>
                                            <input type="date" id="datedebut" name="datedebut" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Date fin</label>
                                            <input type="date" id="datefin" name="datefin" class="form-control border-success">
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <label for="" class="txt-dark">Statut</label>
                                            <select name="statut" id="statut" class="form-control border-success">
                                                <option value="all">Tout</option>
                                                <option value="1">Payer</option>
                                                <option value="0">Impayer</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <button class="btn btn-success btn-icon right-icon" id="search" style="margin-top: 2em; width:100%"><span>RECHERCHEZ </span> <i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                               

                            <div class="col-xl-12">
                                <div class="card overflow-hidden headerP">
                                    <div class="card-header border-0 pb-0 flex-wrap">
                                        <h4 class="heading mb-0" id="HeaderTitle">FACTURES IMPAYEES (<?php echo e($prestataire->Facture->where('payer',0)->count()); ?>)</h4>
                                    </div>
                                    <div class="card-body custome-tooltip p-0">
                                        <div class="table-responsive" style="padding: 15px;">
                                            <table id="table" class="display table" style="min-width: 845px">
                                                <thead>
                                                    <tr>
                                                        <th>ASSURE</th>
                                                        <th>DATE : HEURE</th>
                                                        <th>Nbre ACTES</th>
                                                        <th>MONTANT</th>
                                                        <th>TIER PAYANT</th>
                                                        <th>PAR ASSURE</th>
                                                        <th>ACTIONS</th>
                                                    </tr>
                                                </thead>
                                                <?php if($prestataire->Facture->where('payer',0)->count()>0): ?>
                                                <tbody id="tbody" >
                                                    <?php $__currentLoopData = $prestataire->Facture->where('payer',0); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $facture): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php
                                                            $acte = $facture->PrestaMutua()->first();
                                                        ?>
                                                        <?php if($acte): ?>
                                                            <tr>
                                                                <td class="txt-dark">
                                                                    <div class="d-flex align-items-center">
                                                                        <?php if($acte): ?>
                                                                        <?php if(!$acte->prestamutable->photo): ?>
                                                                                <img src="<?php echo e(URL::asset('erpfiles/dist/img/mock1.jpg')); ?>" class="avatar avatar-md rounded-circle" alt="">
                                                                                <?php else: ?>
                                                                                <?php if($acte->prestamutable_type =='App\Models\Mutualiste'): ?>
                                                                                    <img src='<?php echo e(config("app.imgLink")); ?><?php echo e($acte->prestamutable->photo); ?>' class="avatar avatar-md rounded-circle" alt="">
                                                                                <?php else: ?>
                                                                                    <img src='<?php echo e(config("app.adLink")); ?><?php echo e($acte->prestamutable->photo); ?>' class="avatar avatar-md rounded-circle" alt="">
                                                                                <?php endif; ?>
                                                                        <?php endif; ?>
                                                                        <p class="mb-0 ms-2 txt-dark">
                                                                            <a href="">
                                                                                <?php echo e($acte->prestamutable->first_name); ?> <?php echo e($acte->prestamutable->last_name ?? $acte->prestamutable->name); ?>

                                                                            </a>
                                                                        </p>	
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </td>
                                                                <td class="txt-dark"><?php echo e(date('d/m/Y', strtotime($facture->date))); ?> à <b><?php echo e($facture->heure); ?></b></td>
                                                                <td class="txt-dark text-center" ><span class="badge badge-primary border-0"><?php echo e($facture->PrestaMutua->count()); ?></span></td>
                                                                <td class="txt-dark"><?php echo e(number_format($facture->montant, 0, ',', ' ')); ?> FCFA</td>
                                                                <td class="txt-dark"><?php echo e(number_format($facture->part_assure, 0, ',', ' ')); ?> FCFA</td>
                                                                <td class="txt-dark"><b><?php echo e(number_format($facture->part_muscopci, 0, ',', ' ')); ?> FCFA</b></td>
                                                                <td>
                                                                    <button type="button" href="<?php echo e(route('ViewFacture', [$facture->id] )); ?>" class="btn btn-rounded btn-success lanceModal"><i class="las la-eye"></i> Voir </button>
                                                                </td>
                                                            </tr>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                                <?php endif; ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('body'); ?>
wrapper theme-4-active pimary-color-red
<?php $__env->stopSection(); ?>


<?php $__env->startPush('style.hearder'); ?>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('script.footer1'); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('title'); ?>
Factures
<?php $__env->stopSection(); ?>


<?php $__env->startSection('dd'); ?>
active
<?php $__env->stopSection(); ?>



<?php $__env->startPush('script.footer2'); ?>
<script>
    
    var table = $('#table').DataTable();

    $('#search').on('click',()=>{

        $('#search').html('<div class="spinner-border" role="status"></div>');

        let datedebut = $('#datedebut').val();
        let datefin = $('#datefin').val();
        let statut = $('#statut').val();
        
        table.destroy();

        $.ajax({
            method: 'GET',
            data : {
                datedebut : datedebut,
                datefin : datefin,
                statut : statut,
            },
            url: "<?php echo e(route('RechecheFacture')); ?>",
            success : function(response){

                $("#tbody").html(response);
                table = $('#table').DataTable();
                $('#search').html('<span>RECHERCHEZ </span> <i class="fa fa-search"></i>');
                $('#HeaderTitle').html('<h4 id="HeaderTitle" class="heading mb-0 HeaderTitle">RESULTATS : ('+table.rows().count()+') </h4>');
            }
        });

    });
</script>
<script>
    $(document).on("click",".lanceModal", function(e){
      e.preventDefault();
      var a=$(this);
      $('.retour_modal').text("");
      $.ajax({
        method: 'get',
        url: a.attr("href"),
        success : function(response){
          if (response.statut == 'messageErreur') {
            $.toast({
                heading: response.title,
                text: response.message,
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 7000, 
                stack: 6
            });
          }else{
            console.log(response.code);
              $('.retour_modal').html(response.code);
              $('.affiche').modal("show");
          }
        }
      })
    });
</script>
<?php $__env->stopPush(); ?>






<?php echo $__env->make('layouts.admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/santemv/www/resources/views/admin/facture/factures.blade.php ENDPATH**/ ?>