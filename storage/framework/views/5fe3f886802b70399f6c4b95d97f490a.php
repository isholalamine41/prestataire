<div class="modal fade bd-example-modal-lg affiche" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title white weight-bold" style="color: #fff; font-size: 22px;">
                </h5>
                <button style="font-size: 18px;" type="button" class="btn btn-danger" data-bs-dismiss="modal">X</button>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-9"><h3 class="text-danger">Code : <?php echo e($ordonnance->Code->code); ?></h3></div>
                    <div class="col-md-3"><a href="<?php echo e(route('printOrdonnance',[$ordonnance->actes()->first()->id])); ?>" type="button" class="btn btn-sm btn-warning">IMPRIMER <i class="fa fa-print"></i></a></div>
                </div>
                <div class="row headerP" style="padding: 5px; margin:5px;">
                    <table class="table table-striped">
                        <thead>
                            <th>Quantité</th>
                            <th>Médicament</th>
                            <th>Prix UT</th>
                            <th>Prix TT</th>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $ordonnance->actes()->where('prestataire_id',$prestataire->id)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $presta_muta): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $__currentLoopData = explode(',',$presta_muta->description); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $line): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php 
                                        $data = explode('* ',$line);
                                        $data2 = explode(' = ',$data[1]);
                                    ?>
                                    <tr style="text-align: left;">
                                        <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($data[0]); ?></td>
                                        <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($data2[0]); ?></td>
                                        <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($data2[1]/$data[0]); ?></td>
                                        <td style="text-align: left;padding: 7px;border: 1px solid #c6c6c6;border-left: 0;border-right: 0;border-top: 0;" class="center"><?php echo e($data2[1]); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH /home/santemv/www/resources/views/admin/prestataire/ordonnance/details-ordonnance-prestataire.blade.php ENDPATH**/ ?>