                        <style>
                            .ImgCarteMutualiste {
                                height: 175px;
                                width: 139px;
                                top: 220px;
                                Left: 71px;
                                position: absolute;
                                z-index: 1;
                                border-radius: 20px;
                                margin-top: -6px;
                            }
                            .assureNameMutualiste {
                                top: 237.5px;
                                Left: 371px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                            .dateNaisseMutualiste {
                                top: 287px;
                                Left: 371px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }

                            .assureSexeMutualiste {
                                top: 335px;
                                Left: 371px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                            .assurance_numberMutualiste {
                                top: 189px;
                                Left: 433px;
                                position: absolute;
                                z-index: 1;
                                color: red;
                                font-size: 16px;
                                font-weight: bold;
                            }
                            .assurePoliceMutualiste {
                                top: 420px;
                                Left: 173px;
                                position: absolute;
                                z-index: 1;
                                color: black;
                                font-weight: bold;
                            }
                        </style>
                        <?php if(!is_null($assure)): ?>
                            <img class="inline-block mb-10 ImgCarteMutualiste" src='<?php echo e(config("app.imgLink")); ?><?php echo e($assure->photo); ?>'/>
                            <div style="margin-top: -1.2em;" >
                                <?php if($assure->active): ?>
                                    <p style="color: green;">
                                        ASSURE(E) ACTIVE(E)
                                    </p>
                                    <?php else: ?>
                                    
                                    <p align="center"  class="headerP" style="color: red; font-size:46px; font-weight:bold;  z-index:1; position:absolute; width:460px; bottom:330px;left:95px; ">
                                        ASSURE(E) BLOQUE(E)
                                    </p>
                                <?php endif; ?>
                            </div>
                            <div  align="center" style="text-align: center; vertical-align:middle; height:400px;">
                                <p class="assurance_numberMutualiste">
                                    <?php echo e($assure->assurance_number); ?>

                                </p>
                                <p class="assureNameMutualiste"><?php echo e($assure->first_name); ?> <?php echo e($assure->name ?? $assure->last_name); ?></p>
                                <p class="dateNaisseMutualiste"><?php echo e($assure->birth_date); ?></p>
                                <p class="assureSexeMutualiste">
                                    <?php if($assure->sex == "M"): ?>
                                        Masculin
                                    <?php else: ?>
                                        Feminin
                                    <?php endif; ?>
                                </p>
                                
                                <p class="assurePoliceMutualiste" ><?php echo e($assure->registration_number); ?></p>
                                <img class="headerP CartePosition" src="<?php echo e(URL::asset('erpfiles/images/carte-assure-2.jpeg')); ?>" style="height:350px;border-radius:20px" alt="">
                            </div>
                            <?php if($assure->active): ?>
                                <?php if($assure->bareme->last()->plafondGeneraleBeneficiaire > 0 and $assure->bareme->last()->plafondGeneraleFamillle > 0): ?>
                                <div align="center">
                                    <button type="submit" href="<?php echo e(route('GotoMakeActe',[$assure->id, $typeASS])); ?>" class="btn btn-outline-warning DetailView" style="width: 320px; font-size: 25px;">  SUIVANT &ensp; <i style="font-weight:bold;" class="las la-hand-point-right"></i></button>
                                </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <?php /**PATH /home/santemv/www/resources/views/admin/prestataire/retourAjax/retourInfoCarteMutualiste.blade.php ENDPATH**/ ?>